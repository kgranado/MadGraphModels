# This file was automatically created by FeynRules 2.3.7
# Mathematica version: 9.0 for Linux x86 (64-bit) (November 20, 2012)
# Date: Fri 23 Sep 2016 16:59:06


from object_library import all_vertices, all_CTvertices, Vertex, CTVertex
import particles as P
import CT_couplings as C
import lorentz as L


V_1 = CTVertex(name = 'V_1',
               type = 'R2',
               particles = [ P.g, P.g, P.g ],
               color = [ 'f(1,2,3)' ],
               lorentz = [ L.VVV2 ],
               loop_particles = [ [ [P.b], [P.c], [P.d], [P.s], [P.t], [P.u] ], [ [P.g] ] ],
               couplings = {(0,0,0):C.R2GC_196_93,(0,0,1):C.R2GC_196_94})

V_2 = CTVertex(name = 'V_2',
               type = 'R2',
               particles = [ P.g, P.g, P.g, P.g ],
               color = [ 'd(-1,1,3)*d(-1,2,4)', 'd(-1,1,3)*f(-1,2,4)', 'd(-1,1,4)*d(-1,2,3)', 'd(-1,1,4)*f(-1,2,3)', 'd(-1,2,3)*f(-1,1,4)', 'd(-1,2,4)*f(-1,1,3)', 'f(-1,1,2)*f(-1,3,4)', 'f(-1,1,3)*f(-1,2,4)', 'f(-1,1,4)*f(-1,2,3)', 'Identity(1,2)*Identity(3,4)', 'Identity(1,3)*Identity(2,4)', 'Identity(1,4)*Identity(2,3)' ],
               lorentz = [ L.VVVV2, L.VVVV3, L.VVVV5, L.VVVV9 ],
               loop_particles = [ [ [P.b], [P.c], [P.d], [P.s], [P.t], [P.u] ], [ [P.g] ] ],
               couplings = {(2,0,0):C.R2GC_159_69,(2,0,1):C.R2GC_159_70,(0,0,0):C.R2GC_159_69,(0,0,1):C.R2GC_159_70,(4,0,0):C.R2GC_157_65,(4,0,1):C.R2GC_157_66,(3,0,0):C.R2GC_157_65,(3,0,1):C.R2GC_157_66,(8,0,0):C.R2GC_158_67,(8,0,1):C.R2GC_158_68,(7,0,0):C.R2GC_163_76,(7,0,1):C.R2GC_200_99,(6,0,0):C.R2GC_162_74,(6,0,1):C.R2GC_201_100,(5,0,0):C.R2GC_157_65,(5,0,1):C.R2GC_157_66,(1,0,0):C.R2GC_157_65,(1,0,1):C.R2GC_157_66,(11,3,0):C.R2GC_161_72,(11,3,1):C.R2GC_161_73,(10,3,0):C.R2GC_161_72,(10,3,1):C.R2GC_161_73,(9,3,1):C.R2GC_160_71,(2,1,0):C.R2GC_159_69,(2,1,1):C.R2GC_159_70,(0,1,0):C.R2GC_159_69,(0,1,1):C.R2GC_159_70,(6,1,0):C.R2GC_197_95,(6,1,1):C.R2GC_197_96,(4,1,0):C.R2GC_157_65,(4,1,1):C.R2GC_157_66,(3,1,0):C.R2GC_157_65,(3,1,1):C.R2GC_157_66,(8,1,0):C.R2GC_158_67,(8,1,1):C.R2GC_202_101,(7,1,0):C.R2GC_163_76,(7,1,1):C.R2GC_163_77,(5,1,0):C.R2GC_157_65,(5,1,1):C.R2GC_157_66,(1,1,0):C.R2GC_157_65,(1,1,1):C.R2GC_157_66,(2,2,0):C.R2GC_159_69,(2,2,1):C.R2GC_159_70,(0,2,0):C.R2GC_159_69,(0,2,1):C.R2GC_159_70,(4,2,0):C.R2GC_157_65,(4,2,1):C.R2GC_157_66,(3,2,0):C.R2GC_157_65,(3,2,1):C.R2GC_157_66,(8,2,0):C.R2GC_158_67,(8,2,1):C.R2GC_199_98,(6,2,0):C.R2GC_162_74,(6,2,1):C.R2GC_162_75,(7,2,0):C.R2GC_198_97,(7,2,1):C.R2GC_159_70,(5,2,0):C.R2GC_157_65,(5,2,1):C.R2GC_157_66,(1,2,0):C.R2GC_157_65,(1,2,1):C.R2GC_157_66})

V_3 = CTVertex(name = 'V_3',
               type = 'R2',
               particles = [ P.t__tilde__, P.b, P.G__plus__ ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFS5 ],
               loop_particles = [ [ [P.b, P.g, P.t] ] ],
               couplings = {(0,0,0):C.R2GC_212_106})

V_4 = CTVertex(name = 'V_4',
               type = 'R2',
               particles = [ P.t__tilde__, P.t, P.G0 ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFS1 ],
               loop_particles = [ [ [P.g, P.t] ] ],
               couplings = {(0,0,0):C.R2GC_214_108})

V_5 = CTVertex(name = 'V_5',
               type = 'R2',
               particles = [ P.t__tilde__, P.t, P.H ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFS2 ],
               loop_particles = [ [ [P.g, P.t] ] ],
               couplings = {(0,0,0):C.R2GC_215_109})

V_6 = CTVertex(name = 'V_6',
               type = 'R2',
               particles = [ P.b__tilde__, P.t, P.G__minus__ ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFS3 ],
               loop_particles = [ [ [P.b, P.g, P.t] ] ],
               couplings = {(0,0,0):C.R2GC_213_107})

V_7 = CTVertex(name = 'V_7',
               type = 'R2',
               particles = [ P.b__tilde__, P.b, P.Y1 ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFV5, L.FFV6 ],
               loop_particles = [ [ [P.b, P.g] ] ],
               couplings = {(0,0,0):C.R2GC_112_4,(0,1,0):C.R2GC_113_5})

V_8 = CTVertex(name = 'V_8',
               type = 'R2',
               particles = [ P.c__tilde__, P.c, P.Y1 ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFV5, L.FFV6 ],
               loop_particles = [ [ [P.c, P.g] ] ],
               couplings = {(0,0,0):C.R2GC_115_7,(0,1,0):C.R2GC_116_8})

V_9 = CTVertex(name = 'V_9',
               type = 'R2',
               particles = [ P.d__tilde__, P.d, P.Y1 ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFV5, L.FFV6 ],
               loop_particles = [ [ [P.d, P.g] ] ],
               couplings = {(0,0,0):C.R2GC_118_10,(0,1,0):C.R2GC_119_11})

V_10 = CTVertex(name = 'V_10',
                type = 'R2',
                particles = [ P.s__tilde__, P.s, P.Y1 ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV5, L.FFV6 ],
                loop_particles = [ [ [P.g, P.s] ] ],
                couplings = {(0,0,0):C.R2GC_121_12,(0,1,0):C.R2GC_122_13})

V_11 = CTVertex(name = 'V_11',
                type = 'R2',
                particles = [ P.t__tilde__, P.t, P.Y1 ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV5, L.FFV6 ],
                loop_particles = [ [ [P.g, P.t] ] ],
                couplings = {(0,0,0):C.R2GC_206_102,(0,1,0):C.R2GC_207_103})

V_12 = CTVertex(name = 'V_12',
                type = 'R2',
                particles = [ P.u__tilde__, P.u, P.Y1 ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV5, L.FFV6 ],
                loop_particles = [ [ [P.g, P.u] ] ],
                couplings = {(0,0,0):C.R2GC_125_14,(0,1,0):C.R2GC_126_15})

V_13 = CTVertex(name = 'V_13',
                type = 'R2',
                particles = [ P.u__tilde__, P.u, P.a ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV1 ],
                loop_particles = [ [ [P.g, P.u] ] ],
                couplings = {(0,0,0):C.R2GC_169_81})

V_14 = CTVertex(name = 'V_14',
                type = 'R2',
                particles = [ P.c__tilde__, P.c, P.a ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV1 ],
                loop_particles = [ [ [P.c, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_169_81})

V_15 = CTVertex(name = 'V_15',
                type = 'R2',
                particles = [ P.t__tilde__, P.t, P.a ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV1 ],
                loop_particles = [ [ [P.g, P.t] ] ],
                couplings = {(0,0,0):C.R2GC_169_81})

V_16 = CTVertex(name = 'V_16',
                type = 'R2',
                particles = [ P.u__tilde__, P.u, P.g ],
                color = [ 'T(3,2,1)' ],
                lorentz = [ L.FFV1 ],
                loop_particles = [ [ [P.g, P.u] ] ],
                couplings = {(0,0,0):C.R2GC_167_80})

V_17 = CTVertex(name = 'V_17',
                type = 'R2',
                particles = [ P.c__tilde__, P.c, P.g ],
                color = [ 'T(3,2,1)' ],
                lorentz = [ L.FFV1 ],
                loop_particles = [ [ [P.c, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_167_80})

V_18 = CTVertex(name = 'V_18',
                type = 'R2',
                particles = [ P.t__tilde__, P.t, P.g ],
                color = [ 'T(3,2,1)' ],
                lorentz = [ L.FFV1 ],
                loop_particles = [ [ [P.g, P.t] ] ],
                couplings = {(0,0,0):C.R2GC_167_80})

V_19 = CTVertex(name = 'V_19',
                type = 'R2',
                particles = [ P.d__tilde__, P.u, P.W__minus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV3 ],
                loop_particles = [ [ [P.d, P.g, P.u] ] ],
                couplings = {(0,0,0):C.R2GC_190_86})

V_20 = CTVertex(name = 'V_20',
                type = 'R2',
                particles = [ P.s__tilde__, P.u, P.W__minus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV3 ],
                loop_particles = [ [ [P.g, P.s, P.u] ] ],
                couplings = {(0,0,0):C.R2GC_192_88})

V_21 = CTVertex(name = 'V_21',
                type = 'R2',
                particles = [ P.d__tilde__, P.c, P.W__minus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV3 ],
                loop_particles = [ [ [P.c, P.d, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_186_82})

V_22 = CTVertex(name = 'V_22',
                type = 'R2',
                particles = [ P.s__tilde__, P.c, P.W__minus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV3 ],
                loop_particles = [ [ [P.c, P.g, P.s] ] ],
                couplings = {(0,0,0):C.R2GC_188_84})

V_23 = CTVertex(name = 'V_23',
                type = 'R2',
                particles = [ P.b__tilde__, P.t, P.W__minus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV3 ],
                loop_particles = [ [ [P.b, P.g, P.t] ] ],
                couplings = {(0,0,0):C.R2GC_209_105})

V_24 = CTVertex(name = 'V_24',
                type = 'R2',
                particles = [ P.u__tilde__, P.u, P.Z ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV3, L.FFV4 ],
                loop_particles = [ [ [P.g, P.u] ] ],
                couplings = {(0,0,0):C.R2GC_140_59,(0,1,0):C.R2GC_117_9})

V_25 = CTVertex(name = 'V_25',
                type = 'R2',
                particles = [ P.c__tilde__, P.c, P.Z ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV3, L.FFV4 ],
                loop_particles = [ [ [P.c, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_140_59,(0,1,0):C.R2GC_117_9})

V_26 = CTVertex(name = 'V_26',
                type = 'R2',
                particles = [ P.t__tilde__, P.t, P.Z ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV3, L.FFV4 ],
                loop_particles = [ [ [P.g, P.t] ] ],
                couplings = {(0,0,0):C.R2GC_140_59,(0,1,0):C.R2GC_117_9})

V_27 = CTVertex(name = 'V_27',
                type = 'R2',
                particles = [ P.d__tilde__, P.d, P.a ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV1 ],
                loop_particles = [ [ [P.d, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_165_79})

V_28 = CTVertex(name = 'V_28',
                type = 'R2',
                particles = [ P.s__tilde__, P.s, P.a ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV1 ],
                loop_particles = [ [ [P.g, P.s] ] ],
                couplings = {(0,0,0):C.R2GC_165_79})

V_29 = CTVertex(name = 'V_29',
                type = 'R2',
                particles = [ P.b__tilde__, P.b, P.a ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV1 ],
                loop_particles = [ [ [P.b, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_165_79})

V_30 = CTVertex(name = 'V_30',
                type = 'R2',
                particles = [ P.d__tilde__, P.d, P.g ],
                color = [ 'T(3,2,1)' ],
                lorentz = [ L.FFV1 ],
                loop_particles = [ [ [P.d, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_167_80})

V_31 = CTVertex(name = 'V_31',
                type = 'R2',
                particles = [ P.s__tilde__, P.s, P.g ],
                color = [ 'T(3,2,1)' ],
                lorentz = [ L.FFV1 ],
                loop_particles = [ [ [P.g, P.s] ] ],
                couplings = {(0,0,0):C.R2GC_167_80})

V_32 = CTVertex(name = 'V_32',
                type = 'R2',
                particles = [ P.b__tilde__, P.b, P.g ],
                color = [ 'T(3,2,1)' ],
                lorentz = [ L.FFV1 ],
                loop_particles = [ [ [P.b, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_167_80})

V_33 = CTVertex(name = 'V_33',
                type = 'R2',
                particles = [ P.u__tilde__, P.d, P.W__plus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV3 ],
                loop_particles = [ [ [P.d, P.g, P.u] ] ],
                couplings = {(0,0,0):C.R2GC_191_87})

V_34 = CTVertex(name = 'V_34',
                type = 'R2',
                particles = [ P.c__tilde__, P.d, P.W__plus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV3 ],
                loop_particles = [ [ [P.c, P.d, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_187_83})

V_35 = CTVertex(name = 'V_35',
                type = 'R2',
                particles = [ P.u__tilde__, P.s, P.W__plus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV3 ],
                loop_particles = [ [ [P.g, P.s, P.u] ] ],
                couplings = {(0,0,0):C.R2GC_193_89})

V_36 = CTVertex(name = 'V_36',
                type = 'R2',
                particles = [ P.c__tilde__, P.s, P.W__plus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV3 ],
                loop_particles = [ [ [P.c, P.g, P.s] ] ],
                couplings = {(0,0,0):C.R2GC_189_85})

V_37 = CTVertex(name = 'V_37',
                type = 'R2',
                particles = [ P.t__tilde__, P.b, P.W__plus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV3 ],
                loop_particles = [ [ [P.b, P.g, P.t] ] ],
                couplings = {(0,0,0):C.R2GC_209_105})

V_38 = CTVertex(name = 'V_38',
                type = 'R2',
                particles = [ P.d__tilde__, P.d, P.Z ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV3, L.FFV4 ],
                loop_particles = [ [ [P.d, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_139_58,(0,1,0):C.R2GC_114_6})

V_39 = CTVertex(name = 'V_39',
                type = 'R2',
                particles = [ P.s__tilde__, P.s, P.Z ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV3, L.FFV4 ],
                loop_particles = [ [ [P.g, P.s] ] ],
                couplings = {(0,0,0):C.R2GC_139_58,(0,1,0):C.R2GC_114_6})

V_40 = CTVertex(name = 'V_40',
                type = 'R2',
                particles = [ P.b__tilde__, P.b, P.Z ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV3, L.FFV4 ],
                loop_particles = [ [ [P.b, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_139_58,(0,1,0):C.R2GC_114_6})

V_41 = CTVertex(name = 'V_41',
                type = 'R2',
                particles = [ P.u__tilde__, P.u ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FF1 ],
                loop_particles = [ [ [P.g, P.u] ] ],
                couplings = {(0,0,0):C.R2GC_164_78})

V_42 = CTVertex(name = 'V_42',
                type = 'R2',
                particles = [ P.c__tilde__, P.c ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FF1 ],
                loop_particles = [ [ [P.c, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_164_78})

V_43 = CTVertex(name = 'V_43',
                type = 'R2',
                particles = [ P.t__tilde__, P.t ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FF2, L.FF3, L.FF4, L.FF5 ],
                loop_particles = [ [ [P.g, P.t] ] ],
                couplings = {(0,0,0):C.R2GC_208_104,(0,2,0):C.R2GC_208_104,(0,1,0):C.R2GC_164_78,(0,3,0):C.R2GC_164_78})

V_44 = CTVertex(name = 'V_44',
                type = 'R2',
                particles = [ P.d__tilde__, P.d ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FF1 ],
                loop_particles = [ [ [P.d, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_164_78})

V_45 = CTVertex(name = 'V_45',
                type = 'R2',
                particles = [ P.s__tilde__, P.s ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FF1 ],
                loop_particles = [ [ [P.g, P.s] ] ],
                couplings = {(0,0,0):C.R2GC_164_78})

V_46 = CTVertex(name = 'V_46',
                type = 'R2',
                particles = [ P.b__tilde__, P.b ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FF1 ],
                loop_particles = [ [ [P.b, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_164_78})

V_47 = CTVertex(name = 'V_47',
                type = 'R2',
                particles = [ P.g, P.g ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.VV1, L.VV2, L.VV3 ],
                loop_particles = [ [ [P.b], [P.c], [P.d], [P.s], [P.t], [P.u] ], [ [P.g] ], [ [P.t] ] ],
                couplings = {(0,0,1):C.R2GC_195_92,(0,1,2):C.R2GC_109_1,(0,2,0):C.R2GC_194_90,(0,2,1):C.R2GC_194_91})

V_48 = CTVertex(name = 'V_48',
                type = 'R2',
                particles = [ P.g, P.g, P.H ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.VVS1 ],
                loop_particles = [ [ [P.t] ] ],
                couplings = {(0,0,0):C.R2GC_110_2})

V_49 = CTVertex(name = 'V_49',
                type = 'R2',
                particles = [ P.g, P.g, P.Y1, P.Y1 ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.VVVV2, L.VVVV3, L.VVVV5 ],
                loop_particles = [ [ [P.b] ], [ [P.c] ], [ [P.d] ], [ [P.s] ], [ [P.t] ], [ [P.u] ] ],
                couplings = {(0,0,0):C.R2GC_133_38,(0,0,1):C.R2GC_133_39,(0,0,2):C.R2GC_133_40,(0,0,3):C.R2GC_133_41,(0,0,4):C.R2GC_133_42,(0,0,5):C.R2GC_133_43,(0,1,0):C.R2GC_133_38,(0,1,1):C.R2GC_133_39,(0,1,2):C.R2GC_133_40,(0,1,3):C.R2GC_133_41,(0,1,4):C.R2GC_133_42,(0,1,5):C.R2GC_133_43,(0,2,0):C.R2GC_133_38,(0,2,1):C.R2GC_133_39,(0,2,2):C.R2GC_133_40,(0,2,3):C.R2GC_133_41,(0,2,4):C.R2GC_133_42,(0,2,5):C.R2GC_133_43})

V_50 = CTVertex(name = 'V_50',
                type = 'R2',
                particles = [ P.a, P.g, P.g, P.Y1 ],
                color = [ 'Identity(2,3)' ],
                lorentz = [ L.VVVV2, L.VVVV3, L.VVVV5 ],
                loop_particles = [ [ [P.b] ], [ [P.c] ], [ [P.d] ], [ [P.s] ], [ [P.t] ], [ [P.u] ] ],
                couplings = {(0,0,0):C.R2GC_131_26,(0,0,1):C.R2GC_131_27,(0,0,2):C.R2GC_131_28,(0,0,3):C.R2GC_131_29,(0,0,4):C.R2GC_131_30,(0,0,5):C.R2GC_131_31,(0,1,0):C.R2GC_131_26,(0,1,1):C.R2GC_131_27,(0,1,2):C.R2GC_131_28,(0,1,3):C.R2GC_131_29,(0,1,4):C.R2GC_131_30,(0,1,5):C.R2GC_131_31,(0,2,0):C.R2GC_131_26,(0,2,1):C.R2GC_131_27,(0,2,2):C.R2GC_131_28,(0,2,3):C.R2GC_131_29,(0,2,4):C.R2GC_131_30,(0,2,5):C.R2GC_131_31})

V_51 = CTVertex(name = 'V_51',
                type = 'R2',
                particles = [ P.g, P.g, P.Y1, P.Z ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.VVVV2, L.VVVV3, L.VVVV5 ],
                loop_particles = [ [ [P.b] ], [ [P.c] ], [ [P.d] ], [ [P.s] ], [ [P.t] ], [ [P.u] ] ],
                couplings = {(0,0,0):C.R2GC_137_50,(0,0,1):C.R2GC_137_51,(0,0,2):C.R2GC_137_52,(0,0,3):C.R2GC_137_53,(0,0,4):C.R2GC_137_54,(0,0,5):C.R2GC_137_55,(0,1,0):C.R2GC_137_50,(0,1,1):C.R2GC_137_51,(0,1,2):C.R2GC_137_52,(0,1,3):C.R2GC_137_53,(0,1,4):C.R2GC_137_54,(0,1,5):C.R2GC_137_55,(0,2,0):C.R2GC_137_50,(0,2,1):C.R2GC_137_51,(0,2,2):C.R2GC_137_52,(0,2,3):C.R2GC_137_53,(0,2,4):C.R2GC_137_54,(0,2,5):C.R2GC_137_55})

V_52 = CTVertex(name = 'V_52',
                type = 'R2',
                particles = [ P.g, P.g, P.g, P.Y1 ],
                color = [ 'd(1,2,3)', 'f(1,2,3)' ],
                lorentz = [ L.VVVV1, L.VVVV2, L.VVVV3, L.VVVV5 ],
                loop_particles = [ [ [P.b] ], [ [P.c] ], [ [P.d] ], [ [P.s] ], [ [P.t] ], [ [P.u] ] ],
                couplings = {(1,0,0):C.R2GC_130_20,(1,0,1):C.R2GC_130_21,(1,0,2):C.R2GC_130_22,(1,0,3):C.R2GC_130_23,(1,0,4):C.R2GC_130_24,(1,0,5):C.R2GC_130_25,(0,1,0):C.R2GC_132_32,(0,1,1):C.R2GC_132_33,(0,1,2):C.R2GC_132_34,(0,1,3):C.R2GC_132_35,(0,1,4):C.R2GC_132_36,(0,1,5):C.R2GC_132_37,(0,2,0):C.R2GC_132_32,(0,2,1):C.R2GC_132_33,(0,2,2):C.R2GC_132_34,(0,2,3):C.R2GC_132_35,(0,2,4):C.R2GC_132_36,(0,2,5):C.R2GC_132_37,(0,3,0):C.R2GC_132_32,(0,3,1):C.R2GC_132_33,(0,3,2):C.R2GC_132_34,(0,3,3):C.R2GC_132_35,(0,3,4):C.R2GC_132_36,(0,3,5):C.R2GC_132_37})

V_53 = CTVertex(name = 'V_53',
                type = 'R2',
                particles = [ P.g, P.g, P.W__minus__, P.W__plus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.VVVV2, L.VVVV3, L.VVVV5 ],
                loop_particles = [ [ [P.b, P.t] ], [ [P.c, P.d] ], [ [P.c, P.s] ], [ [P.d, P.u] ], [ [P.s, P.u] ] ],
                couplings = {(0,0,0):C.R2GC_144_60,(0,0,1):C.R2GC_144_61,(0,0,2):C.R2GC_144_62,(0,0,3):C.R2GC_144_63,(0,0,4):C.R2GC_144_64,(0,1,0):C.R2GC_144_60,(0,1,1):C.R2GC_144_61,(0,1,2):C.R2GC_144_62,(0,1,3):C.R2GC_144_63,(0,1,4):C.R2GC_144_64,(0,2,0):C.R2GC_144_60,(0,2,1):C.R2GC_144_61,(0,2,2):C.R2GC_144_62,(0,2,3):C.R2GC_144_63,(0,2,4):C.R2GC_144_64})

V_54 = CTVertex(name = 'V_54',
                type = 'R2',
                particles = [ P.a, P.g, P.g, P.Z ],
                color = [ 'Identity(2,3)' ],
                lorentz = [ L.VVVV2, L.VVVV3, L.VVVV5 ],
                loop_particles = [ [ [P.b], [P.d], [P.s] ], [ [P.c], [P.t], [P.u] ] ],
                couplings = {(0,0,0):C.R2GC_134_44,(0,0,1):C.R2GC_134_45,(0,1,0):C.R2GC_134_44,(0,1,1):C.R2GC_134_45,(0,2,0):C.R2GC_134_44,(0,2,1):C.R2GC_134_45})

V_55 = CTVertex(name = 'V_55',
                type = 'R2',
                particles = [ P.g, P.g, P.Z, P.Z ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.VVVV2, L.VVVV3, L.VVVV5 ],
                loop_particles = [ [ [P.b], [P.d], [P.s] ], [ [P.c], [P.t], [P.u] ] ],
                couplings = {(0,0,0):C.R2GC_138_56,(0,0,1):C.R2GC_138_57,(0,1,0):C.R2GC_138_56,(0,1,1):C.R2GC_138_57,(0,2,0):C.R2GC_138_56,(0,2,1):C.R2GC_138_57})

V_56 = CTVertex(name = 'V_56',
                type = 'R2',
                particles = [ P.a, P.a, P.g, P.g ],
                color = [ 'Identity(3,4)' ],
                lorentz = [ L.VVVV2, L.VVVV3, L.VVVV5 ],
                loop_particles = [ [ [P.b], [P.d], [P.s] ], [ [P.c], [P.t], [P.u] ] ],
                couplings = {(0,0,0):C.R2GC_128_16,(0,0,1):C.R2GC_128_17,(0,1,0):C.R2GC_128_16,(0,1,1):C.R2GC_128_17,(0,2,0):C.R2GC_128_16,(0,2,1):C.R2GC_128_17})

V_57 = CTVertex(name = 'V_57',
                type = 'R2',
                particles = [ P.g, P.g, P.g, P.Z ],
                color = [ 'd(1,2,3)', 'f(1,2,3)' ],
                lorentz = [ L.VVVV1, L.VVVV2, L.VVVV3, L.VVVV5 ],
                loop_particles = [ [ [P.b], [P.d], [P.s] ], [ [P.c], [P.t], [P.u] ] ],
                couplings = {(1,0,0):C.R2GC_136_48,(1,0,1):C.R2GC_136_49,(0,1,0):C.R2GC_135_46,(0,1,1):C.R2GC_135_47,(0,2,0):C.R2GC_135_46,(0,2,1):C.R2GC_135_47,(0,3,0):C.R2GC_135_46,(0,3,1):C.R2GC_135_47})

V_58 = CTVertex(name = 'V_58',
                type = 'R2',
                particles = [ P.a, P.g, P.g, P.g ],
                color = [ 'd(2,3,4)' ],
                lorentz = [ L.VVVV2, L.VVVV3, L.VVVV5 ],
                loop_particles = [ [ [P.b], [P.d], [P.s] ], [ [P.c], [P.t], [P.u] ] ],
                couplings = {(0,0,0):C.R2GC_129_18,(0,0,1):C.R2GC_129_19,(0,1,0):C.R2GC_129_18,(0,1,1):C.R2GC_129_19,(0,2,0):C.R2GC_129_18,(0,2,1):C.R2GC_129_19})

V_59 = CTVertex(name = 'V_59',
                type = 'R2',
                particles = [ P.g, P.g, P.H, P.H ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.VVSS1 ],
                loop_particles = [ [ [P.t] ] ],
                couplings = {(0,0,0):C.R2GC_111_3})

V_60 = CTVertex(name = 'V_60',
                type = 'R2',
                particles = [ P.g, P.g, P.G0, P.G0 ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.VVSS1 ],
                loop_particles = [ [ [P.t] ] ],
                couplings = {(0,0,0):C.R2GC_111_3})

V_61 = CTVertex(name = 'V_61',
                type = 'R2',
                particles = [ P.g, P.g, P.G__minus__, P.G__plus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.VVSS1 ],
                loop_particles = [ [ [P.b, P.t] ] ],
                couplings = {(0,0,0):C.R2GC_111_3})

V_62 = CTVertex(name = 'V_62',
                type = 'UV',
                particles = [ P.g, P.g, P.g ],
                color = [ 'f(1,2,3)' ],
                lorentz = [ L.VVV1, L.VVV2, L.VVV3 ],
                loop_particles = [ [ [P.b], [P.c], [P.d], [P.s], [P.u] ], [ [P.g] ], [ [P.ghG] ], [ [P.t] ] ],
                couplings = {(0,1,0):C.UVGC_196_47,(0,1,3):C.UVGC_196_48,(0,2,1):C.UVGC_145_1,(0,0,2):C.UVGC_146_2})

V_63 = CTVertex(name = 'V_63',
                type = 'UV',
                particles = [ P.g, P.g, P.g, P.g ],
                color = [ 'd(-1,1,3)*d(-1,2,4)', 'd(-1,1,3)*f(-1,2,4)', 'd(-1,1,4)*d(-1,2,3)', 'd(-1,1,4)*f(-1,2,3)', 'd(-1,2,3)*f(-1,1,4)', 'd(-1,2,4)*f(-1,1,3)', 'f(-1,1,2)*f(-1,3,4)', 'f(-1,1,3)*f(-1,2,4)', 'f(-1,1,4)*f(-1,2,3)', 'Identity(1,2)*Identity(3,4)', 'Identity(1,3)*Identity(2,4)', 'Identity(1,4)*Identity(2,3)' ],
                lorentz = [ L.VVVV2, L.VVVV3, L.VVVV5, L.VVVV9 ],
                loop_particles = [ [ [P.b], [P.c], [P.d], [P.s], [P.t], [P.u] ], [ [P.b], [P.c], [P.d], [P.s], [P.u] ], [ [P.g] ], [ [P.ghG] ], [ [P.t] ] ],
                couplings = {(2,0,2):C.UVGC_158_9,(2,0,3):C.UVGC_158_8,(0,0,2):C.UVGC_158_9,(0,0,3):C.UVGC_158_8,(4,0,2):C.UVGC_157_6,(4,0,3):C.UVGC_157_7,(3,0,2):C.UVGC_157_6,(3,0,3):C.UVGC_157_7,(8,0,2):C.UVGC_158_8,(8,0,3):C.UVGC_158_9,(7,0,1):C.UVGC_200_58,(7,0,2):C.UVGC_200_59,(7,0,3):C.UVGC_200_60,(7,0,4):C.UVGC_200_61,(6,0,1):C.UVGC_200_58,(6,0,2):C.UVGC_201_62,(6,0,3):C.UVGC_201_63,(6,0,4):C.UVGC_200_61,(5,0,2):C.UVGC_157_6,(5,0,3):C.UVGC_157_7,(1,0,2):C.UVGC_157_6,(1,0,3):C.UVGC_157_7,(11,3,2):C.UVGC_161_12,(11,3,3):C.UVGC_161_13,(10,3,2):C.UVGC_161_12,(10,3,3):C.UVGC_161_13,(9,3,2):C.UVGC_160_10,(9,3,3):C.UVGC_160_11,(2,1,2):C.UVGC_158_9,(2,1,3):C.UVGC_158_8,(0,1,2):C.UVGC_158_9,(0,1,3):C.UVGC_158_8,(6,1,2):C.UVGC_197_49,(6,1,3):C.UVGC_197_50,(6,1,4):C.UVGC_197_51,(4,1,2):C.UVGC_157_6,(4,1,3):C.UVGC_157_7,(3,1,2):C.UVGC_157_6,(3,1,3):C.UVGC_157_7,(8,1,1):C.UVGC_202_64,(8,1,2):C.UVGC_202_65,(8,1,3):C.UVGC_202_66,(8,1,4):C.UVGC_202_67,(7,1,0):C.UVGC_162_14,(7,1,2):C.UVGC_163_16,(7,1,3):C.UVGC_163_17,(5,1,2):C.UVGC_157_6,(5,1,3):C.UVGC_157_7,(1,1,2):C.UVGC_157_6,(1,1,3):C.UVGC_157_7,(2,2,2):C.UVGC_158_9,(2,2,3):C.UVGC_158_8,(0,2,2):C.UVGC_158_9,(0,2,3):C.UVGC_158_8,(4,2,2):C.UVGC_157_6,(4,2,3):C.UVGC_157_7,(3,2,2):C.UVGC_157_6,(3,2,3):C.UVGC_157_7,(8,2,1):C.UVGC_199_54,(8,2,2):C.UVGC_199_55,(8,2,3):C.UVGC_199_56,(8,2,4):C.UVGC_199_57,(6,2,0):C.UVGC_162_14,(6,2,2):C.UVGC_162_15,(6,2,3):C.UVGC_160_10,(7,2,2):C.UVGC_198_52,(7,2,3):C.UVGC_198_53,(7,2,4):C.UVGC_197_51,(5,2,2):C.UVGC_157_6,(5,2,3):C.UVGC_157_7,(1,2,2):C.UVGC_157_6,(1,2,3):C.UVGC_157_7})

V_64 = CTVertex(name = 'V_64',
                type = 'UV',
                particles = [ P.t__tilde__, P.b, P.G__plus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS5 ],
                loop_particles = [ [ [P.b, P.g] ], [ [P.b, P.g, P.t] ], [ [P.g, P.t] ] ],
                couplings = {(0,0,0):C.UVGC_212_79,(0,0,2):C.UVGC_212_80,(0,0,1):C.UVGC_212_81})

V_65 = CTVertex(name = 'V_65',
                type = 'UV',
                particles = [ P.t__tilde__, P.t, P.G0 ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS1 ],
                loop_particles = [ [ [P.g, P.t] ] ],
                couplings = {(0,0,0):C.UVGC_214_85})

V_66 = CTVertex(name = 'V_66',
                type = 'UV',
                particles = [ P.t__tilde__, P.t, P.H ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2 ],
                loop_particles = [ [ [P.g, P.t] ] ],
                couplings = {(0,0,0):C.UVGC_215_86})

V_67 = CTVertex(name = 'V_67',
                type = 'UV',
                particles = [ P.b__tilde__, P.t, P.G__minus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS3 ],
                loop_particles = [ [ [P.b, P.g] ], [ [P.b, P.g, P.t] ], [ [P.g, P.t] ] ],
                couplings = {(0,0,0):C.UVGC_213_82,(0,0,2):C.UVGC_213_83,(0,0,1):C.UVGC_213_84})

V_68 = CTVertex(name = 'V_68',
                type = 'UV',
                particles = [ P.t__tilde__, P.t, P.Y1 ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV5, L.FFV6 ],
                loop_particles = [ [ [P.g, P.t] ] ],
                couplings = {(0,0,0):C.UVGC_206_71,(0,1,0):C.UVGC_207_72})

V_69 = CTVertex(name = 'V_69',
                type = 'UV',
                particles = [ P.u__tilde__, P.u, P.a ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV1, L.FFV3, L.FFV4 ],
                loop_particles = [ [ [P.g, P.u] ] ],
                couplings = {(0,0,0):C.UVGC_169_26,(0,1,0):C.UVGC_150_5,(0,2,0):C.UVGC_150_5})

V_70 = CTVertex(name = 'V_70',
                type = 'UV',
                particles = [ P.c__tilde__, P.c, P.a ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV1, L.FFV3, L.FFV4 ],
                loop_particles = [ [ [P.c, P.g] ] ],
                couplings = {(0,0,0):C.UVGC_169_26,(0,1,0):C.UVGC_150_5,(0,2,0):C.UVGC_150_5})

V_71 = CTVertex(name = 'V_71',
                type = 'UV',
                particles = [ P.t__tilde__, P.t, P.a ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV1, L.FFV3, L.FFV4 ],
                loop_particles = [ [ [P.g, P.t] ] ],
                couplings = {(0,0,0):C.UVGC_169_26,(0,1,0):C.UVGC_204_69,(0,2,0):C.UVGC_204_69})

V_72 = CTVertex(name = 'V_72',
                type = 'UV',
                particles = [ P.u__tilde__, P.u, P.g ],
                color = [ 'T(3,2,1)' ],
                lorentz = [ L.FFV1, L.FFV3, L.FFV4 ],
                loop_particles = [ [ [P.b], [P.c], [P.d], [P.s], [P.u] ], [ [P.g] ], [ [P.ghG] ], [ [P.g, P.u] ], [ [P.t] ] ],
                couplings = {(0,0,3):C.UVGC_167_25,(0,1,0):C.UVGC_166_20,(0,1,1):C.UVGC_166_21,(0,1,2):C.UVGC_166_22,(0,1,4):C.UVGC_166_23,(0,1,3):C.UVGC_166_24,(0,2,0):C.UVGC_166_20,(0,2,1):C.UVGC_166_21,(0,2,2):C.UVGC_166_22,(0,2,4):C.UVGC_166_23,(0,2,3):C.UVGC_166_24})

V_73 = CTVertex(name = 'V_73',
                type = 'UV',
                particles = [ P.c__tilde__, P.c, P.g ],
                color = [ 'T(3,2,1)' ],
                lorentz = [ L.FFV1, L.FFV3, L.FFV4 ],
                loop_particles = [ [ [P.b], [P.c], [P.d], [P.s], [P.u] ], [ [P.c, P.g] ], [ [P.g] ], [ [P.ghG] ], [ [P.t] ] ],
                couplings = {(0,0,1):C.UVGC_167_25,(0,1,0):C.UVGC_166_20,(0,1,2):C.UVGC_166_21,(0,1,3):C.UVGC_166_22,(0,1,4):C.UVGC_166_23,(0,1,1):C.UVGC_166_24,(0,2,0):C.UVGC_166_20,(0,2,2):C.UVGC_166_21,(0,2,3):C.UVGC_166_22,(0,2,4):C.UVGC_166_23,(0,2,1):C.UVGC_166_24})

V_74 = CTVertex(name = 'V_74',
                type = 'UV',
                particles = [ P.t__tilde__, P.t, P.g ],
                color = [ 'T(3,2,1)' ],
                lorentz = [ L.FFV1, L.FFV3, L.FFV4 ],
                loop_particles = [ [ [P.b], [P.c], [P.d], [P.s], [P.u] ], [ [P.g] ], [ [P.ghG] ], [ [P.g, P.t] ], [ [P.t] ] ],
                couplings = {(0,0,3):C.UVGC_167_25,(0,1,0):C.UVGC_166_20,(0,1,1):C.UVGC_166_21,(0,1,2):C.UVGC_166_22,(0,1,4):C.UVGC_166_23,(0,1,3):C.UVGC_205_70,(0,2,0):C.UVGC_166_20,(0,2,1):C.UVGC_166_21,(0,2,2):C.UVGC_166_22,(0,2,4):C.UVGC_166_23,(0,2,3):C.UVGC_205_70})

V_75 = CTVertex(name = 'V_75',
                type = 'UV',
                particles = [ P.d__tilde__, P.u, P.W__minus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV3 ],
                loop_particles = [ [ [P.d, P.g], [P.g, P.u] ], [ [P.d, P.g, P.u] ] ],
                couplings = {(0,0,0):C.UVGC_190_35,(0,0,1):C.UVGC_190_36})

V_76 = CTVertex(name = 'V_76',
                type = 'UV',
                particles = [ P.s__tilde__, P.u, P.W__minus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV3 ],
                loop_particles = [ [ [P.g, P.s], [P.g, P.u] ], [ [P.g, P.s, P.u] ] ],
                couplings = {(0,0,0):C.UVGC_192_39,(0,0,1):C.UVGC_192_40})

V_77 = CTVertex(name = 'V_77',
                type = 'UV',
                particles = [ P.d__tilde__, P.c, P.W__minus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV3 ],
                loop_particles = [ [ [P.c, P.d, P.g] ], [ [P.c, P.g], [P.d, P.g] ] ],
                couplings = {(0,0,1):C.UVGC_186_27,(0,0,0):C.UVGC_186_28})

V_78 = CTVertex(name = 'V_78',
                type = 'UV',
                particles = [ P.s__tilde__, P.c, P.W__minus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV3 ],
                loop_particles = [ [ [P.c, P.g], [P.g, P.s] ], [ [P.c, P.g, P.s] ] ],
                couplings = {(0,0,0):C.UVGC_188_31,(0,0,1):C.UVGC_188_32})

V_79 = CTVertex(name = 'V_79',
                type = 'UV',
                particles = [ P.b__tilde__, P.t, P.W__minus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV3 ],
                loop_particles = [ [ [P.b, P.g] ], [ [P.b, P.g, P.t] ], [ [P.g, P.t] ] ],
                couplings = {(0,0,0):C.UVGC_209_74,(0,0,2):C.UVGC_209_75,(0,0,1):C.UVGC_209_76})

V_80 = CTVertex(name = 'V_80',
                type = 'UV',
                particles = [ P.t__tilde__, P.t, P.Z ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV3, L.FFV4 ],
                loop_particles = [ [ [P.g, P.t] ] ],
                couplings = {(0,0,0):C.UVGC_210_77,(0,1,0):C.UVGC_211_78})

V_81 = CTVertex(name = 'V_81',
                type = 'UV',
                particles = [ P.d__tilde__, P.d, P.a ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV1, L.FFV3, L.FFV4 ],
                loop_particles = [ [ [P.d, P.g] ] ],
                couplings = {(0,0,0):C.UVGC_165_19,(0,1,0):C.UVGC_148_4,(0,2,0):C.UVGC_148_4})

V_82 = CTVertex(name = 'V_82',
                type = 'UV',
                particles = [ P.s__tilde__, P.s, P.a ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV1, L.FFV3, L.FFV4 ],
                loop_particles = [ [ [P.g, P.s] ] ],
                couplings = {(0,0,0):C.UVGC_165_19,(0,1,0):C.UVGC_148_4,(0,2,0):C.UVGC_148_4})

V_83 = CTVertex(name = 'V_83',
                type = 'UV',
                particles = [ P.b__tilde__, P.b, P.a ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV1, L.FFV3, L.FFV4 ],
                loop_particles = [ [ [P.b, P.g] ] ],
                couplings = {(0,0,0):C.UVGC_165_19,(0,1,0):C.UVGC_148_4,(0,2,0):C.UVGC_148_4})

V_84 = CTVertex(name = 'V_84',
                type = 'UV',
                particles = [ P.d__tilde__, P.d, P.g ],
                color = [ 'T(3,2,1)' ],
                lorentz = [ L.FFV1, L.FFV3, L.FFV4 ],
                loop_particles = [ [ [P.b], [P.c], [P.d], [P.s], [P.u] ], [ [P.d, P.g] ], [ [P.g] ], [ [P.ghG] ], [ [P.t] ] ],
                couplings = {(0,0,1):C.UVGC_167_25,(0,1,0):C.UVGC_166_20,(0,1,2):C.UVGC_166_21,(0,1,3):C.UVGC_166_22,(0,1,4):C.UVGC_166_23,(0,1,1):C.UVGC_166_24,(0,2,0):C.UVGC_166_20,(0,2,2):C.UVGC_166_21,(0,2,3):C.UVGC_166_22,(0,2,4):C.UVGC_166_23,(0,2,1):C.UVGC_166_24})

V_85 = CTVertex(name = 'V_85',
                type = 'UV',
                particles = [ P.s__tilde__, P.s, P.g ],
                color = [ 'T(3,2,1)' ],
                lorentz = [ L.FFV1, L.FFV3, L.FFV4 ],
                loop_particles = [ [ [P.b], [P.c], [P.d], [P.s], [P.u] ], [ [P.g] ], [ [P.ghG] ], [ [P.g, P.s] ], [ [P.t] ] ],
                couplings = {(0,0,3):C.UVGC_167_25,(0,1,0):C.UVGC_166_20,(0,1,1):C.UVGC_166_21,(0,1,2):C.UVGC_166_22,(0,1,4):C.UVGC_166_23,(0,1,3):C.UVGC_166_24,(0,2,0):C.UVGC_166_20,(0,2,1):C.UVGC_166_21,(0,2,2):C.UVGC_166_22,(0,2,4):C.UVGC_166_23,(0,2,3):C.UVGC_166_24})

V_86 = CTVertex(name = 'V_86',
                type = 'UV',
                particles = [ P.b__tilde__, P.b, P.g ],
                color = [ 'T(3,2,1)' ],
                lorentz = [ L.FFV1, L.FFV3, L.FFV4 ],
                loop_particles = [ [ [P.b], [P.c], [P.d], [P.s], [P.u] ], [ [P.b, P.g] ], [ [P.g] ], [ [P.ghG] ], [ [P.t] ] ],
                couplings = {(0,0,1):C.UVGC_167_25,(0,1,0):C.UVGC_166_20,(0,1,2):C.UVGC_166_21,(0,1,3):C.UVGC_166_22,(0,1,4):C.UVGC_166_23,(0,1,1):C.UVGC_166_24,(0,2,0):C.UVGC_166_20,(0,2,2):C.UVGC_166_21,(0,2,3):C.UVGC_166_22,(0,2,4):C.UVGC_166_23,(0,2,1):C.UVGC_166_24})

V_87 = CTVertex(name = 'V_87',
                type = 'UV',
                particles = [ P.u__tilde__, P.d, P.W__plus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV3 ],
                loop_particles = [ [ [P.d, P.g], [P.g, P.u] ], [ [P.d, P.g, P.u] ] ],
                couplings = {(0,0,0):C.UVGC_191_37,(0,0,1):C.UVGC_191_38})

V_88 = CTVertex(name = 'V_88',
                type = 'UV',
                particles = [ P.c__tilde__, P.d, P.W__plus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV3 ],
                loop_particles = [ [ [P.c, P.d, P.g] ], [ [P.c, P.g], [P.d, P.g] ] ],
                couplings = {(0,0,1):C.UVGC_187_29,(0,0,0):C.UVGC_187_30})

V_89 = CTVertex(name = 'V_89',
                type = 'UV',
                particles = [ P.u__tilde__, P.s, P.W__plus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV3 ],
                loop_particles = [ [ [P.g, P.s], [P.g, P.u] ], [ [P.g, P.s, P.u] ] ],
                couplings = {(0,0,0):C.UVGC_193_41,(0,0,1):C.UVGC_193_42})

V_90 = CTVertex(name = 'V_90',
                type = 'UV',
                particles = [ P.c__tilde__, P.s, P.W__plus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV3 ],
                loop_particles = [ [ [P.c, P.g], [P.g, P.s] ], [ [P.c, P.g, P.s] ] ],
                couplings = {(0,0,0):C.UVGC_189_33,(0,0,1):C.UVGC_189_34})

V_91 = CTVertex(name = 'V_91',
                type = 'UV',
                particles = [ P.t__tilde__, P.b, P.W__plus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV3 ],
                loop_particles = [ [ [P.b, P.g] ], [ [P.b, P.g, P.t] ], [ [P.g, P.t] ] ],
                couplings = {(0,0,0):C.UVGC_209_74,(0,0,2):C.UVGC_209_75,(0,0,1):C.UVGC_209_76})

V_92 = CTVertex(name = 'V_92',
                type = 'UV',
                particles = [ P.u__tilde__, P.u ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FF1, L.FF3, L.FF5 ],
                loop_particles = [ [ [P.g, P.u] ] ],
                couplings = {(0,0,0):C.UVGC_164_18,(0,1,0):C.UVGC_147_3,(0,2,0):C.UVGC_147_3})

V_93 = CTVertex(name = 'V_93',
                type = 'UV',
                particles = [ P.c__tilde__, P.c ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FF1, L.FF3, L.FF5 ],
                loop_particles = [ [ [P.c, P.g] ] ],
                couplings = {(0,0,0):C.UVGC_164_18,(0,1,0):C.UVGC_147_3,(0,2,0):C.UVGC_147_3})

V_94 = CTVertex(name = 'V_94',
                type = 'UV',
                particles = [ P.t__tilde__, P.t ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FF2, L.FF3, L.FF4, L.FF5 ],
                loop_particles = [ [ [P.g, P.t] ] ],
                couplings = {(0,0,0):C.UVGC_208_73,(0,2,0):C.UVGC_208_73,(0,1,0):C.UVGC_203_68,(0,3,0):C.UVGC_203_68})

V_95 = CTVertex(name = 'V_95',
                type = 'UV',
                particles = [ P.d__tilde__, P.d ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FF1, L.FF3, L.FF5 ],
                loop_particles = [ [ [P.d, P.g] ] ],
                couplings = {(0,0,0):C.UVGC_164_18,(0,1,0):C.UVGC_147_3,(0,2,0):C.UVGC_147_3})

V_96 = CTVertex(name = 'V_96',
                type = 'UV',
                particles = [ P.s__tilde__, P.s ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FF1, L.FF3, L.FF5 ],
                loop_particles = [ [ [P.g, P.s] ] ],
                couplings = {(0,0,0):C.UVGC_164_18,(0,1,0):C.UVGC_147_3,(0,2,0):C.UVGC_147_3})

V_97 = CTVertex(name = 'V_97',
                type = 'UV',
                particles = [ P.b__tilde__, P.b ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FF1, L.FF3, L.FF5 ],
                loop_particles = [ [ [P.b, P.g] ] ],
                couplings = {(0,0,0):C.UVGC_164_18,(0,1,0):C.UVGC_147_3,(0,2,0):C.UVGC_147_3})

V_98 = CTVertex(name = 'V_98',
                type = 'UV',
                particles = [ P.g, P.g ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.VV1, L.VV3 ],
                loop_particles = [ [ [P.g] ], [ [P.ghG] ], [ [P.t] ] ],
                couplings = {(0,0,0):C.UVGC_195_44,(0,0,1):C.UVGC_195_45,(0,0,2):C.UVGC_195_46,(0,1,2):C.UVGC_194_43})

