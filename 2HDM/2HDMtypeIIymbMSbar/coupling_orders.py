# This file was automatically created by FeynRules 2.1.91
# Mathematica version: 9.0 for Mac OS X x86 (64-bit) (January 24, 2013)
# Date: Fri 20 Mar 2015 20:27:25


from object_library import all_orders, CouplingOrder


QCD = CouplingOrder(name = 'QCD',
                    expansion_order = 99,
                    hierarchy = 1,
                    perturbative_expansion = 1)

QED = CouplingOrder(name = 'QED',
                    expansion_order = 99,
                    hierarchy = 2)

YB = CouplingOrder(name = 'YB',
                   expansion_order = 99,
                   hierarchy = 1)

YT = CouplingOrder(name = 'YT',
                   expansion_order = 99,
                   hierarchy = 1)

