# This file was automatically created by FeynRules 2.3.43
# Mathematica version: 12.1.1 for Mac OS X x86 (64-bit) (June 19, 2020)
# Date: Thu 6 May 2021 23:16:07


from object_library import all_couplings, Coupling

from function_library import complexconjugate, re, im, csc, sec, acsc, asec, cot



GC_1 = Coupling(name = 'GC_1',
                value = '-(ee*complex(0,1))/3.',
                order = {'QED':1})

GC_2 = Coupling(name = 'GC_2',
                value = '(2*ee*complex(0,1))/3.',
                order = {'QED':1})

GC_3 = Coupling(name = 'GC_3',
                value = '-(ee*complex(0,1))',
                order = {'QED':1})

GC_4 = Coupling(name = 'GC_4',
                value = 'ee*complex(0,1)',
                order = {'QED':1})

GC_5 = Coupling(name = 'GC_5',
                value = 'ee**2*complex(0,1)',
                order = {'QED':2})

GC_6 = Coupling(name = 'GC_6',
                value = '(-2*cah*complex(0,1))/falp**2',
                order = {'QED':2})

GC_7 = Coupling(name = 'GC_7',
                value = '-G',
                order = {'QCD':1})

GC_8 = Coupling(name = 'GC_8',
                value = 'complex(0,1)*G',
                order = {'QCD':1})

GC_9 = Coupling(name = 'GC_9',
                value = 'complex(0,1)*G**2',
                order = {'QCD':2})

GC_10 = Coupling(name = 'GC_10',
                 value = 'kDL11/(2.*falp)',
                 order = {'NP':1})

GC_11 = Coupling(name = 'GC_11',
                 value = 'kDL12/(2.*falp)',
                 order = {'NP':1})

GC_12 = Coupling(name = 'GC_12',
                 value = 'kDL13/(2.*falp)',
                 order = {'NP':1})

GC_13 = Coupling(name = 'GC_13',
                 value = 'kDL22/(2.*falp)',
                 order = {'NP':1})

GC_14 = Coupling(name = 'GC_14',
                 value = 'kDL23/(2.*falp)',
                 order = {'NP':1})

GC_15 = Coupling(name = 'GC_15',
                 value = 'kDL33/(2.*falp)',
                 order = {'NP':1})

GC_16 = Coupling(name = 'GC_16',
                 value = '-kdR11/(2.*falp)',
                 order = {'NP':1})

GC_17 = Coupling(name = 'GC_17',
                 value = '-kdR12/(2.*falp)',
                 order = {'NP':1})

GC_18 = Coupling(name = 'GC_18',
                 value = '-kdR13/(2.*falp)',
                 order = {'NP':1})

GC_19 = Coupling(name = 'GC_19',
                 value = '-kdR22/(2.*falp)',
                 order = {'NP':1})

GC_20 = Coupling(name = 'GC_20',
                 value = '-kdR23/(2.*falp)',
                 order = {'NP':1})

GC_21 = Coupling(name = 'GC_21',
                 value = '-kdR33/(2.*falp)',
                 order = {'NP':1})

GC_22 = Coupling(name = 'GC_22',
                 value = 'kEL11/(2.*falp)',
                 order = {'NP':1})

GC_23 = Coupling(name = 'GC_23',
                 value = 'kEL12/(2.*falp)',
                 order = {'NP':1})

GC_24 = Coupling(name = 'GC_24',
                 value = 'kEL22/(2.*falp)',
                 order = {'NP':1})

GC_25 = Coupling(name = 'GC_25',
                 value = '-keR11/(2.*falp)',
                 order = {'NP':1})

GC_26 = Coupling(name = 'GC_26',
                 value = '-keR12/(2.*falp)',
                 order = {'NP':1})

GC_27 = Coupling(name = 'GC_27',
                 value = '-keR22/(2.*falp)',
                 order = {'NP':1})

GC_28 = Coupling(name = 'GC_28',
                 value = 'knuL11/(2.*falp)',
                 order = {'NP':1})

GC_29 = Coupling(name = 'GC_29',
                 value = 'knuL12/(2.*falp)',
                 order = {'NP':1})

GC_30 = Coupling(name = 'GC_30',
                 value = 'knuL13/(2.*falp)',
                 order = {'NP':1})

GC_31 = Coupling(name = 'GC_31',
                 value = 'knuL22/(2.*falp)',
                 order = {'NP':1})

GC_32 = Coupling(name = 'GC_32',
                 value = 'knuL23/(2.*falp)',
                 order = {'NP':1})

GC_33 = Coupling(name = 'GC_33',
                 value = 'knuL33/(2.*falp)',
                 order = {'NP':1})

GC_34 = Coupling(name = 'GC_34',
                 value = 'kUL11/(2.*falp)',
                 order = {'NP':1})

GC_35 = Coupling(name = 'GC_35',
                 value = 'kUL12/(2.*falp)',
                 order = {'NP':1})

GC_36 = Coupling(name = 'GC_36',
                 value = 'kUL13/(2.*falp)',
                 order = {'NP':1})

GC_37 = Coupling(name = 'GC_37',
                 value = 'kUL22/(2.*falp)',
                 order = {'NP':1})

GC_38 = Coupling(name = 'GC_38',
                 value = 'kUL23/(2.*falp)',
                 order = {'NP':1})

GC_39 = Coupling(name = 'GC_39',
                 value = 'kUL33/(2.*falp)',
                 order = {'NP':1})

GC_40 = Coupling(name = 'GC_40',
                 value = '-kuR11/(2.*falp)',
                 order = {'NP':1})

GC_41 = Coupling(name = 'GC_41',
                 value = '-kuR12/(2.*falp)',
                 order = {'NP':1})

GC_42 = Coupling(name = 'GC_42',
                 value = '-kuR13/(2.*falp)',
                 order = {'NP':1})

GC_43 = Coupling(name = 'GC_43',
                 value = '-kuR22/(2.*falp)',
                 order = {'NP':1})

GC_44 = Coupling(name = 'GC_44',
                 value = '-kuR23/(2.*falp)',
                 order = {'NP':1})

GC_45 = Coupling(name = 'GC_45',
                 value = '-kuR33/(2.*falp)',
                 order = {'NP':1})

GC_46 = Coupling(name = 'GC_46',
                 value = '-6*complex(0,1)*lam',
                 order = {'QED':2})

GC_47 = Coupling(name = 'GC_47',
                 value = '-(cWW*ee**2*complex(0,1))/(8.*falp*cmath.pi**2) - (cYY*ee**2*complex(0,1))/(8.*falp*cmath.pi**2)',
                 order = {'NP':1,'QED':2})

GC_48 = Coupling(name = 'GC_48',
                 value = '-(cGG*complex(0,1)*G**2)/(32.*falp*cmath.pi**2)',
                 order = {'NP':1,'QCD':2})

GC_49 = Coupling(name = 'GC_49',
                 value = '-(cGG*G**3)/(16.*falp*cmath.pi**2)',
                 order = {'NP':1,'QCD':3})

GC_50 = Coupling(name = 'GC_50',
                 value = '(cw*cWW*ee**3*complex(0,1))/(4.*falp*cmath.pi**2*sw**3)',
                 order = {'NP':1,'QED':3})

GC_51 = Coupling(name = 'GC_51',
                 value = '(ee**2*complex(0,1))/(2.*sw**2)',
                 order = {'QED':2})

GC_52 = Coupling(name = 'GC_52',
                 value = '-((ee**2*complex(0,1))/sw**2)',
                 order = {'QED':2})

GC_53 = Coupling(name = 'GC_53',
                 value = '(cw**2*ee**2*complex(0,1))/sw**2',
                 order = {'QED':2})

GC_54 = Coupling(name = 'GC_54',
                 value = '-(cWW*ee**2*complex(0,1))/(8.*falp*cmath.pi**2*sw**2)',
                 order = {'NP':1,'QED':2})

GC_55 = Coupling(name = 'GC_55',
                 value = '(cWW*ee**3*complex(0,1))/(4.*falp*cmath.pi**2*sw**2)',
                 order = {'NP':1,'QED':3})

GC_56 = Coupling(name = 'GC_56',
                 value = '(ee*complex(0,1))/(sw*cmath.sqrt(2))',
                 order = {'QED':1})

GC_57 = Coupling(name = 'GC_57',
                 value = '(CKM1x1*ee*complex(0,1))/(sw*cmath.sqrt(2))',
                 order = {'QED':1})

GC_58 = Coupling(name = 'GC_58',
                 value = '(CKM1x2*ee*complex(0,1))/(sw*cmath.sqrt(2))',
                 order = {'QED':1})

GC_59 = Coupling(name = 'GC_59',
                 value = '(CKM1x3*ee*complex(0,1))/(sw*cmath.sqrt(2))',
                 order = {'QED':1})

GC_60 = Coupling(name = 'GC_60',
                 value = '(CKM2x1*ee*complex(0,1))/(sw*cmath.sqrt(2))',
                 order = {'QED':1})

GC_61 = Coupling(name = 'GC_61',
                 value = '(CKM2x2*ee*complex(0,1))/(sw*cmath.sqrt(2))',
                 order = {'QED':1})

GC_62 = Coupling(name = 'GC_62',
                 value = '(CKM2x3*ee*complex(0,1))/(sw*cmath.sqrt(2))',
                 order = {'QED':1})

GC_63 = Coupling(name = 'GC_63',
                 value = '(CKM3x1*ee*complex(0,1))/(sw*cmath.sqrt(2))',
                 order = {'QED':1})

GC_64 = Coupling(name = 'GC_64',
                 value = '(CKM3x2*ee*complex(0,1))/(sw*cmath.sqrt(2))',
                 order = {'QED':1})

GC_65 = Coupling(name = 'GC_65',
                 value = '(CKM3x3*ee*complex(0,1))/(sw*cmath.sqrt(2))',
                 order = {'QED':1})

GC_66 = Coupling(name = 'GC_66',
                 value = '(cw*ee*complex(0,1))/sw',
                 order = {'QED':1})

GC_67 = Coupling(name = 'GC_67',
                 value = '(-2*cw*ee**2*complex(0,1))/sw',
                 order = {'QED':2})

GC_68 = Coupling(name = 'GC_68',
                 value = '(ee*complex(0,1)*sw)/(3.*cw)',
                 order = {'QED':1})

GC_69 = Coupling(name = 'GC_69',
                 value = '(-2*ee*complex(0,1)*sw)/(3.*cw)',
                 order = {'QED':1})

GC_70 = Coupling(name = 'GC_70',
                 value = '(ee*complex(0,1)*sw)/cw',
                 order = {'QED':1})

GC_71 = Coupling(name = 'GC_71',
                 value = '-(cw*ee*complex(0,1))/(2.*sw) - (ee*complex(0,1)*sw)/(6.*cw)',
                 order = {'QED':1})

GC_72 = Coupling(name = 'GC_72',
                 value = '(cw*ee*complex(0,1))/(2.*sw) - (ee*complex(0,1)*sw)/(6.*cw)',
                 order = {'QED':1})

GC_73 = Coupling(name = 'GC_73',
                 value = '-(cw*ee*complex(0,1))/(2.*sw) + (ee*complex(0,1)*sw)/(2.*cw)',
                 order = {'QED':1})

GC_74 = Coupling(name = 'GC_74',
                 value = '(cw*ee*complex(0,1))/(2.*sw) + (ee*complex(0,1)*sw)/(2.*cw)',
                 order = {'QED':1})

GC_75 = Coupling(name = 'GC_75',
                 value = '(cw*cZh5*ee)/(falp*sw) + (cZh5*ee*sw)/(cw*falp)',
                 order = {'NP':1,'QED':1})

GC_76 = Coupling(name = 'GC_76',
                 value = '-(cw*cWW*ee**2*complex(0,1))/(8.*falp*cmath.pi**2*sw) + (cYY*ee**2*complex(0,1)*sw)/(8.*cw*falp*cmath.pi**2)',
                 order = {'NP':1,'QED':2})

GC_77 = Coupling(name = 'GC_77',
                 value = 'ee**2*complex(0,1) + (cw**2*ee**2*complex(0,1))/(2.*sw**2) + (ee**2*complex(0,1)*sw**2)/(2.*cw**2)',
                 order = {'QED':2})

GC_78 = Coupling(name = 'GC_78',
                 value = '-(cw**2*cWW*ee**2*complex(0,1))/(8.*falp*cmath.pi**2*sw**2) - (cYY*ee**2*complex(0,1)*sw**2)/(8.*cw**2*falp*cmath.pi**2)',
                 order = {'NP':1,'QED':2})

GC_79 = Coupling(name = 'GC_79',
                 value = '(-2*cah*complex(0,1)*vev)/falp**2',
                 order = {'QED':1})

GC_80 = Coupling(name = 'GC_80',
                 value = '-6*complex(0,1)*lam*vev',
                 order = {'QED':1})

GC_81 = Coupling(name = 'GC_81',
                 value = '(ee**2*complex(0,1)*vev)/(2.*sw**2)',
                 order = {'QED':1})

GC_82 = Coupling(name = 'GC_82',
                 value = '(cw*cZh5*ee*vev)/(falp*sw) + (cZh5*ee*sw*vev)/(cw*falp)',
                 order = {'NP':1})

GC_83 = Coupling(name = 'GC_83',
                 value = 'ee**2*complex(0,1)*vev + (cw**2*ee**2*complex(0,1)*vev)/(2.*sw**2) + (ee**2*complex(0,1)*sw**2*vev)/(2.*cw**2)',
                 order = {'QED':1})

GC_84 = Coupling(name = 'GC_84',
                 value = '-((complex(0,1)*yb)/cmath.sqrt(2))',
                 order = {'QED':1})

GC_85 = Coupling(name = 'GC_85',
                 value = '-((complex(0,1)*yc)/cmath.sqrt(2))',
                 order = {'QED':1})

GC_86 = Coupling(name = 'GC_86',
                 value = '-((complex(0,1)*ydo)/cmath.sqrt(2))',
                 order = {'QED':1})

GC_87 = Coupling(name = 'GC_87',
                 value = '-((complex(0,1)*ye)/cmath.sqrt(2))',
                 order = {'QED':1})

GC_88 = Coupling(name = 'GC_88',
                 value = '-((complex(0,1)*ym)/cmath.sqrt(2))',
                 order = {'QED':1})

GC_89 = Coupling(name = 'GC_89',
                 value = '-((complex(0,1)*ys)/cmath.sqrt(2))',
                 order = {'QED':1})

GC_90 = Coupling(name = 'GC_90',
                 value = '-((complex(0,1)*yt)/cmath.sqrt(2))',
                 order = {'QED':1})

GC_91 = Coupling(name = 'GC_91',
                 value = '-((complex(0,1)*ytau)/cmath.sqrt(2))',
                 order = {'QED':1})

GC_92 = Coupling(name = 'GC_92',
                 value = '-((complex(0,1)*yup)/cmath.sqrt(2))',
                 order = {'QED':1})

GC_93 = Coupling(name = 'GC_93',
                 value = '(ee*complex(0,1)*complexconjugate(CKM1x1))/(sw*cmath.sqrt(2))',
                 order = {'QED':1})

GC_94 = Coupling(name = 'GC_94',
                 value = '(ee*complex(0,1)*complexconjugate(CKM1x2))/(sw*cmath.sqrt(2))',
                 order = {'QED':1})

GC_95 = Coupling(name = 'GC_95',
                 value = '(ee*complex(0,1)*complexconjugate(CKM1x3))/(sw*cmath.sqrt(2))',
                 order = {'QED':1})

GC_96 = Coupling(name = 'GC_96',
                 value = '(ee*complex(0,1)*complexconjugate(CKM2x1))/(sw*cmath.sqrt(2))',
                 order = {'QED':1})

GC_97 = Coupling(name = 'GC_97',
                 value = '(ee*complex(0,1)*complexconjugate(CKM2x2))/(sw*cmath.sqrt(2))',
                 order = {'QED':1})

GC_98 = Coupling(name = 'GC_98',
                 value = '(ee*complex(0,1)*complexconjugate(CKM2x3))/(sw*cmath.sqrt(2))',
                 order = {'QED':1})

GC_99 = Coupling(name = 'GC_99',
                 value = '(ee*complex(0,1)*complexconjugate(CKM3x1))/(sw*cmath.sqrt(2))',
                 order = {'QED':1})

GC_100 = Coupling(name = 'GC_100',
                  value = '(ee*complex(0,1)*complexconjugate(CKM3x2))/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_101 = Coupling(name = 'GC_101',
                  value = '(ee*complex(0,1)*complexconjugate(CKM3x3))/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

