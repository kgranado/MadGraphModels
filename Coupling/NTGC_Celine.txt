Requestor: Danning Liu
Content: neutral Triple Gauge couplings of Z(ll)+gamma process
          model proposed by prof. Celine Degrande
Paper: https://arxiv.org/pdf/1308.6323.pdf
Website: https://feynrules.irmp.ucl.ac.be/wiki/NTGC#no1
JIRA: https://its.cern.ch/jira/browse/AGENE-2146
