# This file was automatically created by FeynRules 2.4.58
# Mathematica version: 11.0.0 for Mac OS X x86 (64-bit) (July 28, 2016)
# Date: Tue 28 Nov 2017 15:14:54


from object_library import all_vertices, all_CTvertices, Vertex, CTVertex
import particles as P
import CT_couplings as C
import lorentz as L


V_1 = CTVertex(name = 'V_1',
               type = 'R2',
               particles = [ P.g, P.g, P.g ],
               color = [ 'f(1,2,3)' ],
               lorentz = [ L.VVV1 ],
               loop_particles = [ [ [P.b], [P.c], [P.d], [P.s], [P.t], [P.u] ], [ [P.g] ] ],
               couplings = {(0,0,0):C.R2GC_198_81,(0,0,1):C.R2GC_198_82})

V_2 = CTVertex(name = 'V_2',
               type = 'R2',
               particles = [ P.g, P.g, P.g, P.g ],
               color = [ 'd(-1,1,3)*d(-1,2,4)', 'd(-1,1,3)*f(-1,2,4)', 'd(-1,1,4)*d(-1,2,3)', 'd(-1,1,4)*f(-1,2,3)', 'd(-1,2,3)*f(-1,1,4)', 'd(-1,2,4)*f(-1,1,3)', 'f(-1,1,2)*f(-1,3,4)', 'f(-1,1,3)*f(-1,2,4)', 'f(-1,1,4)*f(-1,2,3)', 'Identity(1,2)*Identity(3,4)', 'Identity(1,3)*Identity(2,4)', 'Identity(1,4)*Identity(2,3)' ],
               lorentz = [ L.VVVV1, L.VVVV2, L.VVVV4, L.VVVV9 ],
               loop_particles = [ [ [P.b], [P.c], [P.d], [P.s], [P.t], [P.u] ], [ [P.g] ] ],
               couplings = {(2,0,0):C.R2GC_142_37,(2,0,1):C.R2GC_142_38,(0,0,0):C.R2GC_142_37,(0,0,1):C.R2GC_142_38,(4,0,0):C.R2GC_140_33,(4,0,1):C.R2GC_140_34,(3,0,0):C.R2GC_140_33,(3,0,1):C.R2GC_140_34,(8,0,0):C.R2GC_141_35,(8,0,1):C.R2GC_141_36,(7,0,0):C.R2GC_148_45,(7,0,1):C.R2GC_201_87,(5,0,0):C.R2GC_140_33,(5,0,1):C.R2GC_140_34,(1,0,0):C.R2GC_140_33,(1,0,1):C.R2GC_140_34,(6,0,0):C.R2GC_149_47,(6,0,1):C.R2GC_202_88,(11,3,0):C.R2GC_144_40,(11,3,1):C.R2GC_144_41,(10,3,0):C.R2GC_144_40,(10,3,1):C.R2GC_144_41,(9,3,1):C.R2GC_143_39,(2,1,0):C.R2GC_142_37,(2,1,1):C.R2GC_142_38,(0,1,0):C.R2GC_142_37,(0,1,1):C.R2GC_142_38,(4,1,0):C.R2GC_140_33,(4,1,1):C.R2GC_140_34,(3,1,0):C.R2GC_140_33,(3,1,1):C.R2GC_140_34,(8,1,0):C.R2GC_148_45,(8,1,1):C.R2GC_201_87,(6,1,0):C.R2GC_200_85,(6,1,1):C.R2GC_200_86,(5,1,0):C.R2GC_140_33,(5,1,1):C.R2GC_140_34,(1,1,0):C.R2GC_140_33,(1,1,1):C.R2GC_140_34,(7,1,0):C.R2GC_141_35,(7,1,1):C.R2GC_141_36,(2,2,0):C.R2GC_142_37,(2,2,1):C.R2GC_142_38,(0,2,0):C.R2GC_142_37,(0,2,1):C.R2GC_142_38,(4,2,0):C.R2GC_140_33,(4,2,1):C.R2GC_140_34,(3,2,0):C.R2GC_140_33,(3,2,1):C.R2GC_140_34,(8,2,0):C.R2GC_199_83,(8,2,1):C.R2GC_199_84,(7,2,0):C.R2GC_199_83,(7,2,1):C.R2GC_199_84,(5,2,0):C.R2GC_140_33,(5,2,1):C.R2GC_140_34,(1,2,0):C.R2GC_140_33,(1,2,1):C.R2GC_140_34})

V_3 = CTVertex(name = 'V_3',
               type = 'R2',
               particles = [ P.t__tilde__, P.b, P.G__plus__ ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFS3, L.FFS5 ],
               loop_particles = [ [ [P.b, P.g, P.t] ] ],
               couplings = {(0,0,0):C.R2GC_214_95,(0,1,0):C.R2GC_216_97})

V_4 = CTVertex(name = 'V_4',
               type = 'R2',
               particles = [ P.b__tilde__, P.b, P.G0 ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFS1 ],
               loop_particles = [ [ [P.b, P.g] ] ],
               couplings = {(0,0,0):C.R2GC_195_79})

V_5 = CTVertex(name = 'V_5',
               type = 'R2',
               particles = [ P.b__tilde__, P.b, P.H ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFS2 ],
               loop_particles = [ [ [P.b, P.g] ] ],
               couplings = {(0,0,0):C.R2GC_194_78})

V_6 = CTVertex(name = 'V_6',
               type = 'R2',
               particles = [ P.b__tilde__, P.t, P.G__minus__ ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFS3, L.FFS5 ],
               loop_particles = [ [ [P.b, P.g, P.t] ] ],
               couplings = {(0,0,0):C.R2GC_217_98,(0,1,0):C.R2GC_213_94})

V_7 = CTVertex(name = 'V_7',
               type = 'R2',
               particles = [ P.t__tilde__, P.t, P.G0 ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFS1 ],
               loop_particles = [ [ [P.g, P.t] ] ],
               couplings = {(0,0,0):C.R2GC_218_99})

V_8 = CTVertex(name = 'V_8',
               type = 'R2',
               particles = [ P.t__tilde__, P.t, P.H ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFS2 ],
               loop_particles = [ [ [P.g, P.t] ] ],
               couplings = {(0,0,0):C.R2GC_219_100})

V_9 = CTVertex(name = 'V_9',
               type = 'R2',
               particles = [ P.g, P.g, P.sig8 ],
               color = [ 'd(1,2,3)' ],
               lorentz = [ L.VVS1, L.VVS4, L.VVS6 ],
               loop_particles = [ [ [P.b] ], [ [P.g] ], [ [P.g, P.sig8] ], [ [P.t] ] ],
               couplings = {(0,2,1):C.R2GC_79_102,(0,1,2):C.R2GC_90_107,(0,0,0):C.R2GC_100_1,(0,0,3):C.R2GC_100_2})

V_10 = CTVertex(name = 'V_10',
                type = 'R2',
                particles = [ P.g, P.g, P.g, P.sig8 ],
                color = [ 'd(-1,1,2)*f(-1,3,4)', 'd(-1,1,3)*f(-1,2,4)', 'd(-1,1,4)*f(-1,2,3)', 'd(-1,2,3)*f(-1,1,4)', 'd(-1,2,4)*f(-1,1,3)', 'd(-1,3,4)*f(-1,1,2)' ],
                lorentz = [ L.VVVS1, L.VVVS10, L.VVVS2, L.VVVS3, L.VVVS4, L.VVVS5, L.VVVS6, L.VVVS8, L.VVVS9 ],
                loop_particles = [ [ [P.g] ], [ [P.g, P.sig8] ] ],
                couplings = {(0,0,0):C.R2GC_156_56,(1,0,0):C.R2GC_173_70,(1,0,1):C.R2GC_129_23,(2,0,0):C.R2GC_164_63,(2,0,1):C.R2GC_132_27,(3,0,0):C.R2GC_131_26,(4,0,0):C.R2GC_167_66,(4,0,1):C.R2GC_129_23,(5,0,0):C.R2GC_156_56,(5,0,1):C.R2GC_132_27,(1,2,0):C.R2GC_125_18,(0,2,0):C.R2GC_156_56,(3,2,1):C.R2GC_129_23,(4,2,0):C.R2GC_208_92,(4,2,1):C.R2GC_132_27,(2,2,0):C.R2GC_165_64,(2,2,1):C.R2GC_129_23,(5,2,0):C.R2GC_137_31,(5,2,1):C.R2GC_170_69,(0,3,0):C.R2GC_127_20,(1,3,0):C.R2GC_126_19,(3,3,0):C.R2GC_126_19,(2,3,0):C.R2GC_135_29,(2,3,1):C.R2GC_130_25,(4,3,0):C.R2GC_135_29,(4,3,1):C.R2GC_130_25,(0,4,0):C.R2GC_134_28,(1,4,0):C.R2GC_155_55,(1,4,1):C.R2GC_130_25,(2,4,0):C.R2GC_163_62,(2,4,1):C.R2GC_170_69,(3,4,0):C.R2GC_131_26,(4,4,0):C.R2GC_160_59,(4,4,1):C.R2GC_130_25,(5,4,0):C.R2GC_137_31,(5,4,1):C.R2GC_170_69,(0,5,0):C.R2GC_132_27,(3,5,1):C.R2GC_129_23,(1,5,0):C.R2GC_161_60,(5,5,0):C.R2GC_207_91,(5,5,1):C.R2GC_132_27,(2,5,0):C.R2GC_162_61,(2,5,1):C.R2GC_130_25,(4,5,0):C.R2GC_170_68,(4,5,1):C.R2GC_170_69,(0,6,0):C.R2GC_131_26,(1,6,0):C.R2GC_80_103,(3,6,0):C.R2GC_126_19,(2,6,0):C.R2GC_129_22,(2,6,1):C.R2GC_129_23,(4,6,0):C.R2GC_128_21,(5,6,0):C.R2GC_130_24,(5,6,1):C.R2GC_130_25,(0,7,0):C.R2GC_134_28,(1,7,0):C.R2GC_136_30,(3,7,0):C.R2GC_157_57,(3,7,1):C.R2GC_130_25,(4,7,0):C.R2GC_205_89,(4,7,1):C.R2GC_170_69,(2,7,0):C.R2GC_163_62,(2,7,1):C.R2GC_130_25,(5,7,0):C.R2GC_156_56,(5,7,1):C.R2GC_132_27,(0,8,0):C.R2GC_132_27,(1,8,0):C.R2GC_166_65,(3,8,0):C.R2GC_157_57,(3,8,1):C.R2GC_130_25,(5,8,0):C.R2GC_206_90,(5,8,1):C.R2GC_170_69,(2,8,0):C.R2GC_164_63,(2,8,1):C.R2GC_129_23,(4,8,0):C.R2GC_158_58,(4,8,1):C.R2GC_132_27,(0,1,0):C.R2GC_131_26,(1,1,0):C.R2GC_131_26,(3,1,0):C.R2GC_137_31,(4,1,0):C.R2GC_134_28,(4,1,1):C.R2GC_129_23,(5,1,0):C.R2GC_134_28,(5,1,1):C.R2GC_129_23})

V_11 = CTVertex(name = 'V_11',
                type = 'R2',
                particles = [ P.g, P.sig8, P.sig8 ],
                color = [ 'f(1,2,3)' ],
                lorentz = [ L.VSS1 ],
                loop_particles = [ [ [P.g, P.sig8] ] ],
                couplings = {(0,0,0):C.R2GC_146_42})

V_12 = CTVertex(name = 'V_12',
                type = 'R2',
                particles = [ P.g, P.g, P.sig8, P.sig8 ],
                color = [ 'd(-1,1,3)*d(-1,2,4)', 'd(-1,1,3)*f(-1,2,4)', 'd(-1,1,4)*d(-1,2,3)', 'd(-1,1,4)*f(-1,2,3)', 'd(-1,2,3)*f(-1,1,4)', 'd(-1,2,4)*f(-1,1,3)', 'f(-1,1,3)*f(-1,2,4)', 'f(-1,1,4)*f(-1,2,3)', 'Identity(1,2)*Identity(3,4)', 'Identity(1,3)*Identity(2,4)', 'Identity(1,4)*Identity(2,3)' ],
                lorentz = [ L.VVSS1 ],
                loop_particles = [ [ [P.g] ], [ [P.g, P.sig8] ] ],
                couplings = {(0,0,0):C.R2GC_148_45,(0,0,1):C.R2GC_148_46,(2,0,0):C.R2GC_148_45,(2,0,1):C.R2GC_148_46,(6,0,0):C.R2GC_148_45,(6,0,1):C.R2GC_151_51,(7,0,0):C.R2GC_148_45,(7,0,1):C.R2GC_151_51,(5,0,0):C.R2GC_147_43,(5,0,1):C.R2GC_147_44,(1,0,0):C.R2GC_147_43,(1,0,1):C.R2GC_147_44,(4,0,0):C.R2GC_147_43,(4,0,1):C.R2GC_147_44,(3,0,0):C.R2GC_147_43,(3,0,1):C.R2GC_147_44,(10,0,0):C.R2GC_150_49,(10,0,1):C.R2GC_150_50,(9,0,0):C.R2GC_150_49,(9,0,1):C.R2GC_150_50,(8,0,0):C.R2GC_149_47,(8,0,1):C.R2GC_149_48})

V_13 = CTVertex(name = 'V_13',
                type = 'R2',
                particles = [ P.d__tilde__, P.d, P.sig8 ],
                color = [ 'T(3,2,1)' ],
                lorentz = [ L.FFS2, L.FFS6, L.FFS7 ],
                loop_particles = [ [ [P.d, P.g] ], [ [P.d, P.g, P.sig8] ] ],
                couplings = {(0,1,0):C.R2GC_107_12,(0,0,1):C.R2GC_179_72,(0,2,0):C.R2GC_82_105})

V_14 = CTVertex(name = 'V_14',
                type = 'R2',
                particles = [ P.s__tilde__, P.s, P.sig8 ],
                color = [ 'T(3,2,1)' ],
                lorentz = [ L.FFS2, L.FFS6, L.FFS7 ],
                loop_particles = [ [ [P.g, P.s] ], [ [P.g, P.s, P.sig8] ] ],
                couplings = {(0,1,0):C.R2GC_108_13,(0,0,1):C.R2GC_182_73,(0,2,0):C.R2GC_82_105})

V_15 = CTVertex(name = 'V_15',
                type = 'R2',
                particles = [ P.b__tilde__, P.b, P.sig8 ],
                color = [ 'T(3,2,1)' ],
                lorentz = [ L.FFS2, L.FFS6, L.FFS7 ],
                loop_particles = [ [ [P.b, P.g] ], [ [P.b, P.g, P.sig8] ] ],
                couplings = {(0,1,0):C.R2GC_105_10,(0,0,1):C.R2GC_196_80,(0,2,0):C.R2GC_82_105})

V_16 = CTVertex(name = 'V_16',
                type = 'R2',
                particles = [ P.u__tilde__, P.u, P.sig8 ],
                color = [ 'T(3,2,1)' ],
                lorentz = [ L.FFS2, L.FFS6, L.FFS7 ],
                loop_particles = [ [ [P.g, P.sig8, P.u] ], [ [P.g, P.u] ] ],
                couplings = {(0,1,1):C.R2GC_111_16,(0,0,0):C.R2GC_187_74,(0,2,1):C.R2GC_82_105})

V_17 = CTVertex(name = 'V_17',
                type = 'R2',
                particles = [ P.c__tilde__, P.c, P.sig8 ],
                color = [ 'T(3,2,1)' ],
                lorentz = [ L.FFS2, L.FFS6, L.FFS7 ],
                loop_particles = [ [ [P.c, P.g] ], [ [P.c, P.g, P.sig8] ] ],
                couplings = {(0,1,0):C.R2GC_106_11,(0,0,1):C.R2GC_176_71,(0,2,0):C.R2GC_82_105})

V_18 = CTVertex(name = 'V_18',
                type = 'R2',
                particles = [ P.t__tilde__, P.t, P.sig8 ],
                color = [ 'T(3,2,1)' ],
                lorentz = [ L.FFS2, L.FFS6, L.FFS7 ],
                loop_particles = [ [ [P.g, P.sig8, P.t] ], [ [P.g, P.t] ] ],
                couplings = {(0,1,1):C.R2GC_110_15,(0,0,0):C.R2GC_215_96,(0,2,1):C.R2GC_82_105})

V_19 = CTVertex(name = 'V_19',
                type = 'R2',
                particles = [ P.u__tilde__, P.u, P.g ],
                color = [ 'T(3,2,1)' ],
                lorentz = [ L.FFV1 ],
                loop_particles = [ [ [P.g, P.u] ] ],
                couplings = {(0,0,0):C.R2GC_81_104})

V_20 = CTVertex(name = 'V_20',
                type = 'R2',
                particles = [ P.c__tilde__, P.c, P.g ],
                color = [ 'T(3,2,1)' ],
                lorentz = [ L.FFV1 ],
                loop_particles = [ [ [P.c, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_81_104})

V_21 = CTVertex(name = 'V_21',
                type = 'R2',
                particles = [ P.t__tilde__, P.t, P.g ],
                color = [ 'T(3,2,1)' ],
                lorentz = [ L.FFV1 ],
                loop_particles = [ [ [P.g, P.t] ] ],
                couplings = {(0,0,0):C.R2GC_81_104})

V_22 = CTVertex(name = 'V_22',
                type = 'R2',
                particles = [ P.d__tilde__, P.d, P.g ],
                color = [ 'T(3,2,1)' ],
                lorentz = [ L.FFV1 ],
                loop_particles = [ [ [P.d, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_81_104})

V_23 = CTVertex(name = 'V_23',
                type = 'R2',
                particles = [ P.s__tilde__, P.s, P.g ],
                color = [ 'T(3,2,1)' ],
                lorentz = [ L.FFV1 ],
                loop_particles = [ [ [P.g, P.s] ] ],
                couplings = {(0,0,0):C.R2GC_81_104})

V_24 = CTVertex(name = 'V_24',
                type = 'R2',
                particles = [ P.b__tilde__, P.b, P.g ],
                color = [ 'T(3,2,1)' ],
                lorentz = [ L.FFV1 ],
                loop_particles = [ [ [P.b, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_81_104})

V_25 = CTVertex(name = 'V_25',
                type = 'R2',
                particles = [ P.u__tilde__, P.u ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FF1 ],
                loop_particles = [ [ [P.g, P.u] ] ],
                couplings = {(0,0,0):C.R2GC_191_76})

V_26 = CTVertex(name = 'V_26',
                type = 'R2',
                particles = [ P.c__tilde__, P.c ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FF1 ],
                loop_particles = [ [ [P.c, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_191_76})

V_27 = CTVertex(name = 'V_27',
                type = 'R2',
                particles = [ P.t__tilde__, P.t ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FF2, L.FF3 ],
                loop_particles = [ [ [P.g, P.t] ] ],
                couplings = {(0,0,0):C.R2GC_212_93,(0,1,0):C.R2GC_191_76})

V_28 = CTVertex(name = 'V_28',
                type = 'R2',
                particles = [ P.d__tilde__, P.d ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FF1 ],
                loop_particles = [ [ [P.d, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_191_76})

V_29 = CTVertex(name = 'V_29',
                type = 'R2',
                particles = [ P.s__tilde__, P.s ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FF1 ],
                loop_particles = [ [ [P.g, P.s] ] ],
                couplings = {(0,0,0):C.R2GC_191_76})

V_30 = CTVertex(name = 'V_30',
                type = 'R2',
                particles = [ P.b__tilde__, P.b ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FF2, L.FF3 ],
                loop_particles = [ [ [P.b, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_193_77,(0,1,0):C.R2GC_191_76})

V_31 = CTVertex(name = 'V_31',
                type = 'R2',
                particles = [ P.sig8, P.sig8 ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.SS1, L.SS2 ],
                loop_particles = [ [ [P.g, P.sig8] ] ],
                couplings = {(0,0,0):C.R2GC_190_75,(0,1,0):C.R2GC_89_106})

V_32 = CTVertex(name = 'V_32',
                type = 'R2',
                particles = [ P.g, P.g ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.VV2, L.VV3, L.VV4 ],
                loop_particles = [ [ [P.b] ], [ [P.b], [P.c], [P.d], [P.s], [P.t], [P.u] ], [ [P.g] ], [ [P.t] ] ],
                couplings = {(0,2,2):C.R2GC_78_101,(0,0,0):C.R2GC_99_108,(0,0,3):C.R2GC_99_109,(0,1,1):C.R2GC_104_9})

V_33 = CTVertex(name = 'V_33',
                type = 'R2',
                particles = [ P.g, P.g, P.H ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.VVS1 ],
                loop_particles = [ [ [P.b] ], [ [P.t] ] ],
                couplings = {(0,0,0):C.R2GC_101_3,(0,0,1):C.R2GC_101_4})

V_34 = CTVertex(name = 'V_34',
                type = 'R2',
                particles = [ P.sig8, P.sig8, P.sig8 ],
                color = [ 'd(1,2,3)' ],
                lorentz = [ L.SSS2, L.SSS3 ],
                loop_particles = [ [ [P.g] ], [ [P.g, P.sig8] ] ],
                couplings = {(0,0,1):C.R2GC_152_52,(0,1,0):C.R2GC_124_17})

V_35 = CTVertex(name = 'V_35',
                type = 'R2',
                particles = [ P.g, P.g, P.H, P.H ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.VVSS1 ],
                loop_particles = [ [ [P.b] ], [ [P.t] ] ],
                couplings = {(0,0,0):C.R2GC_103_7,(0,0,1):C.R2GC_103_8})

V_36 = CTVertex(name = 'V_36',
                type = 'R2',
                particles = [ P.g, P.g, P.G0, P.G0 ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.VVSS1 ],
                loop_particles = [ [ [P.b] ], [ [P.t] ] ],
                couplings = {(0,0,0):C.R2GC_103_7,(0,0,1):C.R2GC_103_8})

V_37 = CTVertex(name = 'V_37',
                type = 'R2',
                particles = [ P.g, P.g, P.G__minus__, P.G__plus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.VVSS1 ],
                loop_particles = [ [ [P.b, P.t] ] ],
                couplings = {(0,0,0):C.R2GC_109_14})

V_38 = CTVertex(name = 'V_38',
                type = 'R2',
                particles = [ P.g, P.g, P.H, P.sig8 ],
                color = [ 'd(1,2,4)' ],
                lorentz = [ L.VVSS1 ],
                loop_particles = [ [ [P.b] ], [ [P.t] ] ],
                couplings = {(0,0,0):C.R2GC_102_5,(0,0,1):C.R2GC_102_6})

V_39 = CTVertex(name = 'V_39',
                type = 'R2',
                particles = [ P.g, P.sig8, P.sig8, P.sig8 ],
                color = [ 'd(-1,1,2)*f(-1,3,4)', 'd(-1,1,3)*f(-1,2,4)', 'd(-1,1,4)*f(-1,2,3)', 'd(-1,2,3)*f(-1,1,4)', 'd(-1,2,4)*f(-1,1,3)', 'd(-1,3,4)*f(-1,1,2)' ],
                lorentz = [ L.VSSS1, L.VSSS2, L.VSSS3 ],
                loop_particles = [ [ [P.g] ], [ [P.g, P.sig8] ] ],
                couplings = {(3,0,0):C.R2GC_153_53,(3,0,1):C.R2GC_153_54,(4,0,0):C.R2GC_153_53,(4,0,1):C.R2GC_153_54,(1,0,1):C.R2GC_169_67,(5,0,0):C.R2GC_138_32,(2,0,1):C.R2GC_169_67,(0,1,1):C.R2GC_169_67,(3,1,0):C.R2GC_153_53,(3,1,1):C.R2GC_153_54,(4,1,0):C.R2GC_138_32,(5,1,0):C.R2GC_153_53,(5,1,1):C.R2GC_153_54,(2,1,1):C.R2GC_131_26,(0,2,1):C.R2GC_131_26,(3,2,0):C.R2GC_138_32,(4,2,0):C.R2GC_153_53,(4,2,1):C.R2GC_153_54,(1,2,1):C.R2GC_131_26,(5,2,0):C.R2GC_153_53,(5,2,1):C.R2GC_153_54})

V_40 = CTVertex(name = 'V_40',
                type = 'UV',
                particles = [ P.g, P.g, P.g ],
                color = [ 'f(1,2,3)' ],
                lorentz = [ L.VVV1 ],
                loop_particles = [ [ [P.b] ], [ [P.c], [P.d], [P.s], [P.u] ], [ [P.g] ], [ [P.ghG] ], [ [P.sig8] ], [ [P.t] ] ],
                couplings = {(0,0,0):C.UVGC_198_99,(0,0,1):C.UVGC_198_100,(0,0,2):C.UVGC_198_101,(0,0,3):C.UVGC_198_102,(0,0,4):C.UVGC_198_103,(0,0,5):C.UVGC_198_104})

V_41 = CTVertex(name = 'V_41',
                type = 'UV',
                particles = [ P.g, P.g, P.g, P.g ],
                color = [ 'd(-1,1,3)*d(-1,2,4)', 'd(-1,1,3)*f(-1,2,4)', 'd(-1,1,4)*d(-1,2,3)', 'd(-1,1,4)*f(-1,2,3)', 'd(-1,2,3)*f(-1,1,4)', 'd(-1,2,4)*f(-1,1,3)', 'f(-1,1,2)*f(-1,3,4)', 'f(-1,1,3)*f(-1,2,4)', 'f(-1,1,4)*f(-1,2,3)', 'Identity(1,2)*Identity(3,4)', 'Identity(1,3)*Identity(2,4)', 'Identity(1,4)*Identity(2,3)' ],
                lorentz = [ L.VVVV1, L.VVVV2, L.VVVV4, L.VVVV9 ],
                loop_particles = [ [ [P.b] ], [ [P.c], [P.d], [P.s], [P.u] ], [ [P.g] ], [ [P.ghG] ], [ [P.sig8] ], [ [P.t] ] ],
                couplings = {(2,0,2):C.UVGC_141_24,(2,0,3):C.UVGC_141_23,(0,0,2):C.UVGC_141_24,(0,0,3):C.UVGC_141_23,(4,0,2):C.UVGC_140_21,(4,0,3):C.UVGC_140_22,(3,0,2):C.UVGC_140_21,(3,0,3):C.UVGC_140_22,(8,0,2):C.UVGC_141_23,(8,0,3):C.UVGC_141_24,(7,0,0):C.UVGC_201_113,(7,0,1):C.UVGC_151_44,(7,0,2):C.UVGC_201_114,(7,0,3):C.UVGC_201_115,(7,0,4):C.UVGC_201_116,(7,0,5):C.UVGC_201_117,(5,0,2):C.UVGC_140_21,(5,0,3):C.UVGC_140_22,(1,0,2):C.UVGC_140_21,(1,0,3):C.UVGC_140_22,(6,0,0):C.UVGC_201_113,(6,0,1):C.UVGC_151_44,(6,0,2):C.UVGC_202_118,(6,0,3):C.UVGC_202_119,(6,0,4):C.UVGC_201_116,(6,0,5):C.UVGC_201_117,(11,3,2):C.UVGC_144_27,(11,3,3):C.UVGC_144_28,(10,3,2):C.UVGC_144_27,(10,3,3):C.UVGC_144_28,(9,3,2):C.UVGC_143_25,(9,3,3):C.UVGC_143_26,(2,1,2):C.UVGC_141_24,(2,1,3):C.UVGC_141_23,(0,1,2):C.UVGC_141_24,(0,1,3):C.UVGC_141_23,(4,1,2):C.UVGC_140_21,(4,1,3):C.UVGC_140_22,(3,1,2):C.UVGC_140_21,(3,1,3):C.UVGC_140_22,(8,1,0):C.UVGC_201_113,(8,1,1):C.UVGC_151_44,(8,1,2):C.UVGC_201_114,(8,1,3):C.UVGC_201_115,(8,1,4):C.UVGC_201_116,(8,1,5):C.UVGC_201_117,(6,1,0):C.UVGC_199_105,(6,1,1):C.UVGC_199_106,(6,1,2):C.UVGC_200_111,(6,1,3):C.UVGC_200_112,(6,1,4):C.UVGC_199_109,(6,1,5):C.UVGC_199_110,(5,1,2):C.UVGC_140_21,(5,1,3):C.UVGC_140_22,(1,1,2):C.UVGC_140_21,(1,1,3):C.UVGC_140_22,(7,1,2):C.UVGC_141_23,(7,1,3):C.UVGC_141_24,(2,2,2):C.UVGC_141_24,(2,2,3):C.UVGC_141_23,(0,2,2):C.UVGC_141_24,(0,2,3):C.UVGC_141_23,(4,2,2):C.UVGC_140_21,(4,2,3):C.UVGC_140_22,(3,2,2):C.UVGC_140_21,(3,2,3):C.UVGC_140_22,(8,2,0):C.UVGC_199_105,(8,2,1):C.UVGC_199_106,(8,2,2):C.UVGC_199_107,(8,2,3):C.UVGC_199_108,(8,2,4):C.UVGC_199_109,(8,2,5):C.UVGC_199_110,(7,2,0):C.UVGC_199_105,(7,2,1):C.UVGC_199_106,(7,2,2):C.UVGC_199_107,(7,2,3):C.UVGC_199_108,(7,2,4):C.UVGC_199_109,(7,2,5):C.UVGC_199_110,(5,2,2):C.UVGC_140_21,(5,2,3):C.UVGC_140_22,(1,2,2):C.UVGC_140_21,(1,2,3):C.UVGC_140_22})

V_42 = CTVertex(name = 'V_42',
                type = 'UV',
                particles = [ P.t__tilde__, P.b, P.G__plus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS3, L.FFS5 ],
                loop_particles = [ [ [P.b, P.g] ], [ [P.b, P.g, P.t] ], [ [P.g, P.t] ] ],
                couplings = {(0,0,0):C.UVGC_214_145,(0,0,2):C.UVGC_214_146,(0,0,1):C.UVGC_214_147,(0,1,0):C.UVGC_216_150,(0,1,2):C.UVGC_216_151,(0,1,1):C.UVGC_216_152})

V_43 = CTVertex(name = 'V_43',
                type = 'UV',
                particles = [ P.b__tilde__, P.b, P.G0 ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS1 ],
                loop_particles = [ [ [P.b, P.g] ] ],
                couplings = {(0,0,0):C.UVGC_195_93})

V_44 = CTVertex(name = 'V_44',
                type = 'UV',
                particles = [ P.b__tilde__, P.b, P.H ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2 ],
                loop_particles = [ [ [P.b, P.g] ] ],
                couplings = {(0,0,0):C.UVGC_194_92})

V_45 = CTVertex(name = 'V_45',
                type = 'UV',
                particles = [ P.b__tilde__, P.t, P.G__minus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS3, L.FFS5 ],
                loop_particles = [ [ [P.b, P.g] ], [ [P.b, P.g, P.t] ], [ [P.g, P.t] ] ],
                couplings = {(0,0,0):C.UVGC_217_153,(0,0,2):C.UVGC_217_154,(0,0,1):C.UVGC_217_155,(0,1,0):C.UVGC_213_142,(0,1,2):C.UVGC_213_143,(0,1,1):C.UVGC_213_144})

V_46 = CTVertex(name = 'V_46',
                type = 'UV',
                particles = [ P.t__tilde__, P.t, P.G0 ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS1 ],
                loop_particles = [ [ [P.g, P.t] ] ],
                couplings = {(0,0,0):C.UVGC_218_156})

V_47 = CTVertex(name = 'V_47',
                type = 'UV',
                particles = [ P.t__tilde__, P.t, P.H ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2 ],
                loop_particles = [ [ [P.g, P.t] ] ],
                couplings = {(0,0,0):C.UVGC_219_157})

V_48 = CTVertex(name = 'V_48',
                type = 'UV',
                particles = [ P.g, P.g, P.sig8 ],
                color = [ 'd(1,2,3)' ],
                lorentz = [ L.VVS2, L.VVS3, L.VVS5 ],
                loop_particles = [ [ [P.b] ], [ [P.c], [P.d], [P.s], [P.u] ], [ [P.g] ], [ [P.ghG] ], [ [P.g, P.sig8] ], [ [P.sig8] ], [ [P.t] ] ],
                couplings = {(0,0,0):C.UVGC_203_120,(0,0,1):C.UVGC_203_121,(0,0,3):C.UVGC_203_122,(0,0,5):C.UVGC_203_123,(0,0,6):C.UVGC_203_124,(0,2,2):C.UVGC_112_1,(0,1,4):C.UVGC_118_4})

V_49 = CTVertex(name = 'V_49',
                type = 'UV',
                particles = [ P.g, P.g, P.g, P.sig8 ],
                color = [ 'd(-1,1,2)*f(-1,3,4)', 'd(-1,1,3)*f(-1,2,4)', 'd(-1,1,4)*f(-1,2,3)', 'd(-1,2,3)*f(-1,1,4)', 'd(-1,2,4)*f(-1,1,3)', 'd(-1,3,4)*f(-1,1,2)' ],
                lorentz = [ L.VVVS1, L.VVVS10, L.VVVS2, L.VVVS3, L.VVVS4, L.VVVS5, L.VVVS6, L.VVVS8, L.VVVS9 ],
                loop_particles = [ [ [P.b] ], [ [P.c], [P.d], [P.s], [P.u] ], [ [P.g] ], [ [P.ghG] ], [ [P.g], [P.g, P.sig8] ], [ [P.g, P.sig8] ], [ [P.sig8] ], [ [P.t] ] ],
                couplings = {(0,0,2):C.UVGC_159_53,(0,0,5):C.UVGC_159_54,(1,0,2):C.UVGC_173_67,(1,0,5):C.UVGC_169_64,(2,0,0):C.UVGC_207_132,(2,0,1):C.UVGC_135_15,(2,0,2):C.UVGC_209_138,(2,0,3):C.UVGC_159_54,(2,0,6):C.UVGC_207_134,(2,0,7):C.UVGC_207_135,(2,0,5):C.UVGC_207_136,(3,0,2):C.UVGC_131_11,(4,0,2):C.UVGC_167_62,(4,0,5):C.UVGC_164_59,(5,0,2):C.UVGC_125_6,(5,0,5):C.UVGC_153_49,(1,2,2):C.UVGC_125_6,(0,2,2):C.UVGC_159_53,(0,2,5):C.UVGC_159_54,(3,2,4):C.UVGC_169_64,(4,2,0):C.UVGC_207_132,(4,2,1):C.UVGC_135_15,(4,2,2):C.UVGC_208_137,(4,2,3):C.UVGC_159_54,(4,2,6):C.UVGC_207_134,(4,2,7):C.UVGC_207_135,(4,2,5):C.UVGC_207_136,(2,2,2):C.UVGC_165_60,(2,2,5):C.UVGC_164_59,(5,2,2):C.UVGC_172_66,(5,2,5):C.UVGC_169_64,(0,3,2):C.UVGC_127_8,(1,3,2):C.UVGC_126_7,(3,3,2):C.UVGC_126_7,(2,3,2):C.UVGC_135_15,(4,3,2):C.UVGC_135_15,(0,4,2):C.UVGC_168_63,(0,4,5):C.UVGC_164_59,(1,4,2):C.UVGC_155_51,(1,4,5):C.UVGC_153_49,(2,4,0):C.UVGC_204_125,(2,4,1):C.UVGC_129_9,(2,4,2):C.UVGC_204_126,(2,4,3):C.UVGC_164_59,(2,4,6):C.UVGC_204_127,(2,4,7):C.UVGC_204_128,(2,4,5):C.UVGC_204_129,(3,4,2):C.UVGC_131_11,(4,4,2):C.UVGC_160_55,(4,4,5):C.UVGC_159_54,(5,4,2):C.UVGC_172_66,(5,4,5):C.UVGC_169_64,(0,5,2):C.UVGC_132_12,(3,5,4):C.UVGC_169_64,(1,5,2):C.UVGC_134_14,(1,5,5):C.UVGC_159_54,(5,5,0):C.UVGC_207_132,(5,5,1):C.UVGC_135_15,(5,5,2):C.UVGC_207_133,(5,5,3):C.UVGC_159_54,(5,5,6):C.UVGC_207_134,(5,5,7):C.UVGC_207_135,(5,5,5):C.UVGC_207_136,(2,5,2):C.UVGC_162_56,(2,5,5):C.UVGC_159_54,(4,5,2):C.UVGC_170_65,(4,5,5):C.UVGC_169_64,(0,6,2):C.UVGC_133_13,(3,6,2):C.UVGC_126_7,(2,6,2):C.UVGC_129_9,(4,6,2):C.UVGC_127_8,(5,6,2):C.UVGC_130_10,(0,7,2):C.UVGC_168_63,(0,7,5):C.UVGC_164_59,(1,7,2):C.UVGC_136_16,(3,7,2):C.UVGC_157_52,(3,7,5):C.UVGC_153_49,(4,7,0):C.UVGC_204_125,(4,7,1):C.UVGC_129_9,(4,7,2):C.UVGC_205_130,(4,7,3):C.UVGC_164_59,(4,7,6):C.UVGC_204_127,(4,7,7):C.UVGC_204_128,(4,7,5):C.UVGC_204_129,(2,7,2):C.UVGC_163_57,(2,7,5):C.UVGC_159_54,(5,7,2):C.UVGC_125_6,(5,7,5):C.UVGC_153_49,(0,8,2):C.UVGC_132_12,(1,8,2):C.UVGC_166_61,(1,8,5):C.UVGC_164_59,(3,8,2):C.UVGC_157_52,(3,8,5):C.UVGC_153_49,(5,8,0):C.UVGC_204_125,(5,8,1):C.UVGC_129_9,(5,8,2):C.UVGC_206_131,(5,8,3):C.UVGC_164_59,(5,8,6):C.UVGC_204_127,(5,8,7):C.UVGC_204_128,(5,8,5):C.UVGC_204_129,(2,8,2):C.UVGC_164_58,(2,8,5):C.UVGC_164_59,(4,8,2):C.UVGC_131_11,(4,8,5):C.UVGC_153_49,(0,1,2):C.UVGC_133_13,(1,1,2):C.UVGC_133_13,(3,1,2):C.UVGC_137_17,(4,1,2):C.UVGC_134_14,(5,1,2):C.UVGC_134_14})

V_50 = CTVertex(name = 'V_50',
                type = 'UV',
                particles = [ P.g, P.sig8, P.sig8 ],
                color = [ 'f(1,2,3)' ],
                lorentz = [ L.VSS1 ],
                loop_particles = [ [ [P.c], [P.d], [P.s], [P.u] ], [ [P.g] ], [ [P.ghG] ], [ [P.g, P.sig8] ] ],
                couplings = {(0,0,0):C.UVGC_146_32,(0,0,1):C.UVGC_146_33,(0,0,2):C.UVGC_146_34,(0,0,3):C.UVGC_146_35})

V_51 = CTVertex(name = 'V_51',
                type = 'UV',
                particles = [ P.g, P.g, P.sig8, P.sig8 ],
                color = [ 'd(-1,1,3)*d(-1,2,4)', 'd(-1,1,3)*f(-1,2,4)', 'd(-1,1,4)*d(-1,2,3)', 'd(-1,1,4)*f(-1,2,3)', 'd(-1,2,3)*f(-1,1,4)', 'd(-1,2,4)*f(-1,1,3)', 'f(-1,1,3)*f(-1,2,4)', 'f(-1,1,4)*f(-1,2,3)', 'Identity(1,2)*Identity(3,4)', 'Identity(1,3)*Identity(2,4)', 'Identity(1,4)*Identity(2,3)' ],
                lorentz = [ L.VVSS1 ],
                loop_particles = [ [ [P.c], [P.d], [P.s], [P.u] ], [ [P.g] ], [ [P.ghG] ], [ [P.g, P.sig8] ] ],
                couplings = {(0,0,1):C.UVGC_148_38,(0,0,3):C.UVGC_148_39,(2,0,1):C.UVGC_148_38,(2,0,3):C.UVGC_148_39,(6,0,0):C.UVGC_151_44,(6,0,1):C.UVGC_151_45,(6,0,2):C.UVGC_151_46,(6,0,3):C.UVGC_151_47,(7,0,0):C.UVGC_151_44,(7,0,1):C.UVGC_151_45,(7,0,2):C.UVGC_151_46,(7,0,3):C.UVGC_151_47,(5,0,1):C.UVGC_147_36,(5,0,3):C.UVGC_147_37,(1,0,1):C.UVGC_147_36,(1,0,3):C.UVGC_147_37,(4,0,1):C.UVGC_147_36,(4,0,3):C.UVGC_147_37,(3,0,1):C.UVGC_147_36,(3,0,3):C.UVGC_147_37,(10,0,1):C.UVGC_150_42,(10,0,3):C.UVGC_150_43,(9,0,1):C.UVGC_150_42,(9,0,3):C.UVGC_150_43,(8,0,1):C.UVGC_149_40,(8,0,3):C.UVGC_149_41})

V_52 = CTVertex(name = 'V_52',
                type = 'UV',
                particles = [ P.d__tilde__, P.d, P.sig8 ],
                color = [ 'T(3,2,1)' ],
                lorentz = [ L.FFS2 ],
                loop_particles = [ [ [P.d, P.g] ], [ [P.g, P.sig8], [P.d, P.g, P.sig8] ] ],
                couplings = {(0,0,0):C.UVGC_179_74,(0,0,1):C.UVGC_179_75})

V_53 = CTVertex(name = 'V_53',
                type = 'UV',
                particles = [ P.s__tilde__, P.s, P.sig8 ],
                color = [ 'T(3,2,1)' ],
                lorentz = [ L.FFS2 ],
                loop_particles = [ [ [P.g, P.s] ], [ [P.g, P.sig8], [P.g, P.s, P.sig8] ] ],
                couplings = {(0,0,0):C.UVGC_182_78,(0,0,1):C.UVGC_182_79})

V_54 = CTVertex(name = 'V_54',
                type = 'UV',
                particles = [ P.b__tilde__, P.b, P.sig8 ],
                color = [ 'T(3,2,1)' ],
                lorentz = [ L.FFS2 ],
                loop_particles = [ [ [P.b, P.g] ], [ [P.g, P.sig8], [P.b, P.g, P.sig8] ] ],
                couplings = {(0,0,0):C.UVGC_196_94,(0,0,1):C.UVGC_196_95})

V_55 = CTVertex(name = 'V_55',
                type = 'UV',
                particles = [ P.u__tilde__, P.u, P.sig8 ],
                color = [ 'T(3,2,1)' ],
                lorentz = [ L.FFS2 ],
                loop_particles = [ [ [P.g, P.sig8], [P.g, P.sig8, P.u] ], [ [P.g, P.u] ] ],
                couplings = {(0,0,0):C.UVGC_187_84,(0,0,1):C.UVGC_187_85})

V_56 = CTVertex(name = 'V_56',
                type = 'UV',
                particles = [ P.c__tilde__, P.c, P.sig8 ],
                color = [ 'T(3,2,1)' ],
                lorentz = [ L.FFS2 ],
                loop_particles = [ [ [P.c, P.g] ], [ [P.g, P.sig8], [P.c, P.g, P.sig8] ] ],
                couplings = {(0,0,0):C.UVGC_176_70,(0,0,1):C.UVGC_176_71})

V_57 = CTVertex(name = 'V_57',
                type = 'UV',
                particles = [ P.t__tilde__, P.t, P.sig8 ],
                color = [ 'T(3,2,1)' ],
                lorentz = [ L.FFS2 ],
                loop_particles = [ [ [P.g, P.sig8], [P.g, P.sig8, P.t] ], [ [P.g, P.t] ] ],
                couplings = {(0,0,0):C.UVGC_215_148,(0,0,1):C.UVGC_215_149})

V_58 = CTVertex(name = 'V_58',
                type = 'UV',
                particles = [ P.u__tilde__, P.u, P.g ],
                color = [ 'T(3,2,1)' ],
                lorentz = [ L.FFV4, L.FFV6 ],
                loop_particles = [ [ [P.c], [P.d], [P.s], [P.u] ], [ [P.g] ], [ [P.ghG] ], [ [P.g, P.u] ] ],
                couplings = {(0,1,0):C.UVGC_145_29,(0,1,1):C.UVGC_145_30,(0,1,2):C.UVGC_145_31,(0,0,3):C.UVGC_113_2})

V_59 = CTVertex(name = 'V_59',
                type = 'UV',
                particles = [ P.c__tilde__, P.c, P.g ],
                color = [ 'T(3,2,1)' ],
                lorentz = [ L.FFV4, L.FFV6 ],
                loop_particles = [ [ [P.c], [P.d], [P.s], [P.u] ], [ [P.c, P.g] ], [ [P.g] ], [ [P.ghG] ] ],
                couplings = {(0,1,0):C.UVGC_145_29,(0,1,2):C.UVGC_145_30,(0,1,3):C.UVGC_145_31,(0,0,1):C.UVGC_113_2})

V_60 = CTVertex(name = 'V_60',
                type = 'UV',
                particles = [ P.t__tilde__, P.t, P.g ],
                color = [ 'T(3,2,1)' ],
                lorentz = [ L.FFV5, L.FFV6 ],
                loop_particles = [ [ [P.c], [P.d], [P.s], [P.u] ], [ [P.g] ], [ [P.ghG] ], [ [P.g, P.t] ] ],
                couplings = {(0,1,0):C.UVGC_145_29,(0,1,1):C.UVGC_145_30,(0,1,2):C.UVGC_145_31,(0,1,3):C.UVGC_211_140,(0,0,3):C.UVGC_113_2})

V_61 = CTVertex(name = 'V_61',
                type = 'UV',
                particles = [ P.d__tilde__, P.d, P.g ],
                color = [ 'T(3,2,1)' ],
                lorentz = [ L.FFV4, L.FFV6 ],
                loop_particles = [ [ [P.c], [P.d], [P.s], [P.u] ], [ [P.d, P.g] ], [ [P.g] ], [ [P.ghG] ] ],
                couplings = {(0,1,0):C.UVGC_145_29,(0,1,2):C.UVGC_145_30,(0,1,3):C.UVGC_145_31,(0,0,1):C.UVGC_113_2})

V_62 = CTVertex(name = 'V_62',
                type = 'UV',
                particles = [ P.s__tilde__, P.s, P.g ],
                color = [ 'T(3,2,1)' ],
                lorentz = [ L.FFV4, L.FFV6 ],
                loop_particles = [ [ [P.c], [P.d], [P.s], [P.u] ], [ [P.g] ], [ [P.ghG] ], [ [P.g, P.s] ] ],
                couplings = {(0,1,0):C.UVGC_145_29,(0,1,1):C.UVGC_145_30,(0,1,2):C.UVGC_145_31,(0,0,3):C.UVGC_113_2})

V_63 = CTVertex(name = 'V_63',
                type = 'UV',
                particles = [ P.b__tilde__, P.b, P.g ],
                color = [ 'T(3,2,1)' ],
                lorentz = [ L.FFV5, L.FFV6 ],
                loop_particles = [ [ [P.b, P.g] ], [ [P.c], [P.d], [P.s], [P.u] ], [ [P.g] ], [ [P.ghG] ] ],
                couplings = {(0,1,1):C.UVGC_145_29,(0,1,2):C.UVGC_145_30,(0,1,3):C.UVGC_145_31,(0,1,0):C.UVGC_192_90,(0,0,0):C.UVGC_113_2})

V_64 = CTVertex(name = 'V_64',
                type = 'UV',
                particles = [ P.u__tilde__, P.u ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FF4 ],
                loop_particles = [ [ [P.g, P.u] ] ],
                couplings = {(0,0,0):C.UVGC_114_3})

V_65 = CTVertex(name = 'V_65',
                type = 'UV',
                particles = [ P.c__tilde__, P.c ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FF4 ],
                loop_particles = [ [ [P.c, P.g] ] ],
                couplings = {(0,0,0):C.UVGC_114_3})

V_66 = CTVertex(name = 'V_66',
                type = 'UV',
                particles = [ P.t__tilde__, P.t ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FF2, L.FF3 ],
                loop_particles = [ [ [P.g, P.t] ] ],
                couplings = {(0,0,0):C.UVGC_212_141,(0,1,0):C.UVGC_210_139})

V_67 = CTVertex(name = 'V_67',
                type = 'UV',
                particles = [ P.d__tilde__, P.d ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FF4 ],
                loop_particles = [ [ [P.d, P.g] ] ],
                couplings = {(0,0,0):C.UVGC_114_3})

V_68 = CTVertex(name = 'V_68',
                type = 'UV',
                particles = [ P.s__tilde__, P.s ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FF4 ],
                loop_particles = [ [ [P.g, P.s] ] ],
                couplings = {(0,0,0):C.UVGC_114_3})

V_69 = CTVertex(name = 'V_69',
                type = 'UV',
                particles = [ P.b__tilde__, P.b ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FF2, L.FF3 ],
                loop_particles = [ [ [P.b, P.g] ] ],
                couplings = {(0,0,0):C.UVGC_193_91,(0,1,0):C.UVGC_191_89})

V_70 = CTVertex(name = 'V_70',
                type = 'UV',
                particles = [ P.sig8, P.sig8 ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.SS1 ],
                loop_particles = [ [ [P.g, P.sig8] ] ],
                couplings = {(0,0,0):C.UVGC_190_88})

V_71 = CTVertex(name = 'V_71',
                type = 'UV',
                particles = [ P.g, P.g ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.VV1, L.VV5 ],
                loop_particles = [ [ [P.b] ], [ [P.g] ], [ [P.ghG] ], [ [P.sig8] ], [ [P.t] ] ],
                couplings = {(0,1,0):C.UVGC_197_96,(0,1,3):C.UVGC_197_97,(0,1,4):C.UVGC_197_98,(0,0,1):C.UVGC_139_19,(0,0,2):C.UVGC_139_20})

V_72 = CTVertex(name = 'V_72',
                type = 'UV',
                particles = [ P.sig8, P.sig8, P.sig8 ],
                color = [ 'd(1,2,3)' ],
                lorentz = [ L.SSS2, L.SSS3 ],
                loop_particles = [ [ [P.g] ], [ [P.g, P.sig8] ] ],
                couplings = {(0,0,1):C.UVGC_152_48,(0,1,0):C.UVGC_124_5})

V_73 = CTVertex(name = 'V_73',
                type = 'UV',
                particles = [ P.g, P.sig8, P.sig8, P.sig8 ],
                color = [ 'd(-1,1,2)*f(-1,3,4)', 'd(-1,1,3)*f(-1,2,4)', 'd(-1,1,4)*f(-1,2,3)', 'd(-1,2,3)*f(-1,1,4)', 'd(-1,2,4)*f(-1,1,3)', 'd(-1,3,4)*f(-1,1,2)' ],
                lorentz = [ L.VSSS1, L.VSSS2, L.VSSS3 ],
                loop_particles = [ [ [P.g] ], [ [P.g, P.sig8] ] ],
                couplings = {(3,0,0):C.UVGC_153_49,(3,0,1):C.UVGC_153_50,(4,0,0):C.UVGC_153_49,(4,0,1):C.UVGC_153_50,(1,0,1):C.UVGC_169_64,(5,0,0):C.UVGC_138_18,(2,0,1):C.UVGC_169_64,(0,1,1):C.UVGC_169_64,(3,1,0):C.UVGC_153_49,(3,1,1):C.UVGC_153_50,(4,1,0):C.UVGC_138_18,(5,1,0):C.UVGC_153_49,(5,1,1):C.UVGC_153_50,(2,1,1):C.UVGC_153_49,(0,2,1):C.UVGC_153_49,(3,2,0):C.UVGC_138_18,(4,2,0):C.UVGC_153_49,(4,2,1):C.UVGC_153_50,(1,2,1):C.UVGC_153_49,(5,2,0):C.UVGC_153_49,(5,2,1):C.UVGC_153_50})

V_74 = CTVertex(name = 'V_74',
                type = 'UV',
                particles = [ P.u__tilde__, P.u, P.g, P.g ],
                color = [ 'f(-1,3,4)*T(-1,2,1)' ],
                lorentz = [ L.FFVV1, L.FFVV2, L.FFVV3 ],
                loop_particles = [ [ [P.g, P.sig8, P.u] ] ],
                couplings = {(0,0,0):C.UVGC_189_87,(0,1,0):C.UVGC_188_86,(0,2,0):C.UVGC_188_86})

V_75 = CTVertex(name = 'V_75',
                type = 'UV',
                particles = [ P.c__tilde__, P.c, P.g, P.g ],
                color = [ 'f(-1,3,4)*T(-1,2,1)' ],
                lorentz = [ L.FFVV1, L.FFVV2, L.FFVV3 ],
                loop_particles = [ [ [P.c, P.g, P.sig8] ] ],
                couplings = {(0,0,0):C.UVGC_178_73,(0,1,0):C.UVGC_177_72,(0,2,0):C.UVGC_177_72})

V_76 = CTVertex(name = 'V_76',
                type = 'UV',
                particles = [ P.t__tilde__, P.t, P.g, P.g ],
                color = [ 'f(-1,3,4)*T(-1,2,1)' ],
                lorentz = [ L.FFVV1, L.FFVV2, L.FFVV3 ],
                loop_particles = [ [ [P.g, P.sig8, P.t] ] ],
                couplings = {(0,0,0):C.UVGC_186_83,(0,1,0):C.UVGC_185_82,(0,2,0):C.UVGC_185_82})

V_77 = CTVertex(name = 'V_77',
                type = 'UV',
                particles = [ P.d__tilde__, P.d, P.g, P.g ],
                color = [ 'f(-1,3,4)*T(-1,2,1)' ],
                lorentz = [ L.FFVV1, L.FFVV2, L.FFVV3 ],
                loop_particles = [ [ [P.d, P.g, P.sig8] ] ],
                couplings = {(0,0,0):C.UVGC_181_77,(0,1,0):C.UVGC_180_76,(0,2,0):C.UVGC_180_76})

V_78 = CTVertex(name = 'V_78',
                type = 'UV',
                particles = [ P.s__tilde__, P.s, P.g, P.g ],
                color = [ 'f(-1,3,4)*T(-1,2,1)' ],
                lorentz = [ L.FFVV1, L.FFVV2, L.FFVV3 ],
                loop_particles = [ [ [P.g, P.s, P.sig8] ] ],
                couplings = {(0,0,0):C.UVGC_184_81,(0,1,0):C.UVGC_183_80,(0,2,0):C.UVGC_183_80})

V_79 = CTVertex(name = 'V_79',
                type = 'UV',
                particles = [ P.b__tilde__, P.b, P.g, P.g ],
                color = [ 'f(-1,3,4)*T(-1,2,1)' ],
                lorentz = [ L.FFVV1, L.FFVV2, L.FFVV3 ],
                loop_particles = [ [ [P.b, P.g, P.sig8] ] ],
                couplings = {(0,0,0):C.UVGC_175_69,(0,1,0):C.UVGC_174_68,(0,2,0):C.UVGC_174_68})

