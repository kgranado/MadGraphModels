Requestor: Larry Lee Jr.
Content: Modification of HC_NLO_X0 to adjust coupling and allow gamma gamma modes
JIRA: https://its.cern.ch/jira/browse/AGENE-1148