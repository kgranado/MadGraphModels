Requestor: Wen Yi Song
Content: High-electric-charge spin-0 particle production via DY/photon fusion
Presentation: https://indico.cern.ch/event/841469/contributions/3571143/attachments/1916104/3169563/HIP_UEH190926.pdf
Source: Wendy Taylor and Wen Yi Song
JIRA: https://its.cern.ch/jira/browse/AGENE-1897