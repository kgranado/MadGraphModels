# This file was automatically created by FeynRules 2.4.61
# Mathematica version: 10.4.0 for Linux x86 (64-bit) (February 26, 2016)
# Date: Tue 17 Jul 2018 21:07:09


from object_library import all_couplings, Coupling

from function_library import complexconjugate, re, im, csc, sec, acsc, asec, cot



GC_1 = Coupling(name = 'GC_1',
                value = '-24*dL4*complex(0,1)',
                order = {'QNP':1})

GC_2 = Coupling(name = 'GC_2',
                value = '-(ee*complex(0,1))/3.',
                order = {'QED':1})

GC_3 = Coupling(name = 'GC_3',
                value = '(2*ee*complex(0,1))/3.',
                order = {'QED':1})

GC_4 = Coupling(name = 'GC_4',
                value = '-(ee*complex(0,1))',
                order = {'QED':1})

GC_5 = Coupling(name = 'GC_5',
                value = 'ee*complex(0,1)',
                order = {'QED':1})

GC_6 = Coupling(name = 'GC_6',
                value = 'dKa*ee*complex(0,1)',
                order = {'QED':1,'QNP':1})

GC_7 = Coupling(name = 'GC_7',
                value = 'ee**2*complex(0,1)',
                order = {'QED':2})

GC_8 = Coupling(name = 'GC_8',
                value = '-G',
                order = {'QCD':1})

GC_9 = Coupling(name = 'GC_9',
                value = 'complex(0,1)*G',
                order = {'QCD':1})

GC_10 = Coupling(name = 'GC_10',
                 value = 'complex(0,1)*G**2',
                 order = {'QCD':2})

GC_11 = Coupling(name = 'GC_11',
                 value = '-6*complex(0,1)*lam',
                 order = {'QED':2})

GC_12 = Coupling(name = 'GC_12',
                 value = '-((ee*complex(0,1)*La)/MW**2)',
                 order = {'QED':1,'QNP':1})

GC_13 = Coupling(name = 'GC_13',
                 value = '-((ee**2*complex(0,1)*Lw2a2)/MW**2)',
                 order = {'QED':2,'QNP':1})

GC_14 = Coupling(name = 'GC_14',
                 value = '(2*dCz2*ee**2*complex(0,1))/cw**2 + (2*dCz2*ee**2*complex(0,1))/sw**2',
                 order = {'QED':2,'QNP':1})

GC_15 = Coupling(name = 'GC_15',
                 value = '(ee**2*complex(0,1))/(2.*sw**2)',
                 order = {'QED':2})

GC_16 = Coupling(name = 'GC_16',
                 value = '-((ee**2*complex(0,1))/sw**2)',
                 order = {'QED':2})

GC_17 = Coupling(name = 'GC_17',
                 value = '(cw**2*ee**2*complex(0,1))/sw**2',
                 order = {'QED':2})

GC_18 = Coupling(name = 'GC_18',
                 value = '(2*dCw2*ee**2*complex(0,1))/sw**2',
                 order = {'QED':2,'QNP':1})

GC_19 = Coupling(name = 'GC_19',
                 value = '(cw**2*dGw2z2*ee**2*complex(0,1))/sw**2',
                 order = {'QED':2,'QNP':1})

GC_20 = Coupling(name = 'GC_20',
                 value = '-((dGw4*ee**2*complex(0,1))/sw**2)',
                 order = {'QED':2,'QNP':1})

GC_21 = Coupling(name = 'GC_21',
                 value = '-((cw**2*ee**2*complex(0,1)*Lw2z2)/(MW**2*sw**2))',
                 order = {'QED':2,'QNP':1})

GC_22 = Coupling(name = 'GC_22',
                 value = '(ee**2*complex(0,1)*Lw4)/(MW**2*sw**2)',
                 order = {'QED':2,'QNP':1})

GC_23 = Coupling(name = 'GC_23',
                 value = '(ee*complex(0,1))/(sw*cmath.sqrt(2))',
                 order = {'QED':1})

GC_24 = Coupling(name = 'GC_24',
                 value = '(CKM1x1*ee*complex(0,1))/(sw*cmath.sqrt(2))',
                 order = {'QED':1})

GC_25 = Coupling(name = 'GC_25',
                 value = '(CKM1x2*ee*complex(0,1))/(sw*cmath.sqrt(2))',
                 order = {'QED':1})

GC_26 = Coupling(name = 'GC_26',
                 value = '(CKM1x3*ee*complex(0,1))/(sw*cmath.sqrt(2))',
                 order = {'QED':1})

GC_27 = Coupling(name = 'GC_27',
                 value = '(CKM2x1*ee*complex(0,1))/(sw*cmath.sqrt(2))',
                 order = {'QED':1})

GC_28 = Coupling(name = 'GC_28',
                 value = '(CKM2x2*ee*complex(0,1))/(sw*cmath.sqrt(2))',
                 order = {'QED':1})

GC_29 = Coupling(name = 'GC_29',
                 value = '(CKM2x3*ee*complex(0,1))/(sw*cmath.sqrt(2))',
                 order = {'QED':1})

GC_30 = Coupling(name = 'GC_30',
                 value = '(CKM3x1*ee*complex(0,1))/(sw*cmath.sqrt(2))',
                 order = {'QED':1})

GC_31 = Coupling(name = 'GC_31',
                 value = '(CKM3x2*ee*complex(0,1))/(sw*cmath.sqrt(2))',
                 order = {'QED':1})

GC_32 = Coupling(name = 'GC_32',
                 value = '(CKM3x3*ee*complex(0,1))/(sw*cmath.sqrt(2))',
                 order = {'QED':1})

GC_33 = Coupling(name = 'GC_33',
                 value = '-(cw*ee*complex(0,1))/(2.*sw)',
                 order = {'QED':1})

GC_34 = Coupling(name = 'GC_34',
                 value = '(cw*ee*complex(0,1))/(2.*sw)',
                 order = {'QED':1})

GC_35 = Coupling(name = 'GC_35',
                 value = '(cw*ee*complex(0,1))/sw',
                 order = {'QED':1})

GC_36 = Coupling(name = 'GC_36',
                 value = '(cw*dG1z*ee*complex(0,1))/sw',
                 order = {'QED':1,'QNP':1})

GC_37 = Coupling(name = 'GC_37',
                 value = '(dGLwl1x1*ee*complex(0,1))/(sw*cmath.sqrt(2))',
                 order = {'QED':1,'QNP':1})

GC_38 = Coupling(name = 'GC_38',
                 value = '(dGLwl1x2*ee*complex(0,1))/(sw*cmath.sqrt(2))',
                 order = {'QED':1,'QNP':1})

GC_39 = Coupling(name = 'GC_39',
                 value = '(dGLwl1x3*ee*complex(0,1))/(sw*cmath.sqrt(2))',
                 order = {'QED':1,'QNP':1})

GC_40 = Coupling(name = 'GC_40',
                 value = '(dGLwl2x1*ee*complex(0,1))/(sw*cmath.sqrt(2))',
                 order = {'QED':1,'QNP':1})

GC_41 = Coupling(name = 'GC_41',
                 value = '(dGLwl2x2*ee*complex(0,1))/(sw*cmath.sqrt(2))',
                 order = {'QED':1,'QNP':1})

GC_42 = Coupling(name = 'GC_42',
                 value = '(dGLwl2x3*ee*complex(0,1))/(sw*cmath.sqrt(2))',
                 order = {'QED':1,'QNP':1})

GC_43 = Coupling(name = 'GC_43',
                 value = '(dGLwl3x1*ee*complex(0,1))/(sw*cmath.sqrt(2))',
                 order = {'QED':1,'QNP':1})

GC_44 = Coupling(name = 'GC_44',
                 value = '(dGLwl3x2*ee*complex(0,1))/(sw*cmath.sqrt(2))',
                 order = {'QED':1,'QNP':1})

GC_45 = Coupling(name = 'GC_45',
                 value = '(dGLwl3x3*ee*complex(0,1))/(sw*cmath.sqrt(2))',
                 order = {'QED':1,'QNP':1})

GC_46 = Coupling(name = 'GC_46',
                 value = '(dGLwq1x1*ee*complex(0,1))/(sw*cmath.sqrt(2))',
                 order = {'QED':1,'QNP':1})

GC_47 = Coupling(name = 'GC_47',
                 value = '(dGLwq1x2*ee*complex(0,1))/(sw*cmath.sqrt(2))',
                 order = {'QED':1,'QNP':1})

GC_48 = Coupling(name = 'GC_48',
                 value = '(dGLwq1x3*ee*complex(0,1))/(sw*cmath.sqrt(2))',
                 order = {'QED':1,'QNP':1})

GC_49 = Coupling(name = 'GC_49',
                 value = '(dGLwq2x1*ee*complex(0,1))/(sw*cmath.sqrt(2))',
                 order = {'QED':1,'QNP':1})

GC_50 = Coupling(name = 'GC_50',
                 value = '(dGLwq2x2*ee*complex(0,1))/(sw*cmath.sqrt(2))',
                 order = {'QED':1,'QNP':1})

GC_51 = Coupling(name = 'GC_51',
                 value = '(dGLwq2x3*ee*complex(0,1))/(sw*cmath.sqrt(2))',
                 order = {'QED':1,'QNP':1})

GC_52 = Coupling(name = 'GC_52',
                 value = '(dGLwq3x1*ee*complex(0,1))/(sw*cmath.sqrt(2))',
                 order = {'QED':1,'QNP':1})

GC_53 = Coupling(name = 'GC_53',
                 value = '(dGLwq3x2*ee*complex(0,1))/(sw*cmath.sqrt(2))',
                 order = {'QED':1,'QNP':1})

GC_54 = Coupling(name = 'GC_54',
                 value = '(dGLwq3x3*ee*complex(0,1))/(sw*cmath.sqrt(2))',
                 order = {'QED':1,'QNP':1})

GC_55 = Coupling(name = 'GC_55',
                 value = '(dGRwq1x1*ee*complex(0,1))/(sw*cmath.sqrt(2))',
                 order = {'QED':1,'QNP':1})

GC_56 = Coupling(name = 'GC_56',
                 value = '(dGRwq1x2*ee*complex(0,1))/(sw*cmath.sqrt(2))',
                 order = {'QED':1,'QNP':1})

GC_57 = Coupling(name = 'GC_57',
                 value = '(dGRwq1x3*ee*complex(0,1))/(sw*cmath.sqrt(2))',
                 order = {'QED':1,'QNP':1})

GC_58 = Coupling(name = 'GC_58',
                 value = '(dGRwq2x1*ee*complex(0,1))/(sw*cmath.sqrt(2))',
                 order = {'QED':1,'QNP':1})

GC_59 = Coupling(name = 'GC_59',
                 value = '(dGRwq2x2*ee*complex(0,1))/(sw*cmath.sqrt(2))',
                 order = {'QED':1,'QNP':1})

GC_60 = Coupling(name = 'GC_60',
                 value = '(dGRwq2x3*ee*complex(0,1))/(sw*cmath.sqrt(2))',
                 order = {'QED':1,'QNP':1})

GC_61 = Coupling(name = 'GC_61',
                 value = '(dGRwq3x1*ee*complex(0,1))/(sw*cmath.sqrt(2))',
                 order = {'QED':1,'QNP':1})

GC_62 = Coupling(name = 'GC_62',
                 value = '(dGRwq3x2*ee*complex(0,1))/(sw*cmath.sqrt(2))',
                 order = {'QED':1,'QNP':1})

GC_63 = Coupling(name = 'GC_63',
                 value = '(dGRwq3x3*ee*complex(0,1))/(sw*cmath.sqrt(2))',
                 order = {'QED':1,'QNP':1})

GC_64 = Coupling(name = 'GC_64',
                 value = '(cw*dKz*ee*complex(0,1))/sw',
                 order = {'QED':1,'QNP':1})

GC_65 = Coupling(name = 'GC_65',
                 value = '(-2*cw*ee**2*complex(0,1))/sw',
                 order = {'QED':2})

GC_66 = Coupling(name = 'GC_66',
                 value = '(-2*cw*dGw2za*ee**2*complex(0,1))/sw',
                 order = {'QED':2,'QNP':1})

GC_67 = Coupling(name = 'GC_67',
                 value = '(cw*ee**2*complex(0,1)*Lw2az)/(MW**2*sw)',
                 order = {'QED':2,'QNP':1})

GC_68 = Coupling(name = 'GC_68',
                 value = '(cw*ee**2*complex(0,1)*Lw2za)/(MW**2*sw)',
                 order = {'QED':2,'QNP':1})

GC_69 = Coupling(name = 'GC_69',
                 value = '-((cw*ee*complex(0,1)*Lz)/(MW**2*sw))',
                 order = {'QED':1,'QNP':1})

GC_70 = Coupling(name = 'GC_70',
                 value = '-(ee*complex(0,1)*sw)/(6.*cw)',
                 order = {'QED':1})

GC_71 = Coupling(name = 'GC_71',
                 value = '(ee*complex(0,1)*sw)/(2.*cw)',
                 order = {'QED':1})

GC_72 = Coupling(name = 'GC_72',
                 value = '(cw*ee*complex(0,1))/(2.*sw) + (ee*complex(0,1)*sw)/(2.*cw)',
                 order = {'QED':1})

GC_73 = Coupling(name = 'GC_73',
                 value = '(dGLzd1x1*ee*complex(0,1)*cmath.sqrt(cw**2 + sw**2))/(cw*sw)',
                 order = {'QED':1,'QNP':1})

GC_74 = Coupling(name = 'GC_74',
                 value = '(dGLzd1x2*ee*complex(0,1)*cmath.sqrt(cw**2 + sw**2))/(cw*sw)',
                 order = {'QED':1,'QNP':1})

GC_75 = Coupling(name = 'GC_75',
                 value = '(dGLzd1x3*ee*complex(0,1)*cmath.sqrt(cw**2 + sw**2))/(cw*sw)',
                 order = {'QED':1,'QNP':1})

GC_76 = Coupling(name = 'GC_76',
                 value = '(dGLzd2x1*ee*complex(0,1)*cmath.sqrt(cw**2 + sw**2))/(cw*sw)',
                 order = {'QED':1,'QNP':1})

GC_77 = Coupling(name = 'GC_77',
                 value = '(dGLzd2x2*ee*complex(0,1)*cmath.sqrt(cw**2 + sw**2))/(cw*sw)',
                 order = {'QED':1,'QNP':1})

GC_78 = Coupling(name = 'GC_78',
                 value = '(dGLzd2x3*ee*complex(0,1)*cmath.sqrt(cw**2 + sw**2))/(cw*sw)',
                 order = {'QED':1,'QNP':1})

GC_79 = Coupling(name = 'GC_79',
                 value = '(dGLzd3x1*ee*complex(0,1)*cmath.sqrt(cw**2 + sw**2))/(cw*sw)',
                 order = {'QED':1,'QNP':1})

GC_80 = Coupling(name = 'GC_80',
                 value = '(dGLzd3x2*ee*complex(0,1)*cmath.sqrt(cw**2 + sw**2))/(cw*sw)',
                 order = {'QED':1,'QNP':1})

GC_81 = Coupling(name = 'GC_81',
                 value = '(dGLzd3x3*ee*complex(0,1)*cmath.sqrt(cw**2 + sw**2))/(cw*sw)',
                 order = {'QED':1,'QNP':1})

GC_82 = Coupling(name = 'GC_82',
                 value = '(dGLze1x1*ee*complex(0,1)*cmath.sqrt(cw**2 + sw**2))/(cw*sw)',
                 order = {'QED':1,'QNP':1})

GC_83 = Coupling(name = 'GC_83',
                 value = '(dGLze1x2*ee*complex(0,1)*cmath.sqrt(cw**2 + sw**2))/(cw*sw)',
                 order = {'QED':1,'QNP':1})

GC_84 = Coupling(name = 'GC_84',
                 value = '(dGLze1x3*ee*complex(0,1)*cmath.sqrt(cw**2 + sw**2))/(cw*sw)',
                 order = {'QED':1,'QNP':1})

GC_85 = Coupling(name = 'GC_85',
                 value = '(dGLze2x1*ee*complex(0,1)*cmath.sqrt(cw**2 + sw**2))/(cw*sw)',
                 order = {'QED':1,'QNP':1})

GC_86 = Coupling(name = 'GC_86',
                 value = '(dGLze2x2*ee*complex(0,1)*cmath.sqrt(cw**2 + sw**2))/(cw*sw)',
                 order = {'QED':1,'QNP':1})

GC_87 = Coupling(name = 'GC_87',
                 value = '(dGLze2x3*ee*complex(0,1)*cmath.sqrt(cw**2 + sw**2))/(cw*sw)',
                 order = {'QED':1,'QNP':1})

GC_88 = Coupling(name = 'GC_88',
                 value = '(dGLze3x1*ee*complex(0,1)*cmath.sqrt(cw**2 + sw**2))/(cw*sw)',
                 order = {'QED':1,'QNP':1})

GC_89 = Coupling(name = 'GC_89',
                 value = '(dGLze3x2*ee*complex(0,1)*cmath.sqrt(cw**2 + sw**2))/(cw*sw)',
                 order = {'QED':1,'QNP':1})

GC_90 = Coupling(name = 'GC_90',
                 value = '(dGLze3x3*ee*complex(0,1)*cmath.sqrt(cw**2 + sw**2))/(cw*sw)',
                 order = {'QED':1,'QNP':1})

GC_91 = Coupling(name = 'GC_91',
                 value = '(dGLzu1x1*ee*complex(0,1)*cmath.sqrt(cw**2 + sw**2))/(cw*sw)',
                 order = {'QED':1,'QNP':1})

GC_92 = Coupling(name = 'GC_92',
                 value = '(dGLzu1x2*ee*complex(0,1)*cmath.sqrt(cw**2 + sw**2))/(cw*sw)',
                 order = {'QED':1,'QNP':1})

GC_93 = Coupling(name = 'GC_93',
                 value = '(dGLzu1x3*ee*complex(0,1)*cmath.sqrt(cw**2 + sw**2))/(cw*sw)',
                 order = {'QED':1,'QNP':1})

GC_94 = Coupling(name = 'GC_94',
                 value = '(dGLzu2x1*ee*complex(0,1)*cmath.sqrt(cw**2 + sw**2))/(cw*sw)',
                 order = {'QED':1,'QNP':1})

GC_95 = Coupling(name = 'GC_95',
                 value = '(dGLzu2x2*ee*complex(0,1)*cmath.sqrt(cw**2 + sw**2))/(cw*sw)',
                 order = {'QED':1,'QNP':1})

GC_96 = Coupling(name = 'GC_96',
                 value = '(dGLzu2x3*ee*complex(0,1)*cmath.sqrt(cw**2 + sw**2))/(cw*sw)',
                 order = {'QED':1,'QNP':1})

GC_97 = Coupling(name = 'GC_97',
                 value = '(dGLzu3x1*ee*complex(0,1)*cmath.sqrt(cw**2 + sw**2))/(cw*sw)',
                 order = {'QED':1,'QNP':1})

GC_98 = Coupling(name = 'GC_98',
                 value = '(dGLzu3x2*ee*complex(0,1)*cmath.sqrt(cw**2 + sw**2))/(cw*sw)',
                 order = {'QED':1,'QNP':1})

GC_99 = Coupling(name = 'GC_99',
                 value = '(dGLzu3x3*ee*complex(0,1)*cmath.sqrt(cw**2 + sw**2))/(cw*sw)',
                 order = {'QED':1,'QNP':1})

GC_100 = Coupling(name = 'GC_100',
                  value = '(dGLzv1x1*ee*complex(0,1)*cmath.sqrt(cw**2 + sw**2))/(cw*sw)',
                  order = {'QED':1,'QNP':1})

GC_101 = Coupling(name = 'GC_101',
                  value = '(dGLzv1x2*ee*complex(0,1)*cmath.sqrt(cw**2 + sw**2))/(cw*sw)',
                  order = {'QED':1,'QNP':1})

GC_102 = Coupling(name = 'GC_102',
                  value = '(dGLzv1x3*ee*complex(0,1)*cmath.sqrt(cw**2 + sw**2))/(cw*sw)',
                  order = {'QED':1,'QNP':1})

GC_103 = Coupling(name = 'GC_103',
                  value = '(dGLzv2x1*ee*complex(0,1)*cmath.sqrt(cw**2 + sw**2))/(cw*sw)',
                  order = {'QED':1,'QNP':1})

GC_104 = Coupling(name = 'GC_104',
                  value = '(dGLzv2x2*ee*complex(0,1)*cmath.sqrt(cw**2 + sw**2))/(cw*sw)',
                  order = {'QED':1,'QNP':1})

GC_105 = Coupling(name = 'GC_105',
                  value = '(dGLzv2x3*ee*complex(0,1)*cmath.sqrt(cw**2 + sw**2))/(cw*sw)',
                  order = {'QED':1,'QNP':1})

GC_106 = Coupling(name = 'GC_106',
                  value = '(dGLzv3x1*ee*complex(0,1)*cmath.sqrt(cw**2 + sw**2))/(cw*sw)',
                  order = {'QED':1,'QNP':1})

GC_107 = Coupling(name = 'GC_107',
                  value = '(dGLzv3x2*ee*complex(0,1)*cmath.sqrt(cw**2 + sw**2))/(cw*sw)',
                  order = {'QED':1,'QNP':1})

GC_108 = Coupling(name = 'GC_108',
                  value = '(dGLzv3x3*ee*complex(0,1)*cmath.sqrt(cw**2 + sw**2))/(cw*sw)',
                  order = {'QED':1,'QNP':1})

GC_109 = Coupling(name = 'GC_109',
                  value = '(dGRzd1x1*ee*complex(0,1)*cmath.sqrt(cw**2 + sw**2))/(cw*sw)',
                  order = {'QED':1,'QNP':1})

GC_110 = Coupling(name = 'GC_110',
                  value = '(dGRzd1x2*ee*complex(0,1)*cmath.sqrt(cw**2 + sw**2))/(cw*sw)',
                  order = {'QED':1,'QNP':1})

GC_111 = Coupling(name = 'GC_111',
                  value = '(dGRzd1x3*ee*complex(0,1)*cmath.sqrt(cw**2 + sw**2))/(cw*sw)',
                  order = {'QED':1,'QNP':1})

GC_112 = Coupling(name = 'GC_112',
                  value = '(dGRzd2x1*ee*complex(0,1)*cmath.sqrt(cw**2 + sw**2))/(cw*sw)',
                  order = {'QED':1,'QNP':1})

GC_113 = Coupling(name = 'GC_113',
                  value = '(dGRzd2x2*ee*complex(0,1)*cmath.sqrt(cw**2 + sw**2))/(cw*sw)',
                  order = {'QED':1,'QNP':1})

GC_114 = Coupling(name = 'GC_114',
                  value = '(dGRzd2x3*ee*complex(0,1)*cmath.sqrt(cw**2 + sw**2))/(cw*sw)',
                  order = {'QED':1,'QNP':1})

GC_115 = Coupling(name = 'GC_115',
                  value = '(dGRzd3x1*ee*complex(0,1)*cmath.sqrt(cw**2 + sw**2))/(cw*sw)',
                  order = {'QED':1,'QNP':1})

GC_116 = Coupling(name = 'GC_116',
                  value = '(dGRzd3x2*ee*complex(0,1)*cmath.sqrt(cw**2 + sw**2))/(cw*sw)',
                  order = {'QED':1,'QNP':1})

GC_117 = Coupling(name = 'GC_117',
                  value = '(dGRzd3x3*ee*complex(0,1)*cmath.sqrt(cw**2 + sw**2))/(cw*sw)',
                  order = {'QED':1,'QNP':1})

GC_118 = Coupling(name = 'GC_118',
                  value = '(dGRze1x1*ee*complex(0,1)*cmath.sqrt(cw**2 + sw**2))/(cw*sw)',
                  order = {'QED':1,'QNP':1})

GC_119 = Coupling(name = 'GC_119',
                  value = '(dGRze1x2*ee*complex(0,1)*cmath.sqrt(cw**2 + sw**2))/(cw*sw)',
                  order = {'QED':1,'QNP':1})

GC_120 = Coupling(name = 'GC_120',
                  value = '(dGRze1x3*ee*complex(0,1)*cmath.sqrt(cw**2 + sw**2))/(cw*sw)',
                  order = {'QED':1,'QNP':1})

GC_121 = Coupling(name = 'GC_121',
                  value = '(dGRze2x1*ee*complex(0,1)*cmath.sqrt(cw**2 + sw**2))/(cw*sw)',
                  order = {'QED':1,'QNP':1})

GC_122 = Coupling(name = 'GC_122',
                  value = '(dGRze2x2*ee*complex(0,1)*cmath.sqrt(cw**2 + sw**2))/(cw*sw)',
                  order = {'QED':1,'QNP':1})

GC_123 = Coupling(name = 'GC_123',
                  value = '(dGRze2x3*ee*complex(0,1)*cmath.sqrt(cw**2 + sw**2))/(cw*sw)',
                  order = {'QED':1,'QNP':1})

GC_124 = Coupling(name = 'GC_124',
                  value = '(dGRze3x1*ee*complex(0,1)*cmath.sqrt(cw**2 + sw**2))/(cw*sw)',
                  order = {'QED':1,'QNP':1})

GC_125 = Coupling(name = 'GC_125',
                  value = '(dGRze3x2*ee*complex(0,1)*cmath.sqrt(cw**2 + sw**2))/(cw*sw)',
                  order = {'QED':1,'QNP':1})

GC_126 = Coupling(name = 'GC_126',
                  value = '(dGRze3x3*ee*complex(0,1)*cmath.sqrt(cw**2 + sw**2))/(cw*sw)',
                  order = {'QED':1,'QNP':1})

GC_127 = Coupling(name = 'GC_127',
                  value = '(dGRzu1x1*ee*complex(0,1)*cmath.sqrt(cw**2 + sw**2))/(cw*sw)',
                  order = {'QED':1,'QNP':1})

GC_128 = Coupling(name = 'GC_128',
                  value = '(dGRzu1x2*ee*complex(0,1)*cmath.sqrt(cw**2 + sw**2))/(cw*sw)',
                  order = {'QED':1,'QNP':1})

GC_129 = Coupling(name = 'GC_129',
                  value = '(dGRzu1x3*ee*complex(0,1)*cmath.sqrt(cw**2 + sw**2))/(cw*sw)',
                  order = {'QED':1,'QNP':1})

GC_130 = Coupling(name = 'GC_130',
                  value = '(dGRzu2x1*ee*complex(0,1)*cmath.sqrt(cw**2 + sw**2))/(cw*sw)',
                  order = {'QED':1,'QNP':1})

GC_131 = Coupling(name = 'GC_131',
                  value = '(dGRzu2x2*ee*complex(0,1)*cmath.sqrt(cw**2 + sw**2))/(cw*sw)',
                  order = {'QED':1,'QNP':1})

GC_132 = Coupling(name = 'GC_132',
                  value = '(dGRzu2x3*ee*complex(0,1)*cmath.sqrt(cw**2 + sw**2))/(cw*sw)',
                  order = {'QED':1,'QNP':1})

GC_133 = Coupling(name = 'GC_133',
                  value = '(dGRzu3x1*ee*complex(0,1)*cmath.sqrt(cw**2 + sw**2))/(cw*sw)',
                  order = {'QED':1,'QNP':1})

GC_134 = Coupling(name = 'GC_134',
                  value = '(dGRzu3x2*ee*complex(0,1)*cmath.sqrt(cw**2 + sw**2))/(cw*sw)',
                  order = {'QED':1,'QNP':1})

GC_135 = Coupling(name = 'GC_135',
                  value = '(dGRzu3x3*ee*complex(0,1)*cmath.sqrt(cw**2 + sw**2))/(cw*sw)',
                  order = {'QED':1,'QNP':1})

GC_136 = Coupling(name = 'GC_136',
                  value = 'ee**2*complex(0,1) + (cw**2*ee**2*complex(0,1))/(2.*sw**2) + (ee**2*complex(0,1)*sw**2)/(2.*cw**2)',
                  order = {'QED':2})

GC_137 = Coupling(name = 'GC_137',
                  value = '-(ee*complex(0,1)*tKa)',
                  order = {'QED':1,'QNP':1})

GC_138 = Coupling(name = 'GC_138',
                  value = '-((cw*ee*complex(0,1)*tKz)/sw)',
                  order = {'QED':1,'QNP':1})

GC_139 = Coupling(name = 'GC_139',
                  value = '-((ee*complex(0,1)*tLa)/MW**2)',
                  order = {'QED':1,'QNP':1})

GC_140 = Coupling(name = 'GC_140',
                  value = '(ee**2*complex(0,1)*tLw2a2)/MW**2',
                  order = {'QED':2,'QNP':1})

GC_141 = Coupling(name = 'GC_141',
                  value = '-((cw*ee**2*complex(0,1)*tLw2az)/(MW**2*sw))',
                  order = {'QED':2,'QNP':1})

GC_142 = Coupling(name = 'GC_142',
                  value = '(cw**2*ee**2*complex(0,1)*tLw2z2)/(MW**2*sw**2)',
                  order = {'QED':2,'QNP':1})

GC_143 = Coupling(name = 'GC_143',
                  value = '(cw*ee**2*complex(0,1)*tLw2za)/(MW**2*sw)',
                  order = {'QED':2,'QNP':1})

GC_144 = Coupling(name = 'GC_144',
                  value = '-(ee**2*complex(0,1)*tLw4)/(2.*MW**2*sw**2)',
                  order = {'QED':2,'QNP':1})

GC_145 = Coupling(name = 'GC_145',
                  value = '-((cw*ee*complex(0,1)*tLz)/(MW**2*sw))',
                  order = {'QED':1,'QNP':1})

GC_146 = Coupling(name = 'GC_146',
                  value = '(Czz2*ee**2*complex(0,1))/(cw**2*vev**2) + (Czz2*ee**2*complex(0,1))/(sw**2*vev**2)',
                  order = {'QED':4,'QNP':1})

GC_147 = Coupling(name = 'GC_147',
                  value = '(ee**2*complex(0,1)*tCzz2)/(cw**2*vev**2) + (ee**2*complex(0,1)*tCzz2)/(sw**2*vev**2)',
                  order = {'QED':4,'QNP':1})

GC_148 = Coupling(name = 'GC_148',
                  value = '(dhad1x1*ee)/(4.*vev**2)',
                  order = {'QED':3,'QNP':1})

GC_149 = Coupling(name = 'GC_149',
                  value = '(dhad1x2*ee)/(4.*vev**2)',
                  order = {'QED':3,'QNP':1})

GC_150 = Coupling(name = 'GC_150',
                  value = '(dhad1x3*ee)/(4.*vev**2)',
                  order = {'QED':3,'QNP':1})

GC_151 = Coupling(name = 'GC_151',
                  value = '(dhad2x1*ee)/(4.*vev**2)',
                  order = {'QED':3,'QNP':1})

GC_152 = Coupling(name = 'GC_152',
                  value = '(dhad2x2*ee)/(4.*vev**2)',
                  order = {'QED':3,'QNP':1})

GC_153 = Coupling(name = 'GC_153',
                  value = '(dhad2x3*ee)/(4.*vev**2)',
                  order = {'QED':3,'QNP':1})

GC_154 = Coupling(name = 'GC_154',
                  value = '(dhad3x1*ee)/(4.*vev**2)',
                  order = {'QED':3,'QNP':1})

GC_155 = Coupling(name = 'GC_155',
                  value = '(dhad3x2*ee)/(4.*vev**2)',
                  order = {'QED':3,'QNP':1})

GC_156 = Coupling(name = 'GC_156',
                  value = '(dhad3x3*ee)/(4.*vev**2)',
                  order = {'QED':3,'QNP':1})

GC_157 = Coupling(name = 'GC_157',
                  value = '(dhae1x1*ee)/(4.*vev**2)',
                  order = {'QED':3,'QNP':1})

GC_158 = Coupling(name = 'GC_158',
                  value = '(dhae1x2*ee)/(4.*vev**2)',
                  order = {'QED':3,'QNP':1})

GC_159 = Coupling(name = 'GC_159',
                  value = '(dhae1x3*ee)/(4.*vev**2)',
                  order = {'QED':3,'QNP':1})

GC_160 = Coupling(name = 'GC_160',
                  value = '(dhae2x1*ee)/(4.*vev**2)',
                  order = {'QED':3,'QNP':1})

GC_161 = Coupling(name = 'GC_161',
                  value = '(dhae2x2*ee)/(4.*vev**2)',
                  order = {'QED':3,'QNP':1})

GC_162 = Coupling(name = 'GC_162',
                  value = '(dhae2x3*ee)/(4.*vev**2)',
                  order = {'QED':3,'QNP':1})

GC_163 = Coupling(name = 'GC_163',
                  value = '(dhae3x1*ee)/(4.*vev**2)',
                  order = {'QED':3,'QNP':1})

GC_164 = Coupling(name = 'GC_164',
                  value = '(dhae3x2*ee)/(4.*vev**2)',
                  order = {'QED':3,'QNP':1})

GC_165 = Coupling(name = 'GC_165',
                  value = '(dhae3x3*ee)/(4.*vev**2)',
                  order = {'QED':3,'QNP':1})

GC_166 = Coupling(name = 'GC_166',
                  value = '(dhau1x1*ee)/(4.*vev**2)',
                  order = {'QED':3,'QNP':1})

GC_167 = Coupling(name = 'GC_167',
                  value = '(dhau1x2*ee)/(4.*vev**2)',
                  order = {'QED':3,'QNP':1})

GC_168 = Coupling(name = 'GC_168',
                  value = '(dhau1x3*ee)/(4.*vev**2)',
                  order = {'QED':3,'QNP':1})

GC_169 = Coupling(name = 'GC_169',
                  value = '(dhau2x1*ee)/(4.*vev**2)',
                  order = {'QED':3,'QNP':1})

GC_170 = Coupling(name = 'GC_170',
                  value = '(dhau2x2*ee)/(4.*vev**2)',
                  order = {'QED':3,'QNP':1})

GC_171 = Coupling(name = 'GC_171',
                  value = '(dhau2x3*ee)/(4.*vev**2)',
                  order = {'QED':3,'QNP':1})

GC_172 = Coupling(name = 'GC_172',
                  value = '(dhau3x1*ee)/(4.*vev**2)',
                  order = {'QED':3,'QNP':1})

GC_173 = Coupling(name = 'GC_173',
                  value = '(dhau3x2*ee)/(4.*vev**2)',
                  order = {'QED':3,'QNP':1})

GC_174 = Coupling(name = 'GC_174',
                  value = '(dhau3x3*ee)/(4.*vev**2)',
                  order = {'QED':3,'QNP':1})

GC_175 = Coupling(name = 'GC_175',
                  value = '(Caa2*ee**2*complex(0,1))/vev**2',
                  order = {'QED':4,'QNP':1})

GC_176 = Coupling(name = 'GC_176',
                  value = '(dhgd1x1*G)/(4.*vev**2)',
                  order = {'QCD':1,'QED':2,'QNP':1})

GC_177 = Coupling(name = 'GC_177',
                  value = '(dhgd1x2*G)/(4.*vev**2)',
                  order = {'QCD':1,'QED':2,'QNP':1})

GC_178 = Coupling(name = 'GC_178',
                  value = '(dhgd1x3*G)/(4.*vev**2)',
                  order = {'QCD':1,'QED':2,'QNP':1})

GC_179 = Coupling(name = 'GC_179',
                  value = '(dhgd2x1*G)/(4.*vev**2)',
                  order = {'QCD':1,'QED':2,'QNP':1})

GC_180 = Coupling(name = 'GC_180',
                  value = '(dhgd2x2*G)/(4.*vev**2)',
                  order = {'QCD':1,'QED':2,'QNP':1})

GC_181 = Coupling(name = 'GC_181',
                  value = '(dhgd2x3*G)/(4.*vev**2)',
                  order = {'QCD':1,'QED':2,'QNP':1})

GC_182 = Coupling(name = 'GC_182',
                  value = '(dhgd3x1*G)/(4.*vev**2)',
                  order = {'QCD':1,'QED':2,'QNP':1})

GC_183 = Coupling(name = 'GC_183',
                  value = '(dhgd3x2*G)/(4.*vev**2)',
                  order = {'QCD':1,'QED':2,'QNP':1})

GC_184 = Coupling(name = 'GC_184',
                  value = '(dhgd3x3*G)/(4.*vev**2)',
                  order = {'QCD':1,'QED':2,'QNP':1})

GC_185 = Coupling(name = 'GC_185',
                  value = '(dhgu1x1*G)/(4.*vev**2)',
                  order = {'QCD':1,'QED':2,'QNP':1})

GC_186 = Coupling(name = 'GC_186',
                  value = '(dhgu1x2*G)/(4.*vev**2)',
                  order = {'QCD':1,'QED':2,'QNP':1})

GC_187 = Coupling(name = 'GC_187',
                  value = '(dhgu1x3*G)/(4.*vev**2)',
                  order = {'QCD':1,'QED':2,'QNP':1})

GC_188 = Coupling(name = 'GC_188',
                  value = '(dhgu2x1*G)/(4.*vev**2)',
                  order = {'QCD':1,'QED':2,'QNP':1})

GC_189 = Coupling(name = 'GC_189',
                  value = '(dhgu2x2*G)/(4.*vev**2)',
                  order = {'QCD':1,'QED':2,'QNP':1})

GC_190 = Coupling(name = 'GC_190',
                  value = '(dhgu2x3*G)/(4.*vev**2)',
                  order = {'QCD':1,'QED':2,'QNP':1})

GC_191 = Coupling(name = 'GC_191',
                  value = '(dhgu3x1*G)/(4.*vev**2)',
                  order = {'QCD':1,'QED':2,'QNP':1})

GC_192 = Coupling(name = 'GC_192',
                  value = '(dhgu3x2*G)/(4.*vev**2)',
                  order = {'QCD':1,'QED':2,'QNP':1})

GC_193 = Coupling(name = 'GC_193',
                  value = '(dhgu3x3*G)/(4.*vev**2)',
                  order = {'QCD':1,'QED':2,'QNP':1})

GC_194 = Coupling(name = 'GC_194',
                  value = '(Cgg2*complex(0,1)*G**2)/vev**2',
                  order = {'QCD':2,'QED':2,'QNP':1})

GC_195 = Coupling(name = 'GC_195',
                  value = '-(dhgd1x1*complex(0,1)*G**2)/(4.*vev**2)',
                  order = {'QCD':2,'QED':2,'QNP':1})

GC_196 = Coupling(name = 'GC_196',
                  value = '-(dhgd1x2*complex(0,1)*G**2)/(4.*vev**2)',
                  order = {'QCD':2,'QED':2,'QNP':1})

GC_197 = Coupling(name = 'GC_197',
                  value = '-(dhgd1x3*complex(0,1)*G**2)/(4.*vev**2)',
                  order = {'QCD':2,'QED':2,'QNP':1})

GC_198 = Coupling(name = 'GC_198',
                  value = '-(dhgd2x1*complex(0,1)*G**2)/(4.*vev**2)',
                  order = {'QCD':2,'QED':2,'QNP':1})

GC_199 = Coupling(name = 'GC_199',
                  value = '-(dhgd2x2*complex(0,1)*G**2)/(4.*vev**2)',
                  order = {'QCD':2,'QED':2,'QNP':1})

GC_200 = Coupling(name = 'GC_200',
                  value = '-(dhgd2x3*complex(0,1)*G**2)/(4.*vev**2)',
                  order = {'QCD':2,'QED':2,'QNP':1})

GC_201 = Coupling(name = 'GC_201',
                  value = '-(dhgd3x1*complex(0,1)*G**2)/(4.*vev**2)',
                  order = {'QCD':2,'QED':2,'QNP':1})

GC_202 = Coupling(name = 'GC_202',
                  value = '-(dhgd3x2*complex(0,1)*G**2)/(4.*vev**2)',
                  order = {'QCD':2,'QED':2,'QNP':1})

GC_203 = Coupling(name = 'GC_203',
                  value = '-(dhgd3x3*complex(0,1)*G**2)/(4.*vev**2)',
                  order = {'QCD':2,'QED':2,'QNP':1})

GC_204 = Coupling(name = 'GC_204',
                  value = '-(dhgu1x1*complex(0,1)*G**2)/(4.*vev**2)',
                  order = {'QCD':2,'QED':2,'QNP':1})

GC_205 = Coupling(name = 'GC_205',
                  value = '-(dhgu1x2*complex(0,1)*G**2)/(4.*vev**2)',
                  order = {'QCD':2,'QED':2,'QNP':1})

GC_206 = Coupling(name = 'GC_206',
                  value = '-(dhgu1x3*complex(0,1)*G**2)/(4.*vev**2)',
                  order = {'QCD':2,'QED':2,'QNP':1})

GC_207 = Coupling(name = 'GC_207',
                  value = '-(dhgu2x1*complex(0,1)*G**2)/(4.*vev**2)',
                  order = {'QCD':2,'QED':2,'QNP':1})

GC_208 = Coupling(name = 'GC_208',
                  value = '-(dhgu2x2*complex(0,1)*G**2)/(4.*vev**2)',
                  order = {'QCD':2,'QED':2,'QNP':1})

GC_209 = Coupling(name = 'GC_209',
                  value = '-(dhgu2x3*complex(0,1)*G**2)/(4.*vev**2)',
                  order = {'QCD':2,'QED':2,'QNP':1})

GC_210 = Coupling(name = 'GC_210',
                  value = '-(dhgu3x1*complex(0,1)*G**2)/(4.*vev**2)',
                  order = {'QCD':2,'QED':2,'QNP':1})

GC_211 = Coupling(name = 'GC_211',
                  value = '-(dhgu3x2*complex(0,1)*G**2)/(4.*vev**2)',
                  order = {'QCD':2,'QED':2,'QNP':1})

GC_212 = Coupling(name = 'GC_212',
                  value = '-(dhgu3x3*complex(0,1)*G**2)/(4.*vev**2)',
                  order = {'QCD':2,'QED':2,'QNP':1})

GC_213 = Coupling(name = 'GC_213',
                  value = '(-6*C3g*G**3)/vev**2',
                  order = {'QCD':3,'QED':2,'QNP':1})

GC_214 = Coupling(name = 'GC_214',
                  value = '(6*C4g*complex(0,1)*G**3)/vev**2',
                  order = {'QCD':3,'QED':2,'QNP':1})

GC_215 = Coupling(name = 'GC_215',
                  value = '(Cgg2*G**3)/vev**2',
                  order = {'QCD':3,'QED':2,'QNP':1})

GC_216 = Coupling(name = 'GC_216',
                  value = '(6*C3g*complex(0,1)*G**4)/vev**2',
                  order = {'QCD':4,'QED':2,'QNP':1})

GC_217 = Coupling(name = 'GC_217',
                  value = '(-6*C4g*G**4)/vev**2',
                  order = {'QCD':4,'QED':2,'QNP':1})

GC_218 = Coupling(name = 'GC_218',
                  value = '(6*C4g*G**4)/vev**2',
                  order = {'QCD':4,'QED':2,'QNP':1})

GC_219 = Coupling(name = 'GC_219',
                  value = '-((Cgg2*complex(0,1)*G**4)/vev**2)',
                  order = {'QCD':4,'QED':2,'QNP':1})

GC_220 = Coupling(name = 'GC_220',
                  value = '(-3*C3g*G**5)/vev**2',
                  order = {'QCD':5,'QED':2,'QNP':1})

GC_221 = Coupling(name = 'GC_221',
                  value = '(3*C3g*G**5)/vev**2',
                  order = {'QCD':5,'QED':2,'QNP':1})

GC_222 = Coupling(name = 'GC_222',
                  value = '(-3*C4g*complex(0,1)*G**5)/vev**2',
                  order = {'QCD':5,'QED':2,'QNP':1})

GC_223 = Coupling(name = 'GC_223',
                  value = '(3*C4g*complex(0,1)*G**5)/vev**2',
                  order = {'QCD':5,'QED':2,'QNP':1})

GC_224 = Coupling(name = 'GC_224',
                  value = '-((C3g*complex(0,1)*G**6)/vev**2)',
                  order = {'QCD':6,'QED':2,'QNP':1})

GC_225 = Coupling(name = 'GC_225',
                  value = '(C3g*complex(0,1)*G**6)/vev**2',
                  order = {'QCD':6,'QED':2,'QNP':1})

GC_226 = Coupling(name = 'GC_226',
                  value = '-((dYd23x3*complex(0,1)*MB)/vev**2)',
                  order = {'QED':2,'QNP':1})

GC_227 = Coupling(name = 'GC_227',
                  value = '-((dYu23x3*complex(0,1)*MT)/vev**2)',
                  order = {'QED':2,'QNP':1})

GC_228 = Coupling(name = 'GC_228',
                  value = '-((dYe23x3*complex(0,1)*MTA)/vev**2)',
                  order = {'QED':2,'QNP':1})

GC_229 = Coupling(name = 'GC_229',
                  value = '(Cabx2*ee**2*complex(0,1))/(sw**2*vev**2)',
                  order = {'QED':4,'QNP':1})

GC_230 = Coupling(name = 'GC_230',
                  value = '(Cwbx2*ee**2*complex(0,1))/(sw**2*vev**2)',
                  order = {'QED':4,'QNP':1})

GC_231 = Coupling(name = 'GC_231',
                  value = '(Cww2*ee**2*complex(0,1))/(sw**2*vev**2)',
                  order = {'QED':4,'QNP':1})

GC_232 = Coupling(name = 'GC_232',
                  value = '(Czbx2*ee**2*complex(0,1))/(sw**2*vev**2)',
                  order = {'QED':4,'QNP':1})

GC_233 = Coupling(name = 'GC_233',
                  value = '(dhwl1x1*ee)/(2.*sw*vev**2*cmath.sqrt(2))',
                  order = {'QED':3,'QNP':1})

GC_234 = Coupling(name = 'GC_234',
                  value = '(dhwl1x2*ee)/(2.*sw*vev**2*cmath.sqrt(2))',
                  order = {'QED':3,'QNP':1})

GC_235 = Coupling(name = 'GC_235',
                  value = '(dhwl1x3*ee)/(2.*sw*vev**2*cmath.sqrt(2))',
                  order = {'QED':3,'QNP':1})

GC_236 = Coupling(name = 'GC_236',
                  value = '(dhwl2x1*ee)/(2.*sw*vev**2*cmath.sqrt(2))',
                  order = {'QED':3,'QNP':1})

GC_237 = Coupling(name = 'GC_237',
                  value = '(dhwl2x2*ee)/(2.*sw*vev**2*cmath.sqrt(2))',
                  order = {'QED':3,'QNP':1})

GC_238 = Coupling(name = 'GC_238',
                  value = '(dhwl2x3*ee)/(2.*sw*vev**2*cmath.sqrt(2))',
                  order = {'QED':3,'QNP':1})

GC_239 = Coupling(name = 'GC_239',
                  value = '(dhwl3x1*ee)/(2.*sw*vev**2*cmath.sqrt(2))',
                  order = {'QED':3,'QNP':1})

GC_240 = Coupling(name = 'GC_240',
                  value = '(dhwl3x2*ee)/(2.*sw*vev**2*cmath.sqrt(2))',
                  order = {'QED':3,'QNP':1})

GC_241 = Coupling(name = 'GC_241',
                  value = '(dhwl3x3*ee)/(2.*sw*vev**2*cmath.sqrt(2))',
                  order = {'QED':3,'QNP':1})

GC_242 = Coupling(name = 'GC_242',
                  value = '(Cza2*ee**2*complex(0,1))/(cw*sw*vev**2)',
                  order = {'QED':4,'QNP':1})

GC_243 = Coupling(name = 'GC_243',
                  value = '(dhzd1x1*ee*cmath.sqrt(cw**2 + sw**2))/(4.*cw*sw*vev**2)',
                  order = {'QED':3,'QNP':1})

GC_244 = Coupling(name = 'GC_244',
                  value = '(dhzd1x2*ee*cmath.sqrt(cw**2 + sw**2))/(4.*cw*sw*vev**2)',
                  order = {'QED':3,'QNP':1})

GC_245 = Coupling(name = 'GC_245',
                  value = '(dhzd1x3*ee*cmath.sqrt(cw**2 + sw**2))/(4.*cw*sw*vev**2)',
                  order = {'QED':3,'QNP':1})

GC_246 = Coupling(name = 'GC_246',
                  value = '(dhzd2x1*ee*cmath.sqrt(cw**2 + sw**2))/(4.*cw*sw*vev**2)',
                  order = {'QED':3,'QNP':1})

GC_247 = Coupling(name = 'GC_247',
                  value = '(dhzd2x2*ee*cmath.sqrt(cw**2 + sw**2))/(4.*cw*sw*vev**2)',
                  order = {'QED':3,'QNP':1})

GC_248 = Coupling(name = 'GC_248',
                  value = '(dhzd2x3*ee*cmath.sqrt(cw**2 + sw**2))/(4.*cw*sw*vev**2)',
                  order = {'QED':3,'QNP':1})

GC_249 = Coupling(name = 'GC_249',
                  value = '(dhzd3x1*ee*cmath.sqrt(cw**2 + sw**2))/(4.*cw*sw*vev**2)',
                  order = {'QED':3,'QNP':1})

GC_250 = Coupling(name = 'GC_250',
                  value = '(dhzd3x2*ee*cmath.sqrt(cw**2 + sw**2))/(4.*cw*sw*vev**2)',
                  order = {'QED':3,'QNP':1})

GC_251 = Coupling(name = 'GC_251',
                  value = '(dhzd3x3*ee*cmath.sqrt(cw**2 + sw**2))/(4.*cw*sw*vev**2)',
                  order = {'QED':3,'QNP':1})

GC_252 = Coupling(name = 'GC_252',
                  value = '(dhze1x1*ee*cmath.sqrt(cw**2 + sw**2))/(4.*cw*sw*vev**2)',
                  order = {'QED':3,'QNP':1})

GC_253 = Coupling(name = 'GC_253',
                  value = '(dhze1x2*ee*cmath.sqrt(cw**2 + sw**2))/(4.*cw*sw*vev**2)',
                  order = {'QED':3,'QNP':1})

GC_254 = Coupling(name = 'GC_254',
                  value = '(dhze1x3*ee*cmath.sqrt(cw**2 + sw**2))/(4.*cw*sw*vev**2)',
                  order = {'QED':3,'QNP':1})

GC_255 = Coupling(name = 'GC_255',
                  value = '(dhze2x1*ee*cmath.sqrt(cw**2 + sw**2))/(4.*cw*sw*vev**2)',
                  order = {'QED':3,'QNP':1})

GC_256 = Coupling(name = 'GC_256',
                  value = '(dhze2x2*ee*cmath.sqrt(cw**2 + sw**2))/(4.*cw*sw*vev**2)',
                  order = {'QED':3,'QNP':1})

GC_257 = Coupling(name = 'GC_257',
                  value = '(dhze2x3*ee*cmath.sqrt(cw**2 + sw**2))/(4.*cw*sw*vev**2)',
                  order = {'QED':3,'QNP':1})

GC_258 = Coupling(name = 'GC_258',
                  value = '(dhze3x1*ee*cmath.sqrt(cw**2 + sw**2))/(4.*cw*sw*vev**2)',
                  order = {'QED':3,'QNP':1})

GC_259 = Coupling(name = 'GC_259',
                  value = '(dhze3x2*ee*cmath.sqrt(cw**2 + sw**2))/(4.*cw*sw*vev**2)',
                  order = {'QED':3,'QNP':1})

GC_260 = Coupling(name = 'GC_260',
                  value = '(dhze3x3*ee*cmath.sqrt(cw**2 + sw**2))/(4.*cw*sw*vev**2)',
                  order = {'QED':3,'QNP':1})

GC_261 = Coupling(name = 'GC_261',
                  value = '(dhzu1x1*ee*cmath.sqrt(cw**2 + sw**2))/(4.*cw*sw*vev**2)',
                  order = {'QED':3,'QNP':1})

GC_262 = Coupling(name = 'GC_262',
                  value = '(dhzu1x2*ee*cmath.sqrt(cw**2 + sw**2))/(4.*cw*sw*vev**2)',
                  order = {'QED':3,'QNP':1})

GC_263 = Coupling(name = 'GC_263',
                  value = '(dhzu1x3*ee*cmath.sqrt(cw**2 + sw**2))/(4.*cw*sw*vev**2)',
                  order = {'QED':3,'QNP':1})

GC_264 = Coupling(name = 'GC_264',
                  value = '(dhzu2x1*ee*cmath.sqrt(cw**2 + sw**2))/(4.*cw*sw*vev**2)',
                  order = {'QED':3,'QNP':1})

GC_265 = Coupling(name = 'GC_265',
                  value = '(dhzu2x2*ee*cmath.sqrt(cw**2 + sw**2))/(4.*cw*sw*vev**2)',
                  order = {'QED':3,'QNP':1})

GC_266 = Coupling(name = 'GC_266',
                  value = '(dhzu2x3*ee*cmath.sqrt(cw**2 + sw**2))/(4.*cw*sw*vev**2)',
                  order = {'QED':3,'QNP':1})

GC_267 = Coupling(name = 'GC_267',
                  value = '(dhzu3x1*ee*cmath.sqrt(cw**2 + sw**2))/(4.*cw*sw*vev**2)',
                  order = {'QED':3,'QNP':1})

GC_268 = Coupling(name = 'GC_268',
                  value = '(dhzu3x2*ee*cmath.sqrt(cw**2 + sw**2))/(4.*cw*sw*vev**2)',
                  order = {'QED':3,'QNP':1})

GC_269 = Coupling(name = 'GC_269',
                  value = '(dhzu3x3*ee*cmath.sqrt(cw**2 + sw**2))/(4.*cw*sw*vev**2)',
                  order = {'QED':3,'QNP':1})

GC_270 = Coupling(name = 'GC_270',
                  value = '(-2*G**3*tC3g)/vev**2',
                  order = {'QCD':3,'QED':2,'QNP':1})

GC_271 = Coupling(name = 'GC_271',
                  value = '(-2*complex(0,1)*G**4*tC3g)/vev**2',
                  order = {'QCD':4,'QED':2,'QNP':1})

GC_272 = Coupling(name = 'GC_272',
                  value = '(-2*G**5*tC3g)/vev**2',
                  order = {'QCD':5,'QED':2,'QNP':1})

GC_273 = Coupling(name = 'GC_273',
                  value = '(2*G**5*tC3g)/vev**2',
                  order = {'QCD':5,'QED':2,'QNP':1})

GC_274 = Coupling(name = 'GC_274',
                  value = '-((complex(0,1)*G**6*tC3g)/vev**2)',
                  order = {'QCD':6,'QED':2,'QNP':1})

GC_275 = Coupling(name = 'GC_275',
                  value = '(complex(0,1)*G**6*tC3g)/vev**2',
                  order = {'QCD':6,'QED':2,'QNP':1})

GC_276 = Coupling(name = 'GC_276',
                  value = '(-3*complex(0,1)*G**3*tC4g)/vev**2',
                  order = {'QCD':3,'QED':2,'QNP':1})

GC_277 = Coupling(name = 'GC_277',
                  value = '(-3*G**4*tC4g)/vev**2',
                  order = {'QCD':4,'QED':2,'QNP':1})

GC_278 = Coupling(name = 'GC_278',
                  value = '(3*G**4*tC4g)/vev**2',
                  order = {'QCD':4,'QED':2,'QNP':1})

GC_279 = Coupling(name = 'GC_279',
                  value = '(-3*complex(0,1)*G**5*tC4g)/vev**2',
                  order = {'QCD':5,'QED':2,'QNP':1})

GC_280 = Coupling(name = 'GC_280',
                  value = '(3*complex(0,1)*G**5*tC4g)/vev**2',
                  order = {'QCD':5,'QED':2,'QNP':1})

GC_281 = Coupling(name = 'GC_281',
                  value = '(ee**2*complex(0,1)*tCaa2)/vev**2',
                  order = {'QED':4,'QNP':1})

GC_282 = Coupling(name = 'GC_282',
                  value = '(complex(0,1)*G**2*tCgg2)/vev**2',
                  order = {'QCD':2,'QED':2,'QNP':1})

GC_283 = Coupling(name = 'GC_283',
                  value = '-((G**3*tCgg2)/vev**2)',
                  order = {'QCD':3,'QED':2,'QNP':1})

GC_284 = Coupling(name = 'GC_284',
                  value = '(ee**2*complex(0,1)*tCww2)/(sw**2*vev**2)',
                  order = {'QED':4,'QNP':1})

GC_285 = Coupling(name = 'GC_285',
                  value = '(ee**2*complex(0,1)*tCza2)/(cw*sw*vev**2)',
                  order = {'QED':4,'QNP':1})

GC_286 = Coupling(name = 'GC_286',
                  value = '(ee*tdhad1x1)/(4.*vev**2)',
                  order = {'QED':3,'QNP':1})

GC_287 = Coupling(name = 'GC_287',
                  value = '(ee*tdhad1x2)/(4.*vev**2)',
                  order = {'QED':3,'QNP':1})

GC_288 = Coupling(name = 'GC_288',
                  value = '(ee*tdhad1x3)/(4.*vev**2)',
                  order = {'QED':3,'QNP':1})

GC_289 = Coupling(name = 'GC_289',
                  value = '(ee*tdhad2x1)/(4.*vev**2)',
                  order = {'QED':3,'QNP':1})

GC_290 = Coupling(name = 'GC_290',
                  value = '(ee*tdhad2x2)/(4.*vev**2)',
                  order = {'QED':3,'QNP':1})

GC_291 = Coupling(name = 'GC_291',
                  value = '(ee*tdhad2x3)/(4.*vev**2)',
                  order = {'QED':3,'QNP':1})

GC_292 = Coupling(name = 'GC_292',
                  value = '(ee*tdhad3x1)/(4.*vev**2)',
                  order = {'QED':3,'QNP':1})

GC_293 = Coupling(name = 'GC_293',
                  value = '(ee*tdhad3x2)/(4.*vev**2)',
                  order = {'QED':3,'QNP':1})

GC_294 = Coupling(name = 'GC_294',
                  value = '(ee*tdhad3x3)/(4.*vev**2)',
                  order = {'QED':3,'QNP':1})

GC_295 = Coupling(name = 'GC_295',
                  value = '(ee*tdhae1x1)/(4.*vev**2)',
                  order = {'QED':3,'QNP':1})

GC_296 = Coupling(name = 'GC_296',
                  value = '(ee*tdhae1x2)/(4.*vev**2)',
                  order = {'QED':3,'QNP':1})

GC_297 = Coupling(name = 'GC_297',
                  value = '(ee*tdhae1x3)/(4.*vev**2)',
                  order = {'QED':3,'QNP':1})

GC_298 = Coupling(name = 'GC_298',
                  value = '(ee*tdhae2x1)/(4.*vev**2)',
                  order = {'QED':3,'QNP':1})

GC_299 = Coupling(name = 'GC_299',
                  value = '(ee*tdhae2x2)/(4.*vev**2)',
                  order = {'QED':3,'QNP':1})

GC_300 = Coupling(name = 'GC_300',
                  value = '(ee*tdhae2x3)/(4.*vev**2)',
                  order = {'QED':3,'QNP':1})

GC_301 = Coupling(name = 'GC_301',
                  value = '(ee*tdhae3x1)/(4.*vev**2)',
                  order = {'QED':3,'QNP':1})

GC_302 = Coupling(name = 'GC_302',
                  value = '(ee*tdhae3x2)/(4.*vev**2)',
                  order = {'QED':3,'QNP':1})

GC_303 = Coupling(name = 'GC_303',
                  value = '(ee*tdhae3x3)/(4.*vev**2)',
                  order = {'QED':3,'QNP':1})

GC_304 = Coupling(name = 'GC_304',
                  value = '(ee*tdhau1x1)/(4.*vev**2)',
                  order = {'QED':3,'QNP':1})

GC_305 = Coupling(name = 'GC_305',
                  value = '(ee*tdhau1x2)/(4.*vev**2)',
                  order = {'QED':3,'QNP':1})

GC_306 = Coupling(name = 'GC_306',
                  value = '(ee*tdhau1x3)/(4.*vev**2)',
                  order = {'QED':3,'QNP':1})

GC_307 = Coupling(name = 'GC_307',
                  value = '(ee*tdhau2x1)/(4.*vev**2)',
                  order = {'QED':3,'QNP':1})

GC_308 = Coupling(name = 'GC_308',
                  value = '(ee*tdhau2x2)/(4.*vev**2)',
                  order = {'QED':3,'QNP':1})

GC_309 = Coupling(name = 'GC_309',
                  value = '(ee*tdhau2x3)/(4.*vev**2)',
                  order = {'QED':3,'QNP':1})

GC_310 = Coupling(name = 'GC_310',
                  value = '(ee*tdhau3x1)/(4.*vev**2)',
                  order = {'QED':3,'QNP':1})

GC_311 = Coupling(name = 'GC_311',
                  value = '(ee*tdhau3x2)/(4.*vev**2)',
                  order = {'QED':3,'QNP':1})

GC_312 = Coupling(name = 'GC_312',
                  value = '(ee*tdhau3x3)/(4.*vev**2)',
                  order = {'QED':3,'QNP':1})

GC_313 = Coupling(name = 'GC_313',
                  value = '(G*tdhgd1x1)/(4.*vev**2)',
                  order = {'QCD':1,'QED':2,'QNP':1})

GC_314 = Coupling(name = 'GC_314',
                  value = '-(complex(0,1)*G**2*tdhgd1x1)/(4.*vev**2)',
                  order = {'QCD':2,'QED':2,'QNP':1})

GC_315 = Coupling(name = 'GC_315',
                  value = '(G*tdhgd1x2)/(4.*vev**2)',
                  order = {'QCD':1,'QED':2,'QNP':1})

GC_316 = Coupling(name = 'GC_316',
                  value = '-(complex(0,1)*G**2*tdhgd1x2)/(4.*vev**2)',
                  order = {'QCD':2,'QED':2,'QNP':1})

GC_317 = Coupling(name = 'GC_317',
                  value = '(G*tdhgd1x3)/(4.*vev**2)',
                  order = {'QCD':1,'QED':2,'QNP':1})

GC_318 = Coupling(name = 'GC_318',
                  value = '-(complex(0,1)*G**2*tdhgd1x3)/(4.*vev**2)',
                  order = {'QCD':2,'QED':2,'QNP':1})

GC_319 = Coupling(name = 'GC_319',
                  value = '(G*tdhgd2x1)/(4.*vev**2)',
                  order = {'QCD':1,'QED':2,'QNP':1})

GC_320 = Coupling(name = 'GC_320',
                  value = '-(complex(0,1)*G**2*tdhgd2x1)/(4.*vev**2)',
                  order = {'QCD':2,'QED':2,'QNP':1})

GC_321 = Coupling(name = 'GC_321',
                  value = '(G*tdhgd2x2)/(4.*vev**2)',
                  order = {'QCD':1,'QED':2,'QNP':1})

GC_322 = Coupling(name = 'GC_322',
                  value = '-(complex(0,1)*G**2*tdhgd2x2)/(4.*vev**2)',
                  order = {'QCD':2,'QED':2,'QNP':1})

GC_323 = Coupling(name = 'GC_323',
                  value = '(G*tdhgd2x3)/(4.*vev**2)',
                  order = {'QCD':1,'QED':2,'QNP':1})

GC_324 = Coupling(name = 'GC_324',
                  value = '-(complex(0,1)*G**2*tdhgd2x3)/(4.*vev**2)',
                  order = {'QCD':2,'QED':2,'QNP':1})

GC_325 = Coupling(name = 'GC_325',
                  value = '(G*tdhgd3x1)/(4.*vev**2)',
                  order = {'QCD':1,'QED':2,'QNP':1})

GC_326 = Coupling(name = 'GC_326',
                  value = '-(complex(0,1)*G**2*tdhgd3x1)/(4.*vev**2)',
                  order = {'QCD':2,'QED':2,'QNP':1})

GC_327 = Coupling(name = 'GC_327',
                  value = '(G*tdhgd3x2)/(4.*vev**2)',
                  order = {'QCD':1,'QED':2,'QNP':1})

GC_328 = Coupling(name = 'GC_328',
                  value = '-(complex(0,1)*G**2*tdhgd3x2)/(4.*vev**2)',
                  order = {'QCD':2,'QED':2,'QNP':1})

GC_329 = Coupling(name = 'GC_329',
                  value = '(G*tdhgd3x3)/(4.*vev**2)',
                  order = {'QCD':1,'QED':2,'QNP':1})

GC_330 = Coupling(name = 'GC_330',
                  value = '-(complex(0,1)*G**2*tdhgd3x3)/(4.*vev**2)',
                  order = {'QCD':2,'QED':2,'QNP':1})

GC_331 = Coupling(name = 'GC_331',
                  value = '(G*tdhgu1x1)/(4.*vev**2)',
                  order = {'QCD':1,'QED':2,'QNP':1})

GC_332 = Coupling(name = 'GC_332',
                  value = '-(complex(0,1)*G**2*tdhgu1x1)/(4.*vev**2)',
                  order = {'QCD':2,'QED':2,'QNP':1})

GC_333 = Coupling(name = 'GC_333',
                  value = '(G*tdhgu1x2)/(4.*vev**2)',
                  order = {'QCD':1,'QED':2,'QNP':1})

GC_334 = Coupling(name = 'GC_334',
                  value = '-(complex(0,1)*G**2*tdhgu1x2)/(4.*vev**2)',
                  order = {'QCD':2,'QED':2,'QNP':1})

GC_335 = Coupling(name = 'GC_335',
                  value = '(G*tdhgu1x3)/(4.*vev**2)',
                  order = {'QCD':1,'QED':2,'QNP':1})

GC_336 = Coupling(name = 'GC_336',
                  value = '-(complex(0,1)*G**2*tdhgu1x3)/(4.*vev**2)',
                  order = {'QCD':2,'QED':2,'QNP':1})

GC_337 = Coupling(name = 'GC_337',
                  value = '(G*tdhgu2x1)/(4.*vev**2)',
                  order = {'QCD':1,'QED':2,'QNP':1})

GC_338 = Coupling(name = 'GC_338',
                  value = '-(complex(0,1)*G**2*tdhgu2x1)/(4.*vev**2)',
                  order = {'QCD':2,'QED':2,'QNP':1})

GC_339 = Coupling(name = 'GC_339',
                  value = '(G*tdhgu2x2)/(4.*vev**2)',
                  order = {'QCD':1,'QED':2,'QNP':1})

GC_340 = Coupling(name = 'GC_340',
                  value = '-(complex(0,1)*G**2*tdhgu2x2)/(4.*vev**2)',
                  order = {'QCD':2,'QED':2,'QNP':1})

GC_341 = Coupling(name = 'GC_341',
                  value = '(G*tdhgu2x3)/(4.*vev**2)',
                  order = {'QCD':1,'QED':2,'QNP':1})

GC_342 = Coupling(name = 'GC_342',
                  value = '-(complex(0,1)*G**2*tdhgu2x3)/(4.*vev**2)',
                  order = {'QCD':2,'QED':2,'QNP':1})

GC_343 = Coupling(name = 'GC_343',
                  value = '(G*tdhgu3x1)/(4.*vev**2)',
                  order = {'QCD':1,'QED':2,'QNP':1})

GC_344 = Coupling(name = 'GC_344',
                  value = '-(complex(0,1)*G**2*tdhgu3x1)/(4.*vev**2)',
                  order = {'QCD':2,'QED':2,'QNP':1})

GC_345 = Coupling(name = 'GC_345',
                  value = '(G*tdhgu3x2)/(4.*vev**2)',
                  order = {'QCD':1,'QED':2,'QNP':1})

GC_346 = Coupling(name = 'GC_346',
                  value = '-(complex(0,1)*G**2*tdhgu3x2)/(4.*vev**2)',
                  order = {'QCD':2,'QED':2,'QNP':1})

GC_347 = Coupling(name = 'GC_347',
                  value = '(G*tdhgu3x3)/(4.*vev**2)',
                  order = {'QCD':1,'QED':2,'QNP':1})

GC_348 = Coupling(name = 'GC_348',
                  value = '-(complex(0,1)*G**2*tdhgu3x3)/(4.*vev**2)',
                  order = {'QCD':2,'QED':2,'QNP':1})

GC_349 = Coupling(name = 'GC_349',
                  value = '(ee*tdhzd1x1*cmath.sqrt(cw**2 + sw**2))/(4.*cw*sw*vev**2)',
                  order = {'QED':3,'QNP':1})

GC_350 = Coupling(name = 'GC_350',
                  value = '(ee*tdhzd1x2*cmath.sqrt(cw**2 + sw**2))/(4.*cw*sw*vev**2)',
                  order = {'QED':3,'QNP':1})

GC_351 = Coupling(name = 'GC_351',
                  value = '(ee*tdhzd1x3*cmath.sqrt(cw**2 + sw**2))/(4.*cw*sw*vev**2)',
                  order = {'QED':3,'QNP':1})

GC_352 = Coupling(name = 'GC_352',
                  value = '(ee*tdhzd2x1*cmath.sqrt(cw**2 + sw**2))/(4.*cw*sw*vev**2)',
                  order = {'QED':3,'QNP':1})

GC_353 = Coupling(name = 'GC_353',
                  value = '(ee*tdhzd2x2*cmath.sqrt(cw**2 + sw**2))/(4.*cw*sw*vev**2)',
                  order = {'QED':3,'QNP':1})

GC_354 = Coupling(name = 'GC_354',
                  value = '(ee*tdhzd2x3*cmath.sqrt(cw**2 + sw**2))/(4.*cw*sw*vev**2)',
                  order = {'QED':3,'QNP':1})

GC_355 = Coupling(name = 'GC_355',
                  value = '(ee*tdhzd3x1*cmath.sqrt(cw**2 + sw**2))/(4.*cw*sw*vev**2)',
                  order = {'QED':3,'QNP':1})

GC_356 = Coupling(name = 'GC_356',
                  value = '(ee*tdhzd3x2*cmath.sqrt(cw**2 + sw**2))/(4.*cw*sw*vev**2)',
                  order = {'QED':3,'QNP':1})

GC_357 = Coupling(name = 'GC_357',
                  value = '(ee*tdhzd3x3*cmath.sqrt(cw**2 + sw**2))/(4.*cw*sw*vev**2)',
                  order = {'QED':3,'QNP':1})

GC_358 = Coupling(name = 'GC_358',
                  value = '(ee*tdhze1x1*cmath.sqrt(cw**2 + sw**2))/(4.*cw*sw*vev**2)',
                  order = {'QED':3,'QNP':1})

GC_359 = Coupling(name = 'GC_359',
                  value = '(ee*tdhze1x2*cmath.sqrt(cw**2 + sw**2))/(4.*cw*sw*vev**2)',
                  order = {'QED':3,'QNP':1})

GC_360 = Coupling(name = 'GC_360',
                  value = '(ee*tdhze1x3*cmath.sqrt(cw**2 + sw**2))/(4.*cw*sw*vev**2)',
                  order = {'QED':3,'QNP':1})

GC_361 = Coupling(name = 'GC_361',
                  value = '(ee*tdhze2x1*cmath.sqrt(cw**2 + sw**2))/(4.*cw*sw*vev**2)',
                  order = {'QED':3,'QNP':1})

GC_362 = Coupling(name = 'GC_362',
                  value = '(ee*tdhze2x2*cmath.sqrt(cw**2 + sw**2))/(4.*cw*sw*vev**2)',
                  order = {'QED':3,'QNP':1})

GC_363 = Coupling(name = 'GC_363',
                  value = '(ee*tdhze2x3*cmath.sqrt(cw**2 + sw**2))/(4.*cw*sw*vev**2)',
                  order = {'QED':3,'QNP':1})

GC_364 = Coupling(name = 'GC_364',
                  value = '(ee*tdhze3x1*cmath.sqrt(cw**2 + sw**2))/(4.*cw*sw*vev**2)',
                  order = {'QED':3,'QNP':1})

GC_365 = Coupling(name = 'GC_365',
                  value = '(ee*tdhze3x2*cmath.sqrt(cw**2 + sw**2))/(4.*cw*sw*vev**2)',
                  order = {'QED':3,'QNP':1})

GC_366 = Coupling(name = 'GC_366',
                  value = '(ee*tdhze3x3*cmath.sqrt(cw**2 + sw**2))/(4.*cw*sw*vev**2)',
                  order = {'QED':3,'QNP':1})

GC_367 = Coupling(name = 'GC_367',
                  value = '(ee*tdhzu1x1*cmath.sqrt(cw**2 + sw**2))/(4.*cw*sw*vev**2)',
                  order = {'QED':3,'QNP':1})

GC_368 = Coupling(name = 'GC_368',
                  value = '(ee*tdhzu1x2*cmath.sqrt(cw**2 + sw**2))/(4.*cw*sw*vev**2)',
                  order = {'QED':3,'QNP':1})

GC_369 = Coupling(name = 'GC_369',
                  value = '(ee*tdhzu1x3*cmath.sqrt(cw**2 + sw**2))/(4.*cw*sw*vev**2)',
                  order = {'QED':3,'QNP':1})

GC_370 = Coupling(name = 'GC_370',
                  value = '(ee*tdhzu2x1*cmath.sqrt(cw**2 + sw**2))/(4.*cw*sw*vev**2)',
                  order = {'QED':3,'QNP':1})

GC_371 = Coupling(name = 'GC_371',
                  value = '(ee*tdhzu2x2*cmath.sqrt(cw**2 + sw**2))/(4.*cw*sw*vev**2)',
                  order = {'QED':3,'QNP':1})

GC_372 = Coupling(name = 'GC_372',
                  value = '(ee*tdhzu2x3*cmath.sqrt(cw**2 + sw**2))/(4.*cw*sw*vev**2)',
                  order = {'QED':3,'QNP':1})

GC_373 = Coupling(name = 'GC_373',
                  value = '(ee*tdhzu3x1*cmath.sqrt(cw**2 + sw**2))/(4.*cw*sw*vev**2)',
                  order = {'QED':3,'QNP':1})

GC_374 = Coupling(name = 'GC_374',
                  value = '(ee*tdhzu3x2*cmath.sqrt(cw**2 + sw**2))/(4.*cw*sw*vev**2)',
                  order = {'QED':3,'QNP':1})

GC_375 = Coupling(name = 'GC_375',
                  value = '(ee*tdhzu3x3*cmath.sqrt(cw**2 + sw**2))/(4.*cw*sw*vev**2)',
                  order = {'QED':3,'QNP':1})

GC_376 = Coupling(name = 'GC_376',
                  value = '(dad1x1*ee)/(4.*vev)',
                  order = {'QED':2,'QNP':1})

GC_377 = Coupling(name = 'GC_377',
                  value = '(dad1x2*ee)/(4.*vev)',
                  order = {'QED':2,'QNP':1})

GC_378 = Coupling(name = 'GC_378',
                  value = '(dad1x3*ee)/(4.*vev)',
                  order = {'QED':2,'QNP':1})

GC_379 = Coupling(name = 'GC_379',
                  value = '(dad2x1*ee)/(4.*vev)',
                  order = {'QED':2,'QNP':1})

GC_380 = Coupling(name = 'GC_380',
                  value = '(dad2x2*ee)/(4.*vev)',
                  order = {'QED':2,'QNP':1})

GC_381 = Coupling(name = 'GC_381',
                  value = '(dad2x3*ee)/(4.*vev)',
                  order = {'QED':2,'QNP':1})

GC_382 = Coupling(name = 'GC_382',
                  value = '(dad3x1*ee)/(4.*vev)',
                  order = {'QED':2,'QNP':1})

GC_383 = Coupling(name = 'GC_383',
                  value = '(dad3x2*ee)/(4.*vev)',
                  order = {'QED':2,'QNP':1})

GC_384 = Coupling(name = 'GC_384',
                  value = '(dad3x3*ee)/(4.*vev)',
                  order = {'QED':2,'QNP':1})

GC_385 = Coupling(name = 'GC_385',
                  value = '(dae1x1*ee)/(4.*vev)',
                  order = {'QED':2,'QNP':1})

GC_386 = Coupling(name = 'GC_386',
                  value = '(dae1x2*ee)/(4.*vev)',
                  order = {'QED':2,'QNP':1})

GC_387 = Coupling(name = 'GC_387',
                  value = '(dae1x3*ee)/(4.*vev)',
                  order = {'QED':2,'QNP':1})

GC_388 = Coupling(name = 'GC_388',
                  value = '(dae2x1*ee)/(4.*vev)',
                  order = {'QED':2,'QNP':1})

GC_389 = Coupling(name = 'GC_389',
                  value = '(dae2x2*ee)/(4.*vev)',
                  order = {'QED':2,'QNP':1})

GC_390 = Coupling(name = 'GC_390',
                  value = '(dae2x3*ee)/(4.*vev)',
                  order = {'QED':2,'QNP':1})

GC_391 = Coupling(name = 'GC_391',
                  value = '(dae3x1*ee)/(4.*vev)',
                  order = {'QED':2,'QNP':1})

GC_392 = Coupling(name = 'GC_392',
                  value = '(dae3x2*ee)/(4.*vev)',
                  order = {'QED':2,'QNP':1})

GC_393 = Coupling(name = 'GC_393',
                  value = '(dae3x3*ee)/(4.*vev)',
                  order = {'QED':2,'QNP':1})

GC_394 = Coupling(name = 'GC_394',
                  value = '(dau1x1*ee)/(4.*vev)',
                  order = {'QED':2,'QNP':1})

GC_395 = Coupling(name = 'GC_395',
                  value = '(dau1x2*ee)/(4.*vev)',
                  order = {'QED':2,'QNP':1})

GC_396 = Coupling(name = 'GC_396',
                  value = '(dau1x3*ee)/(4.*vev)',
                  order = {'QED':2,'QNP':1})

GC_397 = Coupling(name = 'GC_397',
                  value = '(dau2x1*ee)/(4.*vev)',
                  order = {'QED':2,'QNP':1})

GC_398 = Coupling(name = 'GC_398',
                  value = '(dau2x2*ee)/(4.*vev)',
                  order = {'QED':2,'QNP':1})

GC_399 = Coupling(name = 'GC_399',
                  value = '(dau2x3*ee)/(4.*vev)',
                  order = {'QED':2,'QNP':1})

GC_400 = Coupling(name = 'GC_400',
                  value = '(dau3x1*ee)/(4.*vev)',
                  order = {'QED':2,'QNP':1})

GC_401 = Coupling(name = 'GC_401',
                  value = '(dau3x2*ee)/(4.*vev)',
                  order = {'QED':2,'QNP':1})

GC_402 = Coupling(name = 'GC_402',
                  value = '(dau3x3*ee)/(4.*vev)',
                  order = {'QED':2,'QNP':1})

GC_403 = Coupling(name = 'GC_403',
                  value = '(Caa*ee**2*complex(0,1))/vev',
                  order = {'QED':3,'QNP':1})

GC_404 = Coupling(name = 'GC_404',
                  value = '(dgd1x1*G)/(4.*vev)',
                  order = {'QCD':1,'QED':1,'QNP':1})

GC_405 = Coupling(name = 'GC_405',
                  value = '(dgd1x2*G)/(4.*vev)',
                  order = {'QCD':1,'QED':1,'QNP':1})

GC_406 = Coupling(name = 'GC_406',
                  value = '(dgd1x3*G)/(4.*vev)',
                  order = {'QCD':1,'QED':1,'QNP':1})

GC_407 = Coupling(name = 'GC_407',
                  value = '(dgd2x1*G)/(4.*vev)',
                  order = {'QCD':1,'QED':1,'QNP':1})

GC_408 = Coupling(name = 'GC_408',
                  value = '(dgd2x2*G)/(4.*vev)',
                  order = {'QCD':1,'QED':1,'QNP':1})

GC_409 = Coupling(name = 'GC_409',
                  value = '(dgd2x3*G)/(4.*vev)',
                  order = {'QCD':1,'QED':1,'QNP':1})

GC_410 = Coupling(name = 'GC_410',
                  value = '(dgd3x1*G)/(4.*vev)',
                  order = {'QCD':1,'QED':1,'QNP':1})

GC_411 = Coupling(name = 'GC_411',
                  value = '(dgd3x2*G)/(4.*vev)',
                  order = {'QCD':1,'QED':1,'QNP':1})

GC_412 = Coupling(name = 'GC_412',
                  value = '(dgd3x3*G)/(4.*vev)',
                  order = {'QCD':1,'QED':1,'QNP':1})

GC_413 = Coupling(name = 'GC_413',
                  value = '(dgu1x1*G)/(4.*vev)',
                  order = {'QCD':1,'QED':1,'QNP':1})

GC_414 = Coupling(name = 'GC_414',
                  value = '(dgu1x2*G)/(4.*vev)',
                  order = {'QCD':1,'QED':1,'QNP':1})

GC_415 = Coupling(name = 'GC_415',
                  value = '(dgu1x3*G)/(4.*vev)',
                  order = {'QCD':1,'QED':1,'QNP':1})

GC_416 = Coupling(name = 'GC_416',
                  value = '(dgu2x1*G)/(4.*vev)',
                  order = {'QCD':1,'QED':1,'QNP':1})

GC_417 = Coupling(name = 'GC_417',
                  value = '(dgu2x2*G)/(4.*vev)',
                  order = {'QCD':1,'QED':1,'QNP':1})

GC_418 = Coupling(name = 'GC_418',
                  value = '(dgu2x3*G)/(4.*vev)',
                  order = {'QCD':1,'QED':1,'QNP':1})

GC_419 = Coupling(name = 'GC_419',
                  value = '(dgu3x1*G)/(4.*vev)',
                  order = {'QCD':1,'QED':1,'QNP':1})

GC_420 = Coupling(name = 'GC_420',
                  value = '(dgu3x2*G)/(4.*vev)',
                  order = {'QCD':1,'QED':1,'QNP':1})

GC_421 = Coupling(name = 'GC_421',
                  value = '(dgu3x3*G)/(4.*vev)',
                  order = {'QCD':1,'QED':1,'QNP':1})

GC_422 = Coupling(name = 'GC_422',
                  value = '(Cgg*complex(0,1)*G**2)/vev',
                  order = {'QCD':2,'QED':1,'QNP':1})

GC_423 = Coupling(name = 'GC_423',
                  value = '-(dgd1x1*complex(0,1)*G**2)/(4.*vev)',
                  order = {'QCD':2,'QED':1,'QNP':1})

GC_424 = Coupling(name = 'GC_424',
                  value = '-(dgd1x2*complex(0,1)*G**2)/(4.*vev)',
                  order = {'QCD':2,'QED':1,'QNP':1})

GC_425 = Coupling(name = 'GC_425',
                  value = '-(dgd1x3*complex(0,1)*G**2)/(4.*vev)',
                  order = {'QCD':2,'QED':1,'QNP':1})

GC_426 = Coupling(name = 'GC_426',
                  value = '-(dgd2x1*complex(0,1)*G**2)/(4.*vev)',
                  order = {'QCD':2,'QED':1,'QNP':1})

GC_427 = Coupling(name = 'GC_427',
                  value = '-(dgd2x2*complex(0,1)*G**2)/(4.*vev)',
                  order = {'QCD':2,'QED':1,'QNP':1})

GC_428 = Coupling(name = 'GC_428',
                  value = '-(dgd2x3*complex(0,1)*G**2)/(4.*vev)',
                  order = {'QCD':2,'QED':1,'QNP':1})

GC_429 = Coupling(name = 'GC_429',
                  value = '-(dgd3x1*complex(0,1)*G**2)/(4.*vev)',
                  order = {'QCD':2,'QED':1,'QNP':1})

GC_430 = Coupling(name = 'GC_430',
                  value = '-(dgd3x2*complex(0,1)*G**2)/(4.*vev)',
                  order = {'QCD':2,'QED':1,'QNP':1})

GC_431 = Coupling(name = 'GC_431',
                  value = '-(dgd3x3*complex(0,1)*G**2)/(4.*vev)',
                  order = {'QCD':2,'QED':1,'QNP':1})

GC_432 = Coupling(name = 'GC_432',
                  value = '-(dgu1x1*complex(0,1)*G**2)/(4.*vev)',
                  order = {'QCD':2,'QED':1,'QNP':1})

GC_433 = Coupling(name = 'GC_433',
                  value = '-(dgu1x2*complex(0,1)*G**2)/(4.*vev)',
                  order = {'QCD':2,'QED':1,'QNP':1})

GC_434 = Coupling(name = 'GC_434',
                  value = '-(dgu1x3*complex(0,1)*G**2)/(4.*vev)',
                  order = {'QCD':2,'QED':1,'QNP':1})

GC_435 = Coupling(name = 'GC_435',
                  value = '-(dgu2x1*complex(0,1)*G**2)/(4.*vev)',
                  order = {'QCD':2,'QED':1,'QNP':1})

GC_436 = Coupling(name = 'GC_436',
                  value = '-(dgu2x2*complex(0,1)*G**2)/(4.*vev)',
                  order = {'QCD':2,'QED':1,'QNP':1})

GC_437 = Coupling(name = 'GC_437',
                  value = '-(dgu2x3*complex(0,1)*G**2)/(4.*vev)',
                  order = {'QCD':2,'QED':1,'QNP':1})

GC_438 = Coupling(name = 'GC_438',
                  value = '-(dgu3x1*complex(0,1)*G**2)/(4.*vev)',
                  order = {'QCD':2,'QED':1,'QNP':1})

GC_439 = Coupling(name = 'GC_439',
                  value = '-(dgu3x2*complex(0,1)*G**2)/(4.*vev)',
                  order = {'QCD':2,'QED':1,'QNP':1})

GC_440 = Coupling(name = 'GC_440',
                  value = '-(dgu3x3*complex(0,1)*G**2)/(4.*vev)',
                  order = {'QCD':2,'QED':1,'QNP':1})

GC_441 = Coupling(name = 'GC_441',
                  value = '(Cgg*G**3)/vev',
                  order = {'QCD':3,'QED':1,'QNP':1})

GC_442 = Coupling(name = 'GC_442',
                  value = '-((Cgg*complex(0,1)*G**4)/vev)',
                  order = {'QCD':4,'QED':1,'QNP':1})

GC_443 = Coupling(name = 'GC_443',
                  value = '(ee**2*complex(0,1)*gHaa)/vev',
                  order = {'QED':3})

GC_444 = Coupling(name = 'GC_444',
                  value = '(complex(0,1)*G**2*gHgg)/vev',
                  order = {'QCD':2,'QED':1})

GC_445 = Coupling(name = 'GC_445',
                  value = '(G**3*gHgg)/vev',
                  order = {'QCD':3,'QED':1})

GC_446 = Coupling(name = 'GC_446',
                  value = '-((complex(0,1)*G**4*gHgg)/vev)',
                  order = {'QCD':4,'QED':1})

GC_447 = Coupling(name = 'GC_447',
                  value = '(ee**2*complex(0,1)*gHza)/vev',
                  order = {'QED':3})

GC_448 = Coupling(name = 'GC_448',
                  value = '-((Cd3x3*dYd3x3*complex(0,1)*MB)/vev)',
                  order = {'QED':1,'QNP':1})

GC_449 = Coupling(name = 'GC_449',
                  value = '-((Cu3x3*dYu3x3*complex(0,1)*MT)/vev)',
                  order = {'QED':1,'QNP':1})

GC_450 = Coupling(name = 'GC_450',
                  value = '-((Ce3x3*dYe3x3*complex(0,1)*MTA)/vev)',
                  order = {'QED':1,'QNP':1})

GC_451 = Coupling(name = 'GC_451',
                  value = '(2*dCw*complex(0,1)*MW**2)/vev',
                  order = {'QED':1,'QNP':1})

GC_452 = Coupling(name = 'GC_452',
                  value = '(2*dCz*complex(0,1)*MZ**2)/vev',
                  order = {'QED':1,'QNP':1})

GC_453 = Coupling(name = 'GC_453',
                  value = '-((dYd3x3*MB*Sd3x3)/vev)',
                  order = {'QED':1,'QNP':1})

GC_454 = Coupling(name = 'GC_454',
                  value = '-((dYe3x3*MTA*Se3x3)/vev)',
                  order = {'QED':1,'QNP':1})

GC_455 = Coupling(name = 'GC_455',
                  value = '-((dYu3x3*MT*Su3x3)/vev)',
                  order = {'QED':1,'QNP':1})

GC_456 = Coupling(name = 'GC_456',
                  value = '-((Cwbx*ee**2*complex(0,1))/(sw**2*vev))',
                  order = {'QED':3,'QNP':1})

GC_457 = Coupling(name = 'GC_457',
                  value = '(Cww*ee**2*complex(0,1))/(sw**2*vev)',
                  order = {'QED':3,'QNP':1})

GC_458 = Coupling(name = 'GC_458',
                  value = '-((Czbx*ee**2*complex(0,1))/(sw**2*vev))',
                  order = {'QED':3,'QNP':1})

GC_459 = Coupling(name = 'GC_459',
                  value = '(Czz*ee**2*complex(0,1))/(cw**2*sw**2*vev)',
                  order = {'QED':3,'QNP':1})

GC_460 = Coupling(name = 'GC_460',
                  value = '(dGLhwl1x1*ee*complex(0,1)*cmath.sqrt(2))/(sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_461 = Coupling(name = 'GC_461',
                  value = '(dGLhwl1x2*ee*complex(0,1)*cmath.sqrt(2))/(sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_462 = Coupling(name = 'GC_462',
                  value = '(dGLhwl1x3*ee*complex(0,1)*cmath.sqrt(2))/(sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_463 = Coupling(name = 'GC_463',
                  value = '(dGLhwl2x1*ee*complex(0,1)*cmath.sqrt(2))/(sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_464 = Coupling(name = 'GC_464',
                  value = '(dGLhwl2x2*ee*complex(0,1)*cmath.sqrt(2))/(sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_465 = Coupling(name = 'GC_465',
                  value = '(dGLhwl2x3*ee*complex(0,1)*cmath.sqrt(2))/(sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_466 = Coupling(name = 'GC_466',
                  value = '(dGLhwl3x1*ee*complex(0,1)*cmath.sqrt(2))/(sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_467 = Coupling(name = 'GC_467',
                  value = '(dGLhwl3x2*ee*complex(0,1)*cmath.sqrt(2))/(sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_468 = Coupling(name = 'GC_468',
                  value = '(dGLhwl3x3*ee*complex(0,1)*cmath.sqrt(2))/(sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_469 = Coupling(name = 'GC_469',
                  value = '(dGLhwq1x1*ee*complex(0,1)*cmath.sqrt(2))/(sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_470 = Coupling(name = 'GC_470',
                  value = '(dGLhwq1x2*ee*complex(0,1)*cmath.sqrt(2))/(sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_471 = Coupling(name = 'GC_471',
                  value = '(dGLhwq1x3*ee*complex(0,1)*cmath.sqrt(2))/(sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_472 = Coupling(name = 'GC_472',
                  value = '(dGLhwq2x1*ee*complex(0,1)*cmath.sqrt(2))/(sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_473 = Coupling(name = 'GC_473',
                  value = '(dGLhwq2x2*ee*complex(0,1)*cmath.sqrt(2))/(sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_474 = Coupling(name = 'GC_474',
                  value = '(dGLhwq2x3*ee*complex(0,1)*cmath.sqrt(2))/(sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_475 = Coupling(name = 'GC_475',
                  value = '(dGLhwq3x1*ee*complex(0,1)*cmath.sqrt(2))/(sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_476 = Coupling(name = 'GC_476',
                  value = '(dGLhwq3x2*ee*complex(0,1)*cmath.sqrt(2))/(sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_477 = Coupling(name = 'GC_477',
                  value = '(dGLhwq3x3*ee*complex(0,1)*cmath.sqrt(2))/(sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_478 = Coupling(name = 'GC_478',
                  value = '(dGRhwq1x1*ee*complex(0,1)*cmath.sqrt(2))/(sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_479 = Coupling(name = 'GC_479',
                  value = '(dGRhwq1x2*ee*complex(0,1)*cmath.sqrt(2))/(sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_480 = Coupling(name = 'GC_480',
                  value = '(dGRhwq1x3*ee*complex(0,1)*cmath.sqrt(2))/(sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_481 = Coupling(name = 'GC_481',
                  value = '(dGRhwq2x1*ee*complex(0,1)*cmath.sqrt(2))/(sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_482 = Coupling(name = 'GC_482',
                  value = '(dGRhwq2x2*ee*complex(0,1)*cmath.sqrt(2))/(sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_483 = Coupling(name = 'GC_483',
                  value = '(dGRhwq2x3*ee*complex(0,1)*cmath.sqrt(2))/(sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_484 = Coupling(name = 'GC_484',
                  value = '(dGRhwq3x1*ee*complex(0,1)*cmath.sqrt(2))/(sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_485 = Coupling(name = 'GC_485',
                  value = '(dGRhwq3x2*ee*complex(0,1)*cmath.sqrt(2))/(sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_486 = Coupling(name = 'GC_486',
                  value = '(dGRhwq3x3*ee*complex(0,1)*cmath.sqrt(2))/(sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_487 = Coupling(name = 'GC_487',
                  value = '(dwl1x1*ee)/(2.*sw*vev*cmath.sqrt(2))',
                  order = {'QED':2,'QNP':1})

GC_488 = Coupling(name = 'GC_488',
                  value = '(dwl1x2*ee)/(2.*sw*vev*cmath.sqrt(2))',
                  order = {'QED':2,'QNP':1})

GC_489 = Coupling(name = 'GC_489',
                  value = '(dwl1x3*ee)/(2.*sw*vev*cmath.sqrt(2))',
                  order = {'QED':2,'QNP':1})

GC_490 = Coupling(name = 'GC_490',
                  value = '(dwl2x1*ee)/(2.*sw*vev*cmath.sqrt(2))',
                  order = {'QED':2,'QNP':1})

GC_491 = Coupling(name = 'GC_491',
                  value = '(dwl2x2*ee)/(2.*sw*vev*cmath.sqrt(2))',
                  order = {'QED':2,'QNP':1})

GC_492 = Coupling(name = 'GC_492',
                  value = '(dwl2x3*ee)/(2.*sw*vev*cmath.sqrt(2))',
                  order = {'QED':2,'QNP':1})

GC_493 = Coupling(name = 'GC_493',
                  value = '(dwl3x1*ee)/(2.*sw*vev*cmath.sqrt(2))',
                  order = {'QED':2,'QNP':1})

GC_494 = Coupling(name = 'GC_494',
                  value = '(dwl3x2*ee)/(2.*sw*vev*cmath.sqrt(2))',
                  order = {'QED':2,'QNP':1})

GC_495 = Coupling(name = 'GC_495',
                  value = '(dwl3x3*ee)/(2.*sw*vev*cmath.sqrt(2))',
                  order = {'QED':2,'QNP':1})

GC_496 = Coupling(name = 'GC_496',
                  value = '-((Cabx*ee**2*complex(0,1))/(cw*sw*vev))',
                  order = {'QED':3,'QNP':1})

GC_497 = Coupling(name = 'GC_497',
                  value = '(Cza*ee**2*complex(0,1))/(cw*sw*vev)',
                  order = {'QED':3,'QNP':1})

GC_498 = Coupling(name = 'GC_498',
                  value = '(2*dGLhzd1x1*ee*complex(0,1)*cmath.sqrt(cw**2 + sw**2))/(cw*sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_499 = Coupling(name = 'GC_499',
                  value = '(2*dGLhzd1x2*ee*complex(0,1)*cmath.sqrt(cw**2 + sw**2))/(cw*sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_500 = Coupling(name = 'GC_500',
                  value = '(2*dGLhzd1x3*ee*complex(0,1)*cmath.sqrt(cw**2 + sw**2))/(cw*sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_501 = Coupling(name = 'GC_501',
                  value = '(2*dGLhzd2x1*ee*complex(0,1)*cmath.sqrt(cw**2 + sw**2))/(cw*sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_502 = Coupling(name = 'GC_502',
                  value = '(2*dGLhzd2x2*ee*complex(0,1)*cmath.sqrt(cw**2 + sw**2))/(cw*sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_503 = Coupling(name = 'GC_503',
                  value = '(2*dGLhzd2x3*ee*complex(0,1)*cmath.sqrt(cw**2 + sw**2))/(cw*sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_504 = Coupling(name = 'GC_504',
                  value = '(2*dGLhzd3x1*ee*complex(0,1)*cmath.sqrt(cw**2 + sw**2))/(cw*sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_505 = Coupling(name = 'GC_505',
                  value = '(2*dGLhzd3x2*ee*complex(0,1)*cmath.sqrt(cw**2 + sw**2))/(cw*sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_506 = Coupling(name = 'GC_506',
                  value = '(2*dGLhzd3x3*ee*complex(0,1)*cmath.sqrt(cw**2 + sw**2))/(cw*sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_507 = Coupling(name = 'GC_507',
                  value = '(2*dGLhze1x1*ee*complex(0,1)*cmath.sqrt(cw**2 + sw**2))/(cw*sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_508 = Coupling(name = 'GC_508',
                  value = '(2*dGLhze1x2*ee*complex(0,1)*cmath.sqrt(cw**2 + sw**2))/(cw*sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_509 = Coupling(name = 'GC_509',
                  value = '(2*dGLhze1x3*ee*complex(0,1)*cmath.sqrt(cw**2 + sw**2))/(cw*sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_510 = Coupling(name = 'GC_510',
                  value = '(2*dGLhze2x1*ee*complex(0,1)*cmath.sqrt(cw**2 + sw**2))/(cw*sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_511 = Coupling(name = 'GC_511',
                  value = '(2*dGLhze2x2*ee*complex(0,1)*cmath.sqrt(cw**2 + sw**2))/(cw*sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_512 = Coupling(name = 'GC_512',
                  value = '(2*dGLhze2x3*ee*complex(0,1)*cmath.sqrt(cw**2 + sw**2))/(cw*sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_513 = Coupling(name = 'GC_513',
                  value = '(2*dGLhze3x1*ee*complex(0,1)*cmath.sqrt(cw**2 + sw**2))/(cw*sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_514 = Coupling(name = 'GC_514',
                  value = '(2*dGLhze3x2*ee*complex(0,1)*cmath.sqrt(cw**2 + sw**2))/(cw*sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_515 = Coupling(name = 'GC_515',
                  value = '(2*dGLhze3x3*ee*complex(0,1)*cmath.sqrt(cw**2 + sw**2))/(cw*sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_516 = Coupling(name = 'GC_516',
                  value = '(2*dGLhzu1x1*ee*complex(0,1)*cmath.sqrt(cw**2 + sw**2))/(cw*sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_517 = Coupling(name = 'GC_517',
                  value = '(2*dGLhzu1x2*ee*complex(0,1)*cmath.sqrt(cw**2 + sw**2))/(cw*sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_518 = Coupling(name = 'GC_518',
                  value = '(2*dGLhzu1x3*ee*complex(0,1)*cmath.sqrt(cw**2 + sw**2))/(cw*sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_519 = Coupling(name = 'GC_519',
                  value = '(2*dGLhzu2x1*ee*complex(0,1)*cmath.sqrt(cw**2 + sw**2))/(cw*sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_520 = Coupling(name = 'GC_520',
                  value = '(2*dGLhzu2x2*ee*complex(0,1)*cmath.sqrt(cw**2 + sw**2))/(cw*sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_521 = Coupling(name = 'GC_521',
                  value = '(2*dGLhzu2x3*ee*complex(0,1)*cmath.sqrt(cw**2 + sw**2))/(cw*sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_522 = Coupling(name = 'GC_522',
                  value = '(2*dGLhzu3x1*ee*complex(0,1)*cmath.sqrt(cw**2 + sw**2))/(cw*sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_523 = Coupling(name = 'GC_523',
                  value = '(2*dGLhzu3x2*ee*complex(0,1)*cmath.sqrt(cw**2 + sw**2))/(cw*sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_524 = Coupling(name = 'GC_524',
                  value = '(2*dGLhzu3x3*ee*complex(0,1)*cmath.sqrt(cw**2 + sw**2))/(cw*sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_525 = Coupling(name = 'GC_525',
                  value = '(2*dGLhzv1x1*ee*complex(0,1)*cmath.sqrt(cw**2 + sw**2))/(cw*sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_526 = Coupling(name = 'GC_526',
                  value = '(2*dGLhzv1x2*ee*complex(0,1)*cmath.sqrt(cw**2 + sw**2))/(cw*sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_527 = Coupling(name = 'GC_527',
                  value = '(2*dGLhzv1x3*ee*complex(0,1)*cmath.sqrt(cw**2 + sw**2))/(cw*sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_528 = Coupling(name = 'GC_528',
                  value = '(2*dGLhzv2x1*ee*complex(0,1)*cmath.sqrt(cw**2 + sw**2))/(cw*sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_529 = Coupling(name = 'GC_529',
                  value = '(2*dGLhzv2x2*ee*complex(0,1)*cmath.sqrt(cw**2 + sw**2))/(cw*sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_530 = Coupling(name = 'GC_530',
                  value = '(2*dGLhzv2x3*ee*complex(0,1)*cmath.sqrt(cw**2 + sw**2))/(cw*sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_531 = Coupling(name = 'GC_531',
                  value = '(2*dGLhzv3x1*ee*complex(0,1)*cmath.sqrt(cw**2 + sw**2))/(cw*sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_532 = Coupling(name = 'GC_532',
                  value = '(2*dGLhzv3x2*ee*complex(0,1)*cmath.sqrt(cw**2 + sw**2))/(cw*sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_533 = Coupling(name = 'GC_533',
                  value = '(2*dGLhzv3x3*ee*complex(0,1)*cmath.sqrt(cw**2 + sw**2))/(cw*sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_534 = Coupling(name = 'GC_534',
                  value = '(2*dGRhzd1x1*ee*complex(0,1)*cmath.sqrt(cw**2 + sw**2))/(cw*sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_535 = Coupling(name = 'GC_535',
                  value = '(2*dGRhzd1x2*ee*complex(0,1)*cmath.sqrt(cw**2 + sw**2))/(cw*sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_536 = Coupling(name = 'GC_536',
                  value = '(2*dGRhzd1x3*ee*complex(0,1)*cmath.sqrt(cw**2 + sw**2))/(cw*sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_537 = Coupling(name = 'GC_537',
                  value = '(2*dGRhzd2x1*ee*complex(0,1)*cmath.sqrt(cw**2 + sw**2))/(cw*sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_538 = Coupling(name = 'GC_538',
                  value = '(2*dGRhzd2x2*ee*complex(0,1)*cmath.sqrt(cw**2 + sw**2))/(cw*sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_539 = Coupling(name = 'GC_539',
                  value = '(2*dGRhzd2x3*ee*complex(0,1)*cmath.sqrt(cw**2 + sw**2))/(cw*sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_540 = Coupling(name = 'GC_540',
                  value = '(2*dGRhzd3x1*ee*complex(0,1)*cmath.sqrt(cw**2 + sw**2))/(cw*sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_541 = Coupling(name = 'GC_541',
                  value = '(2*dGRhzd3x2*ee*complex(0,1)*cmath.sqrt(cw**2 + sw**2))/(cw*sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_542 = Coupling(name = 'GC_542',
                  value = '(2*dGRhzd3x3*ee*complex(0,1)*cmath.sqrt(cw**2 + sw**2))/(cw*sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_543 = Coupling(name = 'GC_543',
                  value = '(2*dGRhze1x1*ee*complex(0,1)*cmath.sqrt(cw**2 + sw**2))/(cw*sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_544 = Coupling(name = 'GC_544',
                  value = '(2*dGRhze1x2*ee*complex(0,1)*cmath.sqrt(cw**2 + sw**2))/(cw*sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_545 = Coupling(name = 'GC_545',
                  value = '(2*dGRhze1x3*ee*complex(0,1)*cmath.sqrt(cw**2 + sw**2))/(cw*sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_546 = Coupling(name = 'GC_546',
                  value = '(2*dGRhze2x1*ee*complex(0,1)*cmath.sqrt(cw**2 + sw**2))/(cw*sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_547 = Coupling(name = 'GC_547',
                  value = '(2*dGRhze2x2*ee*complex(0,1)*cmath.sqrt(cw**2 + sw**2))/(cw*sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_548 = Coupling(name = 'GC_548',
                  value = '(2*dGRhze2x3*ee*complex(0,1)*cmath.sqrt(cw**2 + sw**2))/(cw*sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_549 = Coupling(name = 'GC_549',
                  value = '(2*dGRhze3x1*ee*complex(0,1)*cmath.sqrt(cw**2 + sw**2))/(cw*sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_550 = Coupling(name = 'GC_550',
                  value = '(2*dGRhze3x2*ee*complex(0,1)*cmath.sqrt(cw**2 + sw**2))/(cw*sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_551 = Coupling(name = 'GC_551',
                  value = '(2*dGRhze3x3*ee*complex(0,1)*cmath.sqrt(cw**2 + sw**2))/(cw*sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_552 = Coupling(name = 'GC_552',
                  value = '(2*dGRhzu1x1*ee*complex(0,1)*cmath.sqrt(cw**2 + sw**2))/(cw*sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_553 = Coupling(name = 'GC_553',
                  value = '(2*dGRhzu1x2*ee*complex(0,1)*cmath.sqrt(cw**2 + sw**2))/(cw*sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_554 = Coupling(name = 'GC_554',
                  value = '(2*dGRhzu1x3*ee*complex(0,1)*cmath.sqrt(cw**2 + sw**2))/(cw*sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_555 = Coupling(name = 'GC_555',
                  value = '(2*dGRhzu2x1*ee*complex(0,1)*cmath.sqrt(cw**2 + sw**2))/(cw*sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_556 = Coupling(name = 'GC_556',
                  value = '(2*dGRhzu2x2*ee*complex(0,1)*cmath.sqrt(cw**2 + sw**2))/(cw*sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_557 = Coupling(name = 'GC_557',
                  value = '(2*dGRhzu2x3*ee*complex(0,1)*cmath.sqrt(cw**2 + sw**2))/(cw*sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_558 = Coupling(name = 'GC_558',
                  value = '(2*dGRhzu3x1*ee*complex(0,1)*cmath.sqrt(cw**2 + sw**2))/(cw*sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_559 = Coupling(name = 'GC_559',
                  value = '(2*dGRhzu3x2*ee*complex(0,1)*cmath.sqrt(cw**2 + sw**2))/(cw*sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_560 = Coupling(name = 'GC_560',
                  value = '(2*dGRhzu3x3*ee*complex(0,1)*cmath.sqrt(cw**2 + sw**2))/(cw*sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_561 = Coupling(name = 'GC_561',
                  value = '(dzd1x1*ee*cmath.sqrt(cw**2 + sw**2))/(4.*cw*sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_562 = Coupling(name = 'GC_562',
                  value = '(dzd1x2*ee*cmath.sqrt(cw**2 + sw**2))/(4.*cw*sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_563 = Coupling(name = 'GC_563',
                  value = '(dzd1x3*ee*cmath.sqrt(cw**2 + sw**2))/(4.*cw*sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_564 = Coupling(name = 'GC_564',
                  value = '(dzd2x1*ee*cmath.sqrt(cw**2 + sw**2))/(4.*cw*sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_565 = Coupling(name = 'GC_565',
                  value = '(dzd2x2*ee*cmath.sqrt(cw**2 + sw**2))/(4.*cw*sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_566 = Coupling(name = 'GC_566',
                  value = '(dzd2x3*ee*cmath.sqrt(cw**2 + sw**2))/(4.*cw*sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_567 = Coupling(name = 'GC_567',
                  value = '(dzd3x1*ee*cmath.sqrt(cw**2 + sw**2))/(4.*cw*sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_568 = Coupling(name = 'GC_568',
                  value = '(dzd3x2*ee*cmath.sqrt(cw**2 + sw**2))/(4.*cw*sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_569 = Coupling(name = 'GC_569',
                  value = '(dzd3x3*ee*cmath.sqrt(cw**2 + sw**2))/(4.*cw*sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_570 = Coupling(name = 'GC_570',
                  value = '(dze1x1*ee*cmath.sqrt(cw**2 + sw**2))/(4.*cw*sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_571 = Coupling(name = 'GC_571',
                  value = '(dze1x2*ee*cmath.sqrt(cw**2 + sw**2))/(4.*cw*sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_572 = Coupling(name = 'GC_572',
                  value = '(dze1x3*ee*cmath.sqrt(cw**2 + sw**2))/(4.*cw*sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_573 = Coupling(name = 'GC_573',
                  value = '(dze2x1*ee*cmath.sqrt(cw**2 + sw**2))/(4.*cw*sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_574 = Coupling(name = 'GC_574',
                  value = '(dze2x2*ee*cmath.sqrt(cw**2 + sw**2))/(4.*cw*sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_575 = Coupling(name = 'GC_575',
                  value = '(dze2x3*ee*cmath.sqrt(cw**2 + sw**2))/(4.*cw*sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_576 = Coupling(name = 'GC_576',
                  value = '(dze3x1*ee*cmath.sqrt(cw**2 + sw**2))/(4.*cw*sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_577 = Coupling(name = 'GC_577',
                  value = '(dze3x2*ee*cmath.sqrt(cw**2 + sw**2))/(4.*cw*sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_578 = Coupling(name = 'GC_578',
                  value = '(dze3x3*ee*cmath.sqrt(cw**2 + sw**2))/(4.*cw*sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_579 = Coupling(name = 'GC_579',
                  value = '(dzu1x1*ee*cmath.sqrt(cw**2 + sw**2))/(4.*cw*sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_580 = Coupling(name = 'GC_580',
                  value = '(dzu1x2*ee*cmath.sqrt(cw**2 + sw**2))/(4.*cw*sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_581 = Coupling(name = 'GC_581',
                  value = '(dzu1x3*ee*cmath.sqrt(cw**2 + sw**2))/(4.*cw*sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_582 = Coupling(name = 'GC_582',
                  value = '(dzu2x1*ee*cmath.sqrt(cw**2 + sw**2))/(4.*cw*sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_583 = Coupling(name = 'GC_583',
                  value = '(dzu2x2*ee*cmath.sqrt(cw**2 + sw**2))/(4.*cw*sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_584 = Coupling(name = 'GC_584',
                  value = '(dzu2x3*ee*cmath.sqrt(cw**2 + sw**2))/(4.*cw*sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_585 = Coupling(name = 'GC_585',
                  value = '(dzu3x1*ee*cmath.sqrt(cw**2 + sw**2))/(4.*cw*sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_586 = Coupling(name = 'GC_586',
                  value = '(dzu3x2*ee*cmath.sqrt(cw**2 + sw**2))/(4.*cw*sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_587 = Coupling(name = 'GC_587',
                  value = '(dzu3x3*ee*cmath.sqrt(cw**2 + sw**2))/(4.*cw*sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_588 = Coupling(name = 'GC_588',
                  value = '(ee**2*complex(0,1)*tCaa)/vev',
                  order = {'QED':3,'QNP':1})

GC_589 = Coupling(name = 'GC_589',
                  value = '(complex(0,1)*G**2*tCgg)/vev',
                  order = {'QCD':2,'QED':1,'QNP':1})

GC_590 = Coupling(name = 'GC_590',
                  value = '-((G**3*tCgg)/vev)',
                  order = {'QCD':3,'QED':1,'QNP':1})

GC_591 = Coupling(name = 'GC_591',
                  value = '(ee**2*complex(0,1)*tCww)/(sw**2*vev)',
                  order = {'QED':3,'QNP':1})

GC_592 = Coupling(name = 'GC_592',
                  value = '(ee**2*complex(0,1)*tCza)/(cw*sw*vev)',
                  order = {'QED':3,'QNP':1})

GC_593 = Coupling(name = 'GC_593',
                  value = '(ee**2*complex(0,1)*tCzz)/(cw**2*sw**2*vev)',
                  order = {'QED':3,'QNP':1})

GC_594 = Coupling(name = 'GC_594',
                  value = '(ee*tdad1x1)/(4.*vev)',
                  order = {'QED':2,'QNP':1})

GC_595 = Coupling(name = 'GC_595',
                  value = '(ee*tdad1x2)/(4.*vev)',
                  order = {'QED':2,'QNP':1})

GC_596 = Coupling(name = 'GC_596',
                  value = '(ee*tdad1x3)/(4.*vev)',
                  order = {'QED':2,'QNP':1})

GC_597 = Coupling(name = 'GC_597',
                  value = '(ee*tdad2x1)/(4.*vev)',
                  order = {'QED':2,'QNP':1})

GC_598 = Coupling(name = 'GC_598',
                  value = '(ee*tdad2x2)/(4.*vev)',
                  order = {'QED':2,'QNP':1})

GC_599 = Coupling(name = 'GC_599',
                  value = '(ee*tdad2x3)/(4.*vev)',
                  order = {'QED':2,'QNP':1})

GC_600 = Coupling(name = 'GC_600',
                  value = '(ee*tdad3x1)/(4.*vev)',
                  order = {'QED':2,'QNP':1})

GC_601 = Coupling(name = 'GC_601',
                  value = '(ee*tdad3x2)/(4.*vev)',
                  order = {'QED':2,'QNP':1})

GC_602 = Coupling(name = 'GC_602',
                  value = '(ee*tdad3x3)/(4.*vev)',
                  order = {'QED':2,'QNP':1})

GC_603 = Coupling(name = 'GC_603',
                  value = '(ee*tdae1x1)/(4.*vev)',
                  order = {'QED':2,'QNP':1})

GC_604 = Coupling(name = 'GC_604',
                  value = '(ee*tdae1x2)/(4.*vev)',
                  order = {'QED':2,'QNP':1})

GC_605 = Coupling(name = 'GC_605',
                  value = '(ee*tdae1x3)/(4.*vev)',
                  order = {'QED':2,'QNP':1})

GC_606 = Coupling(name = 'GC_606',
                  value = '(ee*tdae2x1)/(4.*vev)',
                  order = {'QED':2,'QNP':1})

GC_607 = Coupling(name = 'GC_607',
                  value = '(ee*tdae2x2)/(4.*vev)',
                  order = {'QED':2,'QNP':1})

GC_608 = Coupling(name = 'GC_608',
                  value = '(ee*tdae2x3)/(4.*vev)',
                  order = {'QED':2,'QNP':1})

GC_609 = Coupling(name = 'GC_609',
                  value = '(ee*tdae3x1)/(4.*vev)',
                  order = {'QED':2,'QNP':1})

GC_610 = Coupling(name = 'GC_610',
                  value = '(ee*tdae3x2)/(4.*vev)',
                  order = {'QED':2,'QNP':1})

GC_611 = Coupling(name = 'GC_611',
                  value = '(ee*tdae3x3)/(4.*vev)',
                  order = {'QED':2,'QNP':1})

GC_612 = Coupling(name = 'GC_612',
                  value = '(ee*tdau1x1)/(4.*vev)',
                  order = {'QED':2,'QNP':1})

GC_613 = Coupling(name = 'GC_613',
                  value = '(ee*tdau1x2)/(4.*vev)',
                  order = {'QED':2,'QNP':1})

GC_614 = Coupling(name = 'GC_614',
                  value = '(ee*tdau1x3)/(4.*vev)',
                  order = {'QED':2,'QNP':1})

GC_615 = Coupling(name = 'GC_615',
                  value = '(ee*tdau2x1)/(4.*vev)',
                  order = {'QED':2,'QNP':1})

GC_616 = Coupling(name = 'GC_616',
                  value = '(ee*tdau2x2)/(4.*vev)',
                  order = {'QED':2,'QNP':1})

GC_617 = Coupling(name = 'GC_617',
                  value = '(ee*tdau2x3)/(4.*vev)',
                  order = {'QED':2,'QNP':1})

GC_618 = Coupling(name = 'GC_618',
                  value = '(ee*tdau3x1)/(4.*vev)',
                  order = {'QED':2,'QNP':1})

GC_619 = Coupling(name = 'GC_619',
                  value = '(ee*tdau3x2)/(4.*vev)',
                  order = {'QED':2,'QNP':1})

GC_620 = Coupling(name = 'GC_620',
                  value = '(ee*tdau3x3)/(4.*vev)',
                  order = {'QED':2,'QNP':1})

GC_621 = Coupling(name = 'GC_621',
                  value = '(G*tdgd1x1)/(4.*vev)',
                  order = {'QCD':1,'QED':1,'QNP':1})

GC_622 = Coupling(name = 'GC_622',
                  value = '-(complex(0,1)*G**2*tdgd1x1)/(4.*vev)',
                  order = {'QCD':2,'QED':1,'QNP':1})

GC_623 = Coupling(name = 'GC_623',
                  value = '(G*tdgd1x2)/(4.*vev)',
                  order = {'QCD':1,'QED':1,'QNP':1})

GC_624 = Coupling(name = 'GC_624',
                  value = '-(complex(0,1)*G**2*tdgd1x2)/(4.*vev)',
                  order = {'QCD':2,'QED':1,'QNP':1})

GC_625 = Coupling(name = 'GC_625',
                  value = '(G*tdgd1x3)/(4.*vev)',
                  order = {'QCD':1,'QED':1,'QNP':1})

GC_626 = Coupling(name = 'GC_626',
                  value = '-(complex(0,1)*G**2*tdgd1x3)/(4.*vev)',
                  order = {'QCD':2,'QED':1,'QNP':1})

GC_627 = Coupling(name = 'GC_627',
                  value = '(G*tdgd2x1)/(4.*vev)',
                  order = {'QCD':1,'QED':1,'QNP':1})

GC_628 = Coupling(name = 'GC_628',
                  value = '-(complex(0,1)*G**2*tdgd2x1)/(4.*vev)',
                  order = {'QCD':2,'QED':1,'QNP':1})

GC_629 = Coupling(name = 'GC_629',
                  value = '(G*tdgd2x2)/(4.*vev)',
                  order = {'QCD':1,'QED':1,'QNP':1})

GC_630 = Coupling(name = 'GC_630',
                  value = '-(complex(0,1)*G**2*tdgd2x2)/(4.*vev)',
                  order = {'QCD':2,'QED':1,'QNP':1})

GC_631 = Coupling(name = 'GC_631',
                  value = '(G*tdgd2x3)/(4.*vev)',
                  order = {'QCD':1,'QED':1,'QNP':1})

GC_632 = Coupling(name = 'GC_632',
                  value = '-(complex(0,1)*G**2*tdgd2x3)/(4.*vev)',
                  order = {'QCD':2,'QED':1,'QNP':1})

GC_633 = Coupling(name = 'GC_633',
                  value = '(G*tdgd3x1)/(4.*vev)',
                  order = {'QCD':1,'QED':1,'QNP':1})

GC_634 = Coupling(name = 'GC_634',
                  value = '-(complex(0,1)*G**2*tdgd3x1)/(4.*vev)',
                  order = {'QCD':2,'QED':1,'QNP':1})

GC_635 = Coupling(name = 'GC_635',
                  value = '(G*tdgd3x2)/(4.*vev)',
                  order = {'QCD':1,'QED':1,'QNP':1})

GC_636 = Coupling(name = 'GC_636',
                  value = '-(complex(0,1)*G**2*tdgd3x2)/(4.*vev)',
                  order = {'QCD':2,'QED':1,'QNP':1})

GC_637 = Coupling(name = 'GC_637',
                  value = '(G*tdgd3x3)/(4.*vev)',
                  order = {'QCD':1,'QED':1,'QNP':1})

GC_638 = Coupling(name = 'GC_638',
                  value = '-(complex(0,1)*G**2*tdgd3x3)/(4.*vev)',
                  order = {'QCD':2,'QED':1,'QNP':1})

GC_639 = Coupling(name = 'GC_639',
                  value = '(G*tdgu1x1)/(4.*vev)',
                  order = {'QCD':1,'QED':1,'QNP':1})

GC_640 = Coupling(name = 'GC_640',
                  value = '-(complex(0,1)*G**2*tdgu1x1)/(4.*vev)',
                  order = {'QCD':2,'QED':1,'QNP':1})

GC_641 = Coupling(name = 'GC_641',
                  value = '(G*tdgu1x2)/(4.*vev)',
                  order = {'QCD':1,'QED':1,'QNP':1})

GC_642 = Coupling(name = 'GC_642',
                  value = '-(complex(0,1)*G**2*tdgu1x2)/(4.*vev)',
                  order = {'QCD':2,'QED':1,'QNP':1})

GC_643 = Coupling(name = 'GC_643',
                  value = '(G*tdgu1x3)/(4.*vev)',
                  order = {'QCD':1,'QED':1,'QNP':1})

GC_644 = Coupling(name = 'GC_644',
                  value = '-(complex(0,1)*G**2*tdgu1x3)/(4.*vev)',
                  order = {'QCD':2,'QED':1,'QNP':1})

GC_645 = Coupling(name = 'GC_645',
                  value = '(G*tdgu2x1)/(4.*vev)',
                  order = {'QCD':1,'QED':1,'QNP':1})

GC_646 = Coupling(name = 'GC_646',
                  value = '-(complex(0,1)*G**2*tdgu2x1)/(4.*vev)',
                  order = {'QCD':2,'QED':1,'QNP':1})

GC_647 = Coupling(name = 'GC_647',
                  value = '(G*tdgu2x2)/(4.*vev)',
                  order = {'QCD':1,'QED':1,'QNP':1})

GC_648 = Coupling(name = 'GC_648',
                  value = '-(complex(0,1)*G**2*tdgu2x2)/(4.*vev)',
                  order = {'QCD':2,'QED':1,'QNP':1})

GC_649 = Coupling(name = 'GC_649',
                  value = '(G*tdgu2x3)/(4.*vev)',
                  order = {'QCD':1,'QED':1,'QNP':1})

GC_650 = Coupling(name = 'GC_650',
                  value = '-(complex(0,1)*G**2*tdgu2x3)/(4.*vev)',
                  order = {'QCD':2,'QED':1,'QNP':1})

GC_651 = Coupling(name = 'GC_651',
                  value = '(G*tdgu3x1)/(4.*vev)',
                  order = {'QCD':1,'QED':1,'QNP':1})

GC_652 = Coupling(name = 'GC_652',
                  value = '-(complex(0,1)*G**2*tdgu3x1)/(4.*vev)',
                  order = {'QCD':2,'QED':1,'QNP':1})

GC_653 = Coupling(name = 'GC_653',
                  value = '(G*tdgu3x2)/(4.*vev)',
                  order = {'QCD':1,'QED':1,'QNP':1})

GC_654 = Coupling(name = 'GC_654',
                  value = '-(complex(0,1)*G**2*tdgu3x2)/(4.*vev)',
                  order = {'QCD':2,'QED':1,'QNP':1})

GC_655 = Coupling(name = 'GC_655',
                  value = '(G*tdgu3x3)/(4.*vev)',
                  order = {'QCD':1,'QED':1,'QNP':1})

GC_656 = Coupling(name = 'GC_656',
                  value = '-(complex(0,1)*G**2*tdgu3x3)/(4.*vev)',
                  order = {'QCD':2,'QED':1,'QNP':1})

GC_657 = Coupling(name = 'GC_657',
                  value = '(ee*tdzd1x1*cmath.sqrt(cw**2 + sw**2))/(4.*cw*sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_658 = Coupling(name = 'GC_658',
                  value = '(ee*tdzd1x2*cmath.sqrt(cw**2 + sw**2))/(4.*cw*sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_659 = Coupling(name = 'GC_659',
                  value = '(ee*tdzd1x3*cmath.sqrt(cw**2 + sw**2))/(4.*cw*sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_660 = Coupling(name = 'GC_660',
                  value = '(ee*tdzd2x1*cmath.sqrt(cw**2 + sw**2))/(4.*cw*sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_661 = Coupling(name = 'GC_661',
                  value = '(ee*tdzd2x2*cmath.sqrt(cw**2 + sw**2))/(4.*cw*sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_662 = Coupling(name = 'GC_662',
                  value = '(ee*tdzd2x3*cmath.sqrt(cw**2 + sw**2))/(4.*cw*sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_663 = Coupling(name = 'GC_663',
                  value = '(ee*tdzd3x1*cmath.sqrt(cw**2 + sw**2))/(4.*cw*sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_664 = Coupling(name = 'GC_664',
                  value = '(ee*tdzd3x2*cmath.sqrt(cw**2 + sw**2))/(4.*cw*sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_665 = Coupling(name = 'GC_665',
                  value = '(ee*tdzd3x3*cmath.sqrt(cw**2 + sw**2))/(4.*cw*sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_666 = Coupling(name = 'GC_666',
                  value = '(ee*tdze1x1*cmath.sqrt(cw**2 + sw**2))/(4.*cw*sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_667 = Coupling(name = 'GC_667',
                  value = '(ee*tdze1x2*cmath.sqrt(cw**2 + sw**2))/(4.*cw*sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_668 = Coupling(name = 'GC_668',
                  value = '(ee*tdze1x3*cmath.sqrt(cw**2 + sw**2))/(4.*cw*sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_669 = Coupling(name = 'GC_669',
                  value = '(ee*tdze2x1*cmath.sqrt(cw**2 + sw**2))/(4.*cw*sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_670 = Coupling(name = 'GC_670',
                  value = '(ee*tdze2x2*cmath.sqrt(cw**2 + sw**2))/(4.*cw*sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_671 = Coupling(name = 'GC_671',
                  value = '(ee*tdze2x3*cmath.sqrt(cw**2 + sw**2))/(4.*cw*sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_672 = Coupling(name = 'GC_672',
                  value = '(ee*tdze3x1*cmath.sqrt(cw**2 + sw**2))/(4.*cw*sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_673 = Coupling(name = 'GC_673',
                  value = '(ee*tdze3x2*cmath.sqrt(cw**2 + sw**2))/(4.*cw*sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_674 = Coupling(name = 'GC_674',
                  value = '(ee*tdze3x3*cmath.sqrt(cw**2 + sw**2))/(4.*cw*sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_675 = Coupling(name = 'GC_675',
                  value = '(ee*tdzu1x1*cmath.sqrt(cw**2 + sw**2))/(4.*cw*sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_676 = Coupling(name = 'GC_676',
                  value = '(ee*tdzu1x2*cmath.sqrt(cw**2 + sw**2))/(4.*cw*sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_677 = Coupling(name = 'GC_677',
                  value = '(ee*tdzu1x3*cmath.sqrt(cw**2 + sw**2))/(4.*cw*sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_678 = Coupling(name = 'GC_678',
                  value = '(ee*tdzu2x1*cmath.sqrt(cw**2 + sw**2))/(4.*cw*sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_679 = Coupling(name = 'GC_679',
                  value = '(ee*tdzu2x2*cmath.sqrt(cw**2 + sw**2))/(4.*cw*sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_680 = Coupling(name = 'GC_680',
                  value = '(ee*tdzu2x3*cmath.sqrt(cw**2 + sw**2))/(4.*cw*sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_681 = Coupling(name = 'GC_681',
                  value = '(ee*tdzu3x1*cmath.sqrt(cw**2 + sw**2))/(4.*cw*sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_682 = Coupling(name = 'GC_682',
                  value = '(ee*tdzu3x2*cmath.sqrt(cw**2 + sw**2))/(4.*cw*sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_683 = Coupling(name = 'GC_683',
                  value = '(ee*tdzu3x3*cmath.sqrt(cw**2 + sw**2))/(4.*cw*sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_684 = Coupling(name = 'GC_684',
                  value = '-6*dL3*complex(0,1)*vev',
                  order = {'QNP':1})

GC_685 = Coupling(name = 'GC_685',
                  value = '-6*complex(0,1)*lam*vev',
                  order = {'QED':1})

GC_686 = Coupling(name = 'GC_686',
                  value = '(ee**2*complex(0,1)*vev)/(2.*sw**2)',
                  order = {'QED':1})

GC_687 = Coupling(name = 'GC_687',
                  value = 'ee**2*complex(0,1)*vev + (cw**2*ee**2*complex(0,1)*vev)/(2.*sw**2) + (ee**2*complex(0,1)*sw**2*vev)/(2.*cw**2)',
                  order = {'QED':1})

GC_688 = Coupling(name = 'GC_688',
                  value = '-((complex(0,1)*yb)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_689 = Coupling(name = 'GC_689',
                  value = '-((complex(0,1)*yt)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_690 = Coupling(name = 'GC_690',
                  value = '-((complex(0,1)*ytau)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_691 = Coupling(name = 'GC_691',
                  value = '(ee*complex(0,1)*complexconjugate(CKM1x1))/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_692 = Coupling(name = 'GC_692',
                  value = '(ee*complex(0,1)*complexconjugate(CKM1x2))/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_693 = Coupling(name = 'GC_693',
                  value = '(ee*complex(0,1)*complexconjugate(CKM1x3))/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_694 = Coupling(name = 'GC_694',
                  value = '(ee*complex(0,1)*complexconjugate(CKM2x1))/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_695 = Coupling(name = 'GC_695',
                  value = '(ee*complex(0,1)*complexconjugate(CKM2x2))/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_696 = Coupling(name = 'GC_696',
                  value = '(ee*complex(0,1)*complexconjugate(CKM2x3))/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_697 = Coupling(name = 'GC_697',
                  value = '(ee*complex(0,1)*complexconjugate(CKM3x1))/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_698 = Coupling(name = 'GC_698',
                  value = '(ee*complex(0,1)*complexconjugate(CKM3x2))/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_699 = Coupling(name = 'GC_699',
                  value = '(ee*complex(0,1)*complexconjugate(CKM3x3))/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_700 = Coupling(name = 'GC_700',
                  value = '(ee*complex(0,1)*complexconjugate(dGLhwl1x1)*cmath.sqrt(2))/(sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_701 = Coupling(name = 'GC_701',
                  value = '(ee*complex(0,1)*complexconjugate(dGLhwl1x2)*cmath.sqrt(2))/(sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_702 = Coupling(name = 'GC_702',
                  value = '(ee*complex(0,1)*complexconjugate(dGLhwl1x3)*cmath.sqrt(2))/(sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_703 = Coupling(name = 'GC_703',
                  value = '(ee*complex(0,1)*complexconjugate(dGLhwl2x1)*cmath.sqrt(2))/(sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_704 = Coupling(name = 'GC_704',
                  value = '(ee*complex(0,1)*complexconjugate(dGLhwl2x2)*cmath.sqrt(2))/(sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_705 = Coupling(name = 'GC_705',
                  value = '(ee*complex(0,1)*complexconjugate(dGLhwl2x3)*cmath.sqrt(2))/(sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_706 = Coupling(name = 'GC_706',
                  value = '(ee*complex(0,1)*complexconjugate(dGLhwl3x1)*cmath.sqrt(2))/(sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_707 = Coupling(name = 'GC_707',
                  value = '(ee*complex(0,1)*complexconjugate(dGLhwl3x2)*cmath.sqrt(2))/(sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_708 = Coupling(name = 'GC_708',
                  value = '(ee*complex(0,1)*complexconjugate(dGLhwl3x3)*cmath.sqrt(2))/(sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_709 = Coupling(name = 'GC_709',
                  value = '(ee*complex(0,1)*complexconjugate(dGLhwq1x1)*cmath.sqrt(2))/(sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_710 = Coupling(name = 'GC_710',
                  value = '(ee*complex(0,1)*complexconjugate(dGLhwq1x2)*cmath.sqrt(2))/(sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_711 = Coupling(name = 'GC_711',
                  value = '(ee*complex(0,1)*complexconjugate(dGLhwq1x3)*cmath.sqrt(2))/(sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_712 = Coupling(name = 'GC_712',
                  value = '(ee*complex(0,1)*complexconjugate(dGLhwq2x1)*cmath.sqrt(2))/(sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_713 = Coupling(name = 'GC_713',
                  value = '(ee*complex(0,1)*complexconjugate(dGLhwq2x2)*cmath.sqrt(2))/(sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_714 = Coupling(name = 'GC_714',
                  value = '(ee*complex(0,1)*complexconjugate(dGLhwq2x3)*cmath.sqrt(2))/(sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_715 = Coupling(name = 'GC_715',
                  value = '(ee*complex(0,1)*complexconjugate(dGLhwq3x1)*cmath.sqrt(2))/(sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_716 = Coupling(name = 'GC_716',
                  value = '(ee*complex(0,1)*complexconjugate(dGLhwq3x2)*cmath.sqrt(2))/(sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_717 = Coupling(name = 'GC_717',
                  value = '(ee*complex(0,1)*complexconjugate(dGLhwq3x3)*cmath.sqrt(2))/(sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_718 = Coupling(name = 'GC_718',
                  value = '(ee*complex(0,1)*complexconjugate(dGLwl1x1))/(sw*cmath.sqrt(2))',
                  order = {'QED':1,'QNP':1})

GC_719 = Coupling(name = 'GC_719',
                  value = '(ee*complex(0,1)*complexconjugate(dGLwl1x2))/(sw*cmath.sqrt(2))',
                  order = {'QED':1,'QNP':1})

GC_720 = Coupling(name = 'GC_720',
                  value = '(ee*complex(0,1)*complexconjugate(dGLwl1x3))/(sw*cmath.sqrt(2))',
                  order = {'QED':1,'QNP':1})

GC_721 = Coupling(name = 'GC_721',
                  value = '(ee*complex(0,1)*complexconjugate(dGLwl2x1))/(sw*cmath.sqrt(2))',
                  order = {'QED':1,'QNP':1})

GC_722 = Coupling(name = 'GC_722',
                  value = '(ee*complex(0,1)*complexconjugate(dGLwl2x2))/(sw*cmath.sqrt(2))',
                  order = {'QED':1,'QNP':1})

GC_723 = Coupling(name = 'GC_723',
                  value = '(ee*complex(0,1)*complexconjugate(dGLwl2x3))/(sw*cmath.sqrt(2))',
                  order = {'QED':1,'QNP':1})

GC_724 = Coupling(name = 'GC_724',
                  value = '(ee*complex(0,1)*complexconjugate(dGLwl3x1))/(sw*cmath.sqrt(2))',
                  order = {'QED':1,'QNP':1})

GC_725 = Coupling(name = 'GC_725',
                  value = '(ee*complex(0,1)*complexconjugate(dGLwl3x2))/(sw*cmath.sqrt(2))',
                  order = {'QED':1,'QNP':1})

GC_726 = Coupling(name = 'GC_726',
                  value = '(ee*complex(0,1)*complexconjugate(dGLwl3x3))/(sw*cmath.sqrt(2))',
                  order = {'QED':1,'QNP':1})

GC_727 = Coupling(name = 'GC_727',
                  value = '(ee*complex(0,1)*complexconjugate(dGLwq1x1))/(sw*cmath.sqrt(2))',
                  order = {'QED':1,'QNP':1})

GC_728 = Coupling(name = 'GC_728',
                  value = '(ee*complex(0,1)*complexconjugate(dGLwq1x2))/(sw*cmath.sqrt(2))',
                  order = {'QED':1,'QNP':1})

GC_729 = Coupling(name = 'GC_729',
                  value = '(ee*complex(0,1)*complexconjugate(dGLwq1x3))/(sw*cmath.sqrt(2))',
                  order = {'QED':1,'QNP':1})

GC_730 = Coupling(name = 'GC_730',
                  value = '(ee*complex(0,1)*complexconjugate(dGLwq2x1))/(sw*cmath.sqrt(2))',
                  order = {'QED':1,'QNP':1})

GC_731 = Coupling(name = 'GC_731',
                  value = '(ee*complex(0,1)*complexconjugate(dGLwq2x2))/(sw*cmath.sqrt(2))',
                  order = {'QED':1,'QNP':1})

GC_732 = Coupling(name = 'GC_732',
                  value = '(ee*complex(0,1)*complexconjugate(dGLwq2x3))/(sw*cmath.sqrt(2))',
                  order = {'QED':1,'QNP':1})

GC_733 = Coupling(name = 'GC_733',
                  value = '(ee*complex(0,1)*complexconjugate(dGLwq3x1))/(sw*cmath.sqrt(2))',
                  order = {'QED':1,'QNP':1})

GC_734 = Coupling(name = 'GC_734',
                  value = '(ee*complex(0,1)*complexconjugate(dGLwq3x2))/(sw*cmath.sqrt(2))',
                  order = {'QED':1,'QNP':1})

GC_735 = Coupling(name = 'GC_735',
                  value = '(ee*complex(0,1)*complexconjugate(dGLwq3x3))/(sw*cmath.sqrt(2))',
                  order = {'QED':1,'QNP':1})

GC_736 = Coupling(name = 'GC_736',
                  value = '(ee*complex(0,1)*complexconjugate(dGRhwq1x1)*cmath.sqrt(2))/(sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_737 = Coupling(name = 'GC_737',
                  value = '(ee*complex(0,1)*complexconjugate(dGRhwq1x2)*cmath.sqrt(2))/(sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_738 = Coupling(name = 'GC_738',
                  value = '(ee*complex(0,1)*complexconjugate(dGRhwq1x3)*cmath.sqrt(2))/(sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_739 = Coupling(name = 'GC_739',
                  value = '(ee*complex(0,1)*complexconjugate(dGRhwq2x1)*cmath.sqrt(2))/(sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_740 = Coupling(name = 'GC_740',
                  value = '(ee*complex(0,1)*complexconjugate(dGRhwq2x2)*cmath.sqrt(2))/(sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_741 = Coupling(name = 'GC_741',
                  value = '(ee*complex(0,1)*complexconjugate(dGRhwq2x3)*cmath.sqrt(2))/(sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_742 = Coupling(name = 'GC_742',
                  value = '(ee*complex(0,1)*complexconjugate(dGRhwq3x1)*cmath.sqrt(2))/(sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_743 = Coupling(name = 'GC_743',
                  value = '(ee*complex(0,1)*complexconjugate(dGRhwq3x2)*cmath.sqrt(2))/(sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_744 = Coupling(name = 'GC_744',
                  value = '(ee*complex(0,1)*complexconjugate(dGRhwq3x3)*cmath.sqrt(2))/(sw*vev)',
                  order = {'QED':2,'QNP':1})

GC_745 = Coupling(name = 'GC_745',
                  value = '(ee*complex(0,1)*complexconjugate(dGRwq1x1))/(sw*cmath.sqrt(2))',
                  order = {'QED':1,'QNP':1})

GC_746 = Coupling(name = 'GC_746',
                  value = '(ee*complex(0,1)*complexconjugate(dGRwq1x2))/(sw*cmath.sqrt(2))',
                  order = {'QED':1,'QNP':1})

GC_747 = Coupling(name = 'GC_747',
                  value = '(ee*complex(0,1)*complexconjugate(dGRwq1x3))/(sw*cmath.sqrt(2))',
                  order = {'QED':1,'QNP':1})

GC_748 = Coupling(name = 'GC_748',
                  value = '(ee*complex(0,1)*complexconjugate(dGRwq2x1))/(sw*cmath.sqrt(2))',
                  order = {'QED':1,'QNP':1})

GC_749 = Coupling(name = 'GC_749',
                  value = '(ee*complex(0,1)*complexconjugate(dGRwq2x2))/(sw*cmath.sqrt(2))',
                  order = {'QED':1,'QNP':1})

GC_750 = Coupling(name = 'GC_750',
                  value = '(ee*complex(0,1)*complexconjugate(dGRwq2x3))/(sw*cmath.sqrt(2))',
                  order = {'QED':1,'QNP':1})

GC_751 = Coupling(name = 'GC_751',
                  value = '(ee*complex(0,1)*complexconjugate(dGRwq3x1))/(sw*cmath.sqrt(2))',
                  order = {'QED':1,'QNP':1})

GC_752 = Coupling(name = 'GC_752',
                  value = '(ee*complex(0,1)*complexconjugate(dGRwq3x2))/(sw*cmath.sqrt(2))',
                  order = {'QED':1,'QNP':1})

GC_753 = Coupling(name = 'GC_753',
                  value = '(ee*complex(0,1)*complexconjugate(dGRwq3x3))/(sw*cmath.sqrt(2))',
                  order = {'QED':1,'QNP':1})

GC_754 = Coupling(name = 'GC_754',
                  value = '(dhwu1x1*ee)/(2.*sw*vev**2*cmath.sqrt(2)) + (ee*complexconjugate(dhwd1x1))/(2.*sw*vev**2*cmath.sqrt(2))',
                  order = {'QED':3,'QNP':1})

GC_755 = Coupling(name = 'GC_755',
                  value = '(dhwu2x1*ee)/(2.*sw*vev**2*cmath.sqrt(2)) + (ee*complexconjugate(dhwd1x2))/(2.*sw*vev**2*cmath.sqrt(2))',
                  order = {'QED':3,'QNP':1})

GC_756 = Coupling(name = 'GC_756',
                  value = '(dhwu3x1*ee)/(2.*sw*vev**2*cmath.sqrt(2)) + (ee*complexconjugate(dhwd1x3))/(2.*sw*vev**2*cmath.sqrt(2))',
                  order = {'QED':3,'QNP':1})

GC_757 = Coupling(name = 'GC_757',
                  value = '(dhwu1x2*ee)/(2.*sw*vev**2*cmath.sqrt(2)) + (ee*complexconjugate(dhwd2x1))/(2.*sw*vev**2*cmath.sqrt(2))',
                  order = {'QED':3,'QNP':1})

GC_758 = Coupling(name = 'GC_758',
                  value = '(dhwu2x2*ee)/(2.*sw*vev**2*cmath.sqrt(2)) + (ee*complexconjugate(dhwd2x2))/(2.*sw*vev**2*cmath.sqrt(2))',
                  order = {'QED':3,'QNP':1})

GC_759 = Coupling(name = 'GC_759',
                  value = '(dhwu3x2*ee)/(2.*sw*vev**2*cmath.sqrt(2)) + (ee*complexconjugate(dhwd2x3))/(2.*sw*vev**2*cmath.sqrt(2))',
                  order = {'QED':3,'QNP':1})

GC_760 = Coupling(name = 'GC_760',
                  value = '(dhwu1x3*ee)/(2.*sw*vev**2*cmath.sqrt(2)) + (ee*complexconjugate(dhwd3x1))/(2.*sw*vev**2*cmath.sqrt(2))',
                  order = {'QED':3,'QNP':1})

GC_761 = Coupling(name = 'GC_761',
                  value = '(dhwu2x3*ee)/(2.*sw*vev**2*cmath.sqrt(2)) + (ee*complexconjugate(dhwd3x2))/(2.*sw*vev**2*cmath.sqrt(2))',
                  order = {'QED':3,'QNP':1})

GC_762 = Coupling(name = 'GC_762',
                  value = '(dhwu3x3*ee)/(2.*sw*vev**2*cmath.sqrt(2)) + (ee*complexconjugate(dhwd3x3))/(2.*sw*vev**2*cmath.sqrt(2))',
                  order = {'QED':3,'QNP':1})

GC_763 = Coupling(name = 'GC_763',
                  value = '(ee*complexconjugate(dhwl1x1))/(2.*sw*vev**2*cmath.sqrt(2))',
                  order = {'QED':3,'QNP':1})

GC_764 = Coupling(name = 'GC_764',
                  value = '(ee*complexconjugate(dhwl1x2))/(2.*sw*vev**2*cmath.sqrt(2))',
                  order = {'QED':3,'QNP':1})

GC_765 = Coupling(name = 'GC_765',
                  value = '(ee*complexconjugate(dhwl1x3))/(2.*sw*vev**2*cmath.sqrt(2))',
                  order = {'QED':3,'QNP':1})

GC_766 = Coupling(name = 'GC_766',
                  value = '(ee*complexconjugate(dhwl2x1))/(2.*sw*vev**2*cmath.sqrt(2))',
                  order = {'QED':3,'QNP':1})

GC_767 = Coupling(name = 'GC_767',
                  value = '(ee*complexconjugate(dhwl2x2))/(2.*sw*vev**2*cmath.sqrt(2))',
                  order = {'QED':3,'QNP':1})

GC_768 = Coupling(name = 'GC_768',
                  value = '(ee*complexconjugate(dhwl2x3))/(2.*sw*vev**2*cmath.sqrt(2))',
                  order = {'QED':3,'QNP':1})

GC_769 = Coupling(name = 'GC_769',
                  value = '(ee*complexconjugate(dhwl3x1))/(2.*sw*vev**2*cmath.sqrt(2))',
                  order = {'QED':3,'QNP':1})

GC_770 = Coupling(name = 'GC_770',
                  value = '(ee*complexconjugate(dhwl3x2))/(2.*sw*vev**2*cmath.sqrt(2))',
                  order = {'QED':3,'QNP':1})

GC_771 = Coupling(name = 'GC_771',
                  value = '(ee*complexconjugate(dhwl3x3))/(2.*sw*vev**2*cmath.sqrt(2))',
                  order = {'QED':3,'QNP':1})

GC_772 = Coupling(name = 'GC_772',
                  value = '(dhwd1x1*ee)/(2.*sw*vev**2*cmath.sqrt(2)) + (ee*complexconjugate(dhwu1x1))/(2.*sw*vev**2*cmath.sqrt(2))',
                  order = {'QED':3,'QNP':1})

GC_773 = Coupling(name = 'GC_773',
                  value = '(dhwd2x1*ee)/(2.*sw*vev**2*cmath.sqrt(2)) + (ee*complexconjugate(dhwu1x2))/(2.*sw*vev**2*cmath.sqrt(2))',
                  order = {'QED':3,'QNP':1})

GC_774 = Coupling(name = 'GC_774',
                  value = '(dhwd3x1*ee)/(2.*sw*vev**2*cmath.sqrt(2)) + (ee*complexconjugate(dhwu1x3))/(2.*sw*vev**2*cmath.sqrt(2))',
                  order = {'QED':3,'QNP':1})

GC_775 = Coupling(name = 'GC_775',
                  value = '(dhwd1x2*ee)/(2.*sw*vev**2*cmath.sqrt(2)) + (ee*complexconjugate(dhwu2x1))/(2.*sw*vev**2*cmath.sqrt(2))',
                  order = {'QED':3,'QNP':1})

GC_776 = Coupling(name = 'GC_776',
                  value = '(dhwd2x2*ee)/(2.*sw*vev**2*cmath.sqrt(2)) + (ee*complexconjugate(dhwu2x2))/(2.*sw*vev**2*cmath.sqrt(2))',
                  order = {'QED':3,'QNP':1})

GC_777 = Coupling(name = 'GC_777',
                  value = '(dhwd3x2*ee)/(2.*sw*vev**2*cmath.sqrt(2)) + (ee*complexconjugate(dhwu2x3))/(2.*sw*vev**2*cmath.sqrt(2))',
                  order = {'QED':3,'QNP':1})

GC_778 = Coupling(name = 'GC_778',
                  value = '(dhwd1x3*ee)/(2.*sw*vev**2*cmath.sqrt(2)) + (ee*complexconjugate(dhwu3x1))/(2.*sw*vev**2*cmath.sqrt(2))',
                  order = {'QED':3,'QNP':1})

GC_779 = Coupling(name = 'GC_779',
                  value = '(dhwd2x3*ee)/(2.*sw*vev**2*cmath.sqrt(2)) + (ee*complexconjugate(dhwu3x2))/(2.*sw*vev**2*cmath.sqrt(2))',
                  order = {'QED':3,'QNP':1})

GC_780 = Coupling(name = 'GC_780',
                  value = '(dhwd3x3*ee)/(2.*sw*vev**2*cmath.sqrt(2)) + (ee*complexconjugate(dhwu3x3))/(2.*sw*vev**2*cmath.sqrt(2))',
                  order = {'QED':3,'QNP':1})

GC_781 = Coupling(name = 'GC_781',
                  value = '(dwu1x1*ee)/(2.*sw*vev*cmath.sqrt(2)) + (ee*complexconjugate(dwd1x1))/(2.*sw*vev*cmath.sqrt(2))',
                  order = {'QED':2,'QNP':1})

GC_782 = Coupling(name = 'GC_782',
                  value = '(dwu2x1*ee)/(2.*sw*vev*cmath.sqrt(2)) + (ee*complexconjugate(dwd1x2))/(2.*sw*vev*cmath.sqrt(2))',
                  order = {'QED':2,'QNP':1})

GC_783 = Coupling(name = 'GC_783',
                  value = '(dwu3x1*ee)/(2.*sw*vev*cmath.sqrt(2)) + (ee*complexconjugate(dwd1x3))/(2.*sw*vev*cmath.sqrt(2))',
                  order = {'QED':2,'QNP':1})

GC_784 = Coupling(name = 'GC_784',
                  value = '(dwu1x2*ee)/(2.*sw*vev*cmath.sqrt(2)) + (ee*complexconjugate(dwd2x1))/(2.*sw*vev*cmath.sqrt(2))',
                  order = {'QED':2,'QNP':1})

GC_785 = Coupling(name = 'GC_785',
                  value = '(dwu2x2*ee)/(2.*sw*vev*cmath.sqrt(2)) + (ee*complexconjugate(dwd2x2))/(2.*sw*vev*cmath.sqrt(2))',
                  order = {'QED':2,'QNP':1})

GC_786 = Coupling(name = 'GC_786',
                  value = '(dwu3x2*ee)/(2.*sw*vev*cmath.sqrt(2)) + (ee*complexconjugate(dwd2x3))/(2.*sw*vev*cmath.sqrt(2))',
                  order = {'QED':2,'QNP':1})

GC_787 = Coupling(name = 'GC_787',
                  value = '(dwu1x3*ee)/(2.*sw*vev*cmath.sqrt(2)) + (ee*complexconjugate(dwd3x1))/(2.*sw*vev*cmath.sqrt(2))',
                  order = {'QED':2,'QNP':1})

GC_788 = Coupling(name = 'GC_788',
                  value = '(dwu2x3*ee)/(2.*sw*vev*cmath.sqrt(2)) + (ee*complexconjugate(dwd3x2))/(2.*sw*vev*cmath.sqrt(2))',
                  order = {'QED':2,'QNP':1})

GC_789 = Coupling(name = 'GC_789',
                  value = '(dwu3x3*ee)/(2.*sw*vev*cmath.sqrt(2)) + (ee*complexconjugate(dwd3x3))/(2.*sw*vev*cmath.sqrt(2))',
                  order = {'QED':2,'QNP':1})

GC_790 = Coupling(name = 'GC_790',
                  value = '(ee*complexconjugate(dwl1x1))/(2.*sw*vev*cmath.sqrt(2))',
                  order = {'QED':2,'QNP':1})

GC_791 = Coupling(name = 'GC_791',
                  value = '(ee*complexconjugate(dwl1x2))/(2.*sw*vev*cmath.sqrt(2))',
                  order = {'QED':2,'QNP':1})

GC_792 = Coupling(name = 'GC_792',
                  value = '(ee*complexconjugate(dwl1x3))/(2.*sw*vev*cmath.sqrt(2))',
                  order = {'QED':2,'QNP':1})

GC_793 = Coupling(name = 'GC_793',
                  value = '(ee*complexconjugate(dwl2x1))/(2.*sw*vev*cmath.sqrt(2))',
                  order = {'QED':2,'QNP':1})

GC_794 = Coupling(name = 'GC_794',
                  value = '(ee*complexconjugate(dwl2x2))/(2.*sw*vev*cmath.sqrt(2))',
                  order = {'QED':2,'QNP':1})

GC_795 = Coupling(name = 'GC_795',
                  value = '(ee*complexconjugate(dwl2x3))/(2.*sw*vev*cmath.sqrt(2))',
                  order = {'QED':2,'QNP':1})

GC_796 = Coupling(name = 'GC_796',
                  value = '(ee*complexconjugate(dwl3x1))/(2.*sw*vev*cmath.sqrt(2))',
                  order = {'QED':2,'QNP':1})

GC_797 = Coupling(name = 'GC_797',
                  value = '(ee*complexconjugate(dwl3x2))/(2.*sw*vev*cmath.sqrt(2))',
                  order = {'QED':2,'QNP':1})

GC_798 = Coupling(name = 'GC_798',
                  value = '(ee*complexconjugate(dwl3x3))/(2.*sw*vev*cmath.sqrt(2))',
                  order = {'QED':2,'QNP':1})

GC_799 = Coupling(name = 'GC_799',
                  value = '(dwd1x1*ee)/(2.*sw*vev*cmath.sqrt(2)) + (ee*complexconjugate(dwu1x1))/(2.*sw*vev*cmath.sqrt(2))',
                  order = {'QED':2,'QNP':1})

GC_800 = Coupling(name = 'GC_800',
                  value = '(dwd2x1*ee)/(2.*sw*vev*cmath.sqrt(2)) + (ee*complexconjugate(dwu1x2))/(2.*sw*vev*cmath.sqrt(2))',
                  order = {'QED':2,'QNP':1})

GC_801 = Coupling(name = 'GC_801',
                  value = '(dwd3x1*ee)/(2.*sw*vev*cmath.sqrt(2)) + (ee*complexconjugate(dwu1x3))/(2.*sw*vev*cmath.sqrt(2))',
                  order = {'QED':2,'QNP':1})

GC_802 = Coupling(name = 'GC_802',
                  value = '(dwd1x2*ee)/(2.*sw*vev*cmath.sqrt(2)) + (ee*complexconjugate(dwu2x1))/(2.*sw*vev*cmath.sqrt(2))',
                  order = {'QED':2,'QNP':1})

GC_803 = Coupling(name = 'GC_803',
                  value = '(dwd2x2*ee)/(2.*sw*vev*cmath.sqrt(2)) + (ee*complexconjugate(dwu2x2))/(2.*sw*vev*cmath.sqrt(2))',
                  order = {'QED':2,'QNP':1})

GC_804 = Coupling(name = 'GC_804',
                  value = '(dwd3x2*ee)/(2.*sw*vev*cmath.sqrt(2)) + (ee*complexconjugate(dwu2x3))/(2.*sw*vev*cmath.sqrt(2))',
                  order = {'QED':2,'QNP':1})

GC_805 = Coupling(name = 'GC_805',
                  value = '(dwd1x3*ee)/(2.*sw*vev*cmath.sqrt(2)) + (ee*complexconjugate(dwu3x1))/(2.*sw*vev*cmath.sqrt(2))',
                  order = {'QED':2,'QNP':1})

GC_806 = Coupling(name = 'GC_806',
                  value = '(dwd2x3*ee)/(2.*sw*vev*cmath.sqrt(2)) + (ee*complexconjugate(dwu3x2))/(2.*sw*vev*cmath.sqrt(2))',
                  order = {'QED':2,'QNP':1})

GC_807 = Coupling(name = 'GC_807',
                  value = '(dwd3x3*ee)/(2.*sw*vev*cmath.sqrt(2)) + (ee*complexconjugate(dwu3x3))/(2.*sw*vev*cmath.sqrt(2))',
                  order = {'QED':2,'QNP':1})

GC_808 = Coupling(name = 'GC_808',
                  value = '-((complex(0,1)*MB*complexconjugate(dYd23x3))/vev**2)',
                  order = {'QED':2,'QNP':1})

GC_809 = Coupling(name = 'GC_809',
                  value = '-((complex(0,1)*MTA*complexconjugate(dYe23x3))/vev**2)',
                  order = {'QED':2,'QNP':1})

GC_810 = Coupling(name = 'GC_810',
                  value = '-((complex(0,1)*MT*complexconjugate(dYu23x3))/vev**2)',
                  order = {'QED':2,'QNP':1})

