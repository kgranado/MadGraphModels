# This file was automatically created by FeynRules =.2.4
# Mathematica version: 8.0 for Linux x86 (32-bit) (October 10, 2011)
# Date: Sat 3 Sep 2016 20:49:32



from object_library import all_parameters, Parameter


from function_library import complexconjugate, re, im, csc, sec, acsc, asec, cot

# This is a default parameter object representing 0.
ZERO = Parameter(name = 'ZERO',
                 nature = 'internal',
                 type = 'real',
                 value = '0.0',
                 texname = '0')

# This is a default parameter object representing the renormalization scale (MU_R).
MU_R = Parameter(name = 'MU_R',
                 nature = 'external',
                 type = 'real',
                 value = 91.188,
                 texname = '\\text{\\mu_r}',
                 lhablock = 'LOOP',
                 lhacode = [1])

# User-defined parameters.
gsuEL = Parameter(name = 'gsuEL',
                  nature = 'external',
                  type = 'real',
                  value = 0.0,
                  texname = 'g_{\\text{suEL}}',
                  lhablock = 'ELQSD',
                  lhacode = [ 1 ])

gscEL = Parameter(name = 'gscEL',
                  nature = 'external',
                  type = 'real',
                  value = 0.0,
                  texname = 'g_{\\text{scEL}}',
                  lhablock = 'ELQSD',
                  lhacode = [ 2 ])

gstEL = Parameter(name = 'gstEL',
                  nature = 'external',
                  type = 'real',
                  value = 0.0,
                  texname = 'g_{\\text{stEL}}',
                  lhablock = 'ELQSD',
                  lhacode = [ 3 ])

gsuER = Parameter(name = 'gsuER',
                  nature = 'external',
                  type = 'real',
                  value = 0.0,
                  texname = 'g_{\\text{suER}}',
                  lhablock = 'ELQSD',
                  lhacode = [ 4 ])

gscER = Parameter(name = 'gscER',
                  nature = 'external',
                  type = 'real',
                  value = 0.0,
                  texname = 'g_{\\text{scER}}',
                  lhablock = 'ELQSD',
                  lhacode = [ 5 ])

gstER = Parameter(name = 'gstER',
                  nature = 'external',
                  type = 'real',
                  value = 0.0,
                  texname = 'g_{\\text{stER}}',
                  lhablock = 'ELQSD',
                  lhacode = [ 6 ])

gsdVEL = Parameter(name = 'gsdVEL',
                   nature = 'external',
                   type = 'real',
                   value = 0.0,
                   texname = 'g_{\\text{sdVEL}}',
                   lhablock = 'ELQSD',
                   lhacode = [ 7 ])

gssVEL = Parameter(name = 'gssVEL',
                   nature = 'external',
                   type = 'real',
                   value = 0.0,
                   texname = 'g_{\\text{ssVEL}}',
                   lhablock = 'ELQSD',
                   lhacode = [ 8 ])

gsbVEL = Parameter(name = 'gsbVEL',
                   nature = 'external',
                   type = 'real',
                   value = 0.0,
                   texname = 'g_{\\text{sbVEL}}',
                   lhablock = 'ELQSD',
                   lhacode = [ 9 ])

gsdEL = Parameter(name = 'gsdEL',
                  nature = 'external',
                  type = 'real',
                  value = 1,
                  texname = 'g_{\\text{sdEL}}',
                  lhablock = 'ELQSU',
                  lhacode = [ 1 ])

gssEL = Parameter(name = 'gssEL',
                  nature = 'external',
                  type = 'real',
                  value = 0.0,
                  texname = 'g_{\\text{ssEL}}',
                  lhablock = 'ELQSU',
                  lhacode = [ 2 ])

gsbEL = Parameter(name = 'gsbEL',
                  nature = 'external',
                  type = 'real',
                  value = 0.0,
                  texname = 'g_{\\text{sbEL}}',
                  lhablock = 'ELQSU',
                  lhacode = [ 3 ])

gsdER = Parameter(name = 'gsdER',
                  nature = 'external',
                  type = 'real',
                  value = 0.0,
                  texname = 'g_{\\text{sdER}}',
                  lhablock = 'ELQSU',
                  lhacode = [ 4 ])

gssER = Parameter(name = 'gssER',
                  nature = 'external',
                  type = 'real',
                  value = 0.0,
                  texname = 'g_{\\text{ssER}}',
                  lhablock = 'ELQSU',
                  lhacode = [ 5 ])

gsbER = Parameter(name = 'gsbER',
                  nature = 'external',
                  type = 'real',
                  value = 0.0,
                  texname = 'g_{\\text{sbER}}',
                  lhablock = 'ELQSU',
                  lhacode = [ 6 ])

gsuVEL = Parameter(name = 'gsuVEL',
                   nature = 'external',
                   type = 'real',
                   value = 0.0,
                   texname = 'g_{\\text{suVEL}}',
                   lhablock = 'ELQSU',
                   lhacode = [ 7 ])

gscVEL = Parameter(name = 'gscVEL',
                   nature = 'external',
                   type = 'real',
                   value = 0.0,
                   texname = 'g_{\\text{scVEL}}',
                   lhablock = 'ELQSU',
                   lhacode = [ 8 ])

gstVEL = Parameter(name = 'gstVEL',
                   nature = 'external',
                   type = 'real',
                   value = 0.0,
                   texname = 'g_{\\text{stVEL}}',
                   lhablock = 'ELQSU',
                   lhacode = [ 9 ])

gsuML = Parameter(name = 'gsuML',
                  nature = 'external',
                  type = 'real',
                  value = 0.0,
                  texname = 'g_{\\text{suML}}',
                  lhablock = 'MLQSD',
                  lhacode = [ 1 ])

gscML = Parameter(name = 'gscML',
                  nature = 'external',
                  type = 'real',
                  value = 0.0,
                  texname = 'g_{\\text{scML}}',
                  lhablock = 'MLQSD',
                  lhacode = [ 2 ])

gstML = Parameter(name = 'gstML',
                  nature = 'external',
                  type = 'real',
                  value = 0.0,
                  texname = 'g_{\\text{stML}}',
                  lhablock = 'MLQSD',
                  lhacode = [ 3 ])

gsuMR = Parameter(name = 'gsuMR',
                  nature = 'external',
                  type = 'real',
                  value = 0.0,
                  texname = 'g_{\\text{suMR}}',
                  lhablock = 'MLQSD',
                  lhacode = [ 4 ])

gscMR = Parameter(name = 'gscMR',
                  nature = 'external',
                  type = 'real',
                  value = 0.0,
                  texname = 'g_{\\text{scMR}}',
                  lhablock = 'MLQSD',
                  lhacode = [ 5 ])

gstMR = Parameter(name = 'gstMR',
                  nature = 'external',
                  type = 'real',
                  value = 0.0,
                  texname = 'g_{\\text{stMR}}',
                  lhablock = 'MLQSD',
                  lhacode = [ 6 ])

gsdVML = Parameter(name = 'gsdVML',
                   nature = 'external',
                   type = 'real',
                   value = 0.0,
                   texname = 'g_{\\text{sdVML}}',
                   lhablock = 'MLQSD',
                   lhacode = [ 7 ])

gssVML = Parameter(name = 'gssVML',
                   nature = 'external',
                   type = 'real',
                   value = 0.0,
                   texname = 'g_{\\text{ssVML}}',
                   lhablock = 'MLQSD',
                   lhacode = [ 8 ])

gsbVML = Parameter(name = 'gsbVML',
                   nature = 'external',
                   type = 'real',
                   value = 0.0,
                   texname = 'g_{\\text{sbVML}}',
                   lhablock = 'MLQSD',
                   lhacode = [ 9 ])

gsdML = Parameter(name = 'gsdML',
                  nature = 'external',
                  type = 'real',
                  value = 0.0,
                  texname = 'g_{\\text{sdML}}',
                  lhablock = 'MLQSU',
                  lhacode = [ 1 ])

gssML = Parameter(name = 'gssML',
                  nature = 'external',
                  type = 'real',
                  value = 0.0,
                  texname = 'g_{\\text{ssML}}',
                  lhablock = 'MLQSU',
                  lhacode = [ 2 ])

gsbML = Parameter(name = 'gsbML',
                  nature = 'external',
                  type = 'real',
                  value = 0.0,
                  texname = 'g_{\\text{sbML}}',
                  lhablock = 'MLQSU',
                  lhacode = [ 3 ])

gsdMR = Parameter(name = 'gsdMR',
                  nature = 'external',
                  type = 'real',
                  value = 0.0,
                  texname = 'g_{\\text{sdMR}}',
                  lhablock = 'MLQSU',
                  lhacode = [ 4 ])

gssMR = Parameter(name = 'gssMR',
                  nature = 'external',
                  type = 'real',
                  value = 0.0,
                  texname = 'g_{\\text{ssMR}}',
                  lhablock = 'MLQSU',
                  lhacode = [ 5 ])

gsbMR = Parameter(name = 'gsbMR',
                  nature = 'external',
                  type = 'real',
                  value = 0.0,
                  texname = 'g_{\\text{sbMR}}',
                  lhablock = 'MLQSU',
                  lhacode = [ 6 ])

gsuVML = Parameter(name = 'gsuVML',
                   nature = 'external',
                   type = 'real',
                   value = 0.0,
                   texname = 'g_{\\text{suVML}}',
                   lhablock = 'MLQSU',
                   lhacode = [ 7 ])

gscVML = Parameter(name = 'gscVML',
                   nature = 'external',
                   type = 'real',
                   value = 0.0,
                   texname = 'g_{\\text{scVML}}',
                   lhablock = 'MLQSU',
                   lhacode = [ 8 ])

gstVML = Parameter(name = 'gstVML',
                   nature = 'external',
                   type = 'real',
                   value = 0.0,
                   texname = 'g_{\\text{stVML}}',
                   lhablock = 'MLQSU',
                   lhacode = [ 9 ])

aEWM1 = Parameter(name = 'aEWM1',
                  nature = 'external',
                  type = 'real',
                  value = 127.9,
                  texname = '\\text{aEWM1}',
                  lhablock = 'SMINPUTS',
                  lhacode = [ 1 ])

Gf = Parameter(name = 'Gf',
               nature = 'external',
               type = 'real',
               value = 0.0000116637,
               texname = 'G_f',
               lhablock = 'SMINPUTS',
               lhacode = [ 2 ])

aS = Parameter(name = 'aS',
               nature = 'external',
               type = 'real',
               value = 0.1184,
               texname = '\\alpha _s',
               lhablock = 'SMINPUTS',
               lhacode = [ 3 ])

gsuTL = Parameter(name = 'gsuTL',
                  nature = 'external',
                  type = 'real',
                  value = 0.0,
                  texname = 'g_{\\text{suTL}}',
                  lhablock = 'TLQSD',
                  lhacode = [ 1 ])

gscTL = Parameter(name = 'gscTL',
                  nature = 'external',
                  type = 'real',
                  value = 0.0,
                  texname = 'g_{\\text{scTL}}',
                  lhablock = 'TLQSD',
                  lhacode = [ 2 ])

gstTL = Parameter(name = 'gstTL',
                  nature = 'external',
                  type = 'real',
                  value = 0.0,
                  texname = 'g_{\\text{stTL}}',
                  lhablock = 'TLQSD',
                  lhacode = [ 3 ])

gsuTR = Parameter(name = 'gsuTR',
                  nature = 'external',
                  type = 'real',
                  value = 0.0,
                  texname = 'g_{\\text{suTR}}',
                  lhablock = 'TLQSD',
                  lhacode = [ 4 ])

gscTR = Parameter(name = 'gscTR',
                  nature = 'external',
                  type = 'real',
                  value = 0.0,
                  texname = 'g_{\\text{scTR}}',
                  lhablock = 'TLQSD',
                  lhacode = [ 5 ])

gstTR = Parameter(name = 'gstTR',
                  nature = 'external',
                  type = 'real',
                  value = 0.0,
                  texname = 'g_{\\text{stTR}}',
                  lhablock = 'TLQSD',
                  lhacode = [ 6 ])

gsdVTL = Parameter(name = 'gsdVTL',
                   nature = 'external',
                   type = 'real',
                   value = 0.0,
                   texname = 'g_{\\text{sdVTL}}',
                   lhablock = 'TLQSD',
                   lhacode = [ 7 ])

gssVTL = Parameter(name = 'gssVTL',
                   nature = 'external',
                   type = 'real',
                   value = 0.0,
                   texname = 'g_{\\text{ssVTL}}',
                   lhablock = 'TLQSD',
                   lhacode = [ 8 ])

gsbVTL = Parameter(name = 'gsbVTL',
                   nature = 'external',
                   type = 'real',
                   value = 0.0,
                   texname = 'g_{\\text{sbVTL}}',
                   lhablock = 'TLQSD',
                   lhacode = [ 9 ])

gsdTL = Parameter(name = 'gsdTL',
                  nature = 'external',
                  type = 'real',
                  value = 0.0,
                  texname = 'g_{\\text{sdTL}}',
                  lhablock = 'TLQSU',
                  lhacode = [ 1 ])

gssTL = Parameter(name = 'gssTL',
                  nature = 'external',
                  type = 'real',
                  value = 0.0,
                  texname = 'g_{\\text{ssTL}}',
                  lhablock = 'TLQSU',
                  lhacode = [ 2 ])

gsbTL = Parameter(name = 'gsbTL',
                  nature = 'external',
                  type = 'real',
                  value = 0.0,
                  texname = 'g_{\\text{sbTL}}',
                  lhablock = 'TLQSU',
                  lhacode = [ 3 ])

gsdTR = Parameter(name = 'gsdTR',
                  nature = 'external',
                  type = 'real',
                  value = 0.0,
                  texname = 'g_{\\text{sdTR}}',
                  lhablock = 'TLQSU',
                  lhacode = [ 4 ])

gssTR = Parameter(name = 'gssTR',
                  nature = 'external',
                  type = 'real',
                  value = 0.0,
                  texname = 'g_{\\text{ssTR}}',
                  lhablock = 'TLQSU',
                  lhacode = [ 5 ])

gsbTR = Parameter(name = 'gsbTR',
                  nature = 'external',
                  type = 'real',
                  value = 0.0,
                  texname = 'g_{\\text{sbTR}}',
                  lhablock = 'TLQSU',
                  lhacode = [ 6 ])

gsuVTL = Parameter(name = 'gsuVTL',
                   nature = 'external',
                   type = 'real',
                   value = 0.0,
                   texname = 'g_{\\text{suVTL}}',
                   lhablock = 'TLQSU',
                   lhacode = [ 7 ])

gscVTL = Parameter(name = 'gscVTL',
                   nature = 'external',
                   type = 'real',
                   value = 0.0,
                   texname = 'g_{\\text{scVTL}}',
                   lhablock = 'TLQSU',
                   lhacode = [ 8 ])

gstVTL = Parameter(name = 'gstVTL',
                   nature = 'external',
                   type = 'real',
                   value = 0.0,
                   texname = 'g_{\\text{stVTL}}',
                   lhablock = 'TLQSU',
                   lhacode = [ 9 ])

ymb = Parameter(name = 'ymb',
                nature = 'external',
                type = 'real',
                value = 4.7,
                texname = '\\text{ymb}',
                lhablock = 'YUKAWA',
                lhacode = [ 5 ])

ymt = Parameter(name = 'ymt',
                nature = 'external',
                type = 'real',
                value = 172,
                texname = '\\text{ymt}',
                lhablock = 'YUKAWA',
                lhacode = [ 6 ])

ymtau = Parameter(name = 'ymtau',
                  nature = 'external',
                  type = 'real',
                  value = 1.777,
                  texname = '\\text{ymtau}',
                  lhablock = 'YUKAWA',
                  lhacode = [ 15 ])

MZ = Parameter(name = 'MZ',
               nature = 'external',
               type = 'real',
               value = 91.1876,
               texname = '\\text{MZ}',
               lhablock = 'MASS',
               lhacode = [ 23 ])

MTA = Parameter(name = 'MTA',
                nature = 'external',
                type = 'real',
                value = 1.777,
                texname = '\\text{MTA}',
                lhablock = 'MASS',
                lhacode = [ 15 ])

MT = Parameter(name = 'MT',
               nature = 'external',
               type = 'real',
               value = 172,
               texname = '\\text{MT}',
               lhablock = 'MASS',
               lhacode = [ 6 ])

MB = Parameter(name = 'MB',
               nature = 'external',
               type = 'real',
               value = 4.7,
               texname = '\\text{MB}',
               lhablock = 'MASS',
               lhacode = [ 5 ])

MH = Parameter(name = 'MH',
               nature = 'external',
               type = 'real',
               value = 125,
               texname = '\\text{MH}',
               lhablock = 'MASS',
               lhacode = [ 25 ])

Msu = Parameter(name = 'Msu',
                nature = 'external',
                type = 'real',
                value = 800,
                texname = '\\text{Msu}',
                lhablock = 'MASS',
                lhacode = [ 43 ])

Msd = Parameter(name = 'Msd',
                nature = 'external',
                type = 'real',
                value = 800,
                texname = '\\text{Msd}',
                lhablock = 'MASS',
                lhacode = [ 42 ])

WZ = Parameter(name = 'WZ',
               nature = 'external',
               type = 'real',
               value = 2.4952,
               texname = '\\text{WZ}',
               lhablock = 'DECAY',
               lhacode = [ 23 ])

WW = Parameter(name = 'WW',
               nature = 'external',
               type = 'real',
               value = 2.085,
               texname = '\\text{WW}',
               lhablock = 'DECAY',
               lhacode = [ 24 ])

WT = Parameter(name = 'WT',
               nature = 'external',
               type = 'real',
               value = 1.50833649,
               texname = '\\text{WT}',
               lhablock = 'DECAY',
               lhacode = [ 6 ])

WH = Parameter(name = 'WH',
               nature = 'external',
               type = 'real',
               value = 0.00407,
               texname = '\\text{WH}',
               lhablock = 'DECAY',
               lhacode = [ 25 ])

Wsu = Parameter(name = 'Wsu',
                nature = 'external',
                type = 'real',
                value = 1.4324,
                texname = '\\text{Wsu}',
                lhablock = 'DECAY',
                lhacode = [ 43 ])

Wsd = Parameter(name = 'Wsd',
                nature = 'external',
                type = 'real',
                value = 1.4324,
                texname = '\\text{Wsd}',
                lhablock = 'DECAY',
                lhacode = [ 42 ])

aEW = Parameter(name = 'aEW',
                nature = 'internal',
                type = 'real',
                value = '1/aEWM1',
                texname = '\\alpha _{\\text{EW}}')

G = Parameter(name = 'G',
              nature = 'internal',
              type = 'real',
              value = '2*cmath.sqrt(aS)*cmath.sqrt(cmath.pi)',
              texname = 'G')

MW = Parameter(name = 'MW',
               nature = 'internal',
               type = 'real',
               value = 'cmath.sqrt(MZ**2/2. + cmath.sqrt(MZ**4/4. - (aEW*cmath.pi*MZ**2)/(Gf*cmath.sqrt(2))))',
               texname = 'M_W')

ee = Parameter(name = 'ee',
               nature = 'internal',
               type = 'real',
               value = '2*cmath.sqrt(aEW)*cmath.sqrt(cmath.pi)',
               texname = 'e')

sw2 = Parameter(name = 'sw2',
                nature = 'internal',
                type = 'real',
                value = '1 - MW**2/MZ**2',
                texname = '\\text{sw2}')

cw = Parameter(name = 'cw',
               nature = 'internal',
               type = 'real',
               value = 'cmath.sqrt(1 - sw2)',
               texname = 'c_w')

sw = Parameter(name = 'sw',
               nature = 'internal',
               type = 'real',
               value = 'cmath.sqrt(sw2)',
               texname = 's_w')

g1 = Parameter(name = 'g1',
               nature = 'internal',
               type = 'real',
               value = 'ee/cw',
               texname = 'g_1')

gw = Parameter(name = 'gw',
               nature = 'internal',
               type = 'real',
               value = 'ee/sw',
               texname = 'g_w')

vev = Parameter(name = 'vev',
                nature = 'internal',
                type = 'real',
                value = '(2*MW*sw)/ee',
                texname = '\\text{vev}')

lam = Parameter(name = 'lam',
                nature = 'internal',
                type = 'real',
                value = 'MH**2/(2.*vev**2)',
                texname = '\\text{lam}')

yb = Parameter(name = 'yb',
               nature = 'internal',
               type = 'real',
               value = '(ymb*cmath.sqrt(2))/vev',
               texname = '\\text{yb}')

yt = Parameter(name = 'yt',
               nature = 'internal',
               type = 'real',
               value = '(ymt*cmath.sqrt(2))/vev',
               texname = '\\text{yt}')

ytau = Parameter(name = 'ytau',
                 nature = 'internal',
                 type = 'real',
                 value = '(ymtau*cmath.sqrt(2))/vev',
                 texname = '\\text{ytau}')

muH = Parameter(name = 'muH',
                nature = 'internal',
                type = 'real',
                value = 'cmath.sqrt(lam*vev**2)',
                texname = '\\mu')

I1a33 = Parameter(name = 'I1a33',
                  nature = 'internal',
                  type = 'complex',
                  value = 'yb',
                  texname = '\\text{I1a33}')

I2a33 = Parameter(name = 'I2a33',
                  nature = 'internal',
                  type = 'complex',
                  value = 'yt',
                  texname = '\\text{I2a33}')

I3a33 = Parameter(name = 'I3a33',
                  nature = 'internal',
                  type = 'complex',
                  value = 'yt',
                  texname = '\\text{I3a33}')

I4a33 = Parameter(name = 'I4a33',
                  nature = 'internal',
                  type = 'complex',
                  value = 'yb',
                  texname = '\\text{I4a33}')

