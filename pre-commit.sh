#!/bin/sh

# Stash changes that might not be for commit
STASH_NAME="pre-commit-$(date +%s)"
git stash save -q --keep-index $STASH_NAME

# Test prospective commit
# Look for models that don't have descriptions
for a in `find * -maxdepth 1 -type d | grep -ve "PLUGIN" | grep -ve "SUSY-HIT" | grep -e "/"`
do
  if [ ! -e ${a}.txt ]
  then
    echo "The model ${a} is missing a description txt file"
    echo "Please check your commit and try again"
    exit 1
  fi
done

# Look for text files that don't correspond to models
for a in `ls */*txt | grep -ve "shutil_patch"`
do
  if [ ! -d `echo ${a/\.txt/}` ]
  then
    echo "The text file ${a} is missing a model"
    echo "Please check your commit carefully"
    exit 1
  fi
done

# Now look for an updated model list file
for a in `find * -maxdepth 1 -type d | grep -ve "PLUGIN" | grep -ve "SUSY-HIT" | grep -e "/" | sort --ignore-case`
do
  printf "${a}\n" >> model_list_tmp.txt
  cat ${a}.txt >> model_list_tmp.txt
  printf '\n\n' >> model_list_tmp.txt
done
cat -s model_list_tmp.txt > model_list_updated.txt
rm model_list_tmp.txt
if [ -n "`diff model_list.txt model_list_updated.txt`" ]
then
  echo "Commit rejected - model_list.txt file is out of date."
  echo "Please check the difference between the one you have and"
  echo "the one in model_list_updated.txt ; if everything looks"
  echo "sensible, simply move over model_list_updated.txt to"
  echo "model_list.txt , git add, and commit again."
  echo "If this is part of a CI, you can use the pre-commit.sh"
  echo "script to reproduce the error and generate an updated"
  echo "model list file to use."
  echo "Apologies for the trouble."
  exit 1
else
  # Clean up if all was fine
  rm model_list_updated.txt
fi

# Check if we are python3 compatible
./py3compat.py
if [ $? -eq 1 ]
then
   echo "At least one model failed python3 compatability tests"
   echo "Please check your model and try again. There are instructions"
   echo "provided in the README.md file of this package for testing"
   echo "python3 compatibility and upgrading your model if needed."
   exit 1
fi

# Un-stash changes
STASHES=$(git stash list)
if [[ $STASHES == "$STASH_NAME" ]]; then
  git stash pop -q
fi