# This file was automatically created by FeynRules 2.3.43
# Mathematica version: 12.1.1 for Mac OS X x86 (64-bit) (June 19, 2020)
# Date: Thu 6 May 2021 23:16:07



from object_library import all_parameters, Parameter


from function_library import complexconjugate, re, im, csc, sec, acsc, asec, cot

# This is a default parameter object representing 0.
ZERO = Parameter(name = 'ZERO',
                 nature = 'internal',
                 type = 'real',
                 value = '0.0',
                 texname = '0')

# User-defined parameters.
falp = Parameter(name = 'falp',
                 nature = 'external',
                 type = 'real',
                 value = 1000,
                 texname = '\\text{falp}',
                 lhablock = 'ALPPARAMS',
                 lhacode = [ 1 ])

kUL11 = Parameter(name = 'kUL11',
                  nature = 'external',
                  type = 'real',
                  value = 1.,
                  texname = '\\text{kUL11}',
                  lhablock = 'ALPPARAMS',
                  lhacode = [ 2 ])

kUL22 = Parameter(name = 'kUL22',
                  nature = 'external',
                  type = 'real',
                  value = 1.,
                  texname = '\\text{kUL22}',
                  lhablock = 'ALPPARAMS',
                  lhacode = [ 3 ])

kUL33 = Parameter(name = 'kUL33',
                  nature = 'external',
                  type = 'real',
                  value = 1.,
                  texname = '\\text{kUL33}',
                  lhablock = 'ALPPARAMS',
                  lhacode = [ 4 ])

kUL12 = Parameter(name = 'kUL12',
                  nature = 'external',
                  type = 'real',
                  value = 0.,
                  texname = '\\text{kUL12}',
                  lhablock = 'ALPPARAMS',
                  lhacode = [ 5 ])

kUL13 = Parameter(name = 'kUL13',
                  nature = 'external',
                  type = 'real',
                  value = 0.,
                  texname = '\\text{kUL13}',
                  lhablock = 'ALPPARAMS',
                  lhacode = [ 6 ])

kUL23 = Parameter(name = 'kUL23',
                  nature = 'external',
                  type = 'real',
                  value = 0.,
                  texname = '\\text{kUL23}',
                  lhablock = 'ALPPARAMS',
                  lhacode = [ 7 ])

kuR11 = Parameter(name = 'kuR11',
                  nature = 'external',
                  type = 'real',
                  value = 1.,
                  texname = '\\text{kuR11}',
                  lhablock = 'ALPPARAMS',
                  lhacode = [ 8 ])

kuR22 = Parameter(name = 'kuR22',
                  nature = 'external',
                  type = 'real',
                  value = 1.,
                  texname = '\\text{kuR22}',
                  lhablock = 'ALPPARAMS',
                  lhacode = [ 9 ])

kuR33 = Parameter(name = 'kuR33',
                  nature = 'external',
                  type = 'real',
                  value = 1.,
                  texname = '\\text{kuR33}',
                  lhablock = 'ALPPARAMS',
                  lhacode = [ 10 ])

kuR12 = Parameter(name = 'kuR12',
                  nature = 'external',
                  type = 'real',
                  value = 0.,
                  texname = '\\text{kuR12}',
                  lhablock = 'ALPPARAMS',
                  lhacode = [ 11 ])

kuR13 = Parameter(name = 'kuR13',
                  nature = 'external',
                  type = 'real',
                  value = 0.,
                  texname = '\\text{kuR13}',
                  lhablock = 'ALPPARAMS',
                  lhacode = [ 12 ])

kuR23 = Parameter(name = 'kuR23',
                  nature = 'external',
                  type = 'real',
                  value = 0.,
                  texname = '\\text{kuR23}',
                  lhablock = 'ALPPARAMS',
                  lhacode = [ 13 ])

kdR11 = Parameter(name = 'kdR11',
                  nature = 'external',
                  type = 'real',
                  value = 1.,
                  texname = '\\text{kdR11}',
                  lhablock = 'ALPPARAMS',
                  lhacode = [ 14 ])

kdR22 = Parameter(name = 'kdR22',
                  nature = 'external',
                  type = 'real',
                  value = 1.,
                  texname = '\\text{kdR22}',
                  lhablock = 'ALPPARAMS',
                  lhacode = [ 15 ])

kdR33 = Parameter(name = 'kdR33',
                  nature = 'external',
                  type = 'real',
                  value = 1.,
                  texname = '\\text{kdR33}',
                  lhablock = 'ALPPARAMS',
                  lhacode = [ 16 ])

kdR12 = Parameter(name = 'kdR12',
                  nature = 'external',
                  type = 'real',
                  value = 0.,
                  texname = '\\text{kdR12}',
                  lhablock = 'ALPPARAMS',
                  lhacode = [ 17 ])

kdR13 = Parameter(name = 'kdR13',
                  nature = 'external',
                  type = 'real',
                  value = 0.,
                  texname = '\\text{kdR13}',
                  lhablock = 'ALPPARAMS',
                  lhacode = [ 18 ])

kdR23 = Parameter(name = 'kdR23',
                  nature = 'external',
                  type = 'real',
                  value = 0.,
                  texname = '\\text{kdR23}',
                  lhablock = 'ALPPARAMS',
                  lhacode = [ 19 ])

knuL11 = Parameter(name = 'knuL11',
                   nature = 'external',
                   type = 'real',
                   value = 1.,
                   texname = '\\text{knuL11}',
                   lhablock = 'ALPPARAMS',
                   lhacode = [ 20 ])

knuL22 = Parameter(name = 'knuL22',
                   nature = 'external',
                   type = 'real',
                   value = 1.,
                   texname = '\\text{knuL22}',
                   lhablock = 'ALPPARAMS',
                   lhacode = [ 21 ])

knuL33 = Parameter(name = 'knuL33',
                   nature = 'external',
                   type = 'real',
                   value = 1.,
                   texname = '\\text{knuL33}',
                   lhablock = 'ALPPARAMS',
                   lhacode = [ 22 ])

knuL12 = Parameter(name = 'knuL12',
                   nature = 'external',
                   type = 'real',
                   value = 0.,
                   texname = '\\text{knuL12}',
                   lhablock = 'ALPPARAMS',
                   lhacode = [ 23 ])

knuL13 = Parameter(name = 'knuL13',
                   nature = 'external',
                   type = 'real',
                   value = 0.,
                   texname = '\\text{knuL13}',
                   lhablock = 'ALPPARAMS',
                   lhacode = [ 24 ])

knuL23 = Parameter(name = 'knuL23',
                   nature = 'external',
                   type = 'real',
                   value = 0.,
                   texname = '\\text{knuL23}',
                   lhablock = 'ALPPARAMS',
                   lhacode = [ 25 ])

kEL11 = Parameter(name = 'kEL11',
                  nature = 'external',
                  type = 'real',
                  value = 1.,
                  texname = '\\text{kEL11}',
                  lhablock = 'ALPPARAMS',
                  lhacode = [ 26 ])

kEL22 = Parameter(name = 'kEL22',
                  nature = 'external',
                  type = 'real',
                  value = 1.,
                  texname = '\\text{kEL22}',
                  lhablock = 'ALPPARAMS',
                  lhacode = [ 27 ])

kEL33 = Parameter(name = 'kEL33',
                  nature = 'external',
                  type = 'real',
                  value = 1.,
                  texname = '\\text{kEL33}',
                  lhablock = 'ALPPARAMS',
                  lhacode = [ 28 ])

kEL12 = Parameter(name = 'kEL12',
                  nature = 'external',
                  type = 'real',
                  value = 0.,
                  texname = '\\text{kEL12}',
                  lhablock = 'ALPPARAMS',
                  lhacode = [ 29 ])

kEL13 = Parameter(name = 'kEL13',
                  nature = 'external',
                  type = 'real',
                  value = 0.,
                  texname = '\\text{kEL13}',
                  lhablock = 'ALPPARAMS',
                  lhacode = [ 30 ])

kEL23 = Parameter(name = 'kEL23',
                  nature = 'external',
                  type = 'real',
                  value = 0.,
                  texname = '\\text{kEL23}',
                  lhablock = 'ALPPARAMS',
                  lhacode = [ 31 ])

keR11 = Parameter(name = 'keR11',
                  nature = 'external',
                  type = 'real',
                  value = 1.,
                  texname = '\\text{keR11}',
                  lhablock = 'ALPPARAMS',
                  lhacode = [ 32 ])

keR22 = Parameter(name = 'keR22',
                  nature = 'external',
                  type = 'real',
                  value = 1.,
                  texname = '\\text{keR22}',
                  lhablock = 'ALPPARAMS',
                  lhacode = [ 33 ])

keR33 = Parameter(name = 'keR33',
                  nature = 'external',
                  type = 'real',
                  value = 1.,
                  texname = '\\text{keR33}',
                  lhablock = 'ALPPARAMS',
                  lhacode = [ 34 ])

keR12 = Parameter(name = 'keR12',
                  nature = 'external',
                  type = 'real',
                  value = 0.,
                  texname = '\\text{keR12}',
                  lhablock = 'ALPPARAMS',
                  lhacode = [ 35 ])

keR13 = Parameter(name = 'keR13',
                  nature = 'external',
                  type = 'real',
                  value = 0.,
                  texname = '\\text{keR13}',
                  lhablock = 'ALPPARAMS',
                  lhacode = [ 36 ])

keR23 = Parameter(name = 'keR23',
                  nature = 'external',
                  type = 'real',
                  value = 0.,
                  texname = '\\text{keR23}',
                  lhablock = 'ALPPARAMS',
                  lhacode = [ 37 ])

cah = Parameter(name = 'cah',
                nature = 'external',
                type = 'real',
                value = 0.,
                texname = '\\text{cah}',
                lhablock = 'ALPPARAMS',
                lhacode = [ 38 ])

cZh5 = Parameter(name = 'cZh5',
                 nature = 'external',
                 type = 'real',
                 value = 0.,
                 texname = '\\text{cZh5}',
                 lhablock = 'ALPPARAMS',
                 lhacode = [ 39 ])

cWW = Parameter(name = 'cWW',
                nature = 'external',
                type = 'real',
                value = 1.,
                texname = '\\text{cWW}',
                lhablock = 'ALPPARAMS',
                lhacode = [ 40 ])

cYY = Parameter(name = 'cYY',
                nature = 'external',
                type = 'real',
                value = 1.,
                texname = '\\text{cYY}',
                lhablock = 'ALPPARAMS',
                lhacode = [ 41 ])

cGG = Parameter(name = 'cGG',
                nature = 'external',
                type = 'real',
                value = 1.,
                texname = '\\text{cGG}',
                lhablock = 'ALPPARAMS',
                lhacode = [ 42 ])

cabi = Parameter(name = 'cabi',
                 nature = 'external',
                 type = 'real',
                 value = 0.227736,
                 texname = '\\theta _c',
                 lhablock = 'CKMBLOCK',
                 lhacode = [ 1 ])

aEWM1 = Parameter(name = 'aEWM1',
                  nature = 'external',
                  type = 'real',
                  value = 127.9,
                  texname = '\\text{aEWM1}',
                  lhablock = 'SMINPUTS',
                  lhacode = [ 1 ])

Gf = Parameter(name = 'Gf',
               nature = 'external',
               type = 'real',
               value = 0.0000116637,
               texname = 'G_f',
               lhablock = 'SMINPUTS',
               lhacode = [ 2 ])

aS = Parameter(name = 'aS',
               nature = 'external',
               type = 'real',
               value = 0.1184,
               texname = '\\alpha _s',
               lhablock = 'SMINPUTS',
               lhacode = [ 3 ])

ymdo = Parameter(name = 'ymdo',
                 nature = 'external',
                 type = 'real',
                 value = 0.00504,
                 texname = '\\text{ymdo}',
                 lhablock = 'YUKAWA',
                 lhacode = [ 1 ])

ymup = Parameter(name = 'ymup',
                 nature = 'external',
                 type = 'real',
                 value = 0.00255,
                 texname = '\\text{ymup}',
                 lhablock = 'YUKAWA',
                 lhacode = [ 2 ])

yms = Parameter(name = 'yms',
                nature = 'external',
                type = 'real',
                value = 0.101,
                texname = '\\text{yms}',
                lhablock = 'YUKAWA',
                lhacode = [ 3 ])

ymc = Parameter(name = 'ymc',
                nature = 'external',
                type = 'real',
                value = 1.27,
                texname = '\\text{ymc}',
                lhablock = 'YUKAWA',
                lhacode = [ 4 ])

ymb = Parameter(name = 'ymb',
                nature = 'external',
                type = 'real',
                value = 4.7,
                texname = '\\text{ymb}',
                lhablock = 'YUKAWA',
                lhacode = [ 5 ])

ymt = Parameter(name = 'ymt',
                nature = 'external',
                type = 'real',
                value = 172,
                texname = '\\text{ymt}',
                lhablock = 'YUKAWA',
                lhacode = [ 6 ])

yme = Parameter(name = 'yme',
                nature = 'external',
                type = 'real',
                value = 0.000511,
                texname = '\\text{yme}',
                lhablock = 'YUKAWA',
                lhacode = [ 11 ])

ymm = Parameter(name = 'ymm',
                nature = 'external',
                type = 'real',
                value = 0.10566,
                texname = '\\text{ymm}',
                lhablock = 'YUKAWA',
                lhacode = [ 13 ])

ymtau = Parameter(name = 'ymtau',
                  nature = 'external',
                  type = 'real',
                  value = 1.777,
                  texname = '\\text{ymtau}',
                  lhablock = 'YUKAWA',
                  lhacode = [ 15 ])

MZ = Parameter(name = 'MZ',
               nature = 'external',
               type = 'real',
               value = 91.1876,
               texname = '\\text{MZ}',
               lhablock = 'MASS',
               lhacode = [ 23 ])

Me = Parameter(name = 'Me',
               nature = 'external',
               type = 'real',
               value = 0.000511,
               texname = '\\text{Me}',
               lhablock = 'MASS',
               lhacode = [ 11 ])

MMU = Parameter(name = 'MMU',
                nature = 'external',
                type = 'real',
                value = 0.10566,
                texname = '\\text{MMU}',
                lhablock = 'MASS',
                lhacode = [ 13 ])

MTA = Parameter(name = 'MTA',
                nature = 'external',
                type = 'real',
                value = 1.777,
                texname = '\\text{MTA}',
                lhablock = 'MASS',
                lhacode = [ 15 ])

MU = Parameter(name = 'MU',
               nature = 'external',
               type = 'real',
               value = 0.00255,
               texname = 'M',
               lhablock = 'MASS',
               lhacode = [ 2 ])

MC = Parameter(name = 'MC',
               nature = 'external',
               type = 'real',
               value = 1.27,
               texname = '\\text{MC}',
               lhablock = 'MASS',
               lhacode = [ 4 ])

MT = Parameter(name = 'MT',
               nature = 'external',
               type = 'real',
               value = 172,
               texname = '\\text{MT}',
               lhablock = 'MASS',
               lhacode = [ 6 ])

MD = Parameter(name = 'MD',
               nature = 'external',
               type = 'real',
               value = 0.00504,
               texname = '\\text{MD}',
               lhablock = 'MASS',
               lhacode = [ 1 ])

MS = Parameter(name = 'MS',
               nature = 'external',
               type = 'real',
               value = 0.101,
               texname = '\\text{MS}',
               lhablock = 'MASS',
               lhacode = [ 3 ])

MB = Parameter(name = 'MB',
               nature = 'external',
               type = 'real',
               value = 4.7,
               texname = '\\text{MB}',
               lhablock = 'MASS',
               lhacode = [ 5 ])

MH = Parameter(name = 'MH',
               nature = 'external',
               type = 'real',
               value = 125,
               texname = '\\text{MH}',
               lhablock = 'MASS',
               lhacode = [ 25 ])

Ma = Parameter(name = 'Ma',
               nature = 'external',
               type = 'real',
               value = 1.,
               texname = '\\text{Ma}',
               lhablock = 'MASS',
               lhacode = [ 9000005 ])

WZ = Parameter(name = 'WZ',
               nature = 'external',
               type = 'real',
               value = 2.4952,
               texname = '\\text{WZ}',
               lhablock = 'DECAY',
               lhacode = [ 23 ])

WW = Parameter(name = 'WW',
               nature = 'external',
               type = 'real',
               value = 2.085,
               texname = '\\text{WW}',
               lhablock = 'DECAY',
               lhacode = [ 24 ])

WT = Parameter(name = 'WT',
               nature = 'external',
               type = 'real',
               value = 1.50833649,
               texname = '\\text{WT}',
               lhablock = 'DECAY',
               lhacode = [ 6 ])

WH = Parameter(name = 'WH',
               nature = 'external',
               type = 'real',
               value = 0.00407,
               texname = '\\text{WH}',
               lhablock = 'DECAY',
               lhacode = [ 25 ])

WALP = Parameter(name = 'WALP',
                 nature = 'external',
                 type = 'real',
                 value = 1,
                 texname = '\\text{WALP}',
                 lhablock = 'DECAY',
                 lhacode = [ 9000005 ])

aEW = Parameter(name = 'aEW',
                nature = 'internal',
                type = 'real',
                value = '1/aEWM1',
                texname = '\\alpha _{\\text{EW}}')

G = Parameter(name = 'G',
              nature = 'internal',
              type = 'real',
              value = '2*cmath.sqrt(aS)*cmath.sqrt(cmath.pi)',
              texname = 'G')

CKM1x1 = Parameter(name = 'CKM1x1',
                   nature = 'internal',
                   type = 'complex',
                   value = 'cmath.cos(cabi)',
                   texname = '\\text{CKM1x1}')

CKM1x2 = Parameter(name = 'CKM1x2',
                   nature = 'internal',
                   type = 'complex',
                   value = 'cmath.sin(cabi)',
                   texname = '\\text{CKM1x2}')

CKM1x3 = Parameter(name = 'CKM1x3',
                   nature = 'internal',
                   type = 'complex',
                   value = '0',
                   texname = '\\text{CKM1x3}')

CKM2x1 = Parameter(name = 'CKM2x1',
                   nature = 'internal',
                   type = 'complex',
                   value = '-cmath.sin(cabi)',
                   texname = '\\text{CKM2x1}')

CKM2x2 = Parameter(name = 'CKM2x2',
                   nature = 'internal',
                   type = 'complex',
                   value = 'cmath.cos(cabi)',
                   texname = '\\text{CKM2x2}')

CKM2x3 = Parameter(name = 'CKM2x3',
                   nature = 'internal',
                   type = 'complex',
                   value = '0',
                   texname = '\\text{CKM2x3}')

CKM3x1 = Parameter(name = 'CKM3x1',
                   nature = 'internal',
                   type = 'complex',
                   value = '0',
                   texname = '\\text{CKM3x1}')

CKM3x2 = Parameter(name = 'CKM3x2',
                   nature = 'internal',
                   type = 'complex',
                   value = '0',
                   texname = '\\text{CKM3x2}')

CKM3x3 = Parameter(name = 'CKM3x3',
                   nature = 'internal',
                   type = 'complex',
                   value = '1',
                   texname = '\\text{CKM3x3}')

MW = Parameter(name = 'MW',
               nature = 'internal',
               type = 'real',
               value = 'cmath.sqrt(MZ**2/2. + cmath.sqrt(MZ**4/4. - (aEW*cmath.pi*MZ**2)/(Gf*cmath.sqrt(2))))',
               texname = 'M_W')

ee = Parameter(name = 'ee',
               nature = 'internal',
               type = 'real',
               value = '2*cmath.sqrt(aEW)*cmath.sqrt(cmath.pi)',
               texname = 'e')

kDL11 = Parameter(name = 'kDL11',
                  nature = 'internal',
                  type = 'real',
                  value = '(CKM1x1*kUL11 + CKM2x1*kUL12 + CKM3x1*kUL13)*complexconjugate(CKM1x1) + (CKM1x1*kUL12 + CKM2x1*kUL22 + CKM3x1*kUL23)*complexconjugate(CKM2x1) + (CKM1x1*kUL13 + CKM2x1*kUL23 + CKM3x1*kUL33)*complexconjugate(CKM3x1)',
                  texname = '\\text{kDL11}')

kDL12 = Parameter(name = 'kDL12',
                  nature = 'internal',
                  type = 'real',
                  value = '(CKM1x2*kUL11 + CKM2x2*kUL12 + CKM3x2*kUL13)*complexconjugate(CKM1x1) + (CKM1x2*kUL12 + CKM2x2*kUL22 + CKM3x2*kUL23)*complexconjugate(CKM2x1) + (CKM1x2*kUL13 + CKM2x2*kUL23 + CKM3x2*kUL33)*complexconjugate(CKM3x1)',
                  texname = '\\text{kDL12}')

kDL13 = Parameter(name = 'kDL13',
                  nature = 'internal',
                  type = 'real',
                  value = '(CKM1x3*kUL11 + CKM2x3*kUL12 + CKM3x3*kUL13)*complexconjugate(CKM1x1) + (CKM1x3*kUL12 + CKM2x3*kUL22 + CKM3x3*kUL23)*complexconjugate(CKM2x1) + (CKM1x3*kUL13 + CKM2x3*kUL23 + CKM3x3*kUL33)*complexconjugate(CKM3x1)',
                  texname = '\\text{kDL13}')

kDL22 = Parameter(name = 'kDL22',
                  nature = 'internal',
                  type = 'real',
                  value = '(CKM1x2*kUL11 + CKM2x2*kUL12 + CKM3x2*kUL13)*complexconjugate(CKM1x2) + (CKM1x2*kUL12 + CKM2x2*kUL22 + CKM3x2*kUL23)*complexconjugate(CKM2x2) + (CKM1x2*kUL13 + CKM2x2*kUL23 + CKM3x2*kUL33)*complexconjugate(CKM3x2)',
                  texname = '\\text{kDL22}')

kDL23 = Parameter(name = 'kDL23',
                  nature = 'internal',
                  type = 'real',
                  value = '(CKM1x3*kUL11 + CKM2x3*kUL12 + CKM3x3*kUL13)*complexconjugate(CKM1x2) + (CKM1x3*kUL12 + CKM2x3*kUL22 + CKM3x3*kUL23)*complexconjugate(CKM2x2) + (CKM1x3*kUL13 + CKM2x3*kUL23 + CKM3x3*kUL33)*complexconjugate(CKM3x2)',
                  texname = '\\text{kDL23}')

kDL33 = Parameter(name = 'kDL33',
                  nature = 'internal',
                  type = 'real',
                  value = '(CKM1x3*kUL11 + CKM2x3*kUL12 + CKM3x3*kUL13)*complexconjugate(CKM1x3) + (CKM1x3*kUL12 + CKM2x3*kUL22 + CKM3x3*kUL23)*complexconjugate(CKM2x3) + (CKM1x3*kUL13 + CKM2x3*kUL23 + CKM3x3*kUL33)*complexconjugate(CKM3x3)',
                  texname = '\\text{kDL33}')

sw2 = Parameter(name = 'sw2',
                nature = 'internal',
                type = 'real',
                value = '1 - MW**2/MZ**2',
                texname = '\\text{sw2}')

cw = Parameter(name = 'cw',
               nature = 'internal',
               type = 'real',
               value = 'cmath.sqrt(1 - sw2)',
               texname = 'c_w')

sw = Parameter(name = 'sw',
               nature = 'internal',
               type = 'real',
               value = 'cmath.sqrt(sw2)',
               texname = 's_w')

g1 = Parameter(name = 'g1',
               nature = 'internal',
               type = 'real',
               value = 'ee/cw',
               texname = 'g_1')

gw = Parameter(name = 'gw',
               nature = 'internal',
               type = 'real',
               value = 'ee/sw',
               texname = 'g_w')

vev = Parameter(name = 'vev',
                nature = 'internal',
                type = 'real',
                value = '(2*MW*sw)/ee',
                texname = '\\text{vev}')

lam = Parameter(name = 'lam',
                nature = 'internal',
                type = 'real',
                value = 'MH**2/(2.*vev**2)',
                texname = '\\text{lam}')

yb = Parameter(name = 'yb',
               nature = 'internal',
               type = 'real',
               value = '(ymb*cmath.sqrt(2))/vev',
               texname = '\\text{yb}')

yc = Parameter(name = 'yc',
               nature = 'internal',
               type = 'real',
               value = '(ymc*cmath.sqrt(2))/vev',
               texname = '\\text{yc}')

ydo = Parameter(name = 'ydo',
                nature = 'internal',
                type = 'real',
                value = '(ymdo*cmath.sqrt(2))/vev',
                texname = '\\text{ydo}')

ye = Parameter(name = 'ye',
               nature = 'internal',
               type = 'real',
               value = '(yme*cmath.sqrt(2))/vev',
               texname = '\\text{ye}')

ym = Parameter(name = 'ym',
               nature = 'internal',
               type = 'real',
               value = '(ymm*cmath.sqrt(2))/vev',
               texname = '\\text{ym}')

ys = Parameter(name = 'ys',
               nature = 'internal',
               type = 'real',
               value = '(yms*cmath.sqrt(2))/vev',
               texname = '\\text{ys}')

yt = Parameter(name = 'yt',
               nature = 'internal',
               type = 'real',
               value = '(ymt*cmath.sqrt(2))/vev',
               texname = '\\text{yt}')

ytau = Parameter(name = 'ytau',
                 nature = 'internal',
                 type = 'real',
                 value = '(ymtau*cmath.sqrt(2))/vev',
                 texname = '\\text{ytau}')

yup = Parameter(name = 'yup',
                nature = 'internal',
                type = 'real',
                value = '(ymup*cmath.sqrt(2))/vev',
                texname = '\\text{yup}')

muH = Parameter(name = 'muH',
                nature = 'internal',
                type = 'real',
                value = 'cmath.sqrt(lam*vev**2)',
                texname = '\\mu')

