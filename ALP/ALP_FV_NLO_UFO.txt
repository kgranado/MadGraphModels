Requestor: Nicola Orlando (model from Andrea Thamm)
Content: ALP model based on ALP_NLO_UFO including flavour violating interactions
Analysis team: followup of an ongoing publication https://atlas-glance.cern.ch/atlas/analysis/analyses/details.php?id=5248
JIRA: https://its.cern.ch/jira/browse/AGENE-2094
Paper: arXiv:2012.12272
