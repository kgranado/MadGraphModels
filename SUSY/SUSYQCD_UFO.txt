Requestor: Simone Amoroso
Contents: Pair production of gluinos at NLO
Paper: http://arxiv.org/abs/1510.00391
Website: https://feynrules.irmp.ucl.ac.be/raw-attachment/wiki/NLOModels/
JIRA: https://its.cern.ch/jira/browse/AGENE-1226