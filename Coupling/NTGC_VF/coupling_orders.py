# This file was automatically created by FeynRules 2.3.49
# Mathematica version: 11.2.0 for Microsoft Windows (64-bit) (September 11, 2017)
# Date: Thu 28 Sep 2023 13:33:30


from object_library import all_orders, CouplingOrder


QCD = CouplingOrder(name = 'QCD',
                    expansion_order = 99,
                    hierarchy = 1)

QED = CouplingOrder(name = 'QED',
                    expansion_order = 99,
                    hierarchy = 2)

F4A = CouplingOrder(name = 'F4A',
                    expansion_order = 99,
                    hierarchy = 1)

F4Z = CouplingOrder(name = 'F4Z',
                    expansion_order = 99,
                    hierarchy = 1)

F5A = CouplingOrder(name = 'F5A',
                    expansion_order = 99,
                    hierarchy = 1)

F5Z = CouplingOrder(name = 'F5Z',
                    expansion_order = 99,
                    hierarchy = 1)

H1A = CouplingOrder(name = 'H1A',
                    expansion_order = 99,
                    hierarchy = 1)

H1Z = CouplingOrder(name = 'H1Z',
                    expansion_order = 99,
                    hierarchy = 1)

H2A = CouplingOrder(name = 'H2A',
                    expansion_order = 99,
                    hierarchy = 1)

H2Z = CouplingOrder(name = 'H2Z',
                    expansion_order = 99,
                    hierarchy = 1)

H3A = CouplingOrder(name = 'H3A',
                    expansion_order = 99,
                    hierarchy = 1)

H3Z = CouplingOrder(name = 'H3Z',
                    expansion_order = 99,
                    hierarchy = 1)

H4A = CouplingOrder(name = 'H4A',
                    expansion_order = 99,
                    hierarchy = 1)

H4Z = CouplingOrder(name = 'H4Z',
                    expansion_order = 99,
                    hierarchy = 1)

H5A = CouplingOrder(name = 'H5A',
                    expansion_order = 99,
                    hierarchy = 1)

H5Z = CouplingOrder(name = 'H5Z',
                    expansion_order = 99,
                    hierarchy = 1)

H6A = CouplingOrder(name = 'H6A',
                    expansion_order = 99,
                    hierarchy = 1)

H6Z = CouplingOrder(name = 'H6Z',
                    expansion_order = 99,
                    hierarchy = 1)

