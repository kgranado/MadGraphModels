# This file was automatically created by FeynRules 2.3.49
# Mathematica version: 12.0.0 for Linux x86 (64-bit) (April 7, 2019)
# Date: Wed 10 May 2023 13:02:43


from object_library import all_couplings, Coupling

from function_library import complexconjugate, re, im, csc, sec, acsc, asec, cot



GC_1 = Coupling(name = 'GC_1',
                value = '(4*complex(0,1)*gcA)/ff**2',
                order = {'NP':1})

GC_2 = Coupling(name = 'GC_2',
                value = '(complex(0,1)*gcL*vev)/ff',
                order = {'NP':1})

