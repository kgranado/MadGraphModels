# This file was automatically created by FeynRules 2.3.47
# Mathematica version: 13.0.0 for Linux x86 (64-bit) (December 3, 2021)
# Date: Mon 9 May 2022 11:24:32


from object_library import all_couplings, Coupling

from function_library import complexconjugate, re, im, csc, sec, acsc, asec, cot



GC_1 = Coupling(name = 'GC_1',
                value = '-0.3333333333333333*(ee*complex(0,1))',
                order = {'QED':1})

GC_10 = Coupling(name = 'GC_10',
                 value = '8*ee**2*complex(0,1)',
                 order = {'QED':2})

GC_100 = Coupling(name = 'GC_100',
                  value = '(ee*complex(0,1)*KWBYR)/(sw*cmath.sqrt(2))',
                  order = {'VLQ':1})

GC_101 = Coupling(name = 'GC_101',
                  value = '-((ee*complex(0,1)*KWS10S11)/sw)',
                  order = {'QED':1,'SX':1})

GC_102 = Coupling(name = 'GC_102',
                  value = '(ee*complex(0,1)*KWS10S11)/sw',
                  order = {'QED':1,'SX':1})

GC_103 = Coupling(name = 'GC_103',
                  value = '-((ee*complex(0,1)*KWS11S12)/sw)',
                  order = {'QED':1,'SX':1})

GC_104 = Coupling(name = 'GC_104',
                  value = '(ee*complex(0,1)*KWS11S12)/sw',
                  order = {'QED':1,'SX':1})

GC_105 = Coupling(name = 'GC_105',
                  value = '(ee*complex(0,1)*KWTBL)/(sw*cmath.sqrt(2))',
                  order = {'VLQ':1})

GC_106 = Coupling(name = 'GC_106',
                  value = '(ee*complex(0,1)*KWTBR)/(sw*cmath.sqrt(2))',
                  order = {'VLQ':1})

GC_107 = Coupling(name = 'GC_107',
                  value = '(ee*complex(0,1)*KWXTL)/(sw*cmath.sqrt(2))',
                  order = {'VLQ':1})

GC_108 = Coupling(name = 'GC_108',
                  value = '(ee*complex(0,1)*KWXTR)/(sw*cmath.sqrt(2))',
                  order = {'VLQ':1})

GC_109 = Coupling(name = 'GC_109',
                  value = '(ee*complex(0,1)*KXL3)/(sw*cmath.sqrt(2))',
                  order = {'QED':1,'VLQ':1})

GC_11 = Coupling(name = 'GC_11',
                 value = '-G',
                 order = {'QCD':1})

GC_110 = Coupling(name = 'GC_110',
                  value = '(ee*complex(0,1)*KXR3)/(sw*cmath.sqrt(2))',
                  order = {'QED':1,'VLQ':1})

GC_111 = Coupling(name = 'GC_111',
                  value = '(ee*complex(0,1)*KYL3)/(sw*cmath.sqrt(2))',
                  order = {'QED':1,'VLQ':1})

GC_112 = Coupling(name = 'GC_112',
                  value = '(ee*complex(0,1)*KYR3)/(sw*cmath.sqrt(2))',
                  order = {'QED':1,'VLQ':1})

GC_113 = Coupling(name = 'GC_113',
                  value = '(ee*complex(0,1)*KZBBL)/(2.*cw*sw)',
                  order = {'VLQ':1})

GC_114 = Coupling(name = 'GC_114',
                  value = '(ee*complex(0,1)*KZBBR)/(2.*cw*sw)',
                  order = {'VLQ':1})

GC_115 = Coupling(name = 'GC_115',
                  value = '-((ee*complex(0,1)*KZS11S11)/(cw*sw))',
                  order = {'QED':1,'SX':1})

GC_116 = Coupling(name = 'GC_116',
                  value = '-((ee*complex(0,1)*KZS12S12)/(cw*sw))',
                  order = {'QED':1,'SX':1})

GC_117 = Coupling(name = 'GC_117',
                  value = '(ee*complex(0,1)*KZTTL)/(2.*cw*sw)',
                  order = {'VLQ':1})

GC_118 = Coupling(name = 'GC_118',
                  value = '(ee*complex(0,1)*KZTTR)/(2.*cw*sw)',
                  order = {'VLQ':1})

GC_119 = Coupling(name = 'GC_119',
                  value = '(ee*complex(0,1)*KZXXL)/(2.*cw*sw)',
                  order = {'VLQ':1})

GC_12 = Coupling(name = 'GC_12',
                 value = 'complex(0,1)*G',
                 order = {'QCD':1})

GC_120 = Coupling(name = 'GC_120',
                  value = '(ee*complex(0,1)*KZXXR)/(2.*cw*sw)',
                  order = {'VLQ':1})

GC_121 = Coupling(name = 'GC_121',
                  value = '(ee*complex(0,1)*KZYYL)/(2.*cw*sw)',
                  order = {'VLQ':1})

GC_122 = Coupling(name = 'GC_122',
                  value = '(ee*complex(0,1)*KZYYR)/(2.*cw*sw)',
                  order = {'VLQ':1})

GC_123 = Coupling(name = 'GC_123',
                  value = '(ee*complex(0,1)*sw)/(3.*cw)',
                  order = {'QED':1})

GC_124 = Coupling(name = 'GC_124',
                  value = '(ee*complex(0,1)*sw)/(2.*cw)',
                  order = {'QED':1})

GC_125 = Coupling(name = 'GC_125',
                  value = '(-2*ee*complex(0,1)*sw)/(3.*cw)',
                  order = {'QED':1})

GC_126 = Coupling(name = 'GC_126',
                  value = '-0.5*(cw*ee*complex(0,1))/sw - (ee*complex(0,1)*sw)/(6.*cw)',
                  order = {'QED':1})

GC_127 = Coupling(name = 'GC_127',
                  value = '(cw*ee*complex(0,1))/(2.*sw) - (ee*complex(0,1)*sw)/(6.*cw)',
                  order = {'QED':1})

GC_128 = Coupling(name = 'GC_128',
                  value = '(cw*ee*complex(0,1))/(2.*sw) + (ee*complex(0,1)*sw)/(2.*cw)',
                  order = {'QED':1})

GC_129 = Coupling(name = 'GC_129',
                  value = 'ee**2*complex(0,1) + (cw**2*ee**2*complex(0,1))/(2.*sw**2) + (ee**2*complex(0,1)*sw**2)/(2.*cw**2)',
                  order = {'QED':2})

GC_13 = Coupling(name = 'GC_13',
                 value = 'complex(0,1)*G**2',
                 order = {'QCD':2})

GC_130 = Coupling(name = 'GC_130',
                  value = '(-2*complex(0,1)*KHDS10DS10)/vev',
                  order = {'HDSDS':1})

GC_131 = Coupling(name = 'GC_131',
                  value = '-((complex(0,1)*KS11H1)/vev)',
                  order = {'SX':1})

GC_132 = Coupling(name = 'GC_132',
                  value = '-((ee*complex(0,1)*KS11H1)/vev)',
                  order = {'QED':1,'SX':1})

GC_133 = Coupling(name = 'GC_133',
                  value = '(2*ee**2*complex(0,1)*KS11H1)/vev',
                  order = {'QED':2,'SX':1})

GC_134 = Coupling(name = 'GC_134',
                  value = '-((complex(0,1)*KS12H1)/vev)',
                  order = {'SX':1})

GC_135 = Coupling(name = 'GC_135',
                  value = '(-2*ee*complex(0,1)*KS12H1)/vev',
                  order = {'QED':1,'SX':1})

GC_136 = Coupling(name = 'GC_136',
                  value = '(8*ee**2*complex(0,1)*KS12H1)/vev',
                  order = {'QED':2,'SX':1})

GC_137 = Coupling(name = 'GC_137',
                  value = '(2*complex(0,1)*KHWW*MW**2)/vev',
                  order = {'HVVMOD':1,'QED':1})

GC_138 = Coupling(name = 'GC_138',
                  value = '(2*complex(0,1)*KHZZ*MW**2)/(cw**2*vev)',
                  order = {'HVVMOD':1,'QED':1})

GC_139 = Coupling(name = 'GC_139',
                  value = '-0.125*(ee**2*complex(0,1)*KP10AA)/(cmath.pi**2*vev)',
                  order = {'QED':2,'SWZ':1})

GC_14 = Coupling(name = 'GC_14',
                 value = 'complex(0,1)*KBLh3',
                 order = {'VLQ':1})

GC_140 = Coupling(name = 'GC_140',
                  value = '(ee**2*complex(0,1)*KS10AA)/(4.*cmath.pi**2*vev)',
                  order = {'QED':2,'SWZ':1})

GC_141 = Coupling(name = 'GC_141',
                  value = '(ee**2*complex(0,1)*KP10W)/(4.*cmath.pi**2*sw**2*vev)',
                  order = {'QED':2,'SWZ':1})

GC_142 = Coupling(name = 'GC_142',
                  value = '-0.125*(ee**2*complex(0,1)*KP10ZZ)/(cw**2*cmath.pi**2*sw**2*vev)',
                  order = {'QED':2,'SWZ':1})

GC_143 = Coupling(name = 'GC_143',
                  value = '-0.25*(ee**2*complex(0,1)*KP11WZ)/(cw*cmath.pi**2*sw**2*vev)',
                  order = {'QED':2,'SX':1})

GC_144 = Coupling(name = 'GC_144',
                  value = '-0.125*(ee**2*complex(0,1)*KP12W)/(cmath.pi**2*sw**2*vev)',
                  order = {'QED':2,'SX':1})

GC_145 = Coupling(name = 'GC_145',
                  value = '(ee**2*complex(0,1)*KS10W)/(4.*cmath.pi**2*sw**2*vev)',
                  order = {'QED':2,'SWZ':1})

GC_146 = Coupling(name = 'GC_146',
                  value = '(ee**2*complex(0,1)*KS10ZZ)/(4.*cw**2*cmath.pi**2*sw**2*vev)',
                  order = {'QED':2,'SWZ':1})

GC_147 = Coupling(name = 'GC_147',
                  value = '(ee**2*complex(0,1)*KS11WZ)/(4.*cw*cmath.pi**2*sw**2*vev)',
                  order = {'QED':2,'SX':1})

GC_148 = Coupling(name = 'GC_148',
                  value = '(ee**2*complex(0,1)*KS12W)/(4.*cmath.pi**2*sw**2*vev)',
                  order = {'QED':2,'SX':1})

GC_149 = Coupling(name = 'GC_149',
                  value = '(ee**2*complex(0,1)*KP10ZA)/(4.*cw*cmath.pi**2*sw*vev)',
                  order = {'QED':2,'SWZ':1})

GC_15 = Coupling(name = 'GC_15',
                 value = 'complex(0,1)*KBRh3',
                 order = {'VLQ':1})

GC_150 = Coupling(name = 'GC_150',
                  value = '(ee**2*complex(0,1)*KP11WA)/(4.*cmath.pi**2*sw*vev)',
                  order = {'QED':2,'SX':1})

GC_151 = Coupling(name = 'GC_151',
                  value = '(ee**2*complex(0,1)*KS10ZA)/(4.*cw*cmath.pi**2*sw*vev)',
                  order = {'QED':2,'SWZ':1})

GC_152 = Coupling(name = 'GC_152',
                  value = '(ee**2*complex(0,1)*KS11WA)/(4.*cmath.pi**2*sw*vev)',
                  order = {'QED':2,'SX':1})

GC_153 = Coupling(name = 'GC_153',
                  value = '2*complex(0,1)*KHHS10*vev',
                  order = {'SX':1})

GC_154 = Coupling(name = 'GC_154',
                  value = '2*complex(0,1)*KHS10S10*vev',
                  order = {'SX':1})

GC_155 = Coupling(name = 'GC_155',
                  value = 'complex(0,1)*KS11H2*vev',
                  order = {'SX':1})

GC_156 = Coupling(name = 'GC_156',
                  value = 'complex(0,1)*KS12H2*vev',
                  order = {'SX':1})

GC_157 = Coupling(name = 'GC_157',
                  value = '-6*complex(0,1)*lam*vev',
                  order = {'QED':1})

GC_158 = Coupling(name = 'GC_158',
                  value = '(-2*complex(0,1)*MW**2)/vev + (ee**2*complex(0,1)*vev)/(2.*sw**2)',
                  order = {'QED':1})

GC_159 = Coupling(name = 'GC_159',
                  value = '(-2*complex(0,1)*MW**2)/(cw**2*vev) + ee**2*complex(0,1)*vev + (cw**2*ee**2*complex(0,1)*vev)/(2.*sw**2) + (ee**2*complex(0,1)*sw**2*vev)/(2.*cw**2)',
                  order = {'QED':1})

GC_16 = Coupling(name = 'GC_16',
                 value = '-(complex(0,1)*KHBB1)',
                 order = {'HBBMOD':1})

GC_160 = Coupling(name = 'GC_160',
                  value = '-((complex(0,1)*yt)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_17 = Coupling(name = 'GC_17',
                 value = '-KHS10Z',
                 order = {'SX':1})

GC_18 = Coupling(name = 'GC_18',
                 value = '-(complex(0,1)*KHTT1)',
                 order = {'HTTMOD':1})

GC_19 = Coupling(name = 'GC_19',
                 value = '-KP10BB',
                 order = {'VLQ':1})

GC_2 = Coupling(name = 'GC_2',
                value = '(2*ee*complex(0,1))/3.',
                order = {'QED':1})

GC_20 = Coupling(name = 'GC_20',
                 value = '-KP10D3x3',
                 order = {'SX':1})

GC_21 = Coupling(name = 'GC_21',
                 value = '-KP10E3x3',
                 order = {'SX':1})

GC_22 = Coupling(name = 'GC_22',
                 value = '-KP10N3x3',
                 order = {'SX':1})

GC_23 = Coupling(name = 'GC_23',
                 value = '-KP10TT',
                 order = {'VLQ':1})

GC_24 = Coupling(name = 'GC_24',
                 value = '-KP10U3x3',
                 order = {'SX':1})

GC_25 = Coupling(name = 'GC_25',
                 value = '-KP10XX',
                 order = {'VLQ':1})

GC_26 = Coupling(name = 'GC_26',
                 value = '-KP10YY',
                 order = {'VLQ':1})

GC_27 = Coupling(name = 'GC_27',
                 value = 'complex(0,1)*KS10BB',
                 order = {'VLQ':1})

GC_28 = Coupling(name = 'GC_28',
                 value = 'complex(0,1)*KS10BL3',
                 order = {'VLQ':1})

GC_29 = Coupling(name = 'GC_29',
                 value = 'complex(0,1)*KS10BR3',
                 order = {'VLQ':1})

GC_3 = Coupling(name = 'GC_3',
                value = '-(ee*complex(0,1))',
                order = {'QED':1})

GC_30 = Coupling(name = 'GC_30',
                 value = 'complex(0,1)*KS10D3x3',
                 order = {'SX':1})

GC_31 = Coupling(name = 'GC_31',
                 value = 'complex(0,1)*KS10E3x3',
                 order = {'SX':1})

GC_32 = Coupling(name = 'GC_32',
                 value = 'complex(0,1)*KS10N3x3',
                 order = {'SX':1})

GC_33 = Coupling(name = 'GC_33',
                 value = 'complex(0,1)*KS10TL3',
                 order = {'VLQ':1})

GC_34 = Coupling(name = 'GC_34',
                 value = 'complex(0,1)*KS10TR3',
                 order = {'VLQ':1})

GC_35 = Coupling(name = 'GC_35',
                 value = 'complex(0,1)*KS10TT',
                 order = {'VLQ':1})

GC_36 = Coupling(name = 'GC_36',
                 value = 'complex(0,1)*KS10U3x3',
                 order = {'SX':1})

GC_37 = Coupling(name = 'GC_37',
                 value = 'complex(0,1)*KS10XX',
                 order = {'VLQ':1})

GC_38 = Coupling(name = 'GC_38',
                 value = 'complex(0,1)*KS10YY',
                 order = {'VLQ':1})

GC_39 = Coupling(name = 'GC_39',
                 value = 'complex(0,1)*KS11BuL3',
                 order = {'VLQ':1})

GC_4 = Coupling(name = 'GC_4',
                value = 'ee*complex(0,1)',
                order = {'QED':1})

GC_40 = Coupling(name = 'GC_40',
                 value = 'complex(0,1)*KS11BuR3',
                 order = {'VLQ':1})

GC_41 = Coupling(name = 'GC_41',
                 value = 'complex(0,1)*KS11BYL',
                 order = {'VLQ':1})

GC_42 = Coupling(name = 'GC_42',
                 value = 'complex(0,1)*KS11BYR',
                 order = {'VLQ':1})

GC_43 = Coupling(name = 'GC_43',
                 value = '2*complex(0,1)*KS11HH',
                 order = {'SX':1})

GC_44 = Coupling(name = 'GC_44',
                 value = '-KS11HW',
                 order = {'SX':1})

GC_45 = Coupling(name = 'GC_45',
                 value = 'complex(0,1)*KS11ll3x3',
                 order = {'SX':1})

GC_46 = Coupling(name = 'GC_46',
                 value = 'complex(0,1)*KS11qqL3x3',
                 order = {'SX':1})

GC_47 = Coupling(name = 'GC_47',
                 value = 'complex(0,1)*KS11qqR3x3',
                 order = {'SX':1})

GC_48 = Coupling(name = 'GC_48',
                 value = 'complex(0,1)*KS11TBL',
                 order = {'VLQ':1})

GC_49 = Coupling(name = 'GC_49',
                 value = 'complex(0,1)*KS11TBR',
                 order = {'VLQ':1})

GC_5 = Coupling(name = 'GC_5',
                value = '(-4*ee*complex(0,1))/3.',
                order = {'QED':1})

GC_50 = Coupling(name = 'GC_50',
                 value = 'complex(0,1)*KS11TdL3',
                 order = {'VLQ':1})

GC_51 = Coupling(name = 'GC_51',
                 value = 'complex(0,1)*KS11TdR3',
                 order = {'VLQ':1})

GC_52 = Coupling(name = 'GC_52',
                 value = 'complex(0,1)*KS11XTL',
                 order = {'VLQ':1})

GC_53 = Coupling(name = 'GC_53',
                 value = 'complex(0,1)*KS11XTR',
                 order = {'VLQ':1})

GC_54 = Coupling(name = 'GC_54',
                 value = 'complex(0,1)*KS11XuL3',
                 order = {'VLQ':1})

GC_55 = Coupling(name = 'GC_55',
                 value = 'complex(0,1)*KS11XuR3',
                 order = {'VLQ':1})

GC_56 = Coupling(name = 'GC_56',
                 value = 'complex(0,1)*KS11YdL3',
                 order = {'VLQ':1})

GC_57 = Coupling(name = 'GC_57',
                 value = 'complex(0,1)*KS11YdR3',
                 order = {'VLQ':1})

GC_58 = Coupling(name = 'GC_58',
                 value = '2*complex(0,1)*KS12HH',
                 order = {'SX':1})

GC_59 = Coupling(name = 'GC_59',
                 value = 'complex(0,1)*KS12XBL',
                 order = {'VLQ':1})

GC_6 = Coupling(name = 'GC_6',
                value = '(5*ee*complex(0,1))/3.',
                order = {'QED':1})

GC_60 = Coupling(name = 'GC_60',
                 value = 'complex(0,1)*KS12XBR',
                 order = {'VLQ':1})

GC_61 = Coupling(name = 'GC_61',
                 value = 'complex(0,1)*KS12XDL3',
                 order = {'VLQ':1})

GC_62 = Coupling(name = 'GC_62',
                 value = 'complex(0,1)*KS12XDR3',
                 order = {'VLQ':1})

GC_63 = Coupling(name = 'GC_63',
                 value = 'complex(0,1)*KS12YTL',
                 order = {'VLQ':1})

GC_64 = Coupling(name = 'GC_64',
                 value = 'complex(0,1)*KS12YTR',
                 order = {'VLQ':1})

GC_65 = Coupling(name = 'GC_65',
                 value = 'complex(0,1)*KS12YUL3',
                 order = {'VLQ':1})

GC_66 = Coupling(name = 'GC_66',
                 value = 'complex(0,1)*KS12YUR3',
                 order = {'VLQ':1})

GC_67 = Coupling(name = 'GC_67',
                 value = 'complex(0,1)*KTLh3',
                 order = {'VLQ':1})

GC_68 = Coupling(name = 'GC_68',
                 value = 'complex(0,1)*KTRh3',
                 order = {'VLQ':1})

GC_69 = Coupling(name = 'GC_69',
                 value = '-6*complex(0,1)*lam',
                 order = {'QED':2})

GC_7 = Coupling(name = 'GC_7',
                value = '-2*ee*complex(0,1)',
                order = {'QED':1})

GC_70 = Coupling(name = 'GC_70',
                 value = '(ee**2*complex(0,1))/(2.*sw**2)',
                 order = {'QED':2})

GC_71 = Coupling(name = 'GC_71',
                 value = '-((ee**2*complex(0,1))/sw**2)',
                 order = {'QED':2})

GC_72 = Coupling(name = 'GC_72',
                 value = '(cw**2*ee**2*complex(0,1))/sw**2',
                 order = {'QED':2})

GC_73 = Coupling(name = 'GC_73',
                 value = '(2*ee**2*complex(0,1)*KWPWMSS10)/sw**2',
                 order = {'QED':2,'SX':1})

GC_74 = Coupling(name = 'GC_74',
                 value = '(ee**2*complex(0,1)*KWPWMSS11)/sw**2',
                 order = {'QED':2,'SX':1})

GC_75 = Coupling(name = 'GC_75',
                 value = '(ee**2*complex(0,1)*KWPWMSS12)/sw**2',
                 order = {'QED':2,'SX':1})

GC_76 = Coupling(name = 'GC_76',
                 value = '(4*ee**2*complex(0,1)*KWPWPSS11)/sw**2',
                 order = {'QED':2,'SX':1})

GC_77 = Coupling(name = 'GC_77',
                 value = '(2*ee**2*complex(0,1)*KWPWPSS12)/sw**2',
                 order = {'QED':2,'SX':1})

GC_78 = Coupling(name = 'GC_78',
                 value = '(ee**2*complex(0,1)*KWZS10S11)/(cw*sw**2)',
                 order = {'QED':2,'SX':1})

GC_79 = Coupling(name = 'GC_79',
                 value = '(ee**2*complex(0,1)*KWZS11S12)/(cw*sw**2)',
                 order = {'QED':2,'SX':1})

GC_8 = Coupling(name = 'GC_8',
                value = 'ee**2*complex(0,1)',
                order = {'QED':2})

GC_80 = Coupling(name = 'GC_80',
                 value = '(4*ee**2*complex(0,1)*KZZSS10)/(cw**2*sw**2)',
                 order = {'QED':2,'SX':1})

GC_81 = Coupling(name = 'GC_81',
                 value = '(2*ee**2*complex(0,1)*KZZSS11)/(cw**2*sw**2)',
                 order = {'QED':2,'SX':1})

GC_82 = Coupling(name = 'GC_82',
                 value = '(2*ee**2*complex(0,1)*KZZSS12)/(cw**2*sw**2)',
                 order = {'QED':2,'SX':1})

GC_83 = Coupling(name = 'GC_83',
                 value = '(ee*complex(0,1))/(sw*cmath.sqrt(2))',
                 order = {'QED':1})

GC_84 = Coupling(name = 'GC_84',
                 value = '-0.5*(cw*ee*complex(0,1))/sw',
                 order = {'QED':1})

GC_85 = Coupling(name = 'GC_85',
                 value = '(cw*ee*complex(0,1))/sw',
                 order = {'QED':1})

GC_86 = Coupling(name = 'GC_86',
                 value = '(-2*cw*ee**2*complex(0,1))/sw',
                 order = {'QED':2})

GC_87 = Coupling(name = 'GC_87',
                 value = '(ee**2*complex(0,1)*KAWS10S11)/sw',
                 order = {'QED':2,'SX':1})

GC_88 = Coupling(name = 'GC_88',
                 value = '(ee**2*complex(0,1)*KAWS11S12)/sw',
                 order = {'QED':2,'SX':1})

GC_89 = Coupling(name = 'GC_89',
                 value = '(ee**2*complex(0,1)*KAZSS11)/(cw*sw)',
                 order = {'QED':2,'SX':1})

GC_9 = Coupling(name = 'GC_9',
                value = '2*ee**2*complex(0,1)',
                order = {'QED':2})

GC_90 = Coupling(name = 'GC_90',
                 value = '(ee**2*complex(0,1)*KAZSS12)/(cw*sw)',
                 order = {'QED':2,'SX':1})

GC_91 = Coupling(name = 'GC_91',
                 value = '(ee*complex(0,1)*KBLw3)/(sw*cmath.sqrt(2))',
                 order = {'VLQ':1})

GC_92 = Coupling(name = 'GC_92',
                 value = '(ee*complex(0,1)*KBLz3)/(2.*cw*sw)',
                 order = {'VLQ':1})

GC_93 = Coupling(name = 'GC_93',
                 value = '(ee*complex(0,1)*KBRw3)/(sw*cmath.sqrt(2))',
                 order = {'VLQ':1})

GC_94 = Coupling(name = 'GC_94',
                 value = '(ee*complex(0,1)*KBRz3)/(2.*cw*sw)',
                 order = {'VLQ':1})

GC_95 = Coupling(name = 'GC_95',
                 value = '(ee*complex(0,1)*KTLw3)/(sw*cmath.sqrt(2))',
                 order = {'VLQ':1})

GC_96 = Coupling(name = 'GC_96',
                 value = '(ee*complex(0,1)*KTLz3)/(2.*cw*sw)',
                 order = {'VLQ':1})

GC_97 = Coupling(name = 'GC_97',
                 value = '(ee*complex(0,1)*KTRw3)/(sw*cmath.sqrt(2))',
                 order = {'VLQ':1})

GC_98 = Coupling(name = 'GC_98',
                 value = '(ee*complex(0,1)*KTRz3)/(2.*cw*sw)',
                 order = {'VLQ':1})

GC_99 = Coupling(name = 'GC_99',
                 value = '(ee*complex(0,1)*KWBYL)/(sw*cmath.sqrt(2))',
                 order = {'VLQ':1})

