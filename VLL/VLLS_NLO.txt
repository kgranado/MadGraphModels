Requestor: Merve Nazlim Agaras (model from  Prudhvi N.)
Content: Singlet vector-like-lepton model for e/mu/tau
Source: https://github.com/prudhvibhattiprolu/VLL-UFOs/tree/main/UFOmodels/VLLS_NLO
JIRA: https://its.cern.ch/jira/browse/AGENE-2099
Paper: https://arxiv.org/pdf/1905.00498.pdf