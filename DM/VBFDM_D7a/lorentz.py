# This file was automatically created by FeynRules 2.3.1
# Mathematica version: 10.1.0  for Mac OS X x86 (64-bit) (March 24, 2015)
# Date: Wed 27 May 2015 17:26:29


from object_library import all_lorentz, Lorentz

from function_library import complexconjugate, re, im, csc, sec, acsc, asec, cot
try:
   import form_factors as ForFac 
except ImportError:
   pass


UUS15 = Lorentz(name = 'UUS15',
                spins = [ -1, -1, 1 ],
                structure = '1')

UUV22 = Lorentz(name = 'UUV22',
                spins = [ -1, -1, 3 ],
                structure = 'P(3,2) + P(3,3)')

SSS15 = Lorentz(name = 'SSS15',
                spins = [ 1, 1, 1 ],
                structure = '1')

FFS30 = Lorentz(name = 'FFS30',
                spins = [ 2, 2, 1 ],
                structure = 'ProjM(2,1)')

FFS31 = Lorentz(name = 'FFS31',
                spins = [ 2, 2, 1 ],
                structure = 'ProjP(2,1)')

FFV62 = Lorentz(name = 'FFV62',
                spins = [ 2, 2, 3 ],
                structure = 'Gamma(3,2,1)')

FFV63 = Lorentz(name = 'FFV63',
                spins = [ 2, 2, 3 ],
                structure = 'Gamma(3,2,-1)*ProjM(-1,1)')

FFV64 = Lorentz(name = 'FFV64',
                spins = [ 2, 2, 3 ],
                structure = 'Gamma(3,2,-1)*ProjP(-1,1)')

VSS15 = Lorentz(name = 'VSS15',
                spins = [ 3, 1, 1 ],
                structure = 'P(1,2) - P(1,3)')

VVS15 = Lorentz(name = 'VVS15',
                spins = [ 3, 3, 1 ],
                structure = 'Metric(1,2)')

VVV15 = Lorentz(name = 'VVV15',
                spins = [ 3, 3, 3 ],
                structure = 'P(3,1)*Metric(1,2) - P(3,2)*Metric(1,2) - P(2,1)*Metric(1,3) + P(2,3)*Metric(1,3) + P(1,2)*Metric(2,3) - P(1,3)*Metric(2,3)')

SSSS15 = Lorentz(name = 'SSSS15',
                 spins = [ 1, 1, 1, 1 ],
                 structure = '1')

FFVV22 = Lorentz(name = 'FFVV22',
                 spins = [ 2, 2, 3, 3 ],
                 structure = 'P(3,4)*P(4,3)*Identity(2,1) - P(-1,3)*P(-1,4)*Identity(2,1)*Metric(3,4)')

VVSS15 = Lorentz(name = 'VVSS15',
                 spins = [ 3, 3, 1, 1 ],
                 structure = 'Metric(1,2)')

VVVV71 = Lorentz(name = 'VVVV71',
                 spins = [ 3, 3, 3, 3 ],
                 structure = 'Metric(1,4)*Metric(2,3) - Metric(1,3)*Metric(2,4)')

VVVV72 = Lorentz(name = 'VVVV72',
                 spins = [ 3, 3, 3, 3 ],
                 structure = 'Metric(1,4)*Metric(2,3) + Metric(1,3)*Metric(2,4) - 2*Metric(1,2)*Metric(3,4)')

VVVV73 = Lorentz(name = 'VVVV73',
                 spins = [ 3, 3, 3, 3 ],
                 structure = 'Metric(1,4)*Metric(2,3) - Metric(1,2)*Metric(3,4)')

VVVV74 = Lorentz(name = 'VVVV74',
                 spins = [ 3, 3, 3, 3 ],
                 structure = 'Metric(1,3)*Metric(2,4) - Metric(1,2)*Metric(3,4)')

VVVV75 = Lorentz(name = 'VVVV75',
                 spins = [ 3, 3, 3, 3 ],
                 structure = 'Metric(1,4)*Metric(2,3) - (Metric(1,3)*Metric(2,4))/2. - (Metric(1,2)*Metric(3,4))/2.')

FFVVV13 = Lorentz(name = 'FFVVV13',
                  spins = [ 2, 2, 3, 3, 3 ],
                  structure = 'P(5,3)*Identity(2,1)*Metric(3,4) - P(5,4)*Identity(2,1)*Metric(3,4) - P(4,3)*Identity(2,1)*Metric(3,5) + P(4,5)*Identity(2,1)*Metric(3,5) + P(3,4)*Identity(2,1)*Metric(4,5) - P(3,5)*Identity(2,1)*Metric(4,5)')

FFVVVV13 = Lorentz(name = 'FFVVVV13',
                   spins = [ 2, 2, 3, 3, 3, 3 ],
                   structure = 'Identity(2,1)*Metric(3,6)*Metric(4,5) + Identity(2,1)*Metric(3,5)*Metric(4,6) - 2*Identity(2,1)*Metric(3,4)*Metric(5,6)')

FFVVVV14 = Lorentz(name = 'FFVVVV14',
                   spins = [ 2, 2, 3, 3, 3, 3 ],
                   structure = 'Identity(2,1)*Metric(3,6)*Metric(4,5) - (Identity(2,1)*Metric(3,5)*Metric(4,6))/2. - (Identity(2,1)*Metric(3,4)*Metric(5,6))/2.')

