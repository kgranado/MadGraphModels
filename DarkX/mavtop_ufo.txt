Requestor: Hannah van der Schyf, Deepak Kar
Content: MaverickTop with dark photon
Paper 1: https://arxiv.org/abs/1904.05893
Paper 2: https://scipost.org/SciPostPhys.13.2.018
Source: Received directly from authors of paper 1
