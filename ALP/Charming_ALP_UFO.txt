Requestor: Jackson Carl Burzynski
Contents: Axion-like particle, coupling to up-type quarks. ALP lifetime set by c_uR matrix elements.
Provided by Adrian Carmona [adrian@ugr.es], author of the ALPs from the Top paper
Paper ALPs from the Top: https://arxiv.org/abs/2202.09371 
