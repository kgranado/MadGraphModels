Requestor: Shu Li
Content: SM + CP-even singlet scalar (se) and a CP-odd singlet scalar (so)
Paper: http://arxiv.org/abs/1508.05632
JIRA: https://its.cern.ch/jira/browse/AGENE-1086