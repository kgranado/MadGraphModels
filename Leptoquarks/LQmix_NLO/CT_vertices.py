# This file was automatically created by FeynRules =.2.4
# Mathematica version: 8.0 for Linux x86 (32-bit) (October 10, 2011)
# Date: Sat 3 Sep 2016 20:49:33


from object_library import all_vertices, all_CTvertices, Vertex, CTVertex
import particles as P
import CT_couplings as C
import lorentz as L


V_1 = CTVertex(name = 'V_1',
               type = 'R2',
               particles = [ P.g, P.g, P.g ],
               color = [ 'f(1,2,3)' ],
               lorentz = [ L.VVV1, L.VVV2, L.VVV4, L.VVV6, L.VVV7, L.VVV8 ],
               loop_particles = [ [ [P.b], [P.c], [P.d], [P.s], [P.t], [P.u] ], [ [P.g] ] ],
               couplings = {(0,0,0):C.R2GC_292_116,(0,0,1):C.R2GC_292_117,(0,1,0):C.R2GC_295_118,(0,1,1):C.R2GC_295_119,(0,2,0):C.R2GC_295_118,(0,2,1):C.R2GC_295_119,(0,3,0):C.R2GC_292_116,(0,3,1):C.R2GC_292_117,(0,4,0):C.R2GC_292_116,(0,4,1):C.R2GC_292_117,(0,5,0):C.R2GC_295_118,(0,5,1):C.R2GC_295_119})

V_2 = CTVertex(name = 'V_2',
               type = 'R2',
               particles = [ P.g, P.g, P.g, P.g ],
               color = [ 'd(-1,1,3)*d(-1,2,4)', 'd(-1,1,3)*f(-1,2,4)', 'd(-1,1,4)*d(-1,2,3)', 'd(-1,1,4)*f(-1,2,3)', 'd(-1,2,3)*f(-1,1,4)', 'd(-1,2,4)*f(-1,1,3)', 'f(-1,1,2)*f(-1,3,4)', 'f(-1,1,3)*f(-1,2,4)', 'f(-1,1,4)*f(-1,2,3)', 'Identity(1,2)*Identity(3,4)', 'Identity(1,3)*Identity(2,4)', 'Identity(1,4)*Identity(2,3)' ],
               lorentz = [ L.VVVV2, L.VVVV3, L.VVVV4 ],
               loop_particles = [ [ [P.b], [P.c], [P.d], [P.s], [P.t], [P.u] ], [ [P.g] ] ],
               couplings = {(2,0,0):C.R2GC_167_29,(2,0,1):C.R2GC_167_30,(0,0,0):C.R2GC_167_29,(0,0,1):C.R2GC_167_30,(4,0,0):C.R2GC_165_25,(4,0,1):C.R2GC_165_26,(3,0,0):C.R2GC_165_25,(3,0,1):C.R2GC_165_26,(8,0,0):C.R2GC_166_27,(8,0,1):C.R2GC_166_28,(7,0,0):C.R2GC_172_36,(7,0,1):C.R2GC_301_124,(6,0,0):C.R2GC_171_34,(6,0,1):C.R2GC_302_125,(5,0,0):C.R2GC_165_25,(5,0,1):C.R2GC_165_26,(1,0,0):C.R2GC_165_25,(1,0,1):C.R2GC_165_26,(11,0,0):C.R2GC_169_32,(11,0,1):C.R2GC_169_33,(10,0,0):C.R2GC_169_32,(10,0,1):C.R2GC_169_33,(9,0,1):C.R2GC_168_31,(2,1,0):C.R2GC_167_29,(2,1,1):C.R2GC_167_30,(0,1,0):C.R2GC_167_29,(0,1,1):C.R2GC_167_30,(6,1,0):C.R2GC_299_121,(6,1,1):C.R2GC_299_122,(4,1,0):C.R2GC_165_25,(4,1,1):C.R2GC_165_26,(3,1,0):C.R2GC_165_25,(3,1,1):C.R2GC_165_26,(8,1,0):C.R2GC_166_27,(8,1,1):C.R2GC_303_126,(7,1,0):C.R2GC_172_36,(7,1,1):C.R2GC_172_37,(5,1,0):C.R2GC_165_25,(5,1,1):C.R2GC_165_26,(1,1,0):C.R2GC_165_25,(1,1,1):C.R2GC_165_26,(11,1,0):C.R2GC_169_32,(11,1,1):C.R2GC_169_33,(10,1,0):C.R2GC_169_32,(10,1,1):C.R2GC_169_33,(9,1,1):C.R2GC_168_31,(2,2,0):C.R2GC_167_29,(2,2,1):C.R2GC_167_30,(0,2,0):C.R2GC_167_29,(0,2,1):C.R2GC_167_30,(4,2,0):C.R2GC_165_25,(4,2,1):C.R2GC_165_26,(3,2,0):C.R2GC_165_25,(3,2,1):C.R2GC_165_26,(8,2,0):C.R2GC_166_27,(8,2,1):C.R2GC_300_123,(6,2,0):C.R2GC_171_34,(6,2,1):C.R2GC_171_35,(7,2,0):C.R2GC_298_120,(7,2,1):C.R2GC_167_30,(5,2,0):C.R2GC_165_25,(5,2,1):C.R2GC_165_26,(1,2,0):C.R2GC_165_25,(1,2,1):C.R2GC_165_26,(11,2,0):C.R2GC_169_32,(11,2,1):C.R2GC_169_33,(10,2,0):C.R2GC_169_32,(10,2,1):C.R2GC_169_33,(9,2,1):C.R2GC_168_31})

V_3 = CTVertex(name = 'V_3',
               type = 'R2',
               particles = [ P.e__minus__, P.c, P.lqsd__tilde__ ],
               color = [ 'Identity(2,3)' ],
               lorentz = [ L.FFS3, L.FFS4 ],
               loop_particles = [ [ [P.c, P.g, P.lqsd] ] ],
               couplings = {(0,0,0):C.R2GC_206_53,(0,1,0):C.R2GC_208_54})

V_4 = CTVertex(name = 'V_4',
               type = 'R2',
               particles = [ P.mu__minus__, P.c, P.lqsd__tilde__ ],
               color = [ 'Identity(2,3)' ],
               lorentz = [ L.FFS3, L.FFS4 ],
               loop_particles = [ [ [P.c, P.g, P.lqsd] ] ],
               couplings = {(0,0,0):C.R2GC_210_55,(0,1,0):C.R2GC_212_56})

V_5 = CTVertex(name = 'V_5',
               type = 'R2',
               particles = [ P.ta__minus__, P.c, P.lqsd__tilde__ ],
               color = [ 'Identity(2,3)' ],
               lorentz = [ L.FFS3, L.FFS4 ],
               loop_particles = [ [ [P.c, P.g, P.lqsd] ] ],
               couplings = {(0,0,0):C.R2GC_214_57,(0,1,0):C.R2GC_216_58})

V_6 = CTVertex(name = 'V_6',
               type = 'R2',
               particles = [ P.ta__minus__, P.t, P.lqsd__tilde__ ],
               color = [ 'Identity(2,3)' ],
               lorentz = [ L.FFS3, L.FFS4 ],
               loop_particles = [ [ [P.g, P.lqsd, P.t] ] ],
               couplings = {(0,0,0):C.R2GC_252_91,(0,1,0):C.R2GC_253_92})

V_7 = CTVertex(name = 'V_7',
               type = 'R2',
               particles = [ P.ve__tilde__, P.b__tilde__, P.lqsd ],
               color = [ 'Identity(2,3)' ],
               lorentz = [ L.FFS3 ],
               loop_particles = [ [ [P.b, P.g, P.lqsd] ] ],
               couplings = {(0,0,0):C.R2GC_203_50})

V_8 = CTVertex(name = 'V_8',
               type = 'R2',
               particles = [ P.vm__tilde__, P.b__tilde__, P.lqsd ],
               color = [ 'Identity(2,3)' ],
               lorentz = [ L.FFS3 ],
               loop_particles = [ [ [P.b, P.g, P.lqsd] ] ],
               couplings = {(0,0,0):C.R2GC_204_51})

V_9 = CTVertex(name = 'V_9',
               type = 'R2',
               particles = [ P.vt__tilde__, P.b__tilde__, P.lqsd ],
               color = [ 'Identity(2,3)' ],
               lorentz = [ L.FFS3 ],
               loop_particles = [ [ [P.b, P.g, P.lqsd] ] ],
               couplings = {(0,0,0):C.R2GC_205_52})

V_10 = CTVertex(name = 'V_10',
                type = 'R2',
                particles = [ P.ve__tilde__, P.d__tilde__, P.lqsd ],
                color = [ 'Identity(2,3)' ],
                lorentz = [ L.FFS3 ],
                loop_particles = [ [ [P.d, P.g, P.lqsd] ] ],
                couplings = {(0,0,0):C.R2GC_222_63})

V_11 = CTVertex(name = 'V_11',
                type = 'R2',
                particles = [ P.vm__tilde__, P.d__tilde__, P.lqsd ],
                color = [ 'Identity(2,3)' ],
                lorentz = [ L.FFS3 ],
                loop_particles = [ [ [P.d, P.g, P.lqsd] ] ],
                couplings = {(0,0,0):C.R2GC_224_64})

V_12 = CTVertex(name = 'V_12',
                type = 'R2',
                particles = [ P.vt__tilde__, P.d__tilde__, P.lqsd ],
                color = [ 'Identity(2,3)' ],
                lorentz = [ L.FFS3 ],
                loop_particles = [ [ [P.d, P.g, P.lqsd] ] ],
                couplings = {(0,0,0):C.R2GC_226_65})

V_13 = CTVertex(name = 'V_13',
                type = 'R2',
                particles = [ P.ve__tilde__, P.s__tilde__, P.lqsd ],
                color = [ 'Identity(2,3)' ],
                lorentz = [ L.FFS3 ],
                loop_particles = [ [ [P.g, P.lqsd, P.s] ] ],
                couplings = {(0,0,0):C.R2GC_236_78})

V_14 = CTVertex(name = 'V_14',
                type = 'R2',
                particles = [ P.vm__tilde__, P.s__tilde__, P.lqsd ],
                color = [ 'Identity(2,3)' ],
                lorentz = [ L.FFS3 ],
                loop_particles = [ [ [P.g, P.lqsd, P.s] ] ],
                couplings = {(0,0,0):C.R2GC_238_79})

V_15 = CTVertex(name = 'V_15',
                type = 'R2',
                particles = [ P.vt__tilde__, P.s__tilde__, P.lqsd ],
                color = [ 'Identity(2,3)' ],
                lorentz = [ L.FFS3 ],
                loop_particles = [ [ [P.g, P.lqsd, P.s] ] ],
                couplings = {(0,0,0):C.R2GC_240_80})

V_16 = CTVertex(name = 'V_16',
                type = 'R2',
                particles = [ P.e__plus__, P.b, P.lqsu__tilde__ ],
                color = [ 'Identity(2,3)' ],
                lorentz = [ L.FFS3, L.FFS4 ],
                loop_particles = [ [ [P.b, P.g, P.lqsu] ] ],
                couplings = {(0,0,0):C.R2GC_273_102,(0,1,0):C.R2GC_274_103})

V_17 = CTVertex(name = 'V_17',
                type = 'R2',
                particles = [ P.mu__plus__, P.b, P.lqsu__tilde__ ],
                color = [ 'Identity(2,3)' ],
                lorentz = [ L.FFS3, L.FFS4 ],
                loop_particles = [ [ [P.b, P.g, P.lqsu] ] ],
                couplings = {(0,0,0):C.R2GC_275_104,(0,1,0):C.R2GC_276_105})

V_18 = CTVertex(name = 'V_18',
                type = 'R2',
                particles = [ P.ta__plus__, P.b, P.lqsu__tilde__ ],
                color = [ 'Identity(2,3)' ],
                lorentz = [ L.FFS3, L.FFS4 ],
                loop_particles = [ [ [P.b, P.g, P.lqsu] ] ],
                couplings = {(0,0,0):C.R2GC_277_106,(0,1,0):C.R2GC_278_107})

V_19 = CTVertex(name = 'V_19',
                type = 'R2',
                particles = [ P.ve__tilde__, P.c, P.lqsu__tilde__ ],
                color = [ 'Identity(2,3)' ],
                lorentz = [ L.FFS3 ],
                loop_particles = [ [ [P.c, P.g, P.lqsu] ] ],
                couplings = {(0,0,0):C.R2GC_218_59})

V_20 = CTVertex(name = 'V_20',
                type = 'R2',
                particles = [ P.vm__tilde__, P.c, P.lqsu__tilde__ ],
                color = [ 'Identity(2,3)' ],
                lorentz = [ L.FFS3 ],
                loop_particles = [ [ [P.c, P.g, P.lqsu] ] ],
                couplings = {(0,0,0):C.R2GC_219_60})

V_21 = CTVertex(name = 'V_21',
                type = 'R2',
                particles = [ P.vt__tilde__, P.c, P.lqsu__tilde__ ],
                color = [ 'Identity(2,3)' ],
                lorentz = [ L.FFS3 ],
                loop_particles = [ [ [P.c, P.g, P.lqsu] ] ],
                couplings = {(0,0,0):C.R2GC_220_61})

V_22 = CTVertex(name = 'V_22',
                type = 'R2',
                particles = [ P.e__plus__, P.d, P.lqsu__tilde__ ],
                color = [ 'Identity(2,3)' ],
                lorentz = [ L.FFS3, L.FFS4 ],
                loop_particles = [ [ [P.d, P.g, P.lqsu] ] ],
                couplings = {(0,0,0):C.R2GC_228_66,(0,1,0):C.R2GC_229_67})

V_23 = CTVertex(name = 'V_23',
                type = 'R2',
                particles = [ P.mu__plus__, P.d, P.lqsu__tilde__ ],
                color = [ 'Identity(2,3)' ],
                lorentz = [ L.FFS3, L.FFS4 ],
                loop_particles = [ [ [P.d, P.g, P.lqsu] ] ],
                couplings = {(0,0,0):C.R2GC_230_68,(0,1,0):C.R2GC_231_69})

V_24 = CTVertex(name = 'V_24',
                type = 'R2',
                particles = [ P.ta__plus__, P.d, P.lqsu__tilde__ ],
                color = [ 'Identity(2,3)' ],
                lorentz = [ L.FFS3, L.FFS4 ],
                loop_particles = [ [ [P.d, P.g, P.lqsu] ] ],
                couplings = {(0,0,0):C.R2GC_232_70,(0,1,0):C.R2GC_233_71})

V_25 = CTVertex(name = 'V_25',
                type = 'R2',
                particles = [ P.e__plus__, P.s, P.lqsu__tilde__ ],
                color = [ 'Identity(2,3)' ],
                lorentz = [ L.FFS3, L.FFS4 ],
                loop_particles = [ [ [P.g, P.lqsu, P.s] ] ],
                couplings = {(0,0,0):C.R2GC_242_81,(0,1,0):C.R2GC_243_82})

V_26 = CTVertex(name = 'V_26',
                type = 'R2',
                particles = [ P.mu__plus__, P.s, P.lqsu__tilde__ ],
                color = [ 'Identity(2,3)' ],
                lorentz = [ L.FFS3, L.FFS4 ],
                loop_particles = [ [ [P.g, P.lqsu, P.s] ] ],
                couplings = {(0,0,0):C.R2GC_244_83,(0,1,0):C.R2GC_245_84})

V_27 = CTVertex(name = 'V_27',
                type = 'R2',
                particles = [ P.ta__plus__, P.s, P.lqsu__tilde__ ],
                color = [ 'Identity(2,3)' ],
                lorentz = [ L.FFS3, L.FFS4 ],
                loop_particles = [ [ [P.g, P.lqsu, P.s] ] ],
                couplings = {(0,0,0):C.R2GC_246_85,(0,1,0):C.R2GC_247_86})

V_28 = CTVertex(name = 'V_28',
                type = 'R2',
                particles = [ P.ve__tilde__, P.t, P.lqsu__tilde__ ],
                color = [ 'Identity(2,3)' ],
                lorentz = [ L.FFS3 ],
                loop_particles = [ [ [P.g, P.lqsu, P.t] ] ],
                couplings = {(0,0,0):C.R2GC_313_127})

V_29 = CTVertex(name = 'V_29',
                type = 'R2',
                particles = [ P.vm__tilde__, P.t, P.lqsu__tilde__ ],
                color = [ 'Identity(2,3)' ],
                lorentz = [ L.FFS3 ],
                loop_particles = [ [ [P.g, P.lqsu, P.t] ] ],
                couplings = {(0,0,0):C.R2GC_314_128})

V_30 = CTVertex(name = 'V_30',
                type = 'R2',
                particles = [ P.vt__tilde__, P.t, P.lqsu__tilde__ ],
                color = [ 'Identity(2,3)' ],
                lorentz = [ L.FFS3 ],
                loop_particles = [ [ [P.g, P.lqsu, P.t] ] ],
                couplings = {(0,0,0):C.R2GC_315_129})

V_31 = CTVertex(name = 'V_31',
                type = 'R2',
                particles = [ P.ve__tilde__, P.u, P.lqsu__tilde__ ],
                color = [ 'Identity(2,3)' ],
                lorentz = [ L.FFS3 ],
                loop_particles = [ [ [P.g, P.lqsu, P.u] ] ],
                couplings = {(0,0,0):C.R2GC_267_99})

V_32 = CTVertex(name = 'V_32',
                type = 'R2',
                particles = [ P.vm__tilde__, P.u, P.lqsu__tilde__ ],
                color = [ 'Identity(2,3)' ],
                lorentz = [ L.FFS3 ],
                loop_particles = [ [ [P.g, P.lqsu, P.u] ] ],
                couplings = {(0,0,0):C.R2GC_268_100})

V_33 = CTVertex(name = 'V_33',
                type = 'R2',
                particles = [ P.vt__tilde__, P.u, P.lqsu__tilde__ ],
                color = [ 'Identity(2,3)' ],
                lorentz = [ L.FFS3 ],
                loop_particles = [ [ [P.g, P.lqsu, P.u] ] ],
                couplings = {(0,0,0):C.R2GC_269_101})

V_34 = CTVertex(name = 'V_34',
                type = 'R2',
                particles = [ P.t, P.e__minus__, P.lqsd__tilde__ ],
                color = [ 'Identity(1,3)' ],
                lorentz = [ L.FFS3, L.FFS4 ],
                loop_particles = [ [ [P.g, P.lqsd, P.t] ] ],
                couplings = {(0,0,0):C.R2GC_248_87,(0,1,0):C.R2GC_249_88})

V_35 = CTVertex(name = 'V_35',
                type = 'R2',
                particles = [ P.u, P.e__minus__, P.lqsd__tilde__ ],
                color = [ 'Identity(1,3)' ],
                lorentz = [ L.FFS3, L.FFS4 ],
                loop_particles = [ [ [P.g, P.lqsd, P.u] ] ],
                couplings = {(0,0,0):C.R2GC_255_93,(0,1,0):C.R2GC_257_94})

V_36 = CTVertex(name = 'V_36',
                type = 'R2',
                particles = [ P.t, P.mu__minus__, P.lqsd__tilde__ ],
                color = [ 'Identity(1,3)' ],
                lorentz = [ L.FFS3, L.FFS4 ],
                loop_particles = [ [ [P.g, P.lqsd, P.t] ] ],
                couplings = {(0,0,0):C.R2GC_250_89,(0,1,0):C.R2GC_251_90})

V_37 = CTVertex(name = 'V_37',
                type = 'R2',
                particles = [ P.u, P.mu__minus__, P.lqsd__tilde__ ],
                color = [ 'Identity(1,3)' ],
                lorentz = [ L.FFS3, L.FFS4 ],
                loop_particles = [ [ [P.g, P.lqsd, P.u] ] ],
                couplings = {(0,0,0):C.R2GC_259_95,(0,1,0):C.R2GC_261_96})

V_38 = CTVertex(name = 'V_38',
                type = 'R2',
                particles = [ P.u, P.ta__minus__, P.lqsd__tilde__ ],
                color = [ 'Identity(1,3)' ],
                lorentz = [ L.FFS3, L.FFS4 ],
                loop_particles = [ [ [P.g, P.lqsd, P.u] ] ],
                couplings = {(0,0,0):C.R2GC_263_97,(0,1,0):C.R2GC_265_98})

V_39 = CTVertex(name = 'V_39',
                type = 'R2',
                particles = [ P.t__tilde__, P.b, P.G__plus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS3, L.FFS4 ],
                loop_particles = [ [ [P.b, P.g, P.t] ] ],
                couplings = {(0,0,0):C.R2GC_321_132,(0,1,0):C.R2GC_322_133})

V_40 = CTVertex(name = 'V_40',
                type = 'R2',
                particles = [ P.b__tilde__, P.b, P.G0 ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS1 ],
                loop_particles = [ [ [P.b, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_287_110})

V_41 = CTVertex(name = 'V_41',
                type = 'R2',
                particles = [ P.b__tilde__, P.b, P.H ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2 ],
                loop_particles = [ [ [P.b, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_286_109})

V_42 = CTVertex(name = 'V_42',
                type = 'R2',
                particles = [ P.t__tilde__, P.t, P.G0 ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS1 ],
                loop_particles = [ [ [P.g, P.t] ] ],
                couplings = {(0,0,0):C.R2GC_324_135})

V_43 = CTVertex(name = 'V_43',
                type = 'R2',
                particles = [ P.t__tilde__, P.t, P.H ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2 ],
                loop_particles = [ [ [P.g, P.t] ] ],
                couplings = {(0,0,0):C.R2GC_325_136})

V_44 = CTVertex(name = 'V_44',
                type = 'R2',
                particles = [ P.b__tilde__, P.t, P.G__minus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS3, L.FFS4 ],
                loop_particles = [ [ [P.b, P.g, P.t] ] ],
                couplings = {(0,0,0):C.R2GC_323_134,(0,1,0):C.R2GC_320_131})

V_45 = CTVertex(name = 'V_45',
                type = 'R2',
                particles = [ P.e__plus__, P.c__tilde__, P.lqsd ],
                color = [ 'Identity(2,3)' ],
                lorentz = [ L.FFS3, L.FFS4 ],
                loop_particles = [ [ [P.c, P.g, P.lqsd] ] ],
                couplings = {(0,0,0):C.R2GC_208_54,(0,1,0):C.R2GC_206_53})

V_46 = CTVertex(name = 'V_46',
                type = 'R2',
                particles = [ P.mu__plus__, P.c__tilde__, P.lqsd ],
                color = [ 'Identity(2,3)' ],
                lorentz = [ L.FFS3, L.FFS4 ],
                loop_particles = [ [ [P.c, P.g, P.lqsd] ] ],
                couplings = {(0,0,0):C.R2GC_212_56,(0,1,0):C.R2GC_210_55})

V_47 = CTVertex(name = 'V_47',
                type = 'R2',
                particles = [ P.ta__plus__, P.c__tilde__, P.lqsd ],
                color = [ 'Identity(2,3)' ],
                lorentz = [ L.FFS3, L.FFS4 ],
                loop_particles = [ [ [P.c, P.g, P.lqsd] ] ],
                couplings = {(0,0,0):C.R2GC_216_58,(0,1,0):C.R2GC_214_57})

V_48 = CTVertex(name = 'V_48',
                type = 'R2',
                particles = [ P.t__tilde__, P.e__plus__, P.lqsd ],
                color = [ 'Identity(1,3)' ],
                lorentz = [ L.FFS3, L.FFS4 ],
                loop_particles = [ [ [P.g, P.lqsd, P.t] ] ],
                couplings = {(0,0,0):C.R2GC_249_88,(0,1,0):C.R2GC_248_87})

V_49 = CTVertex(name = 'V_49',
                type = 'R2',
                particles = [ P.t__tilde__, P.mu__plus__, P.lqsd ],
                color = [ 'Identity(1,3)' ],
                lorentz = [ L.FFS3, L.FFS4 ],
                loop_particles = [ [ [P.g, P.lqsd, P.t] ] ],
                couplings = {(0,0,0):C.R2GC_251_90,(0,1,0):C.R2GC_250_89})

V_50 = CTVertex(name = 'V_50',
                type = 'R2',
                particles = [ P.ta__plus__, P.t__tilde__, P.lqsd ],
                color = [ 'Identity(2,3)' ],
                lorentz = [ L.FFS3, L.FFS4 ],
                loop_particles = [ [ [P.g, P.lqsd, P.t] ] ],
                couplings = {(0,0,0):C.R2GC_253_92,(0,1,0):C.R2GC_252_91})

V_51 = CTVertex(name = 'V_51',
                type = 'R2',
                particles = [ P.u__tilde__, P.e__plus__, P.lqsd ],
                color = [ 'Identity(1,3)' ],
                lorentz = [ L.FFS3, L.FFS4 ],
                loop_particles = [ [ [P.g, P.lqsd, P.u] ] ],
                couplings = {(0,0,0):C.R2GC_257_94,(0,1,0):C.R2GC_255_93})

V_52 = CTVertex(name = 'V_52',
                type = 'R2',
                particles = [ P.u__tilde__, P.mu__plus__, P.lqsd ],
                color = [ 'Identity(1,3)' ],
                lorentz = [ L.FFS3, L.FFS4 ],
                loop_particles = [ [ [P.g, P.lqsd, P.u] ] ],
                couplings = {(0,0,0):C.R2GC_261_96,(0,1,0):C.R2GC_259_95})

V_53 = CTVertex(name = 'V_53',
                type = 'R2',
                particles = [ P.u__tilde__, P.ta__plus__, P.lqsd ],
                color = [ 'Identity(1,3)' ],
                lorentz = [ L.FFS3, L.FFS4 ],
                loop_particles = [ [ [P.g, P.lqsd, P.u] ] ],
                couplings = {(0,0,0):C.R2GC_265_98,(0,1,0):C.R2GC_263_97})

V_54 = CTVertex(name = 'V_54',
                type = 'R2',
                particles = [ P.b__tilde__, P.e__minus__, P.lqsu ],
                color = [ 'Identity(1,3)' ],
                lorentz = [ L.FFS3, L.FFS4 ],
                loop_particles = [ [ [P.b, P.g, P.lqsu] ] ],
                couplings = {(0,0,0):C.R2GC_274_103,(0,1,0):C.R2GC_273_102})

V_55 = CTVertex(name = 'V_55',
                type = 'R2',
                particles = [ P.b__tilde__, P.mu__minus__, P.lqsu ],
                color = [ 'Identity(1,3)' ],
                lorentz = [ L.FFS3, L.FFS4 ],
                loop_particles = [ [ [P.b, P.g, P.lqsu] ] ],
                couplings = {(0,0,0):C.R2GC_276_105,(0,1,0):C.R2GC_275_104})

V_56 = CTVertex(name = 'V_56',
                type = 'R2',
                particles = [ P.b__tilde__, P.ta__minus__, P.lqsu ],
                color = [ 'Identity(1,3)' ],
                lorentz = [ L.FFS3, L.FFS4 ],
                loop_particles = [ [ [P.b, P.g, P.lqsu] ] ],
                couplings = {(0,0,0):C.R2GC_278_107,(0,1,0):C.R2GC_277_106})

V_57 = CTVertex(name = 'V_57',
                type = 'R2',
                particles = [ P.d__tilde__, P.e__minus__, P.lqsu ],
                color = [ 'Identity(1,3)' ],
                lorentz = [ L.FFS3, L.FFS4 ],
                loop_particles = [ [ [P.d, P.g, P.lqsu] ] ],
                couplings = {(0,0,0):C.R2GC_229_67,(0,1,0):C.R2GC_228_66})

V_58 = CTVertex(name = 'V_58',
                type = 'R2',
                particles = [ P.d__tilde__, P.mu__minus__, P.lqsu ],
                color = [ 'Identity(1,3)' ],
                lorentz = [ L.FFS3, L.FFS4 ],
                loop_particles = [ [ [P.d, P.g, P.lqsu] ] ],
                couplings = {(0,0,0):C.R2GC_231_69,(0,1,0):C.R2GC_230_68})

V_59 = CTVertex(name = 'V_59',
                type = 'R2',
                particles = [ P.d__tilde__, P.ta__minus__, P.lqsu ],
                color = [ 'Identity(1,3)' ],
                lorentz = [ L.FFS3, L.FFS4 ],
                loop_particles = [ [ [P.d, P.g, P.lqsu] ] ],
                couplings = {(0,0,0):C.R2GC_233_71,(0,1,0):C.R2GC_232_70})

V_60 = CTVertex(name = 'V_60',
                type = 'R2',
                particles = [ P.s__tilde__, P.e__minus__, P.lqsu ],
                color = [ 'Identity(1,3)' ],
                lorentz = [ L.FFS3, L.FFS4 ],
                loop_particles = [ [ [P.g, P.lqsu, P.s] ] ],
                couplings = {(0,0,0):C.R2GC_243_82,(0,1,0):C.R2GC_242_81})

V_61 = CTVertex(name = 'V_61',
                type = 'R2',
                particles = [ P.s__tilde__, P.mu__minus__, P.lqsu ],
                color = [ 'Identity(1,3)' ],
                lorentz = [ L.FFS3, L.FFS4 ],
                loop_particles = [ [ [P.g, P.lqsu, P.s] ] ],
                couplings = {(0,0,0):C.R2GC_245_84,(0,1,0):C.R2GC_244_83})

V_62 = CTVertex(name = 'V_62',
                type = 'R2',
                particles = [ P.s__tilde__, P.ta__minus__, P.lqsu ],
                color = [ 'Identity(1,3)' ],
                lorentz = [ L.FFS3, L.FFS4 ],
                loop_particles = [ [ [P.g, P.lqsu, P.s] ] ],
                couplings = {(0,0,0):C.R2GC_247_86,(0,1,0):C.R2GC_246_85})

V_63 = CTVertex(name = 'V_63',
                type = 'R2',
                particles = [ P.ve, P.b, P.lqsd__tilde__ ],
                color = [ 'Identity(2,3)' ],
                lorentz = [ L.FFS4 ],
                loop_particles = [ [ [P.b, P.g, P.lqsd] ] ],
                couplings = {(0,0,0):C.R2GC_203_50})

V_64 = CTVertex(name = 'V_64',
                type = 'R2',
                particles = [ P.vm, P.b, P.lqsd__tilde__ ],
                color = [ 'Identity(2,3)' ],
                lorentz = [ L.FFS4 ],
                loop_particles = [ [ [P.b, P.g, P.lqsd] ] ],
                couplings = {(0,0,0):C.R2GC_204_51})

V_65 = CTVertex(name = 'V_65',
                type = 'R2',
                particles = [ P.vt, P.b, P.lqsd__tilde__ ],
                color = [ 'Identity(2,3)' ],
                lorentz = [ L.FFS4 ],
                loop_particles = [ [ [P.b, P.g, P.lqsd] ] ],
                couplings = {(0,0,0):C.R2GC_205_52})

V_66 = CTVertex(name = 'V_66',
                type = 'R2',
                particles = [ P.ve, P.d, P.lqsd__tilde__ ],
                color = [ 'Identity(2,3)' ],
                lorentz = [ L.FFS4 ],
                loop_particles = [ [ [P.d, P.g, P.lqsd] ] ],
                couplings = {(0,0,0):C.R2GC_222_63})

V_67 = CTVertex(name = 'V_67',
                type = 'R2',
                particles = [ P.vm, P.d, P.lqsd__tilde__ ],
                color = [ 'Identity(2,3)' ],
                lorentz = [ L.FFS4 ],
                loop_particles = [ [ [P.d, P.g, P.lqsd] ] ],
                couplings = {(0,0,0):C.R2GC_224_64})

V_68 = CTVertex(name = 'V_68',
                type = 'R2',
                particles = [ P.vt, P.d, P.lqsd__tilde__ ],
                color = [ 'Identity(2,3)' ],
                lorentz = [ L.FFS4 ],
                loop_particles = [ [ [P.d, P.g, P.lqsd] ] ],
                couplings = {(0,0,0):C.R2GC_226_65})

V_69 = CTVertex(name = 'V_69',
                type = 'R2',
                particles = [ P.ve, P.s, P.lqsd__tilde__ ],
                color = [ 'Identity(2,3)' ],
                lorentz = [ L.FFS4 ],
                loop_particles = [ [ [P.g, P.lqsd, P.s] ] ],
                couplings = {(0,0,0):C.R2GC_236_78})

V_70 = CTVertex(name = 'V_70',
                type = 'R2',
                particles = [ P.vm, P.s, P.lqsd__tilde__ ],
                color = [ 'Identity(2,3)' ],
                lorentz = [ L.FFS4 ],
                loop_particles = [ [ [P.g, P.lqsd, P.s] ] ],
                couplings = {(0,0,0):C.R2GC_238_79})

V_71 = CTVertex(name = 'V_71',
                type = 'R2',
                particles = [ P.vt, P.s, P.lqsd__tilde__ ],
                color = [ 'Identity(2,3)' ],
                lorentz = [ L.FFS4 ],
                loop_particles = [ [ [P.g, P.lqsd, P.s] ] ],
                couplings = {(0,0,0):C.R2GC_240_80})

V_72 = CTVertex(name = 'V_72',
                type = 'R2',
                particles = [ P.c__tilde__, P.ve, P.lqsu ],
                color = [ 'Identity(1,3)' ],
                lorentz = [ L.FFS4 ],
                loop_particles = [ [ [P.c, P.g, P.lqsu] ] ],
                couplings = {(0,0,0):C.R2GC_218_59})

V_73 = CTVertex(name = 'V_73',
                type = 'R2',
                particles = [ P.c__tilde__, P.vm, P.lqsu ],
                color = [ 'Identity(1,3)' ],
                lorentz = [ L.FFS4 ],
                loop_particles = [ [ [P.c, P.g, P.lqsu] ] ],
                couplings = {(0,0,0):C.R2GC_219_60})

V_74 = CTVertex(name = 'V_74',
                type = 'R2',
                particles = [ P.c__tilde__, P.vt, P.lqsu ],
                color = [ 'Identity(1,3)' ],
                lorentz = [ L.FFS4 ],
                loop_particles = [ [ [P.c, P.g, P.lqsu] ] ],
                couplings = {(0,0,0):C.R2GC_220_61})

V_75 = CTVertex(name = 'V_75',
                type = 'R2',
                particles = [ P.t__tilde__, P.ve, P.lqsu ],
                color = [ 'Identity(1,3)' ],
                lorentz = [ L.FFS4 ],
                loop_particles = [ [ [P.g, P.lqsu, P.t] ] ],
                couplings = {(0,0,0):C.R2GC_313_127})

V_76 = CTVertex(name = 'V_76',
                type = 'R2',
                particles = [ P.t__tilde__, P.vm, P.lqsu ],
                color = [ 'Identity(1,3)' ],
                lorentz = [ L.FFS4 ],
                loop_particles = [ [ [P.g, P.lqsu, P.t] ] ],
                couplings = {(0,0,0):C.R2GC_314_128})

V_77 = CTVertex(name = 'V_77',
                type = 'R2',
                particles = [ P.t__tilde__, P.vt, P.lqsu ],
                color = [ 'Identity(1,3)' ],
                lorentz = [ L.FFS4 ],
                loop_particles = [ [ [P.g, P.lqsu, P.t] ] ],
                couplings = {(0,0,0):C.R2GC_315_129})

V_78 = CTVertex(name = 'V_78',
                type = 'R2',
                particles = [ P.u__tilde__, P.ve, P.lqsu ],
                color = [ 'Identity(1,3)' ],
                lorentz = [ L.FFS4 ],
                loop_particles = [ [ [P.g, P.lqsu, P.u] ] ],
                couplings = {(0,0,0):C.R2GC_267_99})

V_79 = CTVertex(name = 'V_79',
                type = 'R2',
                particles = [ P.u__tilde__, P.vm, P.lqsu ],
                color = [ 'Identity(1,3)' ],
                lorentz = [ L.FFS4 ],
                loop_particles = [ [ [P.g, P.lqsu, P.u] ] ],
                couplings = {(0,0,0):C.R2GC_268_100})

V_80 = CTVertex(name = 'V_80',
                type = 'R2',
                particles = [ P.u__tilde__, P.vt, P.lqsu ],
                color = [ 'Identity(1,3)' ],
                lorentz = [ L.FFS4 ],
                loop_particles = [ [ [P.g, P.lqsu, P.u] ] ],
                couplings = {(0,0,0):C.R2GC_269_101})

V_81 = CTVertex(name = 'V_81',
                type = 'R2',
                particles = [ P.g, P.lqsu__tilde__, P.lqsu ],
                color = [ 'T(1,3,2)' ],
                lorentz = [ L.VSS1, L.VSS2 ],
                loop_particles = [ [ [P.g, P.lqsu] ] ],
                couplings = {(0,0,0):C.R2GC_186_47,(0,1,0):C.R2GC_185_46})

V_82 = CTVertex(name = 'V_82',
                type = 'R2',
                particles = [ P.g, P.g, P.lqsu__tilde__, P.lqsu ],
                color = [ 'Identity(1,2)*Identity(3,4)', 'T(1,-1,3)*T(2,4,-1)', 'T(1,4,-1)*T(2,-1,3)' ],
                lorentz = [ L.VVSS1 ],
                loop_particles = [ [ [P.g] ], [ [P.g, P.lqsu] ] ],
                couplings = {(2,0,0):C.R2GC_187_48,(2,0,1):C.R2GC_187_49,(1,0,0):C.R2GC_187_48,(1,0,1):C.R2GC_187_49,(0,0,0):C.R2GC_169_33,(0,0,1):C.R2GC_183_44})

V_83 = CTVertex(name = 'V_83',
                type = 'R2',
                particles = [ P.g, P.lqsd__tilde__, P.lqsd ],
                color = [ 'T(1,3,2)' ],
                lorentz = [ L.VSS1, L.VSS2 ],
                loop_particles = [ [ [P.g, P.lqsd] ] ],
                couplings = {(0,0,0):C.R2GC_186_47,(0,1,0):C.R2GC_185_46})

V_84 = CTVertex(name = 'V_84',
                type = 'R2',
                particles = [ P.g, P.g, P.lqsd__tilde__, P.lqsd ],
                color = [ 'Identity(1,2)*Identity(3,4)', 'T(1,-1,3)*T(2,4,-1)', 'T(1,4,-1)*T(2,-1,3)' ],
                lorentz = [ L.VVSS1 ],
                loop_particles = [ [ [P.g] ], [ [P.g, P.lqsd] ] ],
                couplings = {(2,0,0):C.R2GC_187_48,(2,0,1):C.R2GC_187_49,(1,0,0):C.R2GC_187_48,(1,0,1):C.R2GC_187_49,(0,0,0):C.R2GC_169_33,(0,0,1):C.R2GC_183_44})

V_85 = CTVertex(name = 'V_85',
                type = 'R2',
                particles = [ P.u__tilde__, P.u, P.a ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV1 ],
                loop_particles = [ [ [P.g, P.u] ] ],
                couplings = {(0,0,0):C.R2GC_176_41})

V_86 = CTVertex(name = 'V_86',
                type = 'R2',
                particles = [ P.c__tilde__, P.c, P.a ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV1 ],
                loop_particles = [ [ [P.c, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_176_41})

V_87 = CTVertex(name = 'V_87',
                type = 'R2',
                particles = [ P.t__tilde__, P.t, P.a ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV1 ],
                loop_particles = [ [ [P.g, P.t] ] ],
                couplings = {(0,0,0):C.R2GC_176_41})

V_88 = CTVertex(name = 'V_88',
                type = 'R2',
                particles = [ P.u__tilde__, P.u, P.g ],
                color = [ 'T(3,2,1)' ],
                lorentz = [ L.FFV1 ],
                loop_particles = [ [ [P.g, P.u] ] ],
                couplings = {(0,0,0):C.R2GC_174_39})

V_89 = CTVertex(name = 'V_89',
                type = 'R2',
                particles = [ P.c__tilde__, P.c, P.g ],
                color = [ 'T(3,2,1)' ],
                lorentz = [ L.FFV1 ],
                loop_particles = [ [ [P.c, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_174_39})

V_90 = CTVertex(name = 'V_90',
                type = 'R2',
                particles = [ P.t__tilde__, P.t, P.g ],
                color = [ 'T(3,2,1)' ],
                lorentz = [ L.FFV1 ],
                loop_particles = [ [ [P.g, P.t] ] ],
                couplings = {(0,0,0):C.R2GC_174_39})

V_91 = CTVertex(name = 'V_91',
                type = 'R2',
                particles = [ P.d__tilde__, P.u, P.W__minus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV2 ],
                loop_particles = [ [ [P.d, P.g, P.u] ] ],
                couplings = {(0,0,0):C.R2GC_221_62})

V_92 = CTVertex(name = 'V_92',
                type = 'R2',
                particles = [ P.s__tilde__, P.c, P.W__minus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV2 ],
                loop_particles = [ [ [P.c, P.g, P.s] ] ],
                couplings = {(0,0,0):C.R2GC_221_62})

V_93 = CTVertex(name = 'V_93',
                type = 'R2',
                particles = [ P.b__tilde__, P.t, P.W__minus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV2 ],
                loop_particles = [ [ [P.b, P.g, P.t] ] ],
                couplings = {(0,0,0):C.R2GC_221_62})

V_94 = CTVertex(name = 'V_94',
                type = 'R2',
                particles = [ P.u__tilde__, P.u, P.Z ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV2, L.FFV3 ],
                loop_particles = [ [ [P.g, P.u] ] ],
                couplings = {(0,0,0):C.R2GC_148_21,(0,1,0):C.R2GC_135_1})

V_95 = CTVertex(name = 'V_95',
                type = 'R2',
                particles = [ P.c__tilde__, P.c, P.Z ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV2, L.FFV3 ],
                loop_particles = [ [ [P.c, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_148_21,(0,1,0):C.R2GC_135_1})

V_96 = CTVertex(name = 'V_96',
                type = 'R2',
                particles = [ P.t__tilde__, P.t, P.Z ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV2, L.FFV3 ],
                loop_particles = [ [ [P.g, P.t] ] ],
                couplings = {(0,0,0):C.R2GC_148_21,(0,1,0):C.R2GC_135_1})

V_97 = CTVertex(name = 'V_97',
                type = 'R2',
                particles = [ P.d__tilde__, P.d, P.a ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV1 ],
                loop_particles = [ [ [P.d, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_173_38})

V_98 = CTVertex(name = 'V_98',
                type = 'R2',
                particles = [ P.s__tilde__, P.s, P.a ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV1 ],
                loop_particles = [ [ [P.g, P.s] ] ],
                couplings = {(0,0,0):C.R2GC_173_38})

V_99 = CTVertex(name = 'V_99',
                type = 'R2',
                particles = [ P.b__tilde__, P.b, P.a ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV1 ],
                loop_particles = [ [ [P.b, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_173_38})

V_100 = CTVertex(name = 'V_100',
                 type = 'R2',
                 particles = [ P.d__tilde__, P.d, P.g ],
                 color = [ 'T(3,2,1)' ],
                 lorentz = [ L.FFV1 ],
                 loop_particles = [ [ [P.d, P.g] ] ],
                 couplings = {(0,0,0):C.R2GC_174_39})

V_101 = CTVertex(name = 'V_101',
                 type = 'R2',
                 particles = [ P.s__tilde__, P.s, P.g ],
                 color = [ 'T(3,2,1)' ],
                 lorentz = [ L.FFV1 ],
                 loop_particles = [ [ [P.g, P.s] ] ],
                 couplings = {(0,0,0):C.R2GC_174_39})

V_102 = CTVertex(name = 'V_102',
                 type = 'R2',
                 particles = [ P.b__tilde__, P.b, P.g ],
                 color = [ 'T(3,2,1)' ],
                 lorentz = [ L.FFV1 ],
                 loop_particles = [ [ [P.b, P.g] ] ],
                 couplings = {(0,0,0):C.R2GC_174_39})

V_103 = CTVertex(name = 'V_103',
                 type = 'R2',
                 particles = [ P.u__tilde__, P.d, P.W__plus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV2 ],
                 loop_particles = [ [ [P.d, P.g, P.u] ] ],
                 couplings = {(0,0,0):C.R2GC_221_62})

V_104 = CTVertex(name = 'V_104',
                 type = 'R2',
                 particles = [ P.c__tilde__, P.s, P.W__plus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV2 ],
                 loop_particles = [ [ [P.c, P.g, P.s] ] ],
                 couplings = {(0,0,0):C.R2GC_221_62})

V_105 = CTVertex(name = 'V_105',
                 type = 'R2',
                 particles = [ P.t__tilde__, P.b, P.W__plus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV2 ],
                 loop_particles = [ [ [P.b, P.g, P.t] ] ],
                 couplings = {(0,0,0):C.R2GC_221_62})

V_106 = CTVertex(name = 'V_106',
                 type = 'R2',
                 particles = [ P.d__tilde__, P.d, P.Z ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV2, L.FFV3 ],
                 loop_particles = [ [ [P.d, P.g] ] ],
                 couplings = {(0,0,0):C.R2GC_149_22,(0,1,0):C.R2GC_136_2})

V_107 = CTVertex(name = 'V_107',
                 type = 'R2',
                 particles = [ P.s__tilde__, P.s, P.Z ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV2, L.FFV3 ],
                 loop_particles = [ [ [P.g, P.s] ] ],
                 couplings = {(0,0,0):C.R2GC_149_22,(0,1,0):C.R2GC_136_2})

V_108 = CTVertex(name = 'V_108',
                 type = 'R2',
                 particles = [ P.b__tilde__, P.b, P.Z ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV2, L.FFV3 ],
                 loop_particles = [ [ [P.b, P.g] ] ],
                 couplings = {(0,0,0):C.R2GC_149_22,(0,1,0):C.R2GC_136_2})

V_109 = CTVertex(name = 'V_109',
                 type = 'R2',
                 particles = [ P.u__tilde__, P.u ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FF1 ],
                 loop_particles = [ [ [P.g, P.u] ] ],
                 couplings = {(0,0,0):C.R2GC_175_40})

V_110 = CTVertex(name = 'V_110',
                 type = 'R2',
                 particles = [ P.c__tilde__, P.c ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FF1 ],
                 loop_particles = [ [ [P.c, P.g] ] ],
                 couplings = {(0,0,0):C.R2GC_175_40})

V_111 = CTVertex(name = 'V_111',
                 type = 'R2',
                 particles = [ P.t__tilde__, P.t ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FF3, L.FF4, L.FF5, L.FF6 ],
                 loop_particles = [ [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.R2GC_316_130,(0,2,0):C.R2GC_316_130,(0,1,0):C.R2GC_175_40,(0,3,0):C.R2GC_175_40})

V_112 = CTVertex(name = 'V_112',
                 type = 'R2',
                 particles = [ P.d__tilde__, P.d ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FF1 ],
                 loop_particles = [ [ [P.d, P.g] ] ],
                 couplings = {(0,0,0):C.R2GC_175_40})

V_113 = CTVertex(name = 'V_113',
                 type = 'R2',
                 particles = [ P.s__tilde__, P.s ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FF1 ],
                 loop_particles = [ [ [P.g, P.s] ] ],
                 couplings = {(0,0,0):C.R2GC_175_40})

V_114 = CTVertex(name = 'V_114',
                 type = 'R2',
                 particles = [ P.b__tilde__, P.b ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FF3, L.FF4, L.FF5, L.FF6 ],
                 loop_particles = [ [ [P.b, P.g] ] ],
                 couplings = {(0,0,0):C.R2GC_283_108,(0,2,0):C.R2GC_283_108,(0,1,0):C.R2GC_175_40,(0,3,0):C.R2GC_175_40})

V_115 = CTVertex(name = 'V_115',
                 type = 'R2',
                 particles = [ P.lqsu__tilde__, P.lqsu ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.SS1, L.SS2 ],
                 loop_particles = [ [ [P.g, P.lqsu] ] ],
                 couplings = {(0,0,0):C.R2GC_289_112,(0,1,0):C.R2GC_184_45})

V_116 = CTVertex(name = 'V_116',
                 type = 'R2',
                 particles = [ P.lqsd__tilde__, P.lqsd ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.SS1, L.SS2 ],
                 loop_particles = [ [ [P.g, P.lqsd] ] ],
                 couplings = {(0,0,0):C.R2GC_288_111,(0,1,0):C.R2GC_184_45})

V_117 = CTVertex(name = 'V_117',
                 type = 'R2',
                 particles = [ P.g, P.g ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.VV1, L.VV2, L.VV3 ],
                 loop_particles = [ [ [P.b] ], [ [P.b], [P.c], [P.d], [P.s], [P.t], [P.u] ], [ [P.g] ], [ [P.t] ] ],
                 couplings = {(0,0,2):C.R2GC_291_115,(0,1,0):C.R2GC_139_3,(0,1,3):C.R2GC_139_4,(0,2,1):C.R2GC_290_113,(0,2,2):C.R2GC_290_114})

V_118 = CTVertex(name = 'V_118',
                 type = 'R2',
                 particles = [ P.g, P.g, P.H ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.VVS1 ],
                 loop_particles = [ [ [P.b] ], [ [P.t] ] ],
                 couplings = {(0,0,0):C.R2GC_140_5,(0,0,1):C.R2GC_140_6})

V_119 = CTVertex(name = 'V_119',
                 type = 'R2',
                 particles = [ P.g, P.g, P.W__minus__, P.W__plus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.VVVV2, L.VVVV3, L.VVVV4 ],
                 loop_particles = [ [ [P.b, P.t], [P.c, P.s], [P.d, P.u] ] ],
                 couplings = {(0,0,0):C.R2GC_152_24,(0,1,0):C.R2GC_152_24,(0,2,0):C.R2GC_152_24})

V_120 = CTVertex(name = 'V_120',
                 type = 'R2',
                 particles = [ P.a, P.g, P.g, P.Z ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.VVVV2, L.VVVV3, L.VVVV4 ],
                 loop_particles = [ [ [P.b], [P.d], [P.s] ], [ [P.c], [P.t], [P.u] ] ],
                 couplings = {(0,0,0):C.R2GC_144_13,(0,0,1):C.R2GC_144_14,(0,1,0):C.R2GC_144_13,(0,1,1):C.R2GC_144_14,(0,2,0):C.R2GC_144_13,(0,2,1):C.R2GC_144_14})

V_121 = CTVertex(name = 'V_121',
                 type = 'R2',
                 particles = [ P.g, P.g, P.Z, P.Z ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.VVVV2, L.VVVV3, L.VVVV4 ],
                 loop_particles = [ [ [P.b], [P.d], [P.s] ], [ [P.c], [P.t], [P.u] ] ],
                 couplings = {(0,0,0):C.R2GC_147_19,(0,0,1):C.R2GC_147_20,(0,1,0):C.R2GC_147_19,(0,1,1):C.R2GC_147_20,(0,2,0):C.R2GC_147_19,(0,2,1):C.R2GC_147_20})

V_122 = CTVertex(name = 'V_122',
                 type = 'R2',
                 particles = [ P.a, P.a, P.g, P.g ],
                 color = [ 'Identity(3,4)' ],
                 lorentz = [ L.VVVV2, L.VVVV3, L.VVVV4 ],
                 loop_particles = [ [ [P.b], [P.d], [P.s] ], [ [P.c], [P.t], [P.u] ] ],
                 couplings = {(0,0,0):C.R2GC_142_9,(0,0,1):C.R2GC_142_10,(0,1,0):C.R2GC_142_9,(0,1,1):C.R2GC_142_10,(0,2,0):C.R2GC_142_9,(0,2,1):C.R2GC_142_10})

V_123 = CTVertex(name = 'V_123',
                 type = 'R2',
                 particles = [ P.g, P.g, P.g, P.Z ],
                 color = [ 'd(1,2,3)', 'f(1,2,3)' ],
                 lorentz = [ L.VVVV1, L.VVVV2, L.VVVV3, L.VVVV4 ],
                 loop_particles = [ [ [P.b], [P.d], [P.s] ], [ [P.c], [P.t], [P.u] ] ],
                 couplings = {(1,0,0):C.R2GC_146_17,(1,0,1):C.R2GC_146_18,(0,1,0):C.R2GC_145_15,(0,1,1):C.R2GC_145_16,(0,2,0):C.R2GC_145_15,(0,2,1):C.R2GC_145_16,(0,3,0):C.R2GC_145_15,(0,3,1):C.R2GC_145_16})

V_124 = CTVertex(name = 'V_124',
                 type = 'R2',
                 particles = [ P.a, P.g, P.g, P.g ],
                 color = [ 'd(2,3,4)' ],
                 lorentz = [ L.VVVV2, L.VVVV3, L.VVVV4 ],
                 loop_particles = [ [ [P.b], [P.d], [P.s] ], [ [P.c], [P.t], [P.u] ] ],
                 couplings = {(0,0,0):C.R2GC_143_11,(0,0,1):C.R2GC_143_12,(0,1,0):C.R2GC_143_11,(0,1,1):C.R2GC_143_12,(0,2,0):C.R2GC_143_11,(0,2,1):C.R2GC_143_12})

V_125 = CTVertex(name = 'V_125',
                 type = 'R2',
                 particles = [ P.g, P.g, P.H, P.H ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.b] ], [ [P.t] ] ],
                 couplings = {(0,0,0):C.R2GC_141_7,(0,0,1):C.R2GC_141_8})

V_126 = CTVertex(name = 'V_126',
                 type = 'R2',
                 particles = [ P.g, P.g, P.G0, P.G0 ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.b] ], [ [P.t] ] ],
                 couplings = {(0,0,0):C.R2GC_141_7,(0,0,1):C.R2GC_141_8})

V_127 = CTVertex(name = 'V_127',
                 type = 'R2',
                 particles = [ P.g, P.g, P.G__minus__, P.G__plus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.b, P.t] ] ],
                 couplings = {(0,0,0):C.R2GC_151_23})

V_128 = CTVertex(name = 'V_128',
                 type = 'R2',
                 particles = [ P.lqsu__tilde__, P.lqsu__tilde__, P.lqsu, P.lqsu ],
                 color = [ 'Identity(1,3)*Identity(2,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.g] ], [ [P.g, P.lqsu] ] ],
                 couplings = {(1,0,0):C.R2GC_182_42,(1,0,1):C.R2GC_182_43,(0,0,0):C.R2GC_182_42,(0,0,1):C.R2GC_182_43})

V_129 = CTVertex(name = 'V_129',
                 type = 'R2',
                 particles = [ P.lqsd__tilde__, P.lqsd, P.lqsu__tilde__, P.lqsu ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.g] ], [ [P.g, P.lqsd], [P.g, P.lqsu] ], [ [P.g, P.lqsd, P.lqsu] ] ],
                 couplings = {(1,0,0):C.R2GC_235_75,(1,0,1):C.R2GC_235_76,(1,0,2):C.R2GC_235_77,(0,0,0):C.R2GC_234_72,(0,0,1):C.R2GC_234_73,(0,0,2):C.R2GC_234_74})

V_130 = CTVertex(name = 'V_130',
                 type = 'R2',
                 particles = [ P.lqsd__tilde__, P.lqsd__tilde__, P.lqsd, P.lqsd ],
                 color = [ 'Identity(1,3)*Identity(2,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.g] ], [ [P.g, P.lqsd] ] ],
                 couplings = {(1,0,0):C.R2GC_182_42,(1,0,1):C.R2GC_182_43,(0,0,0):C.R2GC_182_42,(0,0,1):C.R2GC_182_43})

V_131 = CTVertex(name = 'V_131',
                 type = 'UV',
                 particles = [ P.g, P.g, P.g ],
                 color = [ 'f(1,2,3)' ],
                 lorentz = [ L.VVV1, L.VVV2, L.VVV3, L.VVV4, L.VVV5, L.VVV6, L.VVV7, L.VVV8 ],
                 loop_particles = [ [ [P.b] ], [ [P.c], [P.d], [P.s], [P.u] ], [ [P.g] ], [ [P.ghG] ], [ [P.lqsd] ], [ [P.lqsd], [P.lqsu] ], [ [P.lqsu] ], [ [P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_292_236,(0,0,1):C.UVGC_292_237,(0,0,2):C.UVGC_292_238,(0,0,3):C.UVGC_170_16,(0,0,4):C.UVGC_292_239,(0,0,6):C.UVGC_292_240,(0,0,7):C.UVGC_292_241,(0,1,0):C.UVGC_295_248,(0,1,1):C.UVGC_295_249,(0,1,2):C.UVGC_296_254,(0,1,3):C.UVGC_296_255,(0,1,4):C.UVGC_296_256,(0,1,6):C.UVGC_296_257,(0,1,7):C.UVGC_295_253,(0,3,0):C.UVGC_295_248,(0,3,1):C.UVGC_295_249,(0,3,2):C.UVGC_295_250,(0,3,3):C.UVGC_164_6,(0,3,4):C.UVGC_295_251,(0,3,6):C.UVGC_295_252,(0,3,7):C.UVGC_295_253,(0,5,0):C.UVGC_292_236,(0,5,1):C.UVGC_292_237,(0,5,2):C.UVGC_294_244,(0,5,3):C.UVGC_294_245,(0,5,4):C.UVGC_294_246,(0,5,6):C.UVGC_294_247,(0,5,7):C.UVGC_292_241,(0,6,0):C.UVGC_292_236,(0,6,1):C.UVGC_292_237,(0,6,2):C.UVGC_293_242,(0,6,3):C.UVGC_293_243,(0,6,4):C.UVGC_292_239,(0,6,6):C.UVGC_292_240,(0,6,7):C.UVGC_292_241,(0,7,0):C.UVGC_295_248,(0,7,1):C.UVGC_295_249,(0,7,2):C.UVGC_297_258,(0,7,3):C.UVGC_297_259,(0,7,4):C.UVGC_296_256,(0,7,6):C.UVGC_296_257,(0,7,7):C.UVGC_295_253,(0,2,2):C.UVGC_164_5,(0,2,3):C.UVGC_164_6,(0,4,2):C.UVGC_170_15,(0,4,3):C.UVGC_170_16,(0,4,5):C.UVGC_170_17})

V_132 = CTVertex(name = 'V_132',
                 type = 'UV',
                 particles = [ P.g, P.g, P.g, P.g ],
                 color = [ 'd(-1,1,3)*d(-1,2,4)', 'd(-1,1,3)*f(-1,2,4)', 'd(-1,1,4)*d(-1,2,3)', 'd(-1,1,4)*f(-1,2,3)', 'd(-1,2,3)*f(-1,1,4)', 'd(-1,2,4)*f(-1,1,3)', 'f(-1,1,2)*f(-1,3,4)', 'f(-1,1,3)*f(-1,2,4)', 'f(-1,1,4)*f(-1,2,3)', 'Identity(1,2)*Identity(3,4)', 'Identity(1,3)*Identity(2,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.VVVV2, L.VVVV3, L.VVVV4 ],
                 loop_particles = [ [ [P.b] ], [ [P.b], [P.c], [P.d], [P.lqsd], [P.lqsu], [P.s], [P.t], [P.u] ], [ [P.b], [P.c], [P.d], [P.s], [P.t], [P.u] ], [ [P.c], [P.d], [P.s], [P.u] ], [ [P.g] ], [ [P.ghG] ], [ [P.lqsd] ], [ [P.lqsd], [P.lqsu] ], [ [P.lqsu] ], [ [P.t] ] ],
                 couplings = {(2,0,4):C.UVGC_166_10,(2,0,5):C.UVGC_166_9,(0,0,4):C.UVGC_166_10,(0,0,5):C.UVGC_166_9,(4,0,4):C.UVGC_165_7,(4,0,5):C.UVGC_165_8,(3,0,4):C.UVGC_165_7,(3,0,5):C.UVGC_165_8,(8,0,4):C.UVGC_166_9,(8,0,5):C.UVGC_166_10,(7,0,0):C.UVGC_301_277,(7,0,3):C.UVGC_187_49,(7,0,4):C.UVGC_301_278,(7,0,5):C.UVGC_301_279,(7,0,6):C.UVGC_301_280,(7,0,8):C.UVGC_301_281,(7,0,9):C.UVGC_301_282,(6,0,0):C.UVGC_301_277,(6,0,3):C.UVGC_187_49,(6,0,4):C.UVGC_302_283,(6,0,5):C.UVGC_302_284,(6,0,6):C.UVGC_301_280,(6,0,8):C.UVGC_301_281,(6,0,9):C.UVGC_301_282,(5,0,4):C.UVGC_165_7,(5,0,5):C.UVGC_165_8,(1,0,4):C.UVGC_165_7,(1,0,5):C.UVGC_165_8,(11,0,4):C.UVGC_169_13,(11,0,5):C.UVGC_169_14,(10,0,4):C.UVGC_169_13,(10,0,5):C.UVGC_169_14,(9,0,4):C.UVGC_168_11,(9,0,5):C.UVGC_168_12,(2,1,4):C.UVGC_166_10,(2,1,5):C.UVGC_166_9,(0,1,4):C.UVGC_166_10,(0,1,5):C.UVGC_166_9,(6,1,0):C.UVGC_298_260,(6,1,4):C.UVGC_299_266,(6,1,5):C.UVGC_299_267,(6,1,6):C.UVGC_299_268,(6,1,8):C.UVGC_299_269,(6,1,9):C.UVGC_298_265,(4,1,4):C.UVGC_165_7,(4,1,5):C.UVGC_165_8,(3,1,4):C.UVGC_165_7,(3,1,5):C.UVGC_165_8,(8,1,0):C.UVGC_303_285,(8,1,3):C.UVGC_303_286,(8,1,4):C.UVGC_303_287,(8,1,5):C.UVGC_303_288,(8,1,6):C.UVGC_303_289,(8,1,8):C.UVGC_303_290,(8,1,9):C.UVGC_303_291,(7,1,1):C.UVGC_171_18,(7,1,4):C.UVGC_172_21,(7,1,5):C.UVGC_172_22,(5,1,4):C.UVGC_165_7,(5,1,5):C.UVGC_165_8,(1,1,4):C.UVGC_165_7,(1,1,5):C.UVGC_165_8,(11,1,4):C.UVGC_169_13,(11,1,5):C.UVGC_169_14,(10,1,4):C.UVGC_169_13,(10,1,5):C.UVGC_169_14,(9,1,4):C.UVGC_168_11,(9,1,5):C.UVGC_168_12,(2,2,4):C.UVGC_166_10,(2,2,5):C.UVGC_166_9,(0,2,4):C.UVGC_166_10,(0,2,5):C.UVGC_166_9,(4,2,4):C.UVGC_165_7,(4,2,5):C.UVGC_165_8,(3,2,4):C.UVGC_165_7,(3,2,5):C.UVGC_165_8,(8,2,0):C.UVGC_300_270,(8,2,3):C.UVGC_300_271,(8,2,4):C.UVGC_300_272,(8,2,5):C.UVGC_300_273,(8,2,6):C.UVGC_300_274,(8,2,8):C.UVGC_300_275,(8,2,9):C.UVGC_300_276,(6,2,2):C.UVGC_171_18,(6,2,4):C.UVGC_171_19,(6,2,5):C.UVGC_168_11,(6,2,7):C.UVGC_171_20,(7,2,0):C.UVGC_298_260,(7,2,4):C.UVGC_298_261,(7,2,5):C.UVGC_298_262,(7,2,6):C.UVGC_298_263,(7,2,8):C.UVGC_298_264,(7,2,9):C.UVGC_298_265,(5,2,4):C.UVGC_165_7,(5,2,5):C.UVGC_165_8,(1,2,4):C.UVGC_165_7,(1,2,5):C.UVGC_165_8,(11,2,4):C.UVGC_169_13,(11,2,5):C.UVGC_169_14,(10,2,4):C.UVGC_169_13,(10,2,5):C.UVGC_169_14,(9,2,4):C.UVGC_168_11,(9,2,5):C.UVGC_168_12})

V_133 = CTVertex(name = 'V_133',
                 type = 'UV',
                 particles = [ P.e__minus__, P.c, P.lqsd__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3, L.FFS4 ],
                 loop_particles = [ [ [P.c, P.g] ], [ [P.c, P.g, P.lqsd] ], [ [P.g, P.lqsd] ] ],
                 couplings = {(0,0,0):C.UVGC_207_68,(0,0,2):C.UVGC_206_66,(0,0,1):C.UVGC_206_67,(0,1,0):C.UVGC_209_71,(0,1,2):C.UVGC_208_69,(0,1,1):C.UVGC_208_70})

V_134 = CTVertex(name = 'V_134',
                 type = 'UV',
                 particles = [ P.mu__minus__, P.c, P.lqsd__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3, L.FFS4 ],
                 loop_particles = [ [ [P.c, P.g] ], [ [P.c, P.g, P.lqsd] ], [ [P.g, P.lqsd] ] ],
                 couplings = {(0,0,0):C.UVGC_211_74,(0,0,2):C.UVGC_210_72,(0,0,1):C.UVGC_210_73,(0,1,0):C.UVGC_213_77,(0,1,2):C.UVGC_212_75,(0,1,1):C.UVGC_212_76})

V_135 = CTVertex(name = 'V_135',
                 type = 'UV',
                 particles = [ P.ta__minus__, P.c, P.lqsd__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3, L.FFS4 ],
                 loop_particles = [ [ [P.c, P.g] ], [ [P.c, P.g, P.lqsd] ], [ [P.g, P.lqsd] ] ],
                 couplings = {(0,0,0):C.UVGC_215_80,(0,0,2):C.UVGC_214_78,(0,0,1):C.UVGC_214_79,(0,1,0):C.UVGC_217_83,(0,1,2):C.UVGC_216_81,(0,1,1):C.UVGC_216_82})

V_136 = CTVertex(name = 'V_136',
                 type = 'UV',
                 particles = [ P.ta__minus__, P.t, P.lqsd__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3, L.FFS4 ],
                 loop_particles = [ [ [P.g, P.lqsd] ], [ [P.g, P.lqsd, P.t] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_252_163,(0,0,2):C.UVGC_311_299,(0,0,1):C.UVGC_252_164,(0,1,0):C.UVGC_253_165,(0,1,2):C.UVGC_312_300,(0,1,1):C.UVGC_253_166})

V_137 = CTVertex(name = 'V_137',
                 type = 'UV',
                 particles = [ P.ve__tilde__, P.b__tilde__, P.lqsd ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.b, P.g, P.lqsd] ], [ [P.g, P.lqsd] ] ],
                 couplings = {(0,0,1):C.UVGC_203_60,(0,0,0):C.UVGC_203_61})

V_138 = CTVertex(name = 'V_138',
                 type = 'UV',
                 particles = [ P.vm__tilde__, P.b__tilde__, P.lqsd ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.b, P.g, P.lqsd] ], [ [P.g, P.lqsd] ] ],
                 couplings = {(0,0,1):C.UVGC_204_62,(0,0,0):C.UVGC_204_63})

V_139 = CTVertex(name = 'V_139',
                 type = 'UV',
                 particles = [ P.vt__tilde__, P.b__tilde__, P.lqsd ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.b, P.g, P.lqsd] ], [ [P.g, P.lqsd] ] ],
                 couplings = {(0,0,1):C.UVGC_205_64,(0,0,0):C.UVGC_205_65})

V_140 = CTVertex(name = 'V_140',
                 type = 'UV',
                 particles = [ P.ve__tilde__, P.d__tilde__, P.lqsd ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.d, P.g, P.lqsd] ], [ [P.g, P.lqsd] ] ],
                 couplings = {(0,0,1):C.UVGC_222_95,(0,0,0):C.UVGC_222_96})

V_141 = CTVertex(name = 'V_141',
                 type = 'UV',
                 particles = [ P.vm__tilde__, P.d__tilde__, P.lqsd ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.d, P.g, P.lqsd] ], [ [P.g, P.lqsd] ] ],
                 couplings = {(0,0,1):C.UVGC_224_98,(0,0,0):C.UVGC_224_99})

V_142 = CTVertex(name = 'V_142',
                 type = 'UV',
                 particles = [ P.vt__tilde__, P.d__tilde__, P.lqsd ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.d, P.g, P.lqsd] ], [ [P.g, P.lqsd] ] ],
                 couplings = {(0,0,1):C.UVGC_226_101,(0,0,0):C.UVGC_226_102})

V_143 = CTVertex(name = 'V_143',
                 type = 'UV',
                 particles = [ P.ve__tilde__, P.s__tilde__, P.lqsd ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.g, P.lqsd] ], [ [P.g, P.lqsd, P.s] ] ],
                 couplings = {(0,0,0):C.UVGC_236_128,(0,0,1):C.UVGC_236_129})

V_144 = CTVertex(name = 'V_144',
                 type = 'UV',
                 particles = [ P.vm__tilde__, P.s__tilde__, P.lqsd ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.g, P.lqsd] ], [ [P.g, P.lqsd, P.s] ] ],
                 couplings = {(0,0,0):C.UVGC_238_131,(0,0,1):C.UVGC_238_132})

V_145 = CTVertex(name = 'V_145',
                 type = 'UV',
                 particles = [ P.vt__tilde__, P.s__tilde__, P.lqsd ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.g, P.lqsd] ], [ [P.g, P.lqsd, P.s] ] ],
                 couplings = {(0,0,0):C.UVGC_240_134,(0,0,1):C.UVGC_240_135})

V_146 = CTVertex(name = 'V_146',
                 type = 'UV',
                 particles = [ P.e__plus__, P.b, P.lqsu__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3, L.FFS4 ],
                 loop_particles = [ [ [P.b, P.g] ], [ [P.b, P.g, P.lqsu] ], [ [P.g, P.lqsu] ] ],
                 couplings = {(0,0,0):C.UVGC_273_197,(0,0,2):C.UVGC_273_198,(0,0,1):C.UVGC_273_199,(0,1,0):C.UVGC_274_200,(0,1,2):C.UVGC_274_201,(0,1,1):C.UVGC_274_202})

V_147 = CTVertex(name = 'V_147',
                 type = 'UV',
                 particles = [ P.mu__plus__, P.b, P.lqsu__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3, L.FFS4 ],
                 loop_particles = [ [ [P.b, P.g] ], [ [P.b, P.g, P.lqsu] ], [ [P.g, P.lqsu] ] ],
                 couplings = {(0,0,0):C.UVGC_275_203,(0,0,2):C.UVGC_275_204,(0,0,1):C.UVGC_275_205,(0,1,0):C.UVGC_276_206,(0,1,2):C.UVGC_276_207,(0,1,1):C.UVGC_276_208})

V_148 = CTVertex(name = 'V_148',
                 type = 'UV',
                 particles = [ P.ta__plus__, P.b, P.lqsu__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3, L.FFS4 ],
                 loop_particles = [ [ [P.b, P.g] ], [ [P.b, P.g, P.lqsu] ], [ [P.g, P.lqsu] ] ],
                 couplings = {(0,0,0):C.UVGC_277_209,(0,0,2):C.UVGC_277_210,(0,0,1):C.UVGC_277_211,(0,1,0):C.UVGC_278_212,(0,1,2):C.UVGC_278_213,(0,1,1):C.UVGC_278_214})

V_149 = CTVertex(name = 'V_149',
                 type = 'UV',
                 particles = [ P.ve__tilde__, P.c, P.lqsu__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.c, P.g] ], [ [P.c, P.g, P.lqsu] ], [ [P.g, P.lqsu] ] ],
                 couplings = {(0,0,0):C.UVGC_218_84,(0,0,2):C.UVGC_218_85,(0,0,1):C.UVGC_218_86})

V_150 = CTVertex(name = 'V_150',
                 type = 'UV',
                 particles = [ P.vm__tilde__, P.c, P.lqsu__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.c, P.g] ], [ [P.c, P.g, P.lqsu] ], [ [P.g, P.lqsu] ] ],
                 couplings = {(0,0,0):C.UVGC_219_87,(0,0,2):C.UVGC_219_88,(0,0,1):C.UVGC_219_89})

V_151 = CTVertex(name = 'V_151',
                 type = 'UV',
                 particles = [ P.vt__tilde__, P.c, P.lqsu__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.c, P.g] ], [ [P.c, P.g, P.lqsu] ], [ [P.g, P.lqsu] ] ],
                 couplings = {(0,0,0):C.UVGC_220_90,(0,0,2):C.UVGC_220_91,(0,0,1):C.UVGC_220_92})

V_152 = CTVertex(name = 'V_152',
                 type = 'UV',
                 particles = [ P.e__plus__, P.d, P.lqsu__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3, L.FFS4 ],
                 loop_particles = [ [ [P.d, P.g] ], [ [P.d, P.g, P.lqsu] ], [ [P.g, P.lqsu] ] ],
                 couplings = {(0,0,0):C.UVGC_228_104,(0,0,2):C.UVGC_228_105,(0,0,1):C.UVGC_228_106,(0,1,0):C.UVGC_229_107,(0,1,2):C.UVGC_229_108,(0,1,1):C.UVGC_229_109})

V_153 = CTVertex(name = 'V_153',
                 type = 'UV',
                 particles = [ P.mu__plus__, P.d, P.lqsu__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3, L.FFS4 ],
                 loop_particles = [ [ [P.d, P.g] ], [ [P.d, P.g, P.lqsu] ], [ [P.g, P.lqsu] ] ],
                 couplings = {(0,0,0):C.UVGC_230_110,(0,0,2):C.UVGC_230_111,(0,0,1):C.UVGC_230_112,(0,1,0):C.UVGC_231_113,(0,1,2):C.UVGC_231_114,(0,1,1):C.UVGC_231_115})

V_154 = CTVertex(name = 'V_154',
                 type = 'UV',
                 particles = [ P.ta__plus__, P.d, P.lqsu__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3, L.FFS4 ],
                 loop_particles = [ [ [P.d, P.g] ], [ [P.d, P.g, P.lqsu] ], [ [P.g, P.lqsu] ] ],
                 couplings = {(0,0,0):C.UVGC_232_116,(0,0,2):C.UVGC_232_117,(0,0,1):C.UVGC_232_118,(0,1,0):C.UVGC_233_119,(0,1,2):C.UVGC_233_120,(0,1,1):C.UVGC_233_121})

V_155 = CTVertex(name = 'V_155',
                 type = 'UV',
                 particles = [ P.e__plus__, P.s, P.lqsu__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3, L.FFS4 ],
                 loop_particles = [ [ [P.g, P.lqsu] ], [ [P.g, P.lqsu, P.s] ], [ [P.g, P.s] ] ],
                 couplings = {(0,0,0):C.UVGC_242_137,(0,0,2):C.UVGC_242_138,(0,0,1):C.UVGC_242_139,(0,1,0):C.UVGC_243_140,(0,1,2):C.UVGC_243_141,(0,1,1):C.UVGC_243_142})

V_156 = CTVertex(name = 'V_156',
                 type = 'UV',
                 particles = [ P.mu__plus__, P.s, P.lqsu__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3, L.FFS4 ],
                 loop_particles = [ [ [P.g, P.lqsu] ], [ [P.g, P.lqsu, P.s] ], [ [P.g, P.s] ] ],
                 couplings = {(0,0,0):C.UVGC_244_143,(0,0,2):C.UVGC_244_144,(0,0,1):C.UVGC_244_145,(0,1,0):C.UVGC_245_146,(0,1,2):C.UVGC_245_147,(0,1,1):C.UVGC_245_148})

V_157 = CTVertex(name = 'V_157',
                 type = 'UV',
                 particles = [ P.ta__plus__, P.s, P.lqsu__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3, L.FFS4 ],
                 loop_particles = [ [ [P.g, P.lqsu] ], [ [P.g, P.lqsu, P.s] ], [ [P.g, P.s] ] ],
                 couplings = {(0,0,0):C.UVGC_246_149,(0,0,2):C.UVGC_246_150,(0,0,1):C.UVGC_246_151,(0,1,0):C.UVGC_247_152,(0,1,2):C.UVGC_247_153,(0,1,1):C.UVGC_247_154})

V_158 = CTVertex(name = 'V_158',
                 type = 'UV',
                 particles = [ P.ve__tilde__, P.t, P.lqsu__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.g, P.lqsu] ], [ [P.g, P.lqsu, P.t] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_313_301,(0,0,2):C.UVGC_313_302,(0,0,1):C.UVGC_313_303})

V_159 = CTVertex(name = 'V_159',
                 type = 'UV',
                 particles = [ P.vm__tilde__, P.t, P.lqsu__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.g, P.lqsu] ], [ [P.g, P.lqsu, P.t] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_314_304,(0,0,2):C.UVGC_314_305,(0,0,1):C.UVGC_314_306})

V_160 = CTVertex(name = 'V_160',
                 type = 'UV',
                 particles = [ P.vt__tilde__, P.t, P.lqsu__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.g, P.lqsu] ], [ [P.g, P.lqsu, P.t] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_315_307,(0,0,2):C.UVGC_315_308,(0,0,1):C.UVGC_315_309})

V_161 = CTVertex(name = 'V_161',
                 type = 'UV',
                 particles = [ P.ve__tilde__, P.u, P.lqsu__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.g, P.lqsu] ], [ [P.g, P.lqsu, P.u] ], [ [P.g, P.u] ] ],
                 couplings = {(0,0,0):C.UVGC_267_185,(0,0,2):C.UVGC_267_186,(0,0,1):C.UVGC_267_187})

V_162 = CTVertex(name = 'V_162',
                 type = 'UV',
                 particles = [ P.vm__tilde__, P.u, P.lqsu__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.g, P.lqsu] ], [ [P.g, P.lqsu, P.u] ], [ [P.g, P.u] ] ],
                 couplings = {(0,0,0):C.UVGC_268_188,(0,0,2):C.UVGC_268_189,(0,0,1):C.UVGC_268_190})

V_163 = CTVertex(name = 'V_163',
                 type = 'UV',
                 particles = [ P.vt__tilde__, P.u, P.lqsu__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.g, P.lqsu] ], [ [P.g, P.lqsu, P.u] ], [ [P.g, P.u] ] ],
                 couplings = {(0,0,0):C.UVGC_269_191,(0,0,2):C.UVGC_269_192,(0,0,1):C.UVGC_269_193})

V_164 = CTVertex(name = 'V_164',
                 type = 'UV',
                 particles = [ P.t, P.e__minus__, P.lqsd__tilde__ ],
                 color = [ 'Identity(1,3)' ],
                 lorentz = [ L.FFS3, L.FFS4 ],
                 loop_particles = [ [ [P.g, P.lqsd] ], [ [P.g, P.lqsd, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_248_155,(0,0,1):C.UVGC_248_156,(0,1,0):C.UVGC_249_157,(0,1,1):C.UVGC_249_158})

V_165 = CTVertex(name = 'V_165',
                 type = 'UV',
                 particles = [ P.u, P.e__minus__, P.lqsd__tilde__ ],
                 color = [ 'Identity(1,3)' ],
                 lorentz = [ L.FFS3, L.FFS4 ],
                 loop_particles = [ [ [P.g, P.lqsd] ], [ [P.g, P.lqsd, P.u] ] ],
                 couplings = {(0,0,0):C.UVGC_255_167,(0,0,1):C.UVGC_255_168,(0,1,0):C.UVGC_257_170,(0,1,1):C.UVGC_257_171})

V_166 = CTVertex(name = 'V_166',
                 type = 'UV',
                 particles = [ P.t, P.mu__minus__, P.lqsd__tilde__ ],
                 color = [ 'Identity(1,3)' ],
                 lorentz = [ L.FFS3, L.FFS4 ],
                 loop_particles = [ [ [P.g, P.lqsd] ], [ [P.g, P.lqsd, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_250_159,(0,0,1):C.UVGC_250_160,(0,1,0):C.UVGC_251_161,(0,1,1):C.UVGC_251_162})

V_167 = CTVertex(name = 'V_167',
                 type = 'UV',
                 particles = [ P.u, P.mu__minus__, P.lqsd__tilde__ ],
                 color = [ 'Identity(1,3)' ],
                 lorentz = [ L.FFS3, L.FFS4 ],
                 loop_particles = [ [ [P.g, P.lqsd] ], [ [P.g, P.lqsd, P.u] ] ],
                 couplings = {(0,0,0):C.UVGC_259_173,(0,0,1):C.UVGC_259_174,(0,1,0):C.UVGC_261_176,(0,1,1):C.UVGC_261_177})

V_168 = CTVertex(name = 'V_168',
                 type = 'UV',
                 particles = [ P.u, P.ta__minus__, P.lqsd__tilde__ ],
                 color = [ 'Identity(1,3)' ],
                 lorentz = [ L.FFS3, L.FFS4 ],
                 loop_particles = [ [ [P.g, P.lqsd] ], [ [P.g, P.lqsd, P.u] ] ],
                 couplings = {(0,0,0):C.UVGC_263_179,(0,0,1):C.UVGC_263_180,(0,1,0):C.UVGC_265_182,(0,1,1):C.UVGC_265_183})

V_169 = CTVertex(name = 'V_169',
                 type = 'UV',
                 particles = [ P.t__tilde__, P.b, P.G__plus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS3, L.FFS4 ],
                 loop_particles = [ [ [P.b, P.g] ], [ [P.b, P.g, P.t] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_321_318,(0,0,2):C.UVGC_321_319,(0,0,1):C.UVGC_321_320,(0,1,0):C.UVGC_322_321,(0,1,2):C.UVGC_322_322,(0,1,1):C.UVGC_322_323})

V_170 = CTVertex(name = 'V_170',
                 type = 'UV',
                 particles = [ P.b__tilde__, P.b, P.G0 ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS1 ],
                 loop_particles = [ [ [P.b, P.g] ] ],
                 couplings = {(0,0,0):C.UVGC_287_223})

V_171 = CTVertex(name = 'V_171',
                 type = 'UV',
                 particles = [ P.b__tilde__, P.b, P.H ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2 ],
                 loop_particles = [ [ [P.b, P.g] ] ],
                 couplings = {(0,0,0):C.UVGC_286_222})

V_172 = CTVertex(name = 'V_172',
                 type = 'UV',
                 particles = [ P.t__tilde__, P.t, P.G0 ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS1 ],
                 loop_particles = [ [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_324_327})

V_173 = CTVertex(name = 'V_173',
                 type = 'UV',
                 particles = [ P.t__tilde__, P.t, P.H ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2 ],
                 loop_particles = [ [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_325_328})

V_174 = CTVertex(name = 'V_174',
                 type = 'UV',
                 particles = [ P.b__tilde__, P.t, P.G__minus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS3, L.FFS4 ],
                 loop_particles = [ [ [P.b, P.g] ], [ [P.b, P.g, P.t] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_323_324,(0,0,2):C.UVGC_323_325,(0,0,1):C.UVGC_323_326,(0,1,0):C.UVGC_320_315,(0,1,2):C.UVGC_320_316,(0,1,1):C.UVGC_320_317})

V_175 = CTVertex(name = 'V_175',
                 type = 'UV',
                 particles = [ P.e__plus__, P.c__tilde__, P.lqsd ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3, L.FFS4 ],
                 loop_particles = [ [ [P.c, P.g, P.lqsd] ], [ [P.g, P.lqsd] ] ],
                 couplings = {(0,0,1):C.UVGC_208_69,(0,0,0):C.UVGC_208_70,(0,1,1):C.UVGC_206_66,(0,1,0):C.UVGC_206_67})

V_176 = CTVertex(name = 'V_176',
                 type = 'UV',
                 particles = [ P.mu__plus__, P.c__tilde__, P.lqsd ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3, L.FFS4 ],
                 loop_particles = [ [ [P.c, P.g, P.lqsd] ], [ [P.g, P.lqsd] ] ],
                 couplings = {(0,0,1):C.UVGC_212_75,(0,0,0):C.UVGC_212_76,(0,1,1):C.UVGC_210_72,(0,1,0):C.UVGC_210_73})

V_177 = CTVertex(name = 'V_177',
                 type = 'UV',
                 particles = [ P.ta__plus__, P.c__tilde__, P.lqsd ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3, L.FFS4 ],
                 loop_particles = [ [ [P.c, P.g, P.lqsd] ], [ [P.g, P.lqsd] ] ],
                 couplings = {(0,0,1):C.UVGC_216_81,(0,0,0):C.UVGC_216_82,(0,1,1):C.UVGC_214_78,(0,1,0):C.UVGC_214_79})

V_178 = CTVertex(name = 'V_178',
                 type = 'UV',
                 particles = [ P.t__tilde__, P.e__plus__, P.lqsd ],
                 color = [ 'Identity(1,3)' ],
                 lorentz = [ L.FFS3, L.FFS4 ],
                 loop_particles = [ [ [P.g, P.lqsd] ], [ [P.g, P.lqsd, P.t] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_249_157,(0,0,2):C.UVGC_308_296,(0,0,1):C.UVGC_249_158,(0,1,0):C.UVGC_248_155,(0,1,2):C.UVGC_307_295,(0,1,1):C.UVGC_248_156})

V_179 = CTVertex(name = 'V_179',
                 type = 'UV',
                 particles = [ P.t__tilde__, P.mu__plus__, P.lqsd ],
                 color = [ 'Identity(1,3)' ],
                 lorentz = [ L.FFS3, L.FFS4 ],
                 loop_particles = [ [ [P.g, P.lqsd] ], [ [P.g, P.lqsd, P.t] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_251_161,(0,0,2):C.UVGC_310_298,(0,0,1):C.UVGC_251_162,(0,1,0):C.UVGC_250_159,(0,1,2):C.UVGC_309_297,(0,1,1):C.UVGC_250_160})

V_180 = CTVertex(name = 'V_180',
                 type = 'UV',
                 particles = [ P.ta__plus__, P.t__tilde__, P.lqsd ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3, L.FFS4 ],
                 loop_particles = [ [ [P.g, P.lqsd] ], [ [P.g, P.lqsd, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_253_165,(0,0,1):C.UVGC_253_166,(0,1,0):C.UVGC_252_163,(0,1,1):C.UVGC_252_164})

V_181 = CTVertex(name = 'V_181',
                 type = 'UV',
                 particles = [ P.u__tilde__, P.e__plus__, P.lqsd ],
                 color = [ 'Identity(1,3)' ],
                 lorentz = [ L.FFS3, L.FFS4 ],
                 loop_particles = [ [ [P.g, P.lqsd] ], [ [P.g, P.lqsd, P.u] ], [ [P.g, P.u] ] ],
                 couplings = {(0,0,0):C.UVGC_257_170,(0,0,2):C.UVGC_258_172,(0,0,1):C.UVGC_257_171,(0,1,0):C.UVGC_255_167,(0,1,2):C.UVGC_256_169,(0,1,1):C.UVGC_255_168})

V_182 = CTVertex(name = 'V_182',
                 type = 'UV',
                 particles = [ P.u__tilde__, P.mu__plus__, P.lqsd ],
                 color = [ 'Identity(1,3)' ],
                 lorentz = [ L.FFS3, L.FFS4 ],
                 loop_particles = [ [ [P.g, P.lqsd] ], [ [P.g, P.lqsd, P.u] ], [ [P.g, P.u] ] ],
                 couplings = {(0,0,0):C.UVGC_261_176,(0,0,2):C.UVGC_262_178,(0,0,1):C.UVGC_261_177,(0,1,0):C.UVGC_259_173,(0,1,2):C.UVGC_260_175,(0,1,1):C.UVGC_259_174})

V_183 = CTVertex(name = 'V_183',
                 type = 'UV',
                 particles = [ P.u__tilde__, P.ta__plus__, P.lqsd ],
                 color = [ 'Identity(1,3)' ],
                 lorentz = [ L.FFS3, L.FFS4 ],
                 loop_particles = [ [ [P.g, P.lqsd] ], [ [P.g, P.lqsd, P.u] ], [ [P.g, P.u] ] ],
                 couplings = {(0,0,0):C.UVGC_265_182,(0,0,2):C.UVGC_266_184,(0,0,1):C.UVGC_265_183,(0,1,0):C.UVGC_263_179,(0,1,2):C.UVGC_264_181,(0,1,1):C.UVGC_263_180})

V_184 = CTVertex(name = 'V_184',
                 type = 'UV',
                 particles = [ P.b__tilde__, P.e__minus__, P.lqsu ],
                 color = [ 'Identity(1,3)' ],
                 lorentz = [ L.FFS3, L.FFS4 ],
                 loop_particles = [ [ [P.b, P.g] ], [ [P.b, P.g, P.lqsu] ], [ [P.g, P.lqsu] ] ],
                 couplings = {(0,0,0):C.UVGC_274_200,(0,0,2):C.UVGC_274_201,(0,0,1):C.UVGC_274_202,(0,1,0):C.UVGC_273_197,(0,1,2):C.UVGC_273_198,(0,1,1):C.UVGC_273_199})

V_185 = CTVertex(name = 'V_185',
                 type = 'UV',
                 particles = [ P.b__tilde__, P.mu__minus__, P.lqsu ],
                 color = [ 'Identity(1,3)' ],
                 lorentz = [ L.FFS3, L.FFS4 ],
                 loop_particles = [ [ [P.b, P.g] ], [ [P.b, P.g, P.lqsu] ], [ [P.g, P.lqsu] ] ],
                 couplings = {(0,0,0):C.UVGC_276_206,(0,0,2):C.UVGC_276_207,(0,0,1):C.UVGC_276_208,(0,1,0):C.UVGC_275_203,(0,1,2):C.UVGC_275_204,(0,1,1):C.UVGC_275_205})

V_186 = CTVertex(name = 'V_186',
                 type = 'UV',
                 particles = [ P.b__tilde__, P.ta__minus__, P.lqsu ],
                 color = [ 'Identity(1,3)' ],
                 lorentz = [ L.FFS3, L.FFS4 ],
                 loop_particles = [ [ [P.b, P.g] ], [ [P.b, P.g, P.lqsu] ], [ [P.g, P.lqsu] ] ],
                 couplings = {(0,0,0):C.UVGC_278_212,(0,0,2):C.UVGC_278_213,(0,0,1):C.UVGC_278_214,(0,1,0):C.UVGC_277_209,(0,1,2):C.UVGC_277_210,(0,1,1):C.UVGC_277_211})

V_187 = CTVertex(name = 'V_187',
                 type = 'UV',
                 particles = [ P.d__tilde__, P.e__minus__, P.lqsu ],
                 color = [ 'Identity(1,3)' ],
                 lorentz = [ L.FFS3, L.FFS4 ],
                 loop_particles = [ [ [P.d, P.g] ], [ [P.d, P.g, P.lqsu] ], [ [P.g, P.lqsu] ] ],
                 couplings = {(0,0,0):C.UVGC_229_107,(0,0,2):C.UVGC_229_108,(0,0,1):C.UVGC_229_109,(0,1,0):C.UVGC_228_104,(0,1,2):C.UVGC_228_105,(0,1,1):C.UVGC_228_106})

V_188 = CTVertex(name = 'V_188',
                 type = 'UV',
                 particles = [ P.d__tilde__, P.mu__minus__, P.lqsu ],
                 color = [ 'Identity(1,3)' ],
                 lorentz = [ L.FFS3, L.FFS4 ],
                 loop_particles = [ [ [P.d, P.g] ], [ [P.d, P.g, P.lqsu] ], [ [P.g, P.lqsu] ] ],
                 couplings = {(0,0,0):C.UVGC_231_113,(0,0,2):C.UVGC_231_114,(0,0,1):C.UVGC_231_115,(0,1,0):C.UVGC_230_110,(0,1,2):C.UVGC_230_111,(0,1,1):C.UVGC_230_112})

V_189 = CTVertex(name = 'V_189',
                 type = 'UV',
                 particles = [ P.d__tilde__, P.ta__minus__, P.lqsu ],
                 color = [ 'Identity(1,3)' ],
                 lorentz = [ L.FFS3, L.FFS4 ],
                 loop_particles = [ [ [P.d, P.g] ], [ [P.d, P.g, P.lqsu] ], [ [P.g, P.lqsu] ] ],
                 couplings = {(0,0,0):C.UVGC_233_119,(0,0,2):C.UVGC_233_120,(0,0,1):C.UVGC_233_121,(0,1,0):C.UVGC_232_116,(0,1,2):C.UVGC_232_117,(0,1,1):C.UVGC_232_118})

V_190 = CTVertex(name = 'V_190',
                 type = 'UV',
                 particles = [ P.s__tilde__, P.e__minus__, P.lqsu ],
                 color = [ 'Identity(1,3)' ],
                 lorentz = [ L.FFS3, L.FFS4 ],
                 loop_particles = [ [ [P.g, P.lqsu] ], [ [P.g, P.lqsu, P.s] ], [ [P.g, P.s] ] ],
                 couplings = {(0,0,0):C.UVGC_243_140,(0,0,2):C.UVGC_243_141,(0,0,1):C.UVGC_243_142,(0,1,0):C.UVGC_242_137,(0,1,2):C.UVGC_242_138,(0,1,1):C.UVGC_242_139})

V_191 = CTVertex(name = 'V_191',
                 type = 'UV',
                 particles = [ P.s__tilde__, P.mu__minus__, P.lqsu ],
                 color = [ 'Identity(1,3)' ],
                 lorentz = [ L.FFS3, L.FFS4 ],
                 loop_particles = [ [ [P.g, P.lqsu] ], [ [P.g, P.lqsu, P.s] ], [ [P.g, P.s] ] ],
                 couplings = {(0,0,0):C.UVGC_245_146,(0,0,2):C.UVGC_245_147,(0,0,1):C.UVGC_245_148,(0,1,0):C.UVGC_244_143,(0,1,2):C.UVGC_244_144,(0,1,1):C.UVGC_244_145})

V_192 = CTVertex(name = 'V_192',
                 type = 'UV',
                 particles = [ P.s__tilde__, P.ta__minus__, P.lqsu ],
                 color = [ 'Identity(1,3)' ],
                 lorentz = [ L.FFS3, L.FFS4 ],
                 loop_particles = [ [ [P.g, P.lqsu] ], [ [P.g, P.lqsu, P.s] ], [ [P.g, P.s] ] ],
                 couplings = {(0,0,0):C.UVGC_247_152,(0,0,2):C.UVGC_247_153,(0,0,1):C.UVGC_247_154,(0,1,0):C.UVGC_246_149,(0,1,2):C.UVGC_246_150,(0,1,1):C.UVGC_246_151})

V_193 = CTVertex(name = 'V_193',
                 type = 'UV',
                 particles = [ P.ve, P.b, P.lqsd__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS4 ],
                 loop_particles = [ [ [P.b, P.g] ], [ [P.b, P.g, P.lqsd] ], [ [P.g, P.lqsd] ] ],
                 couplings = {(0,0,0):C.UVGC_279_215,(0,0,2):C.UVGC_203_60,(0,0,1):C.UVGC_203_61})

V_194 = CTVertex(name = 'V_194',
                 type = 'UV',
                 particles = [ P.vm, P.b, P.lqsd__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS4 ],
                 loop_particles = [ [ [P.b, P.g] ], [ [P.b, P.g, P.lqsd] ], [ [P.g, P.lqsd] ] ],
                 couplings = {(0,0,0):C.UVGC_280_216,(0,0,2):C.UVGC_204_62,(0,0,1):C.UVGC_204_63})

V_195 = CTVertex(name = 'V_195',
                 type = 'UV',
                 particles = [ P.vt, P.b, P.lqsd__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS4 ],
                 loop_particles = [ [ [P.b, P.g] ], [ [P.b, P.g, P.lqsd] ], [ [P.g, P.lqsd] ] ],
                 couplings = {(0,0,0):C.UVGC_281_217,(0,0,2):C.UVGC_205_64,(0,0,1):C.UVGC_205_65})

V_196 = CTVertex(name = 'V_196',
                 type = 'UV',
                 particles = [ P.ve, P.d, P.lqsd__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS4 ],
                 loop_particles = [ [ [P.d, P.g] ], [ [P.d, P.g, P.lqsd] ], [ [P.g, P.lqsd] ] ],
                 couplings = {(0,0,0):C.UVGC_223_97,(0,0,2):C.UVGC_222_95,(0,0,1):C.UVGC_222_96})

V_197 = CTVertex(name = 'V_197',
                 type = 'UV',
                 particles = [ P.vm, P.d, P.lqsd__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS4 ],
                 loop_particles = [ [ [P.d, P.g] ], [ [P.d, P.g, P.lqsd] ], [ [P.g, P.lqsd] ] ],
                 couplings = {(0,0,0):C.UVGC_225_100,(0,0,2):C.UVGC_224_98,(0,0,1):C.UVGC_224_99})

V_198 = CTVertex(name = 'V_198',
                 type = 'UV',
                 particles = [ P.vt, P.d, P.lqsd__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS4 ],
                 loop_particles = [ [ [P.d, P.g] ], [ [P.d, P.g, P.lqsd] ], [ [P.g, P.lqsd] ] ],
                 couplings = {(0,0,0):C.UVGC_227_103,(0,0,2):C.UVGC_226_101,(0,0,1):C.UVGC_226_102})

V_199 = CTVertex(name = 'V_199',
                 type = 'UV',
                 particles = [ P.ve, P.s, P.lqsd__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS4 ],
                 loop_particles = [ [ [P.g, P.lqsd] ], [ [P.g, P.lqsd, P.s] ], [ [P.g, P.s] ] ],
                 couplings = {(0,0,0):C.UVGC_236_128,(0,0,2):C.UVGC_237_130,(0,0,1):C.UVGC_236_129})

V_200 = CTVertex(name = 'V_200',
                 type = 'UV',
                 particles = [ P.vm, P.s, P.lqsd__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS4 ],
                 loop_particles = [ [ [P.g, P.lqsd] ], [ [P.g, P.lqsd, P.s] ], [ [P.g, P.s] ] ],
                 couplings = {(0,0,0):C.UVGC_238_131,(0,0,2):C.UVGC_239_133,(0,0,1):C.UVGC_238_132})

V_201 = CTVertex(name = 'V_201',
                 type = 'UV',
                 particles = [ P.vt, P.s, P.lqsd__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS4 ],
                 loop_particles = [ [ [P.g, P.lqsd] ], [ [P.g, P.lqsd, P.s] ], [ [P.g, P.s] ] ],
                 couplings = {(0,0,0):C.UVGC_240_134,(0,0,2):C.UVGC_241_136,(0,0,1):C.UVGC_240_135})

V_202 = CTVertex(name = 'V_202',
                 type = 'UV',
                 particles = [ P.c__tilde__, P.ve, P.lqsu ],
                 color = [ 'Identity(1,3)' ],
                 lorentz = [ L.FFS4 ],
                 loop_particles = [ [ [P.c, P.g] ], [ [P.c, P.g, P.lqsu] ], [ [P.g, P.lqsu] ] ],
                 couplings = {(0,0,0):C.UVGC_218_84,(0,0,2):C.UVGC_218_85,(0,0,1):C.UVGC_218_86})

V_203 = CTVertex(name = 'V_203',
                 type = 'UV',
                 particles = [ P.c__tilde__, P.vm, P.lqsu ],
                 color = [ 'Identity(1,3)' ],
                 lorentz = [ L.FFS4 ],
                 loop_particles = [ [ [P.c, P.g] ], [ [P.c, P.g, P.lqsu] ], [ [P.g, P.lqsu] ] ],
                 couplings = {(0,0,0):C.UVGC_219_87,(0,0,2):C.UVGC_219_88,(0,0,1):C.UVGC_219_89})

V_204 = CTVertex(name = 'V_204',
                 type = 'UV',
                 particles = [ P.c__tilde__, P.vt, P.lqsu ],
                 color = [ 'Identity(1,3)' ],
                 lorentz = [ L.FFS4 ],
                 loop_particles = [ [ [P.c, P.g] ], [ [P.c, P.g, P.lqsu] ], [ [P.g, P.lqsu] ] ],
                 couplings = {(0,0,0):C.UVGC_220_90,(0,0,2):C.UVGC_220_91,(0,0,1):C.UVGC_220_92})

V_205 = CTVertex(name = 'V_205',
                 type = 'UV',
                 particles = [ P.t__tilde__, P.ve, P.lqsu ],
                 color = [ 'Identity(1,3)' ],
                 lorentz = [ L.FFS4 ],
                 loop_particles = [ [ [P.g, P.lqsu] ], [ [P.g, P.lqsu, P.t] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_313_301,(0,0,2):C.UVGC_313_302,(0,0,1):C.UVGC_313_303})

V_206 = CTVertex(name = 'V_206',
                 type = 'UV',
                 particles = [ P.t__tilde__, P.vm, P.lqsu ],
                 color = [ 'Identity(1,3)' ],
                 lorentz = [ L.FFS4 ],
                 loop_particles = [ [ [P.g, P.lqsu] ], [ [P.g, P.lqsu, P.t] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_314_304,(0,0,2):C.UVGC_314_305,(0,0,1):C.UVGC_314_306})

V_207 = CTVertex(name = 'V_207',
                 type = 'UV',
                 particles = [ P.t__tilde__, P.vt, P.lqsu ],
                 color = [ 'Identity(1,3)' ],
                 lorentz = [ L.FFS4 ],
                 loop_particles = [ [ [P.g, P.lqsu] ], [ [P.g, P.lqsu, P.t] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_315_307,(0,0,2):C.UVGC_315_308,(0,0,1):C.UVGC_315_309})

V_208 = CTVertex(name = 'V_208',
                 type = 'UV',
                 particles = [ P.u__tilde__, P.ve, P.lqsu ],
                 color = [ 'Identity(1,3)' ],
                 lorentz = [ L.FFS4 ],
                 loop_particles = [ [ [P.g, P.lqsu] ], [ [P.g, P.lqsu, P.u] ], [ [P.g, P.u] ] ],
                 couplings = {(0,0,0):C.UVGC_267_185,(0,0,2):C.UVGC_267_186,(0,0,1):C.UVGC_267_187})

V_209 = CTVertex(name = 'V_209',
                 type = 'UV',
                 particles = [ P.u__tilde__, P.vm, P.lqsu ],
                 color = [ 'Identity(1,3)' ],
                 lorentz = [ L.FFS4 ],
                 loop_particles = [ [ [P.g, P.lqsu] ], [ [P.g, P.lqsu, P.u] ], [ [P.g, P.u] ] ],
                 couplings = {(0,0,0):C.UVGC_268_188,(0,0,2):C.UVGC_268_189,(0,0,1):C.UVGC_268_190})

V_210 = CTVertex(name = 'V_210',
                 type = 'UV',
                 particles = [ P.u__tilde__, P.vt, P.lqsu ],
                 color = [ 'Identity(1,3)' ],
                 lorentz = [ L.FFS4 ],
                 loop_particles = [ [ [P.g, P.lqsu] ], [ [P.g, P.lqsu, P.u] ], [ [P.g, P.u] ] ],
                 couplings = {(0,0,0):C.UVGC_269_191,(0,0,2):C.UVGC_269_192,(0,0,1):C.UVGC_269_193})

V_211 = CTVertex(name = 'V_211',
                 type = 'UV',
                 particles = [ P.g, P.lqsu__tilde__, P.lqsu ],
                 color = [ 'T(1,3,2)' ],
                 lorentz = [ L.VSS1, L.VSS2 ],
                 loop_particles = [ [ [P.b] ], [ [P.c], [P.d], [P.s], [P.u] ], [ [P.g] ], [ [P.ghG] ], [ [P.g, P.lqsu] ], [ [P.lqsd] ], [ [P.lqsu] ], [ [P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_186_40,(0,0,1):C.UVGC_186_41,(0,0,2):C.UVGC_186_42,(0,0,3):C.UVGC_186_43,(0,0,5):C.UVGC_186_44,(0,0,6):C.UVGC_186_45,(0,0,7):C.UVGC_186_46,(0,0,4):C.UVGC_192_58,(0,1,0):C.UVGC_174_24,(0,1,1):C.UVGC_174_25,(0,1,2):C.UVGC_174_26,(0,1,3):C.UVGC_174_27,(0,1,5):C.UVGC_174_28,(0,1,6):C.UVGC_174_29,(0,1,7):C.UVGC_174_30,(0,1,4):C.UVGC_191_57})

V_212 = CTVertex(name = 'V_212',
                 type = 'UV',
                 particles = [ P.g, P.g, P.lqsu__tilde__, P.lqsu ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'T(1,-1,3)*T(2,4,-1)', 'T(1,4,-1)*T(2,-1,3)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.b] ], [ [P.c], [P.d], [P.s], [P.u] ], [ [P.g] ], [ [P.ghG] ], [ [P.g, P.lqsu] ], [ [P.lqsd] ], [ [P.lqsu] ], [ [P.t] ] ],
                 couplings = {(2,0,0):C.UVGC_187_48,(2,0,1):C.UVGC_187_49,(2,0,2):C.UVGC_187_50,(2,0,3):C.UVGC_187_51,(2,0,5):C.UVGC_187_52,(2,0,6):C.UVGC_187_53,(2,0,7):C.UVGC_187_54,(2,0,4):C.UVGC_193_59,(1,0,0):C.UVGC_187_48,(1,0,1):C.UVGC_187_49,(1,0,2):C.UVGC_187_50,(1,0,3):C.UVGC_187_51,(1,0,5):C.UVGC_187_52,(1,0,6):C.UVGC_187_53,(1,0,7):C.UVGC_187_54,(1,0,4):C.UVGC_193_59,(0,0,2):C.UVGC_183_36,(0,0,4):C.UVGC_183_37})

V_213 = CTVertex(name = 'V_213',
                 type = 'UV',
                 particles = [ P.g, P.lqsd__tilde__, P.lqsd ],
                 color = [ 'T(1,3,2)' ],
                 lorentz = [ L.VSS1, L.VSS2 ],
                 loop_particles = [ [ [P.b] ], [ [P.c], [P.d], [P.s], [P.u] ], [ [P.g] ], [ [P.ghG] ], [ [P.g, P.lqsd] ], [ [P.lqsd] ], [ [P.lqsu] ], [ [P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_186_40,(0,0,1):C.UVGC_186_41,(0,0,2):C.UVGC_186_42,(0,0,3):C.UVGC_186_43,(0,0,5):C.UVGC_186_44,(0,0,6):C.UVGC_186_45,(0,0,7):C.UVGC_186_46,(0,0,4):C.UVGC_186_47,(0,1,0):C.UVGC_174_24,(0,1,1):C.UVGC_174_25,(0,1,2):C.UVGC_174_26,(0,1,3):C.UVGC_174_27,(0,1,5):C.UVGC_174_28,(0,1,6):C.UVGC_174_29,(0,1,7):C.UVGC_174_30,(0,1,4):C.UVGC_185_39})

V_214 = CTVertex(name = 'V_214',
                 type = 'UV',
                 particles = [ P.g, P.g, P.lqsd__tilde__, P.lqsd ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'T(1,-1,3)*T(2,4,-1)', 'T(1,4,-1)*T(2,-1,3)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.b] ], [ [P.c], [P.d], [P.s], [P.u] ], [ [P.g] ], [ [P.ghG] ], [ [P.g, P.lqsd] ], [ [P.lqsd] ], [ [P.lqsu] ], [ [P.t] ] ],
                 couplings = {(2,0,0):C.UVGC_187_48,(2,0,1):C.UVGC_187_49,(2,0,2):C.UVGC_187_50,(2,0,3):C.UVGC_187_51,(2,0,5):C.UVGC_187_52,(2,0,6):C.UVGC_187_53,(2,0,7):C.UVGC_187_54,(2,0,4):C.UVGC_187_55,(1,0,0):C.UVGC_187_48,(1,0,1):C.UVGC_187_49,(1,0,2):C.UVGC_187_50,(1,0,3):C.UVGC_187_51,(1,0,5):C.UVGC_187_52,(1,0,6):C.UVGC_187_53,(1,0,7):C.UVGC_187_54,(1,0,4):C.UVGC_187_55,(0,0,2):C.UVGC_183_36,(0,0,4):C.UVGC_183_37})

V_215 = CTVertex(name = 'V_215',
                 type = 'UV',
                 particles = [ P.u__tilde__, P.u, P.a ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV1, L.FFV2, L.FFV3 ],
                 loop_particles = [ [ [P.g, P.u] ] ],
                 couplings = {(0,0,0):C.UVGC_176_33,(0,1,0):C.UVGC_155_2,(0,2,0):C.UVGC_155_2})

V_216 = CTVertex(name = 'V_216',
                 type = 'UV',
                 particles = [ P.c__tilde__, P.c, P.a ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV1, L.FFV2, L.FFV3 ],
                 loop_particles = [ [ [P.c, P.g] ] ],
                 couplings = {(0,0,0):C.UVGC_176_33,(0,1,0):C.UVGC_155_2,(0,2,0):C.UVGC_155_2})

V_217 = CTVertex(name = 'V_217',
                 type = 'UV',
                 particles = [ P.t__tilde__, P.t, P.a ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV1, L.FFV2, L.FFV3 ],
                 loop_particles = [ [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_176_33,(0,1,0):C.UVGC_305_293,(0,2,0):C.UVGC_305_293})

V_218 = CTVertex(name = 'V_218',
                 type = 'UV',
                 particles = [ P.u__tilde__, P.u, P.g ],
                 color = [ 'T(3,2,1)' ],
                 lorentz = [ L.FFV1, L.FFV2, L.FFV3 ],
                 loop_particles = [ [ [P.b] ], [ [P.c], [P.d], [P.s], [P.u] ], [ [P.g] ], [ [P.ghG] ], [ [P.g, P.u] ], [ [P.lqsd] ], [ [P.lqsu] ], [ [P.t] ] ],
                 couplings = {(0,0,4):C.UVGC_174_31,(0,1,0):C.UVGC_174_24,(0,1,1):C.UVGC_174_25,(0,1,2):C.UVGC_174_26,(0,1,3):C.UVGC_174_27,(0,1,5):C.UVGC_174_28,(0,1,6):C.UVGC_174_29,(0,1,7):C.UVGC_174_30,(0,1,4):C.UVGC_158_4,(0,2,0):C.UVGC_174_24,(0,2,1):C.UVGC_174_25,(0,2,2):C.UVGC_174_26,(0,2,3):C.UVGC_174_27,(0,2,5):C.UVGC_174_28,(0,2,6):C.UVGC_174_29,(0,2,7):C.UVGC_174_30,(0,2,4):C.UVGC_158_4})

V_219 = CTVertex(name = 'V_219',
                 type = 'UV',
                 particles = [ P.c__tilde__, P.c, P.g ],
                 color = [ 'T(3,2,1)' ],
                 lorentz = [ L.FFV1, L.FFV2, L.FFV3 ],
                 loop_particles = [ [ [P.b] ], [ [P.c], [P.d], [P.s], [P.u] ], [ [P.c, P.g] ], [ [P.g] ], [ [P.ghG] ], [ [P.lqsd] ], [ [P.lqsu] ], [ [P.t] ] ],
                 couplings = {(0,0,2):C.UVGC_174_31,(0,1,0):C.UVGC_174_24,(0,1,1):C.UVGC_174_25,(0,1,3):C.UVGC_174_26,(0,1,4):C.UVGC_174_27,(0,1,5):C.UVGC_174_28,(0,1,6):C.UVGC_174_29,(0,1,7):C.UVGC_174_30,(0,1,2):C.UVGC_158_4,(0,2,0):C.UVGC_174_24,(0,2,1):C.UVGC_174_25,(0,2,3):C.UVGC_174_26,(0,2,4):C.UVGC_174_27,(0,2,5):C.UVGC_174_28,(0,2,6):C.UVGC_174_29,(0,2,7):C.UVGC_174_30,(0,2,2):C.UVGC_158_4})

V_220 = CTVertex(name = 'V_220',
                 type = 'UV',
                 particles = [ P.t__tilde__, P.t, P.g ],
                 color = [ 'T(3,2,1)' ],
                 lorentz = [ L.FFV1, L.FFV2, L.FFV3 ],
                 loop_particles = [ [ [P.b] ], [ [P.c], [P.d], [P.s], [P.u] ], [ [P.g] ], [ [P.ghG] ], [ [P.g, P.t] ], [ [P.lqsd] ], [ [P.lqsu] ], [ [P.t] ] ],
                 couplings = {(0,0,4):C.UVGC_174_31,(0,1,0):C.UVGC_174_24,(0,1,1):C.UVGC_174_25,(0,1,2):C.UVGC_174_26,(0,1,3):C.UVGC_174_27,(0,1,5):C.UVGC_174_28,(0,1,6):C.UVGC_174_29,(0,1,7):C.UVGC_174_30,(0,1,4):C.UVGC_306_294,(0,2,0):C.UVGC_174_24,(0,2,1):C.UVGC_174_25,(0,2,2):C.UVGC_174_26,(0,2,3):C.UVGC_174_27,(0,2,5):C.UVGC_174_28,(0,2,6):C.UVGC_174_29,(0,2,7):C.UVGC_174_30,(0,2,4):C.UVGC_306_294})

V_221 = CTVertex(name = 'V_221',
                 type = 'UV',
                 particles = [ P.d__tilde__, P.u, P.W__minus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV2 ],
                 loop_particles = [ [ [P.d, P.g], [P.g, P.u] ], [ [P.d, P.g, P.u] ] ],
                 couplings = {(0,0,0):C.UVGC_221_93,(0,0,1):C.UVGC_221_94})

V_222 = CTVertex(name = 'V_222',
                 type = 'UV',
                 particles = [ P.s__tilde__, P.c, P.W__minus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV2 ],
                 loop_particles = [ [ [P.c, P.g], [P.g, P.s] ], [ [P.c, P.g, P.s] ] ],
                 couplings = {(0,0,0):C.UVGC_221_93,(0,0,1):C.UVGC_221_94})

V_223 = CTVertex(name = 'V_223',
                 type = 'UV',
                 particles = [ P.b__tilde__, P.t, P.W__minus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV2 ],
                 loop_particles = [ [ [P.b, P.g] ], [ [P.b, P.g, P.t] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_317_311,(0,0,2):C.UVGC_317_312,(0,0,1):C.UVGC_221_94})

V_224 = CTVertex(name = 'V_224',
                 type = 'UV',
                 particles = [ P.t__tilde__, P.t, P.Z ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV2, L.FFV3 ],
                 loop_particles = [ [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_318_313,(0,1,0):C.UVGC_319_314})

V_225 = CTVertex(name = 'V_225',
                 type = 'UV',
                 particles = [ P.d__tilde__, P.d, P.a ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV1, L.FFV2, L.FFV3 ],
                 loop_particles = [ [ [P.d, P.g] ] ],
                 couplings = {(0,0,0):C.UVGC_173_23,(0,1,0):C.UVGC_157_3,(0,2,0):C.UVGC_157_3})

V_226 = CTVertex(name = 'V_226',
                 type = 'UV',
                 particles = [ P.s__tilde__, P.s, P.a ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV1, L.FFV2, L.FFV3 ],
                 loop_particles = [ [ [P.g, P.s] ] ],
                 couplings = {(0,0,0):C.UVGC_173_23,(0,1,0):C.UVGC_157_3,(0,2,0):C.UVGC_157_3})

V_227 = CTVertex(name = 'V_227',
                 type = 'UV',
                 particles = [ P.b__tilde__, P.b, P.a ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV1, L.FFV2, L.FFV3 ],
                 loop_particles = [ [ [P.b, P.g] ] ],
                 couplings = {(0,0,0):C.UVGC_173_23,(0,1,0):C.UVGC_271_195,(0,2,0):C.UVGC_271_195})

V_228 = CTVertex(name = 'V_228',
                 type = 'UV',
                 particles = [ P.d__tilde__, P.d, P.g ],
                 color = [ 'T(3,2,1)' ],
                 lorentz = [ L.FFV1, L.FFV2, L.FFV3 ],
                 loop_particles = [ [ [P.b] ], [ [P.c], [P.d], [P.s], [P.u] ], [ [P.d, P.g] ], [ [P.g] ], [ [P.ghG] ], [ [P.lqsd] ], [ [P.lqsu] ], [ [P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_174_24,(0,0,1):C.UVGC_174_25,(0,0,3):C.UVGC_174_26,(0,0,4):C.UVGC_174_27,(0,0,5):C.UVGC_174_28,(0,0,6):C.UVGC_174_29,(0,0,7):C.UVGC_174_30,(0,0,2):C.UVGC_174_31,(0,1,2):C.UVGC_158_4,(0,2,2):C.UVGC_158_4})

V_229 = CTVertex(name = 'V_229',
                 type = 'UV',
                 particles = [ P.s__tilde__, P.s, P.g ],
                 color = [ 'T(3,2,1)' ],
                 lorentz = [ L.FFV1, L.FFV2, L.FFV3 ],
                 loop_particles = [ [ [P.b] ], [ [P.c], [P.d], [P.s], [P.u] ], [ [P.g] ], [ [P.ghG] ], [ [P.g, P.s] ], [ [P.lqsd] ], [ [P.lqsu] ], [ [P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_174_24,(0,0,1):C.UVGC_174_25,(0,0,2):C.UVGC_174_26,(0,0,3):C.UVGC_174_27,(0,0,5):C.UVGC_174_28,(0,0,6):C.UVGC_174_29,(0,0,7):C.UVGC_174_30,(0,0,4):C.UVGC_174_31,(0,1,4):C.UVGC_158_4,(0,2,4):C.UVGC_158_4})

V_230 = CTVertex(name = 'V_230',
                 type = 'UV',
                 particles = [ P.b__tilde__, P.b, P.g ],
                 color = [ 'T(3,2,1)' ],
                 lorentz = [ L.FFV1, L.FFV2, L.FFV3 ],
                 loop_particles = [ [ [P.b] ], [ [P.b, P.g] ], [ [P.c], [P.d], [P.s], [P.u] ], [ [P.g] ], [ [P.ghG] ], [ [P.lqsd] ], [ [P.lqsu] ], [ [P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_174_24,(0,0,2):C.UVGC_174_25,(0,0,3):C.UVGC_174_26,(0,0,4):C.UVGC_174_27,(0,0,5):C.UVGC_174_28,(0,0,6):C.UVGC_174_29,(0,0,7):C.UVGC_174_30,(0,0,1):C.UVGC_174_31,(0,1,1):C.UVGC_272_196,(0,2,1):C.UVGC_272_196})

V_231 = CTVertex(name = 'V_231',
                 type = 'UV',
                 particles = [ P.u__tilde__, P.d, P.W__plus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV2 ],
                 loop_particles = [ [ [P.d, P.g], [P.g, P.u] ], [ [P.d, P.g, P.u] ] ],
                 couplings = {(0,0,0):C.UVGC_221_93,(0,0,1):C.UVGC_221_94})

V_232 = CTVertex(name = 'V_232',
                 type = 'UV',
                 particles = [ P.c__tilde__, P.s, P.W__plus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV2 ],
                 loop_particles = [ [ [P.c, P.g], [P.g, P.s] ], [ [P.c, P.g, P.s] ] ],
                 couplings = {(0,0,0):C.UVGC_221_93,(0,0,1):C.UVGC_221_94})

V_233 = CTVertex(name = 'V_233',
                 type = 'UV',
                 particles = [ P.t__tilde__, P.b, P.W__plus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV2 ],
                 loop_particles = [ [ [P.b, P.g] ], [ [P.b, P.g, P.t] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_317_311,(0,0,2):C.UVGC_317_312,(0,0,1):C.UVGC_221_94})

V_234 = CTVertex(name = 'V_234',
                 type = 'UV',
                 particles = [ P.b__tilde__, P.b, P.Z ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV2, L.FFV3 ],
                 loop_particles = [ [ [P.b, P.g] ] ],
                 couplings = {(0,0,0):C.UVGC_284_220,(0,1,0):C.UVGC_285_221})

V_235 = CTVertex(name = 'V_235',
                 type = 'UV',
                 particles = [ P.u__tilde__, P.u ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FF1, L.FF4, L.FF6 ],
                 loop_particles = [ [ [P.g, P.u] ] ],
                 couplings = {(0,0,0):C.UVGC_175_32,(0,1,0):C.UVGC_154_1,(0,2,0):C.UVGC_154_1})

V_236 = CTVertex(name = 'V_236',
                 type = 'UV',
                 particles = [ P.c__tilde__, P.c ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FF1, L.FF4, L.FF6 ],
                 loop_particles = [ [ [P.c, P.g] ] ],
                 couplings = {(0,0,0):C.UVGC_175_32,(0,1,0):C.UVGC_154_1,(0,2,0):C.UVGC_154_1})

V_237 = CTVertex(name = 'V_237',
                 type = 'UV',
                 particles = [ P.t__tilde__, P.t ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FF3, L.FF4, L.FF5, L.FF6 ],
                 loop_particles = [ [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_316_310,(0,2,0):C.UVGC_316_310,(0,1,0):C.UVGC_304_292,(0,3,0):C.UVGC_304_292})

V_238 = CTVertex(name = 'V_238',
                 type = 'UV',
                 particles = [ P.d__tilde__, P.d ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FF1, L.FF4, L.FF6 ],
                 loop_particles = [ [ [P.d, P.g] ] ],
                 couplings = {(0,0,0):C.UVGC_175_32,(0,1,0):C.UVGC_154_1,(0,2,0):C.UVGC_154_1})

V_239 = CTVertex(name = 'V_239',
                 type = 'UV',
                 particles = [ P.s__tilde__, P.s ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FF1, L.FF4, L.FF6 ],
                 loop_particles = [ [ [P.g, P.s] ] ],
                 couplings = {(0,0,0):C.UVGC_175_32,(0,1,0):C.UVGC_154_1,(0,2,0):C.UVGC_154_1})

V_240 = CTVertex(name = 'V_240',
                 type = 'UV',
                 particles = [ P.b__tilde__, P.b ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FF2, L.FF3, L.FF4, L.FF5, L.FF6 ],
                 loop_particles = [ [ [P.b, P.g] ] ],
                 couplings = {(0,1,0):C.UVGC_283_219,(0,3,0):C.UVGC_283_219,(0,2,0):C.UVGC_270_194,(0,4,0):C.UVGC_270_194,(0,0,0):C.UVGC_282_218})

V_241 = CTVertex(name = 'V_241',
                 type = 'UV',
                 particles = [ P.lqsu__tilde__, P.lqsu ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.SS1, L.SS2 ],
                 loop_particles = [ [ [P.g, P.lqsu] ] ],
                 couplings = {(0,0,0):C.UVGC_289_225,(0,1,0):C.UVGC_190_56})

V_242 = CTVertex(name = 'V_242',
                 type = 'UV',
                 particles = [ P.lqsd__tilde__, P.lqsd ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.SS1, L.SS2 ],
                 loop_particles = [ [ [P.g, P.lqsd] ] ],
                 couplings = {(0,0,0):C.UVGC_288_224,(0,1,0):C.UVGC_184_38})

V_243 = CTVertex(name = 'V_243',
                 type = 'UV',
                 particles = [ P.g, P.g ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.VV1, L.VV3 ],
                 loop_particles = [ [ [P.b] ], [ [P.g] ], [ [P.ghG] ], [ [P.lqsd] ], [ [P.lqsu] ], [ [P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_291_230,(0,0,1):C.UVGC_291_231,(0,0,2):C.UVGC_291_232,(0,0,3):C.UVGC_291_233,(0,0,4):C.UVGC_291_234,(0,0,5):C.UVGC_291_235,(0,1,0):C.UVGC_290_226,(0,1,3):C.UVGC_290_227,(0,1,4):C.UVGC_290_228,(0,1,5):C.UVGC_290_229})

V_244 = CTVertex(name = 'V_244',
                 type = 'UV',
                 particles = [ P.lqsu__tilde__, P.lqsu__tilde__, P.lqsu, P.lqsu ],
                 color = [ 'Identity(1,3)*Identity(2,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.g] ], [ [P.g, P.lqsu] ] ],
                 couplings = {(1,0,0):C.UVGC_182_34,(1,0,1):C.UVGC_182_35,(0,0,0):C.UVGC_182_34,(0,0,1):C.UVGC_182_35})

V_245 = CTVertex(name = 'V_245',
                 type = 'UV',
                 particles = [ P.lqsd__tilde__, P.lqsd, P.lqsu__tilde__, P.lqsu ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.g] ], [ [P.g, P.lqsd], [P.g, P.lqsu] ], [ [P.g, P.lqsd, P.lqsu] ] ],
                 couplings = {(1,0,0):C.UVGC_235_125,(1,0,1):C.UVGC_235_126,(1,0,2):C.UVGC_235_127,(0,0,0):C.UVGC_234_122,(0,0,1):C.UVGC_234_123,(0,0,2):C.UVGC_234_124})

V_246 = CTVertex(name = 'V_246',
                 type = 'UV',
                 particles = [ P.lqsd__tilde__, P.lqsd__tilde__, P.lqsd, P.lqsd ],
                 color = [ 'Identity(1,3)*Identity(2,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.g] ], [ [P.g, P.lqsd] ] ],
                 couplings = {(1,0,0):C.UVGC_182_34,(1,0,1):C.UVGC_182_35,(0,0,0):C.UVGC_182_34,(0,0,1):C.UVGC_182_35})

