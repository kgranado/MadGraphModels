# This file was automatically created by FeynRules 2.3.43
# Mathematica version: 11.1.0 for Linux x86 (64-bit) (March 13, 2017)
# Date: Fri 25 Mar 2022 17:47:15



from object_library import all_parameters, Parameter


from function_library import complexconjugate, re, im, csc, sec, acsc, asec, cot

# This is a default parameter object representing 0.
ZERO = Parameter(name = 'ZERO',
                 nature = 'internal',
                 type = 'real',
                 value = '0.0',
                 texname = '0')

# User-defined parameters.
cabi = Parameter(name = 'cabi',
                 nature = 'external',
                 type = 'real',
                 value = 0.227736,
                 texname = '\\theta _c',
                 lhablock = 'CKMBLOCK',
                 lhacode = [ 1 ])

vS = Parameter(name = 'vS',
               nature = 'external',
               type = 'real',
               value = 1000.,
               texname = 'v_S',
               lhablock = 'S2HDMBLOCK',
               lhacode = [ 1 ])

al1 = Parameter(name = 'al1',
                nature = 'external',
                type = 'real',
                value = 1.,
                texname = '\\alpha _1',
                lhablock = 'S2HDMBLOCK',
                lhacode = [ 2 ])

al2 = Parameter(name = 'al2',
                nature = 'external',
                type = 'real',
                value = 1.,
                texname = '\\alpha _2',
                lhablock = 'S2HDMBLOCK',
                lhacode = [ 3 ])

al3 = Parameter(name = 'al3',
                nature = 'external',
                type = 'real',
                value = 1.,
                texname = '\\alpha _3',
                lhablock = 'S2HDMBLOCK',
                lhacode = [ 4 ])

TB = Parameter(name = 'TB',
               nature = 'external',
               type = 'real',
               value = 2.,
               texname = 't_{\\beta }',
               lhablock = 'S2HDMBLOCK',
               lhacode = [ 5 ])

m12sq = Parameter(name = 'm12sq',
                  nature = 'external',
                  type = 'real',
                  value = 60000.,
                  texname = 'm_{12}{}^2',
                  lhablock = 'S2HDMBLOCK',
                  lhacode = [ 6 ])

aEWM1 = Parameter(name = 'aEWM1',
                  nature = 'external',
                  type = 'real',
                  value = 127.9,
                  texname = '\\text{aEWM1}',
                  lhablock = 'SMINPUTS',
                  lhacode = [ 1 ])

Gf = Parameter(name = 'Gf',
               nature = 'external',
               type = 'real',
               value = 0.0000116637,
               texname = 'G_f',
               lhablock = 'SMINPUTS',
               lhacode = [ 2 ])

aS = Parameter(name = 'aS',
               nature = 'external',
               type = 'real',
               value = 0.1184,
               texname = '\\alpha _s',
               lhablock = 'SMINPUTS',
               lhacode = [ 3 ])

ymdo = Parameter(name = 'ymdo',
                 nature = 'external',
                 type = 'real',
                 value = 0.00504,
                 texname = '\\text{ymdo}',
                 lhablock = 'YUKAWA',
                 lhacode = [ 1 ])

ymup = Parameter(name = 'ymup',
                 nature = 'external',
                 type = 'real',
                 value = 0.00255,
                 texname = '\\text{ymup}',
                 lhablock = 'YUKAWA',
                 lhacode = [ 2 ])

yms = Parameter(name = 'yms',
                nature = 'external',
                type = 'real',
                value = 0.101,
                texname = '\\text{yms}',
                lhablock = 'YUKAWA',
                lhacode = [ 3 ])

ymc = Parameter(name = 'ymc',
                nature = 'external',
                type = 'real',
                value = 1.27,
                texname = '\\text{ymc}',
                lhablock = 'YUKAWA',
                lhacode = [ 4 ])

ymb = Parameter(name = 'ymb',
                nature = 'external',
                type = 'real',
                value = 4.7,
                texname = '\\text{ymb}',
                lhablock = 'YUKAWA',
                lhacode = [ 5 ])

ymt = Parameter(name = 'ymt',
                nature = 'external',
                type = 'real',
                value = 172,
                texname = '\\text{ymt}',
                lhablock = 'YUKAWA',
                lhacode = [ 6 ])

yme = Parameter(name = 'yme',
                nature = 'external',
                type = 'real',
                value = 0.000511,
                texname = '\\text{yme}',
                lhablock = 'YUKAWA',
                lhacode = [ 11 ])

ymm = Parameter(name = 'ymm',
                nature = 'external',
                type = 'real',
                value = 0.10566,
                texname = '\\text{ymm}',
                lhablock = 'YUKAWA',
                lhacode = [ 13 ])

ymtau = Parameter(name = 'ymtau',
                  nature = 'external',
                  type = 'real',
                  value = 1.777,
                  texname = '\\text{ymtau}',
                  lhablock = 'YUKAWA',
                  lhacode = [ 15 ])

Mnue = Parameter(name = 'Mnue',
                 nature = 'external',
                 type = 'real',
                 value = 0.,
                 texname = '\\text{Mnue}',
                 lhablock = 'MASS',
                 lhacode = [ 12 ])

Mnum = Parameter(name = 'Mnum',
                 nature = 'external',
                 type = 'real',
                 value = 0.,
                 texname = '\\text{Mnum}',
                 lhablock = 'MASS',
                 lhacode = [ 14 ])

Mnut = Parameter(name = 'Mnut',
                 nature = 'external',
                 type = 'real',
                 value = 0.,
                 texname = '\\text{Mnut}',
                 lhablock = 'MASS',
                 lhacode = [ 16 ])

Me = Parameter(name = 'Me',
               nature = 'external',
               type = 'real',
               value = 0.000511,
               texname = '\\text{Me}',
               lhablock = 'MASS',
               lhacode = [ 11 ])

MMU = Parameter(name = 'MMU',
                nature = 'external',
                type = 'real',
                value = 0.10566,
                texname = '\\text{MMU}',
                lhablock = 'MASS',
                lhacode = [ 13 ])

MTA = Parameter(name = 'MTA',
                nature = 'external',
                type = 'real',
                value = 1.777,
                texname = '\\text{MTA}',
                lhablock = 'MASS',
                lhacode = [ 15 ])

MU = Parameter(name = 'MU',
               nature = 'external',
               type = 'real',
               value = 0.00255,
               texname = 'M',
               lhablock = 'MASS',
               lhacode = [ 2 ])

MC = Parameter(name = 'MC',
               nature = 'external',
               type = 'real',
               value = 1.27,
               texname = '\\text{MC}',
               lhablock = 'MASS',
               lhacode = [ 4 ])

MT = Parameter(name = 'MT',
               nature = 'external',
               type = 'real',
               value = 172.,
               texname = '\\text{MT}',
               lhablock = 'MASS',
               lhacode = [ 6 ])

MD = Parameter(name = 'MD',
               nature = 'external',
               type = 'real',
               value = 0.00504,
               texname = '\\text{MD}',
               lhablock = 'MASS',
               lhacode = [ 1 ])

MS = Parameter(name = 'MS',
               nature = 'external',
               type = 'real',
               value = 0.101,
               texname = '\\text{MS}',
               lhablock = 'MASS',
               lhacode = [ 3 ])

MB = Parameter(name = 'MB',
               nature = 'external',
               type = 'real',
               value = 4.7,
               texname = '\\text{MB}',
               lhablock = 'MASS',
               lhacode = [ 5 ])

MZ = Parameter(name = 'MZ',
               nature = 'external',
               type = 'real',
               value = 91.1876,
               texname = '\\text{MZ}',
               lhablock = 'MASS',
               lhacode = [ 23 ])

Mh1 = Parameter(name = 'Mh1',
                nature = 'external',
                type = 'real',
                value = 96.,
                texname = '\\text{Mh1}',
                lhablock = 'MASS',
                lhacode = [ 25 ])

Mh2 = Parameter(name = 'Mh2',
                nature = 'external',
                type = 'real',
                value = 125.,
                texname = '\\text{Mh2}',
                lhablock = 'MASS',
                lhacode = [ 29 ])

Mh3 = Parameter(name = 'Mh3',
                nature = 'external',
                type = 'real',
                value = 650.,
                texname = '\\text{Mh3}',
                lhablock = 'MASS',
                lhacode = [ 36 ])

MA0 = Parameter(name = 'MA0',
                nature = 'external',
                type = 'real',
                value = 450.,
                texname = '\\text{MA0}',
                lhablock = 'MASS',
                lhacode = [ 38 ])

MHp = Parameter(name = 'MHp',
                nature = 'external',
                type = 'real',
                value = 650.,
                texname = '\\text{MHp}',
                lhablock = 'MASS',
                lhacode = [ 37 ])

MXi = Parameter(name = 'MXi',
                nature = 'external',
                type = 'real',
                value = 65.,
                texname = '\\text{MXi}',
                lhablock = 'MASS',
                lhacode = [ 30 ])

WT = Parameter(name = 'WT',
               nature = 'external',
               type = 'real',
               value = 1.50833649,
               texname = '\\text{WT}',
               lhablock = 'DECAY',
               lhacode = [ 6 ])

WZ = Parameter(name = 'WZ',
               nature = 'external',
               type = 'real',
               value = 2.4952,
               texname = '\\text{WZ}',
               lhablock = 'DECAY',
               lhacode = [ 23 ])

WW = Parameter(name = 'WW',
               nature = 'external',
               type = 'real',
               value = 2.085,
               texname = '\\text{WW}',
               lhablock = 'DECAY',
               lhacode = [ 24 ])

Wh1 = Parameter(name = 'Wh1',
                nature = 'external',
                type = 'real',
                value = 0.00407,
                texname = '\\text{Wh1}',
                lhablock = 'DECAY',
                lhacode = [ 25 ])

Wh2 = Parameter(name = 'Wh2',
                nature = 'external',
                type = 'real',
                value = 10.85,
                texname = '\\text{Wh2}',
                lhablock = 'DECAY',
                lhacode = [ 29 ])

Wh3 = Parameter(name = 'Wh3',
                nature = 'external',
                type = 'real',
                value = 20.85,
                texname = '\\text{Wh3}',
                lhablock = 'DECAY',
                lhacode = [ 36 ])

WA0 = Parameter(name = 'WA0',
                nature = 'external',
                type = 'real',
                value = 20.85,
                texname = '\\text{WA0}',
                lhablock = 'DECAY',
                lhacode = [ 38 ])

WHp = Parameter(name = 'WHp',
                nature = 'external',
                type = 'real',
                value = 20.85,
                texname = '\\text{WHp}',
                lhablock = 'DECAY',
                lhacode = [ 37 ])

aEW = Parameter(name = 'aEW',
                nature = 'internal',
                type = 'real',
                value = '1/aEWM1',
                texname = '\\alpha _{\\text{EW}}')

G = Parameter(name = 'G',
              nature = 'internal',
              type = 'real',
              value = '2*cmath.sqrt(aS)*cmath.sqrt(cmath.pi)',
              texname = 'G')

CB = Parameter(name = 'CB',
               nature = 'internal',
               type = 'real',
               value = '1/cmath.sqrt(1 + TB**2)',
               texname = 'c_{\\beta }')

SB = Parameter(name = 'SB',
               nature = 'internal',
               type = 'real',
               value = 'TB/cmath.sqrt(1 + TB**2)',
               texname = 's_{\\beta }')

CKM1x1 = Parameter(name = 'CKM1x1',
                   nature = 'internal',
                   type = 'complex',
                   value = 'cmath.cos(cabi)',
                   texname = '\\text{CKM1x1}')

CKM1x2 = Parameter(name = 'CKM1x2',
                   nature = 'internal',
                   type = 'complex',
                   value = 'cmath.sin(cabi)',
                   texname = '\\text{CKM1x2}')

CKM1x3 = Parameter(name = 'CKM1x3',
                   nature = 'internal',
                   type = 'complex',
                   value = '0',
                   texname = '\\text{CKM1x3}')

CKM2x1 = Parameter(name = 'CKM2x1',
                   nature = 'internal',
                   type = 'complex',
                   value = '-cmath.sin(cabi)',
                   texname = '\\text{CKM2x1}')

CKM2x2 = Parameter(name = 'CKM2x2',
                   nature = 'internal',
                   type = 'complex',
                   value = 'cmath.cos(cabi)',
                   texname = '\\text{CKM2x2}')

CKM2x3 = Parameter(name = 'CKM2x3',
                   nature = 'internal',
                   type = 'complex',
                   value = '0',
                   texname = '\\text{CKM2x3}')

CKM3x1 = Parameter(name = 'CKM3x1',
                   nature = 'internal',
                   type = 'complex',
                   value = '0',
                   texname = '\\text{CKM3x1}')

CKM3x2 = Parameter(name = 'CKM3x2',
                   nature = 'internal',
                   type = 'complex',
                   value = '0',
                   texname = '\\text{CKM3x2}')

CKM3x3 = Parameter(name = 'CKM3x3',
                   nature = 'internal',
                   type = 'complex',
                   value = '1',
                   texname = '\\text{CKM3x3}')

RA1x1 = Parameter(name = 'RA1x1',
                  nature = 'internal',
                  type = 'real',
                  value = 'cmath.cos(al1)*cmath.cos(al2)',
                  texname = '\\text{RA1x1}')

RA1x2 = Parameter(name = 'RA1x2',
                  nature = 'internal',
                  type = 'real',
                  value = 'cmath.cos(al2)*cmath.sin(al1)',
                  texname = '\\text{RA1x2}')

RA1x3 = Parameter(name = 'RA1x3',
                  nature = 'internal',
                  type = 'real',
                  value = 'cmath.sin(al2)',
                  texname = '\\text{RA1x3}')

RA2x1 = Parameter(name = 'RA2x1',
                  nature = 'internal',
                  type = 'real',
                  value = '-(cmath.cos(al3)*cmath.sin(al1)) - cmath.cos(al1)*cmath.sin(al2)*cmath.sin(al3)',
                  texname = '\\text{RA2x1}')

RA2x2 = Parameter(name = 'RA2x2',
                  nature = 'internal',
                  type = 'real',
                  value = 'cmath.cos(al1)*cmath.cos(al3) - cmath.sin(al1)*cmath.sin(al2)*cmath.sin(al3)',
                  texname = '\\text{RA2x2}')

RA2x3 = Parameter(name = 'RA2x3',
                  nature = 'internal',
                  type = 'real',
                  value = 'cmath.cos(al2)*cmath.sin(al3)',
                  texname = '\\text{RA2x3}')

RA3x1 = Parameter(name = 'RA3x1',
                  nature = 'internal',
                  type = 'real',
                  value = '-(cmath.cos(al1)*cmath.cos(al3)*cmath.sin(al2)) + cmath.sin(al1)*cmath.sin(al3)',
                  texname = '\\text{RA3x1}')

RA3x2 = Parameter(name = 'RA3x2',
                  nature = 'internal',
                  type = 'real',
                  value = '-(cmath.cos(al3)*cmath.sin(al1)*cmath.sin(al2)) - cmath.cos(al1)*cmath.sin(al3)',
                  texname = '\\text{RA3x2}')

RA3x3 = Parameter(name = 'RA3x3',
                  nature = 'internal',
                  type = 'real',
                  value = 'cmath.cos(al2)*cmath.cos(al3)',
                  texname = '\\text{RA3x3}')

mXsq = Parameter(name = 'mXsq',
                 nature = 'internal',
                 type = 'real',
                 value = 'MXi**2',
                 texname = 'm_{\\chi }{}^2')

musq = Parameter(name = 'musq',
                 nature = 'internal',
                 type = 'real',
                 value = 'm12sq/(CB*SB)',
                 texname = '\\mu ^2')

MW = Parameter(name = 'MW',
               nature = 'internal',
               type = 'real',
               value = 'cmath.sqrt(MZ**2/2. + cmath.sqrt(MZ**4/4. - (aEW*cmath.pi*MZ**2)/(Gf*cmath.sqrt(2))))',
               texname = 'm_W')

ee = Parameter(name = 'ee',
               nature = 'internal',
               type = 'real',
               value = '2*cmath.sqrt(aEW)*cmath.sqrt(cmath.pi)',
               texname = 'e')

lam6 = Parameter(name = 'lam6',
                 nature = 'internal',
                 type = 'real',
                 value = '(Mh1**2*RA1x3**2 + Mh2**2*RA2x3**2 + Mh3**2*RA3x3**2)/vS**2',
                 texname = '\\lambda _6')

CW2 = Parameter(name = 'CW2',
                nature = 'internal',
                type = 'real',
                value = 'MW**2/MZ**2',
                texname = '\\text{CW2}')

CW = Parameter(name = 'CW',
               nature = 'internal',
               type = 'real',
               value = 'cmath.sqrt(CW2)',
               texname = 'c_w')

SW = Parameter(name = 'SW',
               nature = 'internal',
               type = 'real',
               value = 'cmath.sqrt(1 - CW2)',
               texname = 's_w')

SW2 = Parameter(name = 'SW2',
                nature = 'internal',
                type = 'real',
                value = '1 - CW2',
                texname = '\\text{SW2}')

g1 = Parameter(name = 'g1',
               nature = 'internal',
               type = 'real',
               value = 'ee/CW',
               texname = 'g_1')

gw = Parameter(name = 'gw',
               nature = 'internal',
               type = 'real',
               value = 'ee/SW',
               texname = 'g_w')

vH = Parameter(name = 'vH',
               nature = 'internal',
               type = 'real',
               value = '(2*MW*SW)/ee',
               texname = 'v_H')

lam1 = Parameter(name = 'lam1',
                 nature = 'internal',
                 type = 'real',
                 value = '(Mh1**2*RA1x1**2 + Mh2**2*RA2x1**2 + Mh3**2*RA3x1**2 - musq*SB**2)/(CB**2*vH**2)',
                 texname = '\\lambda _1')

lam2 = Parameter(name = 'lam2',
                 nature = 'internal',
                 type = 'real',
                 value = '(-(CB**2*musq) + Mh1**2*RA1x2**2 + Mh2**2*RA2x2**2 + Mh3**2*RA3x2**2)/(SB**2*vH**2)',
                 texname = '\\lambda _2')

lam3 = Parameter(name = 'lam3',
                 nature = 'internal',
                 type = 'real',
                 value = '(2*MHp**2 - musq + (Mh1**2*RA1x1*RA1x2 + Mh2**2*RA2x1*RA2x2 + Mh3**2*RA3x1*RA3x2)/(CB*SB))/vH**2',
                 texname = '\\lambda _3')

lam4 = Parameter(name = 'lam4',
                 nature = 'internal',
                 type = 'real',
                 value = '(MA0**2 - 2*MHp**2 + musq)/vH**2',
                 texname = '\\lambda _4')

lam5 = Parameter(name = 'lam5',
                 nature = 'internal',
                 type = 'real',
                 value = '(-MA0**2 + musq)/vH**2',
                 texname = '\\lambda _5')

lam7 = Parameter(name = 'lam7',
                 nature = 'internal',
                 type = 'real',
                 value = '(Mh1**2*RA1x1*RA1x3 + Mh2**2*RA2x1*RA2x3 + Mh3**2*RA3x1*RA3x3)/(CB*vH*vS)',
                 texname = '\\lambda _7')

lam8 = Parameter(name = 'lam8',
                 nature = 'internal',
                 type = 'real',
                 value = '(Mh1**2*RA1x2*RA1x3 + Mh2**2*RA2x2*RA2x3 + Mh3**2*RA3x2*RA3x3)/(SB*vH*vS)',
                 texname = '\\lambda _8')

v1 = Parameter(name = 'v1',
               nature = 'internal',
               type = 'real',
               value = 'CB*vH',
               texname = 'v_1')

v2 = Parameter(name = 'v2',
               nature = 'internal',
               type = 'real',
               value = 'SB*vH',
               texname = 'v_2')

m11sq = Parameter(name = 'm11sq',
                  nature = 'internal',
                  type = 'real',
                  value = 'm12sq*TB + (-(CB**2*lam1*vH**2) - (lam3 + lam4 + lam5)*SB**2*vH**2 - lam7*vS**2)/2.',
                  texname = 'm_{11}{}^2')

m22sq = Parameter(name = 'm22sq',
                  nature = 'internal',
                  type = 'real',
                  value = 'm12sq/TB + (-(CB**2*(lam3 + lam4 + lam5)*vH**2) - lam2*SB**2*vH**2 - lam8*vS**2)/2.',
                  texname = 'm_{22}{}^2')

mSsq = Parameter(name = 'mSsq',
                 nature = 'internal',
                 type = 'real',
                 value = 'MXi**2 - CB**2*lam7*vH**2 - lam8*SB**2*vH**2 - lam6*vS**2',
                 texname = 'm_S{}^2')

yb = Parameter(name = 'yb',
               nature = 'internal',
               type = 'real',
               value = '(ymb*cmath.sqrt(2))/v1',
               texname = '\\text{yb}')

yc = Parameter(name = 'yc',
               nature = 'internal',
               type = 'real',
               value = '(ymc*cmath.sqrt(2))/v2',
               texname = '\\text{yc}')

ydo = Parameter(name = 'ydo',
                nature = 'internal',
                type = 'real',
                value = '(ymdo*cmath.sqrt(2))/v1',
                texname = '\\text{ydo}')

ye = Parameter(name = 'ye',
               nature = 'internal',
               type = 'real',
               value = '(yme*cmath.sqrt(2))/v2',
               texname = '\\text{ye}')

ym = Parameter(name = 'ym',
               nature = 'internal',
               type = 'real',
               value = '(ymm*cmath.sqrt(2))/v2',
               texname = '\\text{ym}')

ys = Parameter(name = 'ys',
               nature = 'internal',
               type = 'real',
               value = '(yms*cmath.sqrt(2))/v1',
               texname = '\\text{ys}')

yt = Parameter(name = 'yt',
               nature = 'internal',
               type = 'real',
               value = '(ymt*cmath.sqrt(2))/v2',
               texname = '\\text{yt}')

ytau = Parameter(name = 'ytau',
                 nature = 'internal',
                 type = 'real',
                 value = '(ymtau*cmath.sqrt(2))/v2',
                 texname = '\\text{ytau}')

yup = Parameter(name = 'yup',
                nature = 'internal',
                type = 'real',
                value = '(ymup*cmath.sqrt(2))/v2',
                texname = '\\text{yup}')

I1a11 = Parameter(name = 'I1a11',
                  nature = 'internal',
                  type = 'complex',
                  value = 'ydo*complexconjugate(CKM1x1)',
                  texname = '\\text{I1a11}')

I1a12 = Parameter(name = 'I1a12',
                  nature = 'internal',
                  type = 'complex',
                  value = 'ydo*complexconjugate(CKM2x1)',
                  texname = '\\text{I1a12}')

I1a13 = Parameter(name = 'I1a13',
                  nature = 'internal',
                  type = 'complex',
                  value = 'ydo*complexconjugate(CKM3x1)',
                  texname = '\\text{I1a13}')

I1a21 = Parameter(name = 'I1a21',
                  nature = 'internal',
                  type = 'complex',
                  value = 'ys*complexconjugate(CKM1x2)',
                  texname = '\\text{I1a21}')

I1a22 = Parameter(name = 'I1a22',
                  nature = 'internal',
                  type = 'complex',
                  value = 'ys*complexconjugate(CKM2x2)',
                  texname = '\\text{I1a22}')

I1a23 = Parameter(name = 'I1a23',
                  nature = 'internal',
                  type = 'complex',
                  value = 'ys*complexconjugate(CKM3x2)',
                  texname = '\\text{I1a23}')

I1a31 = Parameter(name = 'I1a31',
                  nature = 'internal',
                  type = 'complex',
                  value = 'yb*complexconjugate(CKM1x3)',
                  texname = '\\text{I1a31}')

I1a32 = Parameter(name = 'I1a32',
                  nature = 'internal',
                  type = 'complex',
                  value = 'yb*complexconjugate(CKM2x3)',
                  texname = '\\text{I1a32}')

I1a33 = Parameter(name = 'I1a33',
                  nature = 'internal',
                  type = 'complex',
                  value = 'yb*complexconjugate(CKM3x3)',
                  texname = '\\text{I1a33}')

I2a11 = Parameter(name = 'I2a11',
                  nature = 'internal',
                  type = 'complex',
                  value = 'yup*complexconjugate(CKM1x1)',
                  texname = '\\text{I2a11}')

I2a12 = Parameter(name = 'I2a12',
                  nature = 'internal',
                  type = 'complex',
                  value = 'yc*complexconjugate(CKM2x1)',
                  texname = '\\text{I2a12}')

I2a13 = Parameter(name = 'I2a13',
                  nature = 'internal',
                  type = 'complex',
                  value = 'yt*complexconjugate(CKM3x1)',
                  texname = '\\text{I2a13}')

I2a21 = Parameter(name = 'I2a21',
                  nature = 'internal',
                  type = 'complex',
                  value = 'yup*complexconjugate(CKM1x2)',
                  texname = '\\text{I2a21}')

I2a22 = Parameter(name = 'I2a22',
                  nature = 'internal',
                  type = 'complex',
                  value = 'yc*complexconjugate(CKM2x2)',
                  texname = '\\text{I2a22}')

I2a23 = Parameter(name = 'I2a23',
                  nature = 'internal',
                  type = 'complex',
                  value = 'yt*complexconjugate(CKM3x2)',
                  texname = '\\text{I2a23}')

I2a31 = Parameter(name = 'I2a31',
                  nature = 'internal',
                  type = 'complex',
                  value = 'yup*complexconjugate(CKM1x3)',
                  texname = '\\text{I2a31}')

I2a32 = Parameter(name = 'I2a32',
                  nature = 'internal',
                  type = 'complex',
                  value = 'yc*complexconjugate(CKM2x3)',
                  texname = '\\text{I2a32}')

I2a33 = Parameter(name = 'I2a33',
                  nature = 'internal',
                  type = 'complex',
                  value = 'yt*complexconjugate(CKM3x3)',
                  texname = '\\text{I2a33}')

I3a11 = Parameter(name = 'I3a11',
                  nature = 'internal',
                  type = 'complex',
                  value = 'CKM1x1*yup',
                  texname = '\\text{I3a11}')

I3a12 = Parameter(name = 'I3a12',
                  nature = 'internal',
                  type = 'complex',
                  value = 'CKM1x2*yup',
                  texname = '\\text{I3a12}')

I3a13 = Parameter(name = 'I3a13',
                  nature = 'internal',
                  type = 'complex',
                  value = 'CKM1x3*yup',
                  texname = '\\text{I3a13}')

I3a21 = Parameter(name = 'I3a21',
                  nature = 'internal',
                  type = 'complex',
                  value = 'CKM2x1*yc',
                  texname = '\\text{I3a21}')

I3a22 = Parameter(name = 'I3a22',
                  nature = 'internal',
                  type = 'complex',
                  value = 'CKM2x2*yc',
                  texname = '\\text{I3a22}')

I3a23 = Parameter(name = 'I3a23',
                  nature = 'internal',
                  type = 'complex',
                  value = 'CKM2x3*yc',
                  texname = '\\text{I3a23}')

I3a31 = Parameter(name = 'I3a31',
                  nature = 'internal',
                  type = 'complex',
                  value = 'CKM3x1*yt',
                  texname = '\\text{I3a31}')

I3a32 = Parameter(name = 'I3a32',
                  nature = 'internal',
                  type = 'complex',
                  value = 'CKM3x2*yt',
                  texname = '\\text{I3a32}')

I3a33 = Parameter(name = 'I3a33',
                  nature = 'internal',
                  type = 'complex',
                  value = 'CKM3x3*yt',
                  texname = '\\text{I3a33}')

I4a11 = Parameter(name = 'I4a11',
                  nature = 'internal',
                  type = 'complex',
                  value = 'CKM1x1*ydo',
                  texname = '\\text{I4a11}')

I4a12 = Parameter(name = 'I4a12',
                  nature = 'internal',
                  type = 'complex',
                  value = 'CKM1x2*ys',
                  texname = '\\text{I4a12}')

I4a13 = Parameter(name = 'I4a13',
                  nature = 'internal',
                  type = 'complex',
                  value = 'CKM1x3*yb',
                  texname = '\\text{I4a13}')

I4a21 = Parameter(name = 'I4a21',
                  nature = 'internal',
                  type = 'complex',
                  value = 'CKM2x1*ydo',
                  texname = '\\text{I4a21}')

I4a22 = Parameter(name = 'I4a22',
                  nature = 'internal',
                  type = 'complex',
                  value = 'CKM2x2*ys',
                  texname = '\\text{I4a22}')

I4a23 = Parameter(name = 'I4a23',
                  nature = 'internal',
                  type = 'complex',
                  value = 'CKM2x3*yb',
                  texname = '\\text{I4a23}')

I4a31 = Parameter(name = 'I4a31',
                  nature = 'internal',
                  type = 'complex',
                  value = 'CKM3x1*ydo',
                  texname = '\\text{I4a31}')

I4a32 = Parameter(name = 'I4a32',
                  nature = 'internal',
                  type = 'complex',
                  value = 'CKM3x2*ys',
                  texname = '\\text{I4a32}')

I4a33 = Parameter(name = 'I4a33',
                  nature = 'internal',
                  type = 'complex',
                  value = 'CKM3x3*yb',
                  texname = '\\text{I4a33}')

