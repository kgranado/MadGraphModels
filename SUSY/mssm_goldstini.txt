Requestor: David Dodsworth
Contents: SUSY gauge mediation scenario with multiple goldstini
Paper: https://arxiv.org/abs/1112.5058
Source: Kentarou Mawatari (private communication)
JIRA: https://its.cern.ch/jira/browse/AGENE-1665